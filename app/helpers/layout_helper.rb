# frozen_string_literal: true

module LayoutHelper
  def title(page_title, show_title = true)
    content_for(:title) { page_title.to_s }
    @show_title = show_title
  end

  def show_title?
    @show_title
  end

  def root_page?
    current_page?(root_path)
  end

  def stylesheet(*)
    content_for(:head) { stylesheet_link_tag(*) }
  end

  def javascript(*)
    content_for(:head) { javascript_include_tag(*) }
  end

  def jquery_include_tag
    if Rails.env.production?
      javascript_include_tag 'https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js'
    else
      javascript_include_tag 'jquery3'
    end
  end

  def flash_messages
    flash_messages = []
    flash.each do |type, message|
      type = type.to_sym
      type = :success if type == :notice
      type = :danger if type == :alert
      type = :danger if type == :error
      next unless %i[success info warning danger].include?(type)

      Array(message).each do |msg|
        next unless msg

        flash_messages << render('application/flash_dismissible', { type: }) { msg }
      end
    end
    flash_messages.join("\n").html_safe
  end

  def active_class(controller)
    controller_name == controller.to_s ? 'nav-item active' : 'nav-item'
  end

  def table_class
    'table table-hover table-striped'
  end

  def form_check_class
    'form-check ' + form_spacing_class
  end

  def form_control_class
    'form-control ' + form_spacing_class
  end

  def form_spacing_class
    'my-1 mr-md-2'
  end

  def form_label_class
    'my-1 mr-2'
  end

  def btn_group(button_text = nil, &)
    caret_class = ''
    if button_text.blank?
      button_text = '<span class="fa fa-bars">'.html_safe
      caret_class = ' caret-off'
    end
    button = button_tag(button_text, type: 'button', class: 'btn btn-outline-secondary dropdown-toggle' + caret_class,
                                     'data-toggle': 'dropdown', 'aria-haspopup': 'true', 'aria-expanded': 'false')
    content = button + tag.div(class: 'dropdown-menu', &)
    tag.div(content, class: 'btn-group', role: 'group')
  end

  def show_banners(placement)
    render partial: 'banner', collection: Banner.show(placement)
  end
end
