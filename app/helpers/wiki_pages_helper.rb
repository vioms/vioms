module WikiPagesHelper
  acts_as_wiki_pages_helper
end

module Irwi::Helpers::WikiPagesHelper
  def wiki_page_new_path
    page = params[:path] if params && params[:path].present?
    wiki_page_path(page, :new)
  end
end
