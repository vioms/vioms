module PerPageRememberHelper
  PER_PAGE_DEFAULT = 25

  def per_page_remember
    get_session_value(:per_page) || PER_PAGE_DEFAULT
  end
end
