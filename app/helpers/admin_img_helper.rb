module AdminImgHelper
  def admin_img(action)
    case action
    when :send_sms
      image_tag_with_hint('admin/send_sms.png', 'site.send_sms')
    when :with_selected
      image_tag_with_hint('admin/arrow_up_left.png', 'admin.with_selected')
    else
      raise ArgumentError, 'Unknown image'
    end
  end

  protected

  def image_tag_with_hint(img, hint)
    image_tag(img, alt: t(hint), title: t(hint), 'data-toggle': 'tooltip')
  end
end
