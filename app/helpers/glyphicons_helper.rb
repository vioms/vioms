# frozen_string_literal: true

# rubocop:disable Metrics/ModuleLength
module GlyphiconsHelper
  def yes_no(val)
    val ? admin_icon(:yes) : admin_icon(:no)
  end

  def yes_no_text(val)
    val == true ? t('site.yes') : t('site.no')
  end

  def visible_invisible(val)
    val ? admin_icon(:visible) : admin_icon(:invisible)
  end

  # rubocop:disable Metrics/CyclomaticComplexity
  def admin_icon(action)
    case action
    when :edit
      glyphicon_tag(:fa, 'pencil-alt')
    when :destroy
      glyphicon_tag(:fa, 'trash-alt')
    when :plus
      glyphicon_tag(:fa, 'plus')
    when :mailing_pending
      glyphicon_tag(:far, 'clock', title: t('site.mailing_pending'), 'data-toggle': 'tooltip')
    when :mailing_sending
      glyphicon_tag(:fa, 'paper-plane', title: t('site.email_sending'), 'data-toggle': 'tooltip')
    when :mailing_sent
      glyphicon_tag(:fa, 'thumbs-up', title: t('site.mailing_sent'), 'data-toggle': 'tooltip')
    when :send_failed
      glyphicon_tag(:mi, 'warning', title: t('site.send_failed'), 'data-toggle': 'tooltip')
    when :send_mailing
      glyphicon_tag(:im, 'rocket')
    when :no_money
      glyphicon_tag(:mi, 'money_off', title: t('site.no_money'), 'data-toggle': 'tooltip')
    when :preview
      glyphicon_tag(:fa, 'eye')
    when :copy
      glyphicon_tag(:fa, 'copy')
    when :import
      glyphicon_tag(:fa, 'file-import')
    when :export
      glyphicon_tag(:im, 'file-excel')
    when :visible
      glyphicon_tag(:mi, 'visibility', class: 'yes', title: t('site.visible'), 'data-toggle': 'tooltip')
    when :invisible
      glyphicon_tag(:mi, 'visibility_off', class: 'no', title: t('site.invisible'), 'data-toggle': 'tooltip')
    when :settings
      glyphicon_tag(:fa, 'cog')
    when :list
      glyphicon_tag(:fa, 'list')
    when :sms
      glyphicon_tag(:mi, 'sms')
    when :yes
      glyphicon_tag(:fa, 'check-circle', class: 'yes', title: t('site.yes'), 'data-toggle': 'tooltip')
    when :no
      glyphicon_tag(:fa, 'times-circle', class: 'no', title: t('site.no'), 'data-toggle': 'tooltip')
    when :banners
      glyphicon_tag(:fa, 'images', title: t('headers.banners'), 'data-toggle': 'tooltip')
    when :move
      glyphicon_tag(:fa, 'arrows')
    when :cancel
      glyphicon_tag(:fa, 'plane-slash')
    when :error
      glyphicon_tag(:mi, 'warning', title: t('site.error'), 'data-toggle': 'tooltip')
    when :logout
      glyphicon_tag(:fa, 'sign-out-alt', title: t('site.logout'), 'data-toggle': 'tooltip', 'data-placement': 'bottom')
    when :rss
      glyphicon_tag(:fa, 'rss', style: 'color: #f26522')
    when :vk
      glyphicon_tag(:fab, 'vk')
    when :facebook
      glyphicon_tag(:fab, 'facebook-square')
    when :git
      glyphicon_tag(:fab, 'git')
    when :control_panel
      glyphicon_tag(:fa, 'user', title: t('site.control_panel'), 'data-toggle': 'tooltip', 'data-placement': 'left')
    when :create_template
      glyphicon_tag(:fa, 'file-code')
    when :poll
      glyphicon_tag(:fa, 'poll')
    when :search
      glyphicon_tag(:fa, 'search')
    when :clipboard
      glyphicon_tag(:fa, 'clipboard')
    else
      raise ArgumentError, 'Unknown icon'
    end
  end
  # rubocop:enable Metrics/CyclomaticComplexity

  private

  def glyphicon_tag(library, icon, options = {})
    klass =
      case library
      when :fa # Font Awesome, solid style
        "fa fa-#{icon}"
      when :far # Font Awesome, regular style
        "far fa-#{icon}"
      when :fab # Font Awesome, brands style
        "fab fa-#{icon}"
      when :mi # Material Icon
        return material_icon_tag(icon, options)
      when :io # Ionicons
        "ionicons ion-#{icon}"
      when :im # IcoMoon
        "icomoon icon-#{icon}"
      when :fl # Font Linux
        "fl-#{icon}"
      else
        raise ArgumentError, "Unknown glyphicon library: #{library}"
      end
    options[:class] = "icon #{klass} #{options[:class]}".strip
    tag.span('', **options)
  end

  def material_icon_tag(icon, options = {})
    options[:class] = "material-icons #{options[:class]}".strip
    tag.i(icon, **options)
  end
end
# rubocop:enable Metrics/ModuleLength
