# frozen_string_literal: true

module TabsHelper
  def tabs_block(&)
    tag.ul(capture(&), class: 'nav nav-tabs', role: 'tablist')
  end

  def tabs_content(&)
    tag.div(class: 'tab-content mt-3 px-2', &)
  end

  def tab_presentation(href, name, active = nil, css_class: nil, style: nil)
    tag.li(class: "nav-item #{css_class}".strip, style:) do
      active = active ? ' active' : ''
      link_to(name, "##{href}", class: 'nav-item nav-link' + active, id: "#{href}-tab", 'data-toggle': 'tab',
                                role: 'tab', 'aria-controls': href, 'aria-selected': active ? 'true' : 'false')
    end
  end

  def tab_pane(href, active = nil, &)
    active = active ? ' show active' : ''
    tag.div(id: href, class: 'tab-pane fade' + active, role: 'tabpanel',
            'aria-labelledby': "#{href}-tab", &)
  end
end
