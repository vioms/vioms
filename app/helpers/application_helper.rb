# frozen_string_literal: true

module ApplicationHelper
  def date_ru(date)
    if date
      l(date, format: :long).html_safe
    else
      '&nbsp;'.html_safe
    end
  end

  def time_ru(time)
    time.strftime('%H:%M').html_safe if time
  end

  def create_subscriber_for_login
    SubscriberUser.new(remember_me: true)
  end

  def create_user_for_login
    User.new(remember_me: true)
  end

  delegate :effective_admin?, to: :current_user

  def zerofill(val, n = 2)
    format("%0#{n}d", val)
  end

  def fieldset(text = '', &)
    render partial: 'fieldset', locals: { text:, content: capture(&) }
  end

  def dropdown_li(name, align: :left, &)
    render partial: 'dropdown_li', locals: { name:, align:, content: capture(&) }
  end

  def p_strong(name, text = nil, &block)
    text = capture(&block) if block
    render partial: 'p_strong', locals: { name:, text: }
  end

  def iframe(url)
    render partial: 'iframe', locals: { url: }
  end

  def r
    UrlHelpers.new
  end

  # TODO: don't rely on this and remove
  def get_session_value(name)
    session[name] = params[name].nil? ? session[name] : params[name].to_i
  end
end
