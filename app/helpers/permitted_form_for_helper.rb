module PermittedFormForHelper
  def permitted_form_for(*args, &)
    object = args.first
    model_name = model_name(object)
    options = args.extract_options!.reverse_merge(
      builder: PermittedFormBuilder,
      permitted_attributes: _permitted_attributes(model_name)
    )
    simple_form_for(*(args << options), &)
  end

  def permitted_nested_form_for(*args, &)
    object = args.first
    model_name = model_name(object)
    options = args.extract_options!.reverse_merge(
      builder: PermittedNestedFormBuilder,
      permitted_attributes: _permitted_attributes(model_name)
    )
    simple_form_for(*(args << options)) do |f|
      capture(f, &).to_s << after_nested_form_callbacks
    end
  end

  private

  def model_name(object)
    (object.is_a?(Array) ? object.last : object).class.to_s.underscore.to_sym
  end
end
