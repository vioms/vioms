module Admin::UserHelper
  def user_css_class(user)
    if user.is_blocked?
      'table-danger'
    elsif user.is_admin?
      'table-success'
    end
  end
end
