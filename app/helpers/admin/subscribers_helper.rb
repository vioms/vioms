module Admin::SubscribersHelper
  def has_email_confirmation_token?(subscriber)
    subscriber.email_confirmation_token.present?
  end

  def email_problems(subscriber)
    value = ''
    size = subscriber.email_problems_count
    value = size.to_s if size > 0
    if subscriber.raised_exception_at.present?
      value += ', ' if value.present?
      value += "Exception: #{subscriber.raised_exception_at.to_fs(:rus_datetime)}"
    end

    if value.present?
      render partial: 'admin/subscribers/red', locals: { value: }
    else
      ''
    end
  end

  def phone_problems(subscriber)
    size = subscriber.phone_problems_count
    if size > 0
      render partial: 'admin/subscribers/red', locals: { value: size }
    else
      ''
    end
  end
end
