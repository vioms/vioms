module Admin::SendingsHelper
  def sending_edit_link(object)
    if object.published
      link_to admin_icon(:edit), '#', class: 'btn btn-success disabled'
    else
      link_to_edit [:admin, object]
    end
  end

  def sending_status_column(object)
    if object.sent
      admin_icon(:mailing_sent)
    elsif object.sending
      admin_icon(:mailing_sending)
    elsif object.send_failed
      admin_icon(:send_failed)
    elsif object.try(:no_money)
      admin_icon(:no_money)
    elsif object.published
      if object.try(:digest)
        sending_status_column(object.email_digest)
      else
        admin_icon(:mailing_pending)
      end
    end
  end

  def cancel_sending_li(object)
    return if object.sent || object.send_failed || !object.published

    the_link =
      if object.sending
        klass = 'disabled'
        link_to_with_icon(t('site.cancel_sending'), '#', :cancel)
      else
        link =
          case object
          when Mailing
            cancel_sending_admin_mailing_path(object)
          when TelegramMessage
            cancel_sending_admin_telegram_message_path(object)
          when EmailDigest
            cancel_sending_admin_email_digest_path(object)
          else
            raise ArgumentError, "Unknown class: #{object.inspect}"
          end
        link_to_with_icon(
          t('site.cancel_sending'), link, :cancel, method: :put,
                                                   data: { confirm: t('site.cancel_sending_confirmation') },
                                                   class: 'dropdown-item'
        )
      end
    tag.div(the_link, class: klass)
  end

  def publish_sending_li(object)
    return nil if object.published || object.try(:digest)

    the_link =
      case object
      when Mailing
        send_mailing_admin_mailing_path(object)
      when TelegramMessage
        send_message_admin_telegram_message_path(object)
      when EmailDigest
        send_digest_admin_email_digest_path(object)
      else
        raise ArgumentError, "Unknown class: #{object.inspect}"
      end
    link_to_with_icon(t('site.send_mailing'), the_link, :send_mailing, method: :put,
                                                                       data: { confirm: t('site.send_mailing_confirmation') }, class: 'dropdown-item')
  end
end
