module Admin::DigestCategoriesHelper
  def digest_categories_title
    title = t('headers.digest_categories')
    title += " для #{@email_list.name}" if @email_list
    title.html_safe
  end
end
