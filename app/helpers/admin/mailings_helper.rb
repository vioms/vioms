module Admin::MailingsHelper
  def is_mobile_browser?(http_agent)
    http_agent =~ /android/i ? true : false
  end

  def email_list_var_as(var_name)
    return :ckeditor if var_name.with_editor?
    return :text     if var_name.is_text?

    :string
  end
end
