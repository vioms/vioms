module Admin::TimeZonesHelper
  def show_tz(sign, zone)
    ret = sign ? '+' : '-'
    ret + zone.strftime('%k').strip
  end
end
