# frozen_string_literal: true

module Admin::ManageSmsSendHistoryHelper
  def link_to_subscriber(sms_send_history)
    subscriber = sms_send_history.subscriber
    if subscriber.present?
      name =
        subscriber.name.presence || subscriber.id.to_s
      link_to(name, edit_admin_subscriber_path(subscriber))
    else
      tag.i(sms_send_history.subscriber_id.to_s)
    end
  end
end
