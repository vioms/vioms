module ProblemTypesHelper
  module Types
    PROBLEM_INFO      = 0
    PROBLEM_TEMPORARY = 1
    PROBLEM_PERMANENT = 2
  end

  def get_problem_type_name(problem_type_id)
    case problem_type_id
    when Types::PROBLEM_INFO
      t('problem_type.info')
    when Types::PROBLEM_TEMPORARY
      t('problem_type.temporary')
    when Types::PROBLEM_PERMANENT
      t('problem_type.permanent')
    else
      raise ArgumentError, 'Unknown problem type'
    end
  end
end
