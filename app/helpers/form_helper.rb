# frozen_string_literal: true

module FormHelper
  def form_wrapper(heading, panel_type = 'default', &)
    title(heading, false)
    tag.div(class: "card panel-#{panel_type}") do
      tag.div(heading, class: 'card-header') +
        tag.div(class: 'card-body') do
          capture(&)
        end
    end
  end

  def error_notifications(f)
    f.error_notification.to_s +
      (f.error_notification(message: f.object.errors[:base].to_sentence) if f.object.errors[:base].present?).to_s
  end
end
