# frozen_string_literal: true

module LinksHelper
  def common_link_to_new(paths, controller)
    link_to paths.flatten.compact, class: 'btn btn-primary mt-2 mb-3' do
      admin_icon(:plus) + '&nbsp;'.html_safe + t("actions.#{controller}.create")
    end
  end

  def link_to_new(controller, parent_object = nil)
    common_link_to_new([:new, :admin, parent_object, controller].flatten.compact, controller)
  end

  def subscriber_link_to_new(controller, parent_object = nil)
    common_link_to_new([:new, parent_object, controller].flatten.compact, controller)
  end

  def link_to_back(path)
    tag.p(link_to(t('site.back'), path, class: 'btn btn-sm btn-outline-secondary'))
  end

  def link_to_edit(path)
    link_to(admin_icon(:edit), [:edit, path].flatten, class: 'btn btn-success', title: t('site.edit'),
                                                      'data-toggle': 'tooltip')
  end

  def link_to_destroy(path)
    link_to(admin_icon(:destroy), path, 'data-confirm': t('site.confirm'), method: :delete,
                                        class: 'btn btn-danger', title: t('site.destroy'), 'data-toggle': 'tooltip')
  end

  def link_to_with_icon(text, href, icon, options = {})
    link_to((admin_icon(icon) + '&nbsp;'.html_safe + text).html_safe, href, options)
  end
end
