# frozen_string_literal: true

# Renders checkboxes for my settings edit action
module MySettingsHelper
  def subscription_check_boxes(collection_name, my_settings)
    items = Subscriber::Subscriptions.call(collection_name, my_settings)
    render_subscription_items(items, collection_name)
  end

  private

  def render_subscription_items(items, collection_name)
    items = items.sort_by { |item| item[:name] }.inject('') do |memo, item|
      memo + render(partial: 'my_settings/check_box', locals: {
                      collection_name: "#{collection_name}_list_ids",
                      id: item[:id],
                      name: item[:name],
                      checked: item[:checked] ? 'checked' : false
                    })
    end
    render partial: 'my_settings/check_boxes', locals: { items: items.html_safe }
  end
end
