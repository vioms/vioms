module ErrorMessagesHelper
  # Render error messages for the given objects. The :message and :header_message options are allowed.
  def error_messages_for(*objects)
    options = objects.extract_options!
    options[:header_message] ||= t('simple_form.error_notification.header')
    options[:message] ||= t('simple_form.error_notification.default_message')
    messages = objects.compact.map { |o| o.errors.full_messages }.flatten
    return if messages.empty?

    tag.div(class: 'error_messages') do
      list_items = messages.map { |msg| tag.li(msg) }
      tag.h2(options[:header_message]) + tag.p(options[:message]) + tag.ul(list_items.join.html_safe)
    end
  end

  module FormBuilderAdditions
    def error_messages(options = {})
      @template.error_messages_for(@object, options)
    end
  end
end

ActionView::Helpers::FormBuilder.include ErrorMessagesHelper::FormBuilderAdditions
