# frozen_string_literal: true

class Subscriber
  module NotifyOnSubscriptions
    module_function

    def call(subscriber); end

    def notify_subscription
      return unless email_list_ids_before

      (email_list_ids - email_list_ids_before).each do |email_list_id|
        send_subscribed_notification(email_list_id)
      end
      (email_list_ids_was - email_list_ids).each do |email_list_id|
        send_unsubscribed_notification(email_list_id)
      end
    end

    def notify_unsubscription_on_destroy
      email_list_ids.each do |email_list_id|
        send_unsubscribed_notification(email_list_id)
      end
    end

    def send_subscribed_notification(email_list_id)
      email_list = EmailList
                   .select(%i[id notify_subscribed notify_email])
                   .find(email_list_id)
      return unless email_list.notify_subscribed && email_list.notify_email.present?

      ViomsMailer.subscribed_notification(id, email_list_id).deliver!
    end

    def send_unsubscribed_notification(email_list_id)
      email_list = EmailList.select(%i[id notify_unsubscribed notify_email]).find(email_list_id)
      return unless email_list.notify_unsubscribed && email_list.notify_email.present?

      ViomsMailer.unsubscribed_notification(id, email_list_id).deliver!
    end
  end
end
