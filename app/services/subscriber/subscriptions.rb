# frozen_string_literal: true

class Subscriber
  module Subscriptions
    module_function

    def call(collection_name, subscriber)
      subscriptions = subscriber.public_send(:"#{collection_name}_list_ids")
      collection = "#{collection_name.capitalize}List".constantize.visible.without_never_visible.select(:id, :name)
      items = collection.inject([]) do |memo, item|
        memo + [{ id: item.id, name: item.name, checked: subscriptions.include?(item.id) }]
      end
      collection = subscriber.public_send(:"#{collection_name}_lists_invisible_checked").select(:id, :name)
      collection.inject(items) do |memo, item|
        memo + [{ id: item.id, name: item.name, checked: true }]
      end
    end
  end
end
