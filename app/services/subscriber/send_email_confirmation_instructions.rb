# frozen_string_literal: true

class Subscriber
  module SendEmailConfirmationInstructions
    module_function

    def call(subscriber_user)
      return if already_sent?(subscriber_user)

      subscriber_user.email_confirmation_attempts += 1
      subscriber_user.save!(validate: false)
      ViomsMailer.email_confirmation(subscriber_user).deliver
    end

    def already_sent?(subscriber_user)
      !EmailSendHistory.check(subscriber_user.email, subscriber_user.id, value: generate_token)
    end

    def generate_token
      SecureRandom.base64(15).tr('+/=', 'xyz')
    end
  end
end
