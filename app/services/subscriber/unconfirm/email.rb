# frozen_string_literal: true

class Subscriber
  module Unconfirm
    # Nullify confirmation if email has changed
    class Email
      include Interactor

      delegate :subscriber, to: :context

      def call
        return if !subscriber.email_changed? || subscriber.email_was.blank?

        subscriber.confirmed_at = nil
      end
    end
  end
end
