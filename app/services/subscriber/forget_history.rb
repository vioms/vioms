# frozen_string_literal: true

class Subscriber
  module ForgetHistory
    module_function

    def call(subscriber)
      EmailSendHistory.forget(subscriber.email) if subscriber.email.present?
      SmsSendHistory.forget(subscriber.phone) if subscriber.phone.present?
    end
  end
end
