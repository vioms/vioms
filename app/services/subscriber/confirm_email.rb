# frozen_string_literal: true

class Subscriber
  module ConfirmEmail
    module_function

    def call(subscriber, save: true)
      return if subscriber.email.blank?

      subscriber.email_problems.temporary_not_solved.update_all(solved: true)
      subscriber.update_email_problems_count
      subscriber.confirmed_at = Time.zone.now
      subscriber.save! if save
      EmailSendHistory.forget(subscriber.email)
    end
  end
end
