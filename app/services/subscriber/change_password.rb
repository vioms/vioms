# frozen_string_literal: true

class Subscriber
  module ChangePassword
    module_function

    def call(subscriber_user, params)
      current_password = params.delete(:current_password)
      subscriber_user.set_password = '1' # To enable password validation

      if subscriber_user.encrypted_password.blank? || subscriber_user.valid_password?(current_password)
        subscriber_user.update(params)
      else
        subscriber_user.valid?
        subscriber_user.errors.add(:current_password, current_password.blank? ? :blank : :invalid)
        false
      end
    end
  end
end
