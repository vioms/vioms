# frozen_string_literal: true

class Subscriber
  class Import
    class ImportOne
      delegate :subscriber_import, to: :@import_person

      def initialize(import_person)
        @import_person = import_person
      end

      def call
        find_or_create_subscribers(import_email, import_phone) => { subscribers:, error: }

        subscribers.each do |subscriber|
          unless subscriber.valid?
            error = :not_valid
            break
          end

          ensure_name(subscriber, @import_person.name)
          Subscriber::ConfirmEmail.call(subscriber, save: false)
          subscriber.save!

          subscriber_import.email_lists.each do |email_list|
            subscribe_to_email_list(subscriber:, email_list:)
          end
        end

        @import_person.error_id = ::SubscriberImport::ERROR_IDS[error] if error
        @import_person.save!

        return [] if error

        subscribers
      end

      private

      def import_email = Subscriber.extract_email(@import_person.email)
      def import_phone = Subscriber.extract_phone(@import_person.phone)

      def find_or_create_subscribers(email, phone)
        return { subscribers: [], error: :bad_phone } if phone == false

        if email.present?
          new_subscriber_email = Subscriber.find_or_initialize_by(email:)
          if phone.present?
            if new_subscriber_email.phone.blank?
              new_subscriber_phone = Subscriber.find_or_initialize_by(phone:)
              if new_subscriber_phone.new_record?
                new_subscriber_email.phone = phone
                new_subscriber_phone = nil
              elsif new_subscriber_email.new_record?
                new_subscriber_phone.email = email
                new_subscriber_email = nil
              end
            elsif new_subscriber_email.phone != phone
              new_subscriber_phone = Subscriber.find_or_initialize_by(phone:)
            end
          end
        elsif phone.present?
          new_subscriber_phone = Subscriber.find_or_initialize_by(phone:)
        else
          return { subscribers: [], error: :no_data }
        end

        { subscribers: [new_subscriber_email, new_subscriber_phone].compact, error: nil }
      end

      def ensure_name(subscriber, name)
        return if subscriber.name.present?

        subscriber.name = name
      end

      def subscribe_to_email_list(subscriber:, email_list:)
        return if email_list.subscribers.include?(subscriber)

        email_list.subscribers << subscriber
        email_list.update_column(:subscribers_count_cache, nil)
      end
    end
  end
end
