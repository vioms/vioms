# frozen_string_literal: true

class Subscriber
  class Import
    class ParseSubscribersData
      delegate :subscribers_data, :name_column, :first_name_column, :last_name_column, :email_column, :phone_column,
               to: :@subscribers_import

      def initialize(subscribers_import)
        @subscribers_import = subscribers_import
      end

      def call
        subscribers_data.split("\n").compact_blank!.map do |row|
          subscriber = row.split(',').map { |s| s.split("\t") }.flatten.map(&:strip)
          [name(subscriber), email(subscriber), phone(subscriber)]
        end
      end

      def name(subscriber)
        if name_column.present?
          subscriber[name_column.to_i - 1]
        elsif last_name_column.present? && first_name_column.present?
          subscriber[last_name_column.to_i - 1] + ' ' + subscriber[first_name_column.to_i - 1]
        end
      end

      def email(subscriber)
        return unless email_column.present?

        subscriber[email_column.to_i - 1]
      end

      def phone(subscriber)
        return unless phone_column.present?

        subscriber[phone_column.to_i - 1]
      end
    end
  end
end
