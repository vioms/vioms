# frozen_string_literal: true

class Subscriber
  class AuthenticateFromToken
    def initialize(email, auth_token)
      @email = email
      @auth_token = auth_token
    end

    attr_reader :subscriber

    def call
      @subscriber = @email.present? && Subscriber.find_by(email: @email)
      @subscriber && Devise.secure_compare(@subscriber.authentication_token, @auth_token)
    end
  end
end
