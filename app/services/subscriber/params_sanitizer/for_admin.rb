# frozen_string_literal: true

class Subscriber
  module ParamsSanitizer
    module ForAdmin
      module_function

      def call(params, subscriber:, user:)
        return params if user.try(:effective_admin?)

        params = params.dup
        %w[email sms].each do |name|
          name_ids = :"#{name}_list_ids"
          next unless params[name_ids]

          klass = klass(name)
          params[name_ids] =
            CheckboxSanitizer.call(
              new: params[name_ids],
              old: subscriber_subscriptions(subscriber, name_ids),
              permitted: permitted_to_user_lists(user, name_ids) + visible_ids(klass),
              not_permitted: not_permitted_to_user_lists(name, user, name_ids) - visible_ids(klass)
            )
        end

        params
      end

      def subscriber_subscriptions(subscriber, name_ids)
        subscriber.public_send(name_ids)
      end

      def klass(name)
        "#{name.capitalize}List".constantize
      end

      def visible_ids(klass)
        klass.visible.ids
      end

      def permitted_to_user_lists(user, name_ids)
        user.public_send(name_ids)
      end

      def not_permitted_to_user_lists(name, user, name_ids)
        klass(name).ids - permitted_to_user_lists(user, name_ids)
      end
    end
  end
end
