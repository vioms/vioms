# frozen_string_literal: true

class Subscriber
  module ParamsSanitizer
    module ForSubscriber
      module_function

      def call(params, subscriber: nil)
        return params unless params[:email_list_ids]

        params = params.dup

        subscriptions = subscriber_subscriptions(subscriber)
        params[:email_list_ids] =
          CheckboxSanitizer.call(
            new: params[:email_list_ids],
            old: subscriptions,
            permitted: subscriptions + visible_ids,
            not_permitted: never_visible_ids
          )

        params
      end

      def subscriber_subscriptions(subscriber)
        return [] if subscriber.nil?

        subscriber.email_list_ids
      end

      def visible_ids = EmailList.visible.ids
      def never_visible_ids = EmailList.never_visible.ids
    end
  end
end
