# frozen_string_literal: true

class Subscriber
  class Import
    def initialize(subscriber_import)
      @subscriber_import = subscriber_import
    end

    delegate :subscriber_import_people, :email_list_ids, :delete_old, to: :@subscriber_import

    def call
      subscriber_ids = []
      imported_subscribers_count = 0
      subscribers_hash = ParseSubscribersData.new(@subscriber_import).call
      subscribers_hash.each do |name, email, phone|
        import_person = subscriber_import_people.build(name:, email:, phone:)
        subscribers = ImportOne.new(import_person).call
        next if subscribers.empty?

        subscriber_ids.push(*subscribers.map(&:id))
        imported_subscribers_count += 1
      end

      do_delete_old(subscriber_ids) if delete_old && subscriber_ids.any?
      @subscriber_import.errors_count = subscribers_hash.size - imported_subscribers_count
      @subscriber_import.save!(validate: false)
    end

    def do_delete_old(subscriber_ids)
      return unless email_list_ids.any?

      EmailSubscription.where(email_list_id: email_list_ids).where.not(subscriber_id: subscriber_ids).delete_all
    end
  end
end
