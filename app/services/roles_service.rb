# frozen_string_literal: true

# Sets and retrieves roles set from a bitmask value
class RolesService
  def initialize(all_roles)
    @all_roles = all_roles
  end

  def is_role?(role, bitmask)
    bitmask_to_set(bitmask).include?(role)
  end

  def set_role(role, bitmask, value)
    set = bitmask_to_set(bitmask)
    if [true, '1'].include?(value)
      set.add(role)
    else
      set.delete(role)
    end
    set_to_bitmask(set)
  end

  private

  def set_to_bitmask(set)
    (set & @all_roles.keys).map { |r| 2**@all_roles[r] }.sum
  end

  def bitmask_to_set(bitmask)
    @all_roles.keys.select { |r| (bitmask || 0).anybits?(2**@all_roles[r]) }.to_set
  end
end
