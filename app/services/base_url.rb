# frozen_string_literal: true

module BaseUrl
  module_function

  def call
    env = Rails.env

    case env
    when 'production'
      'https://www.vioms.ru'
    when 'development'
      'http://lvh.me:3000'
    when 'test'
      'http://vioms.example.com'
    else
      raise UnknownEnvironment, env
    end
  end

  class UnknownEnvironment < StandardError; end
end
