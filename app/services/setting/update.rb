# frozen_string_literal: true

class Setting
  module Update
    module_function

    def call(setting, params)
      res = setting.update(params)
      ::Admin::BaseController.stripo_version = nil if setting.name == 'stripo_version'
      res
    end
  end
end
