# frozen_string_literal: true

module VoteService
  module_function

  def vote(subscriber:, answer:)
    sa = SubscriberAnswer.find_or_initialize_by(
      poll_id: answer.try(:poll_id),
      subscriber:
    )
    sa.update(answer:)
  end
end
