# frozen_string_literal: true

# Adds problems to subscriber's contact (email or phone) and creates not found contacts array
class ImportProblemsService
  attr_reader :not_imported_contacts

  # Contact type is :email or :phone
  def initialize(contact_type:, user:)
    @contact_type = contact_type
    @user = user

    @not_imported_contacts = []
  end

  def import!(input:, problem_name_id:)
    @not_imported_contacts.clear

    contacts(input).each do |contact|
      subscriber = Subscriber.find_by(@contact_type => contact)
      if subscriber.present? && allowed_subscriber?(subscriber)
        problem_class.create!(problem_name_attr => problem_name_id, subscriber_id: subscriber.id)
      else
        @not_imported_contacts << contact
      end
    end
  end

  private

  # Converts input from text field to array of contacts
  def contacts(input)
    input.split("\n").map { |contact| contact.split(',') }.flatten.compact_blank.map(&:strip)
  end

  def problem_class
    @problem_class ||= "#{@contact_type.to_s.camelize}Problem".constantize
  end

  def problem_name_attr
    @problem_name_attr ||= "#{@contact_type}_problem_name_id"
  end

  def allowed_subscriber?(subscriber)
    case @contact_type
    when :email
      @user.allowed_subscriber_email?(subscriber)
    when :phone
      @user.allowed_subscriber_phone?
    end
  end
end
