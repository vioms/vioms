# frozen_string_literal: true

module Stripo
  module Version
    module_function

    def call
      version = Setting.get_setting('stripo_version')
      if version == 'latest'
        'latest'
      else
        "rev/#{version}"
      end
    end
  end
end
