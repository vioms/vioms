# frozen_string_literal: true

module Stripo
  class Template
    class Destroy
      include Interactor::Organizer

      around do |interactor|
        Template.transaction { interactor.call }
      end

      organize RecycleStripoFileKey, DestroyTemplate
    end
  end
end
