# frozen_string_literal: true

module Stripo
  class Template
    class Create
      include Interactor::Organizer

      organize CreateTemplate, CreateStripoFileKey
    end
  end
end
