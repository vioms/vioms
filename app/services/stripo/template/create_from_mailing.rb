# frozen_string_literal: true

module Stripo
  class Template
    class CreateFromMailing
      include Interactor::Organizer

      organize PrepareParams, ::Stripo::Template::Create
    end
  end
end
