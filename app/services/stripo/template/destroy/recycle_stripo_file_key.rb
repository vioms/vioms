# frozen_string_literal: true

module Stripo
  class Template
    class Destroy
      # When we destroy a template, the corresponding Stripo file key might be reused for another template or mailing.
      class RecycleStripoFileKey
        include Interactor

        delegate :template, to: :context
        delegate :stripo_file_key, to: :template

        def call
          stripo_file_key.files.destroy_all
          stripo_file_key.update!(reusable: true, stripo_template_id: nil)
          template.stripo_file_key = nil
        end
      end
    end
  end
end
