# frozen_string_literal: true

module Stripo
  class Template
    class Destroy
      class DestroyTemplate
        include Interactor

        delegate :template, to: :context

        def call
          template.reload
          template.destroy
        end
      end
    end
  end
end
