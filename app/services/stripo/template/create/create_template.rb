# frozen_string_literal: true

module Stripo
  class Template
    class Create
      class CreateTemplate
        include Interactor

        delegate :params, to: :context

        def call
          template = ::Stripo::Template.new(params)
          context.template = template
          context.fail! unless template.save
        end
      end
    end
  end
end
