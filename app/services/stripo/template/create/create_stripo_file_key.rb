# frozen_string_literal: true

module Stripo
  class Template
    class Create
      class CreateStripoFileKey
        include Interactor

        delegate :template, to: :context

        def call
          template.stripo_file_key = ::Stripo::FindOrCreateFileKey.call
        end
      end
    end
  end
end
