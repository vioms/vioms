# frozen_string_literal: true

module Stripo
  class Template
    class CreateFromMailing
      class PrepareParams
        include Interactor

        delegate :params, to: :context

        def call
          mailing = Mailing.find(params[:mailing_id])
          params[:html] = mailing.html_content
          params[:css] = mailing.css
        end
      end
    end
  end
end
