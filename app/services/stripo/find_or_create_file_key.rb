# frozen_string_literal: true

module Stripo
  module FindOrCreateFileKey
    module_function

    def call
      plugin_id = Setting.get_setting('stripo_app_id')
      file_key = ::Stripo::FileKey.where(plugin_id:, reusable: true).first
      if file_key
        file_key.update!(reusable: false)
      else
        email_id = 1 + ::Stripo::FileKey.where(plugin_id:).maximum(:email_id).to_i
        file_key = ::Stripo::FileKey.create!(plugin_id:, email_id:)
      end
      file_key
    end
  end
end
