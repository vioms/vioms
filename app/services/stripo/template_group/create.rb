# frozen_string_literal: true

module Stripo
  class TemplateGroup
    class Create
      include Interactor

      delegate :params, :user, to: :context

      def call
        stripo_template_group = ::Stripo::TemplateGroup.new(params)
        if params[:parent_id].present?
          ResetSubgroupAttributes.call(stripo_template_group)
        else
          stripo_template_group.user = user
        end
        context.stripo_template_group = stripo_template_group
        context.fail! unless stripo_template_group.save
      end
    end
  end
end
