# frozen_string_literal: true

module Stripo
  class TemplateGroup
    module UsersList
      module_function

      def call(user)
        scope = User.select(:id, :name).ordered
        return scope if user.effective_admin?

        return [] if user.email_list_ids.empty?

        scope
          .joins(:email_list_permissions)
          .where(email_list_permissions: { email_list_id: user.email_list_ids })
          .or(scope.joins(:email_list_permissions).where(id: user.id))
          .distinct
      end
    end
  end
end
