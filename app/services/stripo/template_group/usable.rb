# frozen_string_literal: true

module Stripo
  class TemplateGroup
    class Usable
      def initialize(email_list:, user:)
        @email_list = email_list
        @user = user
      end

      def call
        template_groups = root_groups.inject([]) do |array, template_group|
          group = ::Stripo::TemplateGroup.select(:id, :name, :ancestry).find(template_group.id)
          array.push(group)
          array.concat(group.children.select(:id, :name, :ancestry).ordered)
        end
        template_groups.map do |group|
          { id: group.id, name: NameWithDepthIndent.call(group) }
        end
      end

      private

      attr_reader :email_list, :user

      def root_groups
        groups =
          if user.effective_admin?
            ::Stripo::TemplateGroup.roots
          else
            authorized_root_groups
          end
        groups.ordered.distinct
      end

      def authorized_root_groups
        email_list_ids = user.email_list_ids & [email_list.id]
        ::Stripo::TemplateGroup.with_joined_tables.where(public: true)
                               .or(::Stripo::TemplateGroup.with_joined_tables
                                     .where(stripo_template_group_email_lists: { email_list_id: email_list_ids }))
      end
    end
  end
end
