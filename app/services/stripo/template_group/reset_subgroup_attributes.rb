# frozen_string_literal: true

module Stripo
  class TemplateGroup
    module ResetSubgroupAttributes
      module_function

      def call(stripo_template_group)
        stripo_template_group.public = nil
        stripo_template_group.user = nil
        stripo_template_group.email_lists = []
        stripo_template_group.users = []
      end
    end
  end
end
