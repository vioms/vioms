# frozen_string_literal: true

module Stripo
  class TemplateGroup
    class Update
      include Interactor

      delegate :stripo_template_group, :params, :user, to: :context

      def call
        stripo_template_group.attributes = params
        if params[:parent_id].present?
          ResetSubgroupAttributes.call(stripo_template_group)
        else
          stripo_template_group.user = user
        end
        context.fail! unless stripo_template_group.save
      end
    end
  end
end
