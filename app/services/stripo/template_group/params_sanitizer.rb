# frozen_string_literal: true

module Stripo
  class TemplateGroup
    module ParamsSanitizer
      module_function

      def call(params, user:, template_group: Stripo::TemplateGroup.new)
        return params if user.try(:effective_admin?)

        params = params.dup
        params[:email_list_ids] = email_lists_params(email_list_ids: params[:email_list_ids], user:,
                                                     template_group:)
        params[:user_ids] = users_params(user_ids: params[:user_ids], user:, template_group:)

        params
      end

      def email_lists_params(email_list_ids:, user:, template_group:)
        permitted_ids = user.email_list_ids
        CheckboxSanitizer.call(
          new: Array(email_list_ids),
          old: template_group.email_list_ids,
          permitted: permitted_ids,
          not_permitted: EmailList.ids - permitted_ids
        )
      end

      def users_params(user_ids:, user:, template_group:)
        permitted_ids = Stripo::TemplateGroup::UsersList.call(user).map(&:id)
        CheckboxSanitizer.call(
          new: Array(user_ids),
          old: template_group.user_ids,
          permitted: permitted_ids,
          not_permitted: User.ids - permitted_ids
        )
      end
    end
  end
end
