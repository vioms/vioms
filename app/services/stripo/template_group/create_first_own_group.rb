# frozen_string_literal: true

module Stripo
  class TemplateGroup
    module CreateFirstOwnGroup
      module_function

      def call(user)
        return if ::Stripo::TemplateGroup.by_owner(user).any?

        ::Stripo::TemplateGroup.create!(name: I18n.t('view.stripo/template.my_templates'), user:)
      end
    end
  end
end
