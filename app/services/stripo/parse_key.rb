# frozen_string_literal: true

module Stripo
  module ParseKey
    module_function

    def call(key)
      if key.include?('_email_')
        (plugin_id, email_id) = key.match(/pluginId_(.+)_email_(\d+)/)[1, 2]
        email_id = email_id.to_i
      else
        plugin_id = key.match(/pluginId_(.+)_.+/)[1]
        email_id = nil
      end
      { plugin_id:, email_id: }
    end
  end
end
