# frozen_string_literal: true

module Stripo
  module FileOperations
    module_function

    def list
      ::Stripo::FileKey.all.map do |file_key|
        {
          key: file_key.stripo_key,
          documents: file_key.files.map { |file| one_file_info(file) }
        }
      end
    end

    def info(src:)
      stripo_file = ::Stripo::File.find_by!(cached_url: src)
      {
        originalName: stripo_file.file['filename'],
        size: stripo_file.file['size']
      }
    end

    def one_file_info(stripo_file)
      file = stripo_file.file
      {
        url: stripo_file.cached_url,
        originalName: file['filename'],
        uploadTime: stripo_file.created_at.to_i * 1000,
        size: file['size'],
        height: file['height'],
        width: file['width'],
        thumbnailUrl: stripo_file.file_url(:thumb, host: BaseUrl.call)
      }
    end

    def upload(key:, file:)
      stripo_file =
        ::Stripo::File.transaction do
          key_hash = ::Stripo::ParseKey.call(key)
          file_key =
            if key_hash[:email_id]
              ::Stripo::FileKey.find_by!(plugin_id: key_hash[:plugin_id], email_id: key_hash[:email_id])
            else
              ::Stripo::FileKey.find_or_create_by!(plugin_id: key_hash[:plugin_id], email_id: nil)
            end
          stripo_file = ::Stripo::File.new(key: file_key, file:)
          raise ActiveRecord::RecordInvalid, stripo_file unless stripo_file.valid?

          stripo_file.file_derivatives!
          stripo_file.save!(validate: false)
          stripo_file.cached_url = stripo_file.file_url(host: BaseUrl.call)
          stripo_file.save!
          stripo_file
        end

      one_file_info(stripo_file)
    end

    def delete(url:, key:)
      ::Stripo::File.find_by!(cached_url: url).destroy
      { url:, key: }
    end
  end
end
