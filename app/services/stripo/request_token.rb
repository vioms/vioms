# frozen_string_literal: true

module Stripo
  module RequestToken
    AUTH_URL = 'https://plugins.stripo.email/api/v1/auth'

    module_function

    def call
      response = RestClient.post(AUTH_URL, payload.to_json, content_type: :json, accept: :json)
      JSON.parse(response.body)['token']
    end

    def payload
      {
        pluginId: Setting.get_setting('stripo_app_id'),
        secretKey: Setting.get_setting('stripo_secret_key')
      }
    end
  end
end
