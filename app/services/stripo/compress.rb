# frozen_string_literal: true

module Stripo
  module Compress
    COMPRESS_URL = 'https://plugins.stripo.email/api/v1/cleaner/v1/compress'

    module_function

    def call(html:, css:)
      response = RestClient.post(
        COMPRESS_URL,
        { html:, css:, minimize: true }.to_json,
        es_plugin_auth: token, content_type: :json, accept: :json
      )
      JSON.parse(response.body)['html']
    end

    def token
      "Bearer #{RequestToken.call}"
    end
  end
end
