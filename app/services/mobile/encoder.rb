# frozen_string_literal: true

module Mobile
  module Encoder
    module_function

    def encode(payload) = JWT.encode(payload, key)
    def decode(token) = JWT.decode(token, key).first
    def key = Rails.application.secret_key_base
  end
end
