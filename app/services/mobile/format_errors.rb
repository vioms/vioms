module Mobile
  module FormatErrors
    module_function

    def call(obj)
      errors = obj.errors.to_a
      return if errors.empty?

      if errors.length > 1
        errors.map.with_index { |error, i| "#{i + 1}. #{error}" }.join("\n")
      else
        errors[0]
      end
    end
  end
end
