# frozen_string_literal: true

module Mobile
  module Token
    module_function

    def create!(subscriber:, device_token:, device_type:)
      mobile_token = subscriber.mobile_tokens.create!(device_token:, device_type:)
      encoded_token(mobile_token)
    end

    def next!(mobile_token, serial)
      mobile_token.increment!(:serial, 1) if mobile_token.serial == serial
      encoded_token(mobile_token)
    end

    def encoded_token(mobile_token) = Encoder.encode({ id: mobile_token.id, serial: mobile_token.serial })
  end
end
