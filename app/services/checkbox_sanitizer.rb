# frozen_string_literal: true

module CheckboxSanitizer
  module_function

  # @param new [Array<String, Integer>] New values
  # @param old [Array<String, Integer>] Old values
  # @param permitted [Array<String, Integer>] Values that can be changed
  # @param not_permitted [Array<String, Integer>] Values that must be kept unchanged
  # @return [Array<Integer>]
  def call(new:, old:, permitted:, not_permitted:)
    (new.map(&:to_i) & permitted.map(&:to_i)) + (old.map(&:to_i) & not_permitted.map(&:to_i))
  end
end
