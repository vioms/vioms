# frozen_string_literal: true

module Telegram
  class PublishMessage
    include Interactor::Organizer

    organize Publish, CreateSchedule, CreateJob
  end
end
