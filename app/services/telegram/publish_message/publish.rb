# frozen_string_literal: true

module Telegram
  class PublishMessage
    class Publish
      include Interactor

      delegate :telegram_message, :user, to: :context

      def call
        telegram_message.published = true
        telegram_message.user = user

        context.fail!(message: error_message) unless telegram_message.save
      end

      def rollback
        telegram_message.published = false
        telegram_message.user = nil
        telegram_message.save(validate: false)
      end

      private

      def error_message
        messages = telegram_message.errors.full_messages.join('. ')
        "Обнаружены ошибки в сообщении: #{messages}. Отправка невозможна."
      end
    end
  end
end
