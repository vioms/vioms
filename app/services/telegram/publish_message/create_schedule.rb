# frozen_string_literal: true

module Telegram
  class PublishMessage
    class CreateSchedule
      include Interactor

      delegate :telegram_message, to: :context

      def call
        telegram_message.create_schedule!
      rescue StandardError => e
        context.fail!(message: "Произошла ошибка: #{e.message}")
      end

      def rollback
        telegram_message.schedule.destroy
      end
    end
  end
end
