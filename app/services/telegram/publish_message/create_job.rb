# frozen_string_literal: true

module Telegram
  class PublishMessage
    class CreateJob
      include Interactor

      delegate :telegram_message, to: :context

      def call
        TelegramMessageJob
          .set(wait_until: telegram_message.deliver_at)
          .perform_later(telegram_message.schedule.id)

        context.message = "Сообщение в Телеграм будет отправлено #{deliver_at_formatted}"
      rescue StandardError => e
        context.fail!(message: "Произошла ошибка: #{e.message}")
      end

      private

      def deliver_at_formatted
        I18n.l(telegram_message.deliver_at, format: :long)
      end
    end
  end
end
