# frozen_string_literal: true

module Telegram
  class CancelMessage
    class CancelSchedule
      include Interactor

      SENDING_CANCEL_PERIOD = 1.minute

      delegate :telegram_message, to: :context
      delegate :deliver_at, :sent, :send_failed, :sending, to: :telegram_message

      def call
        if deliver_at - SENDING_CANCEL_PERIOD > Time.zone.now || sent || send_failed || sending
          telegram_message.schedule.destroy
          context.message = 'Отправка отменена'
        else
          context.fail!(message: 'Отправка уже началась, невозможно отменить')
        end
      end
    end
  end
end
