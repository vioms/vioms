# frozen_string_literal: true

module Telegram
  class CancelMessage
    class Unpublish
      include Interactor

      delegate :telegram_message, to: :context

      def call
        telegram_message.published = false
        telegram_message.user = nil

        context.fail!(message: 'Произошла ошибка') unless telegram_message.save(validate: false)
      end
    end
  end
end
