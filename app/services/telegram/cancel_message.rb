# frozen_string_literal: true

module Telegram
  class CancelMessage
    include Interactor::Organizer

    organize CancelSchedule, Unpublish
  end
end
