# frozen_string_literal: true

module Telegram
  module UrlBuilder
    module_function

    def new(telegram_message, telegram_group)
      if telegram_message.image
        UrlBuilders::Photo.new(telegram_message, telegram_group)
      else
        UrlBuilders::Message.new(telegram_message, telegram_group)
      end
    end
  end
end
