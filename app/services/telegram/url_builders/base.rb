# frozen_string_literal: true

module Telegram
  module UrlBuilders
    class Base
      def initialize(telegram_message, telegram_group)
        @telegram_message = telegram_message
        @telegram_group   = telegram_group
      end

      attr_reader :telegram_message, :telegram_group

      # @abstract
      def url
        raise MethodNotImplementedError
      end

      # @abstract
      def params
        raise MethodNotImplementedError
      end

      def format
        :json
      end
    end
  end
end
