# frozen_string_literal: true

module Telegram
  module UrlBuilders
    class Photo < Base
      def url
        "https://api.telegram.org/bot#{telegram_group.bot_token}/sendPhoto"
      end

      def params
        {
          chat_id: telegram_group.chat_id.to_s,
          photo: telegram_message.image.to_io,
          caption: telegram_message.message,
          parse_mode: 'HTML'
        }
      end
    end
  end
end
