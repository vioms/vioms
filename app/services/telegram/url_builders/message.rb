# frozen_string_literal: true

module Telegram
  module UrlBuilders
    class Message < Base
      def url
        "https://api.telegram.org/bot#{telegram_group.bot_token}/sendMessage"
      end

      def params
        {
          chat_id: telegram_group.chat_id.to_s,
          text: telegram_message.message,
          parse_mode: 'HTML',
          disable_web_page_preview: true
        }
      end
    end
  end
end
