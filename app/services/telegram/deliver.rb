# frozen_string_literal: true

module Telegram
  module Deliver
    module_function

    def call(telegram_message)
      telegram_message.update_column(:sending, true)

      ([telegram_message.telegram_group] + telegram_message.included_telegram_groups).uniq.each do |telegram_group|
        url_builder = Telegram::UrlBuilder.new(telegram_message, telegram_group)
        retry_attempts = 0
        response =
          begin
            resp = RestClient.post(url_builder.url, url_builder.params, format: url_builder.format)
            # https://core.telegram.org/bots/faq#my-bot-is-hitting-limits-how-do-i-avoid-this
            sleep(5) unless Rails.env.test?
            resp
          rescue RestClient::RequestTimeout => e
            if retry_attempts < 5
              retry_attempts += 1
              sleep(10)
              Rails.telegram_logger.error("Retrying telegram message: #{telegram_message.attributes}" \
                                          "\nAttempt: #{retry_attempts}" \
                                          "\nError message: #{e.message}\nBacktrace: #{e.backtrace.join("\n")}")
              retry
            else
              raise(e)
            end
          rescue RestClient::ExceptionWithResponse => e
            telegram_message.raised_exception_at = Time.zone.now
            Rails.telegram_logger.error("Message: #{telegram_message.attributes}" \
                                        "\nError message: #{e.message}\nResponse: #{e.response.inspect}" \
                                        "\nBacktrace: #{e.backtrace.join("\n")}")
            e.response
          rescue StandardError => e
            telegram_message.raised_exception_at = Time.zone.now
            Rails.telegram_logger.error("Message: #{telegram_message.attributes}" \
                                        "\nError message: #{e.message}\nBacktrace: #{e.backtrace.join("\n")}")
            nil
          end

        if response&.code == 200
          telegram_message.sent = true
        else
          telegram_message.sent = false
          telegram_message.send_failed = true
          break
        end
      end
      telegram_message.update_attribute(:sending, false)
    end
  end
end
