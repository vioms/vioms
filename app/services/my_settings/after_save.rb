# frozen_string_literal: true

module MySettings
  class AfterSave
    include Interactor

    delegate :subscriber, to: :context

    def call
      subscriber.do_email_confirmation_task
      subscriber.notify_subscription
    end
  end
end
