# frozen_string_literal: true

module MySettings
  class Save
    include Interactor

    delegate :subscriber, :params, to: :context

    def call
      subscriber.email_list_ids_before = subscriber.email_list_ids
      return if subscriber.update(Subscriber::ParamsSanitizer::ForSubscriber.call(params, subscriber:))

      context.fail!
    end
  end
end
