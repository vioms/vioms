# frozen_string_literal: true

module MySettings
  class Update
    include Interactor::Organizer

    organize Subscriber::Unconfirm::Email,
             Save,
             AfterSave
  end
end
