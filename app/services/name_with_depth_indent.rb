# frozen_string_literal: true

module NameWithDepthIndent
  module_function

  def call(record)
    return record.name if record.depth == 0

    "|#{'––' * record.depth} #{record.name}"
  end
end
