# frozen_string_literal: true

class EmailDigest
  module Content
    module_function

    def call(email_digest)
      content = email_digest.email_list.digest_template
      return if content.blank?

      content = replace_vars(str: content, email_digest:)

      mailings = String.new
      email_digest.email_list.digest_categories.each do |digest_category|
        inner_mailings = String.new
        mailings_for_digest = digest_category.mailings.for_digest(email_digest.id)
        mailings_for_digest.each do |mailing|
          inner_mailings << render_email_digest(mailing:, digest_category:)
        end
        mailings << digest_category.template.gsub('[MAILINGS]', inner_mailings) if mailings_for_digest.any?
      end
      content.gsub!('[MAILINGS]', mailings)

      add_footer(content)
    end

    def replace_vars(str:, email_digest:)
      str
        .gsub('[NUM]', email_digest.num.to_s)
        .gsub('[YEAR]', email_digest.year.to_s)
    end

    def render_email_digest(mailing:, digest_category:)
      content = digest_category.template_entry.dup
      content
        .gsub('[MAILING_ID]', mailing.id.to_s)
        .gsub('[MAILING_NAME]', mailing.subject)
        .gsub('[MAILING_TEXT]', mailing.digest_content)
        .gsub('[MAILING_IMG]', mailing.digest_image.try(:url, :thumb).to_s)
    end

    def add_footer(content)
      content + "\n[FOOTER]"
    end
  end
end
