# frozen_string_literal: true

class EmailDigest
  module ReplaceVars
    module_function

    def call(content:, subscriber:)
      content = content.dup
      ::Mailing::ReplaceVars.replace_subscriber_vars(content:, subscriber:)
    end
  end
end
