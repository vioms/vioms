# frozen_string_literal: true

class EmailDigest
  module View
    module AdminPreview
      module_function

      def call(content:, email_digest:, email_list:)
        footer =
          if !email_list.never_visible && !email_list.skip_unsubscription_links
            footer(email_digest:, email_list:)
          else
            ''
          end
        content.gsub('[FOOTER]', footer)
      end

      def footer(email_digest:, email_list:)
        email_digest.decorate.footer(email_list)
      end
    end
  end
end
