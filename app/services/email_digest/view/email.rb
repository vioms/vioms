# frozen_string_literal: true

class EmailDigest
  module View
    module Email
      module_function

      def call(content:, email_digest:, email_list:, subscriber:)
        footer = ::EmailDigest::View::AdminPreview.footer(email_digest:, email_list:)
        content = content.gsub('[FOOTER]', footer)
        ::EmailDigest::ReplaceVars.call(content:, subscriber:)
      end
    end
  end
end
