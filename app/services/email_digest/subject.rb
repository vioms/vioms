# frozen_string_literal: true

class EmailDigest
  module Subject
    module_function

    def call(email_digest)
      subject = email_digest.email_list.digest_subject_template
      ::EmailDigest::Content.replace_vars(str: subject, email_digest:)
    end
  end
end
