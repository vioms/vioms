# frozen_string_literal: true

class EmailDigest
  class Update
    include Interactor::Organizer

    around do |interactor|
      EmailDigest.transaction { interactor.call }
    end

    organize UpdateEmailDigest, RenderContent
  end
end
