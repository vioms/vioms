# frozen_string_literal: true

class EmailDigest
  module Render
    module_function

    def call(email_digest)
      if email_digest.rendered_html.blank?
        email_digest.update_column(:rendered_html, ::EmailDigest::Content.call(email_digest))
      end

      email_digest.rendered_html
    end
  end
end
