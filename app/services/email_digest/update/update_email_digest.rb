# frozen_string_literal: true

class EmailDigest
  class Update
    class UpdateEmailDigest
      include Interactor

      delegate :email_digest, :params, to: :context

      def call
        context.fail! unless email_digest.update(params)
      end
    end
  end
end
