# frozen_string_literal: true

class EmailDigest
  class Update
    class RenderContent
      include Interactor

      delegate :email_digest, to: :context

      def call
        email_digest.rendered_html = nil
        email_digest.save!(validate: false)
        RenderDigestContentJob.perform_later(email_digest.id)
      end
    end
  end
end
