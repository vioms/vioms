# frozen_string_literal: true

class EmailDigest
  module Deliver
    module_function

    def call(email_digest)
      email_digest.update_column(:sending, true)

      errors = false
      rendered_subject = ::EmailDigest::Subject.call(email_digest)
      rendered_html = ::EmailDigest::Render.call(email_digest)
      email_digest.email_list.subscribers.for_email_sending_with_select.each do |subscriber|
        error = send_to_subscriber(email_digest:, subscriber:, rendered_html:, rendered_subject:)
        next if error.blank?

        ::Mailing::Deliver.log_error(obj: subscriber, error:)
        subscriber.update_column(:raised_exception_at, Time.zone.now)
        errors = true
      end

      if errors
        email_digest.send_failed = true
      else
        email_digest.sent = true
      end

      email_digest.update_attribute(:sending, false)
    end

    def send_to_subscriber(email_digest:, subscriber:, rendered_html:, rendered_subject:)
      mail = ViomsMailer.email_digest(
        email_digest:,
        subscriber:,
        rendered_html:,
        rendered_subject:
      )
      mail.deliver_now
      nil
    rescue StandardError => e
      e
    end
  end
end
