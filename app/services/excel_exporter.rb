# frozen_string_literal: true

module ExcelExporter
  module_function

  def call(rows)
    workbook = RubyXL::Workbook.new
    sheet = workbook.worksheets[0]

    rows.each_with_index do |row, i|
      values = row.attributes.except('id').values
      values.each_with_index do |attr, j|
        sheet.add_cell(i, j, attr)
      end
    end

    workbook.stream.string
  end

  def mime_type
    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
  end
end
