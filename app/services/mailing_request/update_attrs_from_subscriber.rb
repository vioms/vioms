# frozen_string_literal: true

class MailingRequest
  module UpdateAttrsFromSubscriber
    module_function

    def call(mailing_request, mailing_request_message)
      mailing_request_message.author = :subscriber
      mailing_request_message.old_status = mailing_request.status
      mailing_request_message.new_status = new_status
      mailing_request.status = new_status
    end

    def new_status
      :awaiting_admin_response
    end
  end
end
