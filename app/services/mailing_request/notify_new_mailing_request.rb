# frozen_string_literal: true

class MailingRequest
  module NotifyNewMailingRequest
    module_function

    def call(mailing_request)
      users_from_mailing_request(mailing_request).each do |user|
        MailingRequestsMailer.new_mailing_request_notification(user, mailing_request).deliver_now
      end
    end

    def users_from_mailing_request(mailing_request)
      User.joins(mailing_destinations: :email_lists)
          .where(mailing_destinations: { email_lists: { id: mailing_request.email_list_ids } })
          .distinct
    end
  end
end
