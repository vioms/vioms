# frozen_string_literal: true

class MailingRequest
  module NotifyNewMailingMessage
    module_function

    def call(mailing_request, mailing_request_message)
      NotifyNewMailingRequest.users_from_mailing_request(mailing_request).each do |user|
        MailingRequestsMailer.user_notification(user, mailing_request, mailing_request_message).deliver_now
      end
    end
  end
end
