# frozen_string_literal: true

class MailingRequest
  module UpdateAttrsFromAdmin
    module_function

    def call(mailing_request, mailing_request_message, user)
      mailing_request_message.author = :admin
      mailing_request_message.user = user
      mailing_request_message.old_status = mailing_request.status
      mailing_request.status = mailing_request_message.new_status
    end
  end
end
