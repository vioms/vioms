# frozen_string_literal: true

class MailingRequest
  module MaybeUpdateDonation
    module_function

    def call(mailing_request, mailing_request_message)
      return unless mailing_request.awaiting_donation_status?

      mailing_request.donation_method_id = mailing_request_message.donation_method_id
      mailing_request.donated_at = mailing_request_message.donated_at
    end
  end
end
