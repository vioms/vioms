# frozen_string_literal: true

class Mailing
  class Create
    include Interactor::Organizer

    around do |interactor|
      Mailing.transaction do
        interactor.call
        context.mailing.save!
      end
    end

    organize CreateMailing, SetupStripoTemplate, CreateStripoFileKey, SetContentHeaders
  end
end
