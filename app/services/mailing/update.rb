# frozen_string_literal: true

class Mailing
  class Update
    include Interactor::Organizer

    organize UpdateMailing, RenderContent
  end
end
