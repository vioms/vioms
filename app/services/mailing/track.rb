# frozen_string_literal: true

class Mailing
  module Track
    module_function

    def spacer_path
      Rails.root.join('app', 'assets', 'images', 'spacer.gif').to_s
    end
  end
end
