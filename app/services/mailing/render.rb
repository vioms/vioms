# frozen_string_literal: true

class Mailing
  module Render
    module_function

    def call(mailing)
      mailing.update_column(:rendered_html, ::Mailing::Content.call(mailing)) if mailing.rendered_html.blank?
      if mailing.rendered_html.present?
        mailing.update_column(:plain_content, ActionController::Base.helpers.strip_tags(mailing.rendered_html))
      end

      mailing.rendered_html
    end
  end
end
