# frozen_string_literal: true

class Mailing
  class Copy
    class CopyMailing
      include Interactor

      delegate :mailing, to: :context

      def call
        mailing_copy = mailing.dup
        mailing_copy.rendered_html = nil
        mailing_copy.plain_content = nil
        mailing_copy.searchable = nil
        mailing_copy.creating = true
        mailing_copy.deliver_at = nil
        mailing_copy.published = false
        mailing_copy.sent = false
        mailing_copy.sending = false
        mailing_copy.send_failed = false
        mailing_copy.raised_exception_at = nil
        mailing_copy.email_list_vars = mailing.email_list_vars.map(&:dup)
        mailing_copy.stripo_template_id = -1 # Hack to pass validation
        mailing_copy.save!
        context.mailing = mailing_copy
      end
    end
  end
end
