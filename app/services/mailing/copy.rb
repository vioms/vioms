# frozen_string_literal: true

class Mailing
  class Copy
    include Interactor::Organizer

    around do |interactor|
      Mailing.transaction { interactor.call }
    end

    organize CopyMailing, ::Mailing::Create::CreateStripoFileKey
  end
end
