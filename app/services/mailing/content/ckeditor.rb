# frozen_string_literal: true

class Mailing
  module Content
    module Ckeditor
      module_function

      def call(mailing)
        content = ::Mailing::Content::AddContentHeaders.call(mailing:)
        add_wrapper(content)
      end

      def add_wrapper(content)
        ApplicationController.render(
          partial: 'mailings/ckeditor_wrapper',
          assigns: { content: content.html_safe }
        )
      end
    end
  end
end
