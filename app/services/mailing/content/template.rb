# frozen_string_literal: true

class Mailing
  module Content
    module Template
      module_function

      def call(mailing)
        content = mailing.email_list.template.dup
        mailing.email_list_vars.each do |var|
          content.gsub!("[#{var.email_list_var_name.name}]", var.value)
        end
        content = ::Mailing::Content::AddContentHeaders.call(mailing:, content:)
        ::Mailing::Content::Ckeditor.add_wrapper(content)
      end
    end
  end
end
