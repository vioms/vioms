# frozen_string_literal: true

class Mailing
  module Content
    module Stripo
      module_function

      def call(mailing)
        content = ::Mailing::Content::AddContentHeaders.call(mailing:)
        ::Stripo::Compress.call(html: content, css: mailing.css)
      end
    end
  end
end
