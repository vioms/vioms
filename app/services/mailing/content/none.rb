# frozen_string_literal: true

class Mailing
  module Content
    module None
      module_function

      def call(mailing)
        ::Mailing::Content::AddContentHeaders.call(mailing:)
      end
    end
  end
end
