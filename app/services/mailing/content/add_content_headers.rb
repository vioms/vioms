# frozen_string_literal: true

class Mailing
  module Content
    module AddContentHeaders
      module_function

      def call(mailing:, content: mailing.html_content)
        return content unless mailing.enable_content_headers

        email_list = mailing.email_list
        email_list.content_header.to_s + content + email_list.content_footer.to_s
      end
    end
  end
end
