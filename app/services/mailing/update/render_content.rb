# frozen_string_literal: true

class Mailing
  class Update
    class RenderContent
      include Interactor

      delegate :mailing, to: :context

      def call
        Mailing::Render.call(mailing)
      end
    end
  end
end
