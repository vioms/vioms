# frozen_string_literal: true

class Mailing
  class Update
    class UpdateMailing
      include Interactor

      delegate :mailing, :params, to: :context

      def call
        return if mailing.update(params.to_h.merge({ rendered_html: nil, plain_content: nil }))

        context.fail!
      end
    end
  end
end
