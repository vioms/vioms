# frozen_string_literal: true

class Mailing
  class Destroy
    class DestroyMailing
      include Interactor

      delegate :mailing, to: :context

      def call
        mailing.reload
        mailing.destroy
      end
    end
  end
end
