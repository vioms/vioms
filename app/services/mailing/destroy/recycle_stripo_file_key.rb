# frozen_string_literal: true

class Mailing
  class Destroy
    # When we destroy a mailing, the corresponding Stripo file key might be reused for another mailing if it was not
    # sent or for a template.
    class RecycleStripoFileKey
      include Interactor

      delegate :mailing, to: :context
      delegate :stripo_file_key, to: :mailing

      def call
        return if mailing.sent || !mailing.stripo_editor_type?

        stripo_file_key.files.destroy_all
        stripo_file_key.update!(reusable: true, mailing_id: nil)
        mailing.stripo_file_key = nil
      end
    end
  end
end
