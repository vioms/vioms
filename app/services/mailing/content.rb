# frozen_string_literal: true

class Mailing
  module Content
    module_function

    def call(mailing)
      content = "::Mailing::Content::#{mailing.editor_type.camelize}".constantize.call(mailing)
      content = replace_title(content, mailing.subject)
      add_footer(content:, mailing:)
    end

    def replace_title(content, title)
      content.sub(%r{(<title>).*(</title>)}i, "\\1#{CGI.escapeHTML(title)}\\2")
    end

    def add_footer(content:, mailing:)
      return content if content.include?('[FOOTER]')

      content += "\n<hr>".html_safe if mailing.ckeditor_editor_type?
      content + "\n[FOOTER]"
    end
  end
end
