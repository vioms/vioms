# frozen_string_literal: true

class Mailing
  class Deliver
    include Dry::Monads[:result]

    def initialize(mailing:)
      @mailing = mailing
    end

    def call
      mailing.update_column(:sending, true)

      if prepare.success?
        ::Mailing::Deliver::SendMailings.new(mailing:).call => { deliveries_count:, delivery_errors_count:, errors: }
      else
        deliveries_count = 0
        delivery_errors_count = 0
        errors = true
      end

      finalize(deliveries_count:, delivery_errors_count:, errors:)
    end

    def self.log_error(obj:, error:)
      Rails.logger.error(obj.attributes.to_s + error.message + error.backtrace.join("\n"))
    end

    private

    attr_reader :mailing

    def prepare
      mailing.email_list.sync_json_contacts
      mailing.resolve_external_redirects
      Success()
    rescue StandardError => e
      mailing.raised_exception_at = Time.zone.now
      ::Mailing::Deliver.log_error(obj: mailing, error: e)
      Failure()
    end

    def finalize(deliveries_count:, delivery_errors_count:, errors:)
      if errors
        mailing.send_failed = true
      else
        mailing.sent = true
      end

      mailing.sending = false
      mailing.deliveries_count = deliveries_count
      mailing.delivery_errors_count = delivery_errors_count
      mailing.save!(validate: false)
    end
  end
end
