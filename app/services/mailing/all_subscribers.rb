# frozen_string_literal: true

class Mailing
  class AllSubscribers
    def initialize(mailing:, email_list:, exclude_email_list_ids:)
      @mailing = mailing
      @email_list = email_list
      @exclude_email_list_ids = exclude_email_list_ids
    end

    def call
      Subscriber
        .distinct
        .joins(:email_lists)
        .where(email_subscriptions: { email_list_id: @email_list.id })
        .where.not(id: excluded_subscriber_ids)
        .for_email_sending_with_select
    end

    def excluded_subscriber_ids
      Subscriber
        .select(:id)
        .joins(:email_lists)
        .where(email_subscriptions: { email_list_id: @mailing.excluded_email_lists + @exclude_email_list_ids })
    end

    def subscribers(email_list) = email_list.subscribers.for_email_sending_with_select
  end
end
