# frozen_string_literal: true

class Mailing
  module ReplaceVars
    module_function

    def call(content:, mailing:, subscriber:)
      content = content.dup
      content = replace_subscriber_vars(content:, subscriber:)
      replace_custom_vars(content:, mailing:, subscriber:)
    end

    def replace_subscriber_vars(content:, subscriber:)
      content = replace_var(content, 'SUBSCRIBER_ID', subscriber.id)
      content = replace_var(content, 'SUBSCRIBER_EMAIL', CGI.escape(subscriber.email))
      content = replace_var(content, 'AUTH_TOKEN', subscriber.authentication_token)
      replace_var(content, 'NAME', subscriber.name)
    end

    def replace_custom_vars(content:, mailing:, subscriber:)
      return content unless mailing.email_list.use_custom_vars

      subscriber
        .subscriber_custom_vars.joins(:email_list_custom_var)
        .where(email_list_custom_vars: { email_list_id: mailing.email_list_id })
        .find_each do |subscriber_custom_var|
        content = replace_var(content, subscriber_custom_var.email_list_custom_var.name, subscriber_custom_var.value)
      end
      content
    end

    def replace_var(content, name, value)
      value = value.to_s
      content
        .gsub("[#{name}]", value)
        .gsub("%5B#{name}%5D", value)
    end
  end
end
