# frozen_string_literal: true

class Mailing
  module View
    module AdminPreview
      module_function

      def call(content:, mailing:, email_list:)
        footer = footer(mailing:, email_list:)
        content.gsub('[FOOTER]', footer)
      end

      def footer(mailing:, email_list:)
        mailing.decorate.footer(email_list)
      end
    end
  end
end
