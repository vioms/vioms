# frozen_string_literal: true

class Mailing
  module View
    module Email
      module_function

      def call(content:, mailing:, email_list:, subscriber:)
        footer = ::Mailing::View::AdminPreview.footer(mailing:, email_list:)
        content = content.gsub('[FOOTER]', tracking_pixel(mailing:, subscriber:) + footer)
        ::Mailing::ReplaceVars.call(content:, mailing:, subscriber:)
      end

      def tracking_pixel(mailing:, subscriber:)
        email = u(subscriber.email)
        auth_token = subscriber.authentication_token
        %(<img src="#{BaseUrl.call}/mailings/#{mailing.id}/track_email.gif?email=#{email}&auth_token=#{auth_token}">)
      end
    end
  end
end
