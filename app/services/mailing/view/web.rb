# frozen_string_literal: true

class Mailing
  module View
    module Web
      module_function

      def call(content)
        content.gsub('[FOOTER]', '')
      end
    end
  end
end
