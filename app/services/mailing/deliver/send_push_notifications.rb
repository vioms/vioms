# frozen_string_literal: true

class Mailing
  class Deliver
    class SendPushNotifications
      def initialize(all_subscribers:, email_list:, mailing:)
        @all_subscribers = all_subscribers
        @email_list = email_list
        @mailing = mailing
      end

      def call
        return unless tokens

        redis.publish(channel_name, message)
      end

      attr_reader :all_subscribers, :email_list, :mailing

      def redis = Redis.new(url: ENV.fetch('VIOMS_GO_REDIS_URL'))
      def channel_name = ENV.fetch('VIOMS_GO_CHANNEL')

      def message
        {
          operation: 'push_notifications',
          params: {
            title: email_list.name,
            body: mailing.subject,
            email_list_id: email_list.id,
            mailing_id: mailing.id,
            tokens:
          }
        }.to_json
      end

      def tokens
        @tokens ||= all_subscribers.map do |subscriber|
          subscriber.mobile_tokens.map(&:device_token)
        end.flatten.compact
      end
    end
  end
end
