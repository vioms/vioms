# frozen_string_literal: true

class Mailing
  class Deliver
    class SendMailings
      def initialize(mailing:)
        @mailing = mailing
        @errors = false
        @deliveries_count = 0
        @delivery_errors_count = 0
        @exclude_email_list_ids = []
      end

      def call
        all_email_lists.each do |email_list|
          all_subscribers = all_subscribers(email_list)
          Mailing::Deliver::SendPushNotifications.new(all_subscribers:, email_list:, mailing:).call
          all_subscribers.each do |subscriber|
            SendToSubscriber.new(mailing:, email_list:, subscriber:, all_subscribers:, rendered_html:).call
                            .then { handle_error(_1, subscriber:) }
          end
          exclude_email_list_ids << email_list.id
        end

        { deliveries_count:, delivery_errors_count:, errors: }
      end

      private

      attr_reader :mailing, :errors, :deliveries_count, :delivery_errors_count, :exclude_email_list_ids

      def all_email_lists
        [mailing.email_list] +
          (mailing.included_email_lists - mailing.excluded_email_lists).sort_by(&:group_priority).uniq.reverse
      end

      def all_subscribers(email_list)
        Mailing::AllSubscribers.new(mailing:, email_list:, exclude_email_list_ids:).call
      end

      def handle_error(result, subscriber:)
        if result.success?
          @deliveries_count += 1
        else
          ::Mailing::Deliver.log_error(obj: subscriber, error: result.failure)
          subscriber.update_column(:raised_exception_at, Time.zone.now)
          @delivery_errors_count += 1
          @errors = true
        end
      end

      def rendered_html = mailing.rendered_html
    end
  end
end
