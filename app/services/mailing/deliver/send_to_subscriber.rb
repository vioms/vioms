# frozen_string_literal: true

class Mailing
  class Deliver
    class SendToSubscriber
      include Dry::Monads[:result]

      def initialize(mailing:, email_list:, subscriber:, all_subscribers:, rendered_html:)
        @mailing = mailing
        @email_list = email_list
        @subscriber = subscriber
        @all_subscribers = all_subscribers
        @rendered_html = rendered_html
      end

      def call
        ViomsMailer.mailing(mailing:, email_list:, subscriber:, content:).deliver_now
        Success()
      rescue StandardError => e
        return Success() unless all_subscribers.include?(subscriber)

        Failure(e)
      end

      private

      attr_reader :mailing, :email_list, :subscriber, :all_subscribers, :rendered_html

      def content = Mailing::View::Email.call(mailing:, email_list:, content: rendered_html, subscriber:)
    end
  end
end
