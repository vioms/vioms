# frozen_string_literal: true

class Mailing
  module Track
    module Write
      module_function

      def call(mailing:, subscriber:)
        mv = MailingView.find_or_initialize_by(mailing:, subscriber:)
        mv.increment(:count)
        mv.save!
      end
    end
  end
end
