# frozen_string_literal: true

class Mailing
  module Track
    module FromEmailAndToken
      module_function

      def call(mailing:, email:, auth_token:)
        authenticator = Subscriber::AuthenticateFromToken.new(email, auth_token)
        return unless authenticator.call

        Write.call(mailing:, subscriber: authenticator.subscriber)
      end
    end
  end
end
