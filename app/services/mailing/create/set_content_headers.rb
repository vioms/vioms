# frozen_string_literal: true

class Mailing
  class Create
    class SetContentHeaders
      include Interactor

      delegate :mailing, :params, to: :context

      def call
        if mailing.none_editor_type? || mailing.stripo_editor_type?
          mailing.enable_content_headers = false
        elsif mailing.ckeditor_editor_type? || mailing.template_editor_type?
          mailing.enable_content_headers = true
        else
          context.fail!
        end
      end
    end
  end
end
