# frozen_string_literal: true

class Mailing
  class Create
    class SetupStripoTemplate
      include Interactor

      delegate :mailing, to: :context

      def call
        return unless mailing.stripo_editor_type?

        template = Stripo::Template.find(mailing.stripo_template_id)
        mailing.html_content = template.html
        mailing.css = template.css
      end
    end
  end
end
