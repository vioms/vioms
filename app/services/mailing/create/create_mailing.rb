# frozen_string_literal: true

class Mailing
  class Create
    class CreateMailing
      include Interactor

      delegate :params, to: :context

      def call
        mailing = Mailing.new(params)
        mailing.creating = true
        context.mailing = mailing
        context.fail! unless mailing.valid?
      end
    end
  end
end
