# frozen_string_literal: true

class Mailing
  class Create
    class CreateStripoFileKey
      include Interactor

      delegate :mailing, to: :context

      def call
        return unless mailing.stripo_editor_type?

        mailing.stripo_file_key = ::Stripo::FindOrCreateFileKey.call
      end
    end
  end
end
