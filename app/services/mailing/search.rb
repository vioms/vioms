# frozen_string_literal: true

class Mailing
  module Search
    module_function

    def call(mailings, email_list_id, search)
      if search
        mailings = mailings.select(:id, :subject, :email_list_id).includes(:email_list).search_fulltext(search)
        mailings = mailings.where(email_list_id:) if email_list_id.to_i > 0
        mailings
      else
        Mailing.none
      end
    end
  end
end
