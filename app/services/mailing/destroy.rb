# frozen_string_literal: true

class Mailing
  class Destroy
    include Interactor::Organizer

    around do |interactor|
      Mailing.transaction { interactor.call }
    end

    organize RecycleStripoFileKey, DestroyMailing
  end
end
