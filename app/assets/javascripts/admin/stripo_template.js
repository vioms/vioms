$(function() {
  $('#stripo_template_submit_button').click(function(e) {
    e.preventDefault();
    window.StripoApi.getTemplate(function(html, css) {
      $('#stripo_template_html').val(html);
      $('#stripo_template_css').val(css);
      $('form.edit_stripo_template').trigger('submit');
    });
  });
});
