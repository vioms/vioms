jQuery(function(){

    // Popup window
    $('a.popup').click(function(e) {
        var width  = 575,
            height = 400,
            left   = ( $(window).width()  - width)  / 2,
            top    = ( $(window).height() - height) / 2,
            url    = this.href,
            opts   = 'status=0' +
                ',width='  + width  +
                ',height=' + height +
                ',top='    + top    +
                ',left='   + left;
        window.open(url, 'popup', opts);
        e.preventDefault();
        return false;
    });

});
