$(function() {
  $('#mailing_submit_button').click(function(e) {
    e.preventDefault();
    var $mailingHtmlContent = $('#mailing_html_content');
    var $mailingCss = $('#mailing_css');
    var $form = $('form.edit_mailing');
    window.frames['stripoFrame'].contentWindow.StripoApi.getTemplate(function(html, css) {
      $mailingHtmlContent.val(html);
      $mailingCss.val(css);
      $form.trigger('submit');
    });
  });
});
