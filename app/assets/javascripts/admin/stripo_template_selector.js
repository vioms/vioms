function addStripoTemplateGroupListener(selector, destinationSelector, templatePreview, templatePreviewImg) {
  selector.change(function () {
    if (templatePreview && templatePreviewImg) {
      showHidePreview(null, templatePreview, templatePreviewImg);
    }
    var stripoTemplateGroupId = selector.val();
    destinationSelector.empty();
    if (stripoTemplateGroupId) {
      populateStripoTemplateGroups(destinationSelector, stripoTemplateGroupId)
    }
  });
}

function populateStripoTemplateGroups(destinationSelector, stripoTemplateGroupId, callback) {
  $.getJSON('/admin/stripo_template_groups/' + stripoTemplateGroupId + '/stripo_templates', function(data) {
    destinationSelector.append($('<option />'));
    $.each(data, function(key, val) {
      var option = $('<option />').text(val['name']).val(val['id']).data('preview_url', val['preview_url']);
      destinationSelector.append(option);
    });
    if (typeof callback === 'function') { callback(); }
  });
}

function isStripoEditorType(selector) {
  return selector.val() === 'stripo';
}

function showHideTemplateSelectors(editorTypeSelector, mailingStripoTemplateGroupDiv, mailingStripoTemplateDiv) {
  if (isStripoEditorType(editorTypeSelector)) {
    mailingStripoTemplateGroupDiv.show();
    mailingStripoTemplateDiv.show();
  }
  else {
    mailingStripoTemplateGroupDiv.hide();
    mailingStripoTemplateDiv.hide();
  }
}

function showHidePreview(mailingStripoTemplateId, templatePreview, templatePreviewImg) {
  var preview_url = null;
  if (mailingStripoTemplateId) {
    preview_url = mailingStripoTemplateId.find('option:selected').data('preview_url');
  }
  if (preview_url) {
    templatePreviewImg.attr('src', preview_url);
    templatePreview.show();
  } else {
    templatePreview.hide();
    templatePreviewImg.attr('src', null);
  }
}
