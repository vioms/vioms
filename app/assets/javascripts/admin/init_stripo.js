function init_stripo(email_id, html, css) {
  window.Stripo.init({
    settingsId: 'stripoSettingsContainer',
    previewId: 'stripoPreviewContainer',
    locale: 'ru',
    html: html,
    css: css,
    apiRequestData: {
      emailId: email_id
    },
    getAuthToken: function(callback) {
      $.ajax({
        type: 'GET',
        url: '/api/stripo/token',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: data => callback(data.token),
        error: error => callback(null)
      });
    }
  });
}
