$(function() {
  var $mailingEmailListId = $('#mailing_email_list_id');
  var $mailingEditorType = $('#mailing_editor_type');
  var $mailingEditorTypeDiv = $('div.mailing_editor_type');
  var $mailingStripoTemplateGroupId = $('#mailing_stripo_template_group_id');
  var $mailingStripoTemplateGroupDiv = $('div.mailing_stripo_template_group');
  var $mailingStripoTemplateId = $('#mailing_stripo_template_id');
  var $mailingStripoTemplateDiv = $('div.mailing_stripo_template');
  var $templatePreview = $('#template_preview');
  var $templatePreviewImg = $templatePreview.find($('img'));

  $mailingEmailListId.change(function() {
    var email_list_id = $mailingEmailListId.val();
    $mailingStripoTemplateId.empty();
    if (!email_list_id) {
      $mailingEditorTypeDiv.hide();
      $mailingEditorType.val(null);
      showHideTemplateSelectors($mailingEditorType, $mailingStripoTemplateGroupDiv, $mailingStripoTemplateDiv);
      showHidePreview(null, $templatePreview, $templatePreviewImg);
      return;
    }
    $.getJSON('/admin/email_lists/' + email_list_id, function(data) {
      var email_list = data.email_list;
      $mailingEditorTypeDiv.show();
      $mailingEditorType.val(email_list.default_editor_type);
      showHideTemplateSelectors($mailingEditorType, $mailingStripoTemplateGroupDiv, $mailingStripoTemplateDiv);
      if (!isStripoEditorType($mailingEditorType)) {
        showHidePreview(null, $templatePreview, $templatePreviewImg);
        return;
      }

      $mailingStripoTemplateGroupId.val(null);
      $mailingStripoTemplateGroupId.empty();
      $mailingStripoTemplateGroupId.append($('<option />'));
      $.each(data.template_groups, function(key, val) {
        var option = $('<option />').text(val['name']).val(val['id']);
        $mailingStripoTemplateGroupId.append(option);
      });
      $mailingStripoTemplateGroupId.val(email_list.default_stripo_template_group_id);
      populateStripoTemplateGroups($mailingStripoTemplateId, $mailingStripoTemplateGroupId.val(), function() {
        $mailingStripoTemplateId.val(email_list.default_stripo_template_id);
        showHidePreview($mailingStripoTemplateId, $templatePreview, $templatePreviewImg);
      });
    });
  });

  $mailingEditorType.change(function() {
    var email_list_id = $mailingEmailListId.val();
    $.getJSON('/admin/email_lists/' + email_list_id, function(data) {
      $mailingStripoTemplateGroupId.val(null);
      $mailingStripoTemplateGroupId.empty();
      $mailingStripoTemplateGroupId.append($('<option />'));

      $.each(data.template_groups, function(key, val) {
        var option = $('<option />').text(val['name']).val(val['id']);
        $mailingStripoTemplateGroupId.append(option);
      });
    });
    $mailingStripoTemplateId.val(null);
    $mailingStripoTemplateId.empty();
    showHideTemplateSelectors($mailingEditorType, $mailingStripoTemplateGroupDiv, $mailingStripoTemplateDiv);
    showHidePreview(null, $templatePreview, $templatePreviewImg);
  });

  $mailingStripoTemplateId.change(function() {
    showHidePreview($mailingStripoTemplateId, $templatePreview, $templatePreviewImg);
  });

  addStripoTemplateGroupListener($mailingStripoTemplateGroupId, $mailingStripoTemplateId, $templatePreview,
                                 $templatePreviewImg);
});
