$(function() {
  var $stripoTemplateGroupParentId = $('#stripo_template_group_parent_id');
  $stripoTemplateGroupParentId.change(function() {
    if ($stripoTemplateGroupParentId.val()) {
      $('.root-group-elements').hide();
    } else {
      $('.root-group-elements').show();
    }
  });
});
