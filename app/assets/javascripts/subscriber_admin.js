$(document).ready(function() {
    $("#subscriber_phone").typeWatch({wait: 750, captureLength: 9, callback: function(value) {
        if ($('#subscriber_sms_operator_id option:selected').val() == '') {
            $.getScript($.queryString('/admin/subscribers/sms_operator', { 'phone' : value }));
        }
    }});
});
