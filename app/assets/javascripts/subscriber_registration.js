$(function() {
    var $checkbox = $('#subscriber_user_set_password'),
        $password_fields = $('#password-fields');

    if ($checkbox.is(':checked'))
        $password_fields.show();

    $checkbox.change(function() {
        if (this.checked)
            $password_fields.show();
        else
            $password_fields.hide();
    });
});
