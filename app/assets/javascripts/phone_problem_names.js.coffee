jQuery ->
  fixHelper = (e, ui) ->
    ui.children().each ->
      $(this).width($(this).width())
    ui

  $('#phone_problem_names tbody').sortable
    helper: fixHelper
    axis: 'y'
    handle: '.handle'
    update: ->
      $.post($(this).parent().data('update-url'), $(this).sortable('serialize'))
  .disableSelection
