$(function() {
    $.fragmentChange(true);
    $(document).bind("fragmentChange", function() {
        $("#spinner").show();
        $.get($.queryString(document.location.href, {
            "page" : $.fragment().page,
            "email_list" : $.fragment().email_list,
            "sms_list" : $.fragment().sms_list,
            "telegram_group" : $.fragment().telegram_group,
            "sms_account" : $.fragment().sms_account,
            "sms_operator" : $.fragment().sms_operator,
            "message_type" : $.fragment().message_type,
            "id" : $.fragment().id,
            "name" : $.fragment().name,
            "subject" : $.fragment().subject,
            "message" : $.fragment().message,
            "email" : $.fragment().email,
            "phone" : $.fragment().phone,
            "show_cancelled" : $.fragment().show_cancelled,
            "per_page" : $.fragment().per_page
        }), null, null, 'script');
    });

    if ($.fragment().page || $.fragment().email_list || $.fragment().sms_list || $.fragment().telegram_group ||
        $.fragment().sms_operator || $.fragment().id || $.fragment().name || $.fragment().subject ||
        $.fragment().message || $.fragment().email || $.fragment().show_cancelled) {
        $("#email_list option[value='" + $.fragment().email_list + "']").prop('selected');
        $("#sms_list option[value='" + $.fragment().sms_list + "']").prop('selected');
        $("#telegram_group option[value='" + $.fragment().telegram_group + "']").prop('selected');
        $("#sms_account option[value='" + $.fragment().sms_account + "']").prop('selected');
        $("#sms_operator option[value='" + $.fragment().sms_operator + "']").prop('selected');
        $("#show_cancelled").prop('checked', $.fragment().show_cancelled == 'true');
        $('#id').val($.fragment().id);
        $('#name').val($.fragment().name);
        $('#subject').val($.fragment().subject);
        $('#message').val($.fragment().message);
        $('#email').val($.fragment().email);
        $('#phone').val($.fragment().phone);
        $(document).trigger("fragmentChange");
    }

    $(document).on('click', '.pagination a', function() {
        $.setFragment({ "page" : $.queryString(this.href).page });
        return false;
    });

    $(document).on('change', '#email_list', function() {
        $.setFragment({
            "page" : 1,
            "email_list" : $('#email_list option:selected').val(),
            "id" : $('#id').val(),
            "name" : $('#name').val(),
            "subject" : $('#subject').val(),
            "email" : $('#email').val(),
            "phone" : $('#phone').val()
        });
    });

    $(document).on('change', '#sms_list', function() {
        $.setFragment({
            "page" : 1,
            "id" : $('#id').val(),
            "name" : $('#name').val(),
            "email" : $('#email').val(),
            "sms_list" : $('#sms_list option:selected').val(),
            "message" : $('#message').val(),
            "phone" : $('#phone').val()
        });
    });

    $(document).on('change', '#telegram_group', function() {
        $.setFragment({
            "page" : 1,
            "telegram_group" : $('#telegram_group option:selected').val(),
            "message" : $('#message').val()
        });
    });

    $(document).on('change', '#sms_account', function() {
        $.setFragment({
            "page" : 1,
            "id" : $('#id').val(),
            "name" : $('#name').val(),
            "email" : $('#email').val(),
            "sms_account" : $('#sms_account option:selected').val(),
            "message" : $('#message').val(),
            "phone" : $('#phone').val()
        });
    });

    $(document).on('change', '#sms_operator', function() {
        $.setFragment({
            "page" : 1,
            "id" : $('#id').val(),
            "name" : $('#name').val(),
            "email" : $('#email').val(),
            "sms_operator" : $('#sms_operator option:selected').val(),
            "message" : $('#message').val(),
            "phone" : $('#phone').val()
        });
    });

    $(document).on('change', '#message_type', function() {
        $.setFragment({
            "page" : 1,
            "phone" : $('#phone').val(),
            "message_type" : $('#message_type option:selected').val()
        });
    });

    $(document).on('change', '#per_page', function() {
        $.setFragment({
            "page" : 1,
            "id" : $('#id').val(),
            "name" : $('#name').val(),
            "email" : $('#email').val(),
            "subject" : $('#subject').val(),
            "phone" : $('#phone').val(),
            "message" : $('#message').val(),
            "per_page" : $('#per_page').val()
        });
    });

    $('#show_cancelled').change(function() {
        $.setFragment({
            "page" : 1,
            "show_cancelled": this.checked,
            "per_page" : $('#per_page').val()
        });
    });

    $('#search_form').submit(function() {
        $.setFragment({
            "page" : 1,
            "id" : $('#id').val(),
            "name" : $('#name').val(),
            "subject" : $('#subject').val(),
            "message" : $('#message').val(),
            "email" : $('#email').val(),
            "phone" : $('#phone').val(),
            "per_page" : $('#per_page').val()
        });
        return false;
    });
});
