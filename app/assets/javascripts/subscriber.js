$(function() {
    $('#resend_email_confirmation_button').click(function() {
        update_email_task(1, 'Письмо для подтверждения email будет отправлено после сохранения');
        return false;
    });

    $('#force_email_confirmation_button').click(function() {
        update_email_task(2, 'Email подписчика будет подтвержден после сохранения');
        return false;
    });

    $('#undo_email_confirmation_button').click(function() {
        $('#email_confirmation_message').hide();
        $('#email_confirmation_buttons').show();
        $('#email_confirmation_undo').hide();
        $('#subscriber_email_confirmation_task').val(0);
        return false;
    });

    function update_email_task(value, msg) {
        $('#email_confirmation_message').html(msg).show();
        $('#email_confirmation_buttons').hide();
        $('#email_confirmation_undo').show();
        $('#subscriber_email_confirmation_task').val(value);
    }
});
