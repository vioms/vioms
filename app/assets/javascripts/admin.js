//= require jquery_ujs
//= require popper
//= require bootstrap-sprockets
//= require jquery_nested_form
//= require jquery.typewatch
//= require jquery-ui/widgets/sortable
//= require resize_iframe
//= require admin/stripo_template_selector

$(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('.alert').alert();
});

function copy_to_clipboard(el) {
    el.select();
    el.setSelectionRange(0, 99999); // For mobile devices
    navigator.clipboard.writeText(el.value);
}
