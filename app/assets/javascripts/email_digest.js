$(document).ready(function() {
    $('#email_digest_email_list_id').change(function() {
        var email_list_id = $('#email_digest_email_list_id option:selected').val();
        if (email_list_id != '') {
            $.getScript($.queryString('/admin/email_digests/new', { 'email_list' : email_list_id }));
        }
        else {
            $('#email_digest_num').val('');
        }
    });
});
