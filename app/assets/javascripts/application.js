// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery_ujs
//= require popper
//= require bootstrap-sprockets
//= require jquery.maskedinput
//= require resize_iframe
//= require jquery_nested_form

$(function() {
    $('[data-toggle="tooltip"]').tooltip();
    $('.dropdown-menu a[data-toggle="tab"]').click(function (e) {
        e.stopPropagation();
        $(this).tab('show');
    });

    $('.huawei-appgallery img').hover(function() {
        this.setAttribute('src', '/assets/Huawei_AppGallery-hover-e82a275ffc8b251e2d1ded2cf91701d68ab6485957aa8f0d46c879da639f802f.svg');
    }, function() {
        this.setAttribute('src', '/assets/Huawei_AppGallery-1b113b40f92a01bd2e811932f561594a6fdfd0119b012289ce41c4d3f7623e70.svg');
    });
});
