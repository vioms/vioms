$(function() {
  function loadMailings(email_list_id) {
    $.getJSON('recent_mailings?email_list_id=' + email_list_id, function(json) {
      var options = $.map(json, function(mailing) {
        return "<option value=\"" + mailing.id + "\">" + mailing.subject + "</option>";
      })
      $('#mailings').html("<option></option>" + options);
    });
  }

  $('#addLinkToMailingModal').click(function() {
    $.getJSON('../email_lists', function(json) {
      var options = $.map(json, function(email_list) {
        return "<option value=\"" + email_list.id + "\">" + email_list.name + "</option>";
      })
      $('#email_lists').html("<option value=\"\">Все</option>" + options);
    });
    loadMailings('');
  });

  $('#email_lists').change(function() {
    $('#addLinkToMailing').prop('disabled', true);
    var email_list_id = $("#email_lists option:selected").val();
    loadMailings(email_list_id);
  });

  $('#mailings').change(function() {
    var disabled = true;
    if ($("#mailings option:selected").val()) {
      disabled = false;
    }
    $('#addLinkToMailing').prop('disabled', disabled);
  });

  $('#addLinkToMailing').click(function() {
    var $message = $('#telegram_message_message')
    var mailing_id = $("#mailings option:selected").val();
    var link = "\n<a href=\"https://vioms.ru/mailings/" + mailing_id + "\">Подробнее...</a> 👉";
    var old_message = $message.val();
    $message.val(old_message + link);
  });
});
