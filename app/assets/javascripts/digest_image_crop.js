$(function() {
    $('#mailing_digest_image_cropbox').Jcrop({
        onSelect: update_coords,
        onRelease: release_coords,
        aspectRatio: 1,
        setSelect: [0, 0, 800, 800],
        minSize: [85, 85]
    });
});

function update_coords(coords) {
    $('#mailing_digest_image_crop_x').val(coords.x);
    $('#mailing_digest_image_crop_y').val(coords.y);
    $('#mailing_digest_image_crop_w').val(coords.w);
    $('#mailing_digest_image_crop_h').val(coords.h);
}

function release_coords(coords) {
    $('#mailing_digest_image_crop_x').val(null);
    $('#mailing_digest_image_crop_y').val(null);
    $('#mailing_digest_image_crop_w').val(null);
    $('#mailing_digest_image_crop_h').val(null);
}

//  updatePreview: (coords) =>
//    $('#mailing_digest_image_previewbox').css
//      width: Math.round(85/coords.w * $('#mailing_digest_image_cropbox').width()) + 'px'
//      height: Math.round(85/coords.h * $('#mailing_digest_image_cropbox').height()) + 'px'
//      marginLeft: '-' + Math.round(85/coords.w * coords.x) + 'px'
//      marginTop: '-' + Math.round(85/coords.h * coords.y) + 'px'
