atom_feed do |feed|
  feed.title rss_feed_title
  feed.updated Mailing.maximum(:updated_at)

  @email_lists.each do |email_list|
    feed.entry email_list, url: email_list_url(email_list, 'atom') do |entry|
      entry.title email_list.name
      entry.content email_list.description, type: 'html'
      entry.author { |a| a.name rss_feed_title }
    end
  end
end
