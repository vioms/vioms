xml.instruct! :xml, version: '1.0'
xml.rss version: '2.0', 'xmlns:atom' => 'http://www.w3.org/2005/Atom' do
  xml.channel do
    xml.title rss_feed_title
    xml.description rss_feed_title
    xml.link root_url
    xml.language 'ru'
    xml.tag! 'atom:link', href: email_lists_url(format: 'rss'), rel: 'self', type: 'application/rss+xml'

    @email_lists.each do |email_list|
      xml.item do
        xml.title email_list.name
        xml.description email_list.description
        xml.link email_list_url(email_list, 'rss').to_s
        xml.guid email_list_url(email_list, 'rss').to_s
      end
    end
  end
end
