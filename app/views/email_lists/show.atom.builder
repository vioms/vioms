atom_feed do |feed|
  feed.title @email_list.name
  feed.updated @mailings.maximum(:deliver_at)
  # feed.link(rel: 'self', type: 'application/atom+xml', href: mailing_url(page: @mailings.current_page, format: 'atom'))
  # feed.link(rel: 'next', href: mailing_url(page: @mailings.current_page+1, format: 'atom')) unless @mailings.last_page?
  # feed.link(rel: 'first', href: mailing_url(page: 1, format: 'atom'))
  # feed.link(rel: 'previous', href: mailing_url(page: @mailings.current_page-1, format: 'atom')) unless @mailings.first_page?
  # feed.link(rel: 'last', href: mailing_url(page: @mailings.total_pages, format: 'atom'))

  @mailings.decorate.each do |mailing|
    feed.entry mailing, published: mailing.deliver_at do |entry|
      entry.title mailing.subject
      entry.content mailing.web_view, type: 'html'
      entry.author { |a| a.name(@email_list.from_name) }
    end
  end
end
