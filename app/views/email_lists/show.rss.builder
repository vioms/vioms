xml.instruct! :xml, version: '1.0'
xml.rss version: '2.0', 'xmlns:atom' => 'http://www.w3.org/2005/Atom' do
  xml.channel do
    xml.title @email_list.name
    xml.description @email_list.name
    xml.link root_url
    xml.language 'ru'
    xml.tag! 'atom:link', href: email_list_url(@email_list, 'rss'), rel: 'self', type: 'application/rss+xml'

    @mailings.decorate.each do |mailing|
      xml.item do
        xml.title mailing.subject
        xml.description mailing.web_view
        xml.pubDate mailing.deliver_at.to_fs(:rfc822)
        xml.link mailing_url(mailing).to_s
        xml.guid mailing_url(mailing).to_s
      end
    end
  end
end
