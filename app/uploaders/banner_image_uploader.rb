class BannerImageUploader < CarrierWave::Uploader::Base
  include FileNameTransliteration
  include CarrierWave::MiniMagick
  storage :file

  # Override the directory where uploaded files will be stored.
  # This is a sensible default for uploaders that are meant to be mounted:
  def store_dir
    "banners/#{model.id}"
  end

  def extension_white_list
    %w[jpg jpeg gif png]
  end
end
