class EmailAttachmentUploader < CarrierWave::Uploader::Base
  include FileNameTransliteration

  storage :file

  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  def extension_white_list
    %w[jpg jpeg gif png doc docx xls xlsx odt ods pdf txt htm html ppt pptx]
  end
end
