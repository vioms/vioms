class DigestImageUploader < CarrierWave::Uploader::Base
  include FileNameTransliteration
  include CarrierWave::MiniMagick

  storage :file

  # Override the directory where uploaded files will be stored.
  # This is a sensible default for uploaders that are meant to be mounted:
  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  # Provide a default URL as a default if there hasn't been a file uploaded:
  # def default_url
  #   # For Rails 3.1+ asset pipeline compatibility:
  #   # ActionController::Base.helpers.asset_path("fallback/" + [version_name, "default.png"].compact.join('_'))
  #
  #   "/images/fallback/" + [version_name, "default.png"].compact.join('_')
  # end

  # Process files as they are uploaded:
  # process :scale => [200, 300]
  #
  # def scale(width, height)
  #   # do something
  # end

  # Create different versions of your uploaded files:
  version :large do
    process resize_to_limit: [800, 800]
  end

  version :thumb do
    process :crop
    process resize_to_fill: [85, 85]
  end

  def extension_white_list
    %w[jpg jpeg gif png]
  end

  # Override the filename of the uploaded files:
  # Avoid using model.id or version_name here, see uploader/store.rb for details.
  # def filename
  #   "something.jpg" if original_filename
  # end

  def crop
    return unless model.digest_image_crop_x.present?

    resize_to_limit(800, 800)
    manipulate! do |img|
      x = model.digest_image_crop_x.to_i
      y = model.digest_image_crop_y.to_i
      w = model.digest_image_crop_w.to_i
      h = model.digest_image_crop_h.to_i
      img.crop("#{w}x#{h}+#{x}+#{y}")
    end
  end
end
