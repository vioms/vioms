module FileNameTransliteration
  CarrierWave::SanitizedFile.sanitize_regexp = /[^a-zA-Zа-яА-ЯёЁ0-9.\-+_]/

  def filename
    Russian.translit(original_filename).to_url.gsub('-tochka-', '.') if original_filename
  end
end
