# frozen_string_literal: true

module ShrineUploaders
  class MailingRequestImage < Shrine
    plugin :pretty_location
    plugin :validation_helpers
    plugin :remove_invalid

    Attacher.validate do
      validate_max_size 10.megabytes, message:
        t('activerecord.errors.models.mailing_request_image.image.too_large')
      validate_mime_type %w[image/jpeg image/png image/webp image/tiff image/gif]
      validate_extension %w[jpg jpeg png webp tiff tif gif]
    end
  end
end
