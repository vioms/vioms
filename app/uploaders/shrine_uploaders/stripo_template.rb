# frozen_string_literal: true

module ShrineUploaders
  class StripoTemplate < Shrine
    plugin :pretty_location
    plugin :derivatives
    plugin :remove_invalid

    Attacher.derivatives do |original|
      magick = ImageProcessing::MiniMagick.source(original)

      {
        thumb: magick.resize_to_limit!(2136, 2136)
      }
    end
  end
end
