# frozen_string_literal: true

module ShrineUploaders
  class StripoFile < Shrine
    plugin :derivatives
    plugin :add_metadata

    Attacher.derivatives do |original|
      magick = ImageProcessing::MiniMagick.source(original)

      {
        thumb: magick.resize_to_limit!(130, 130)
      }
    end

    add_metadata do |uploaded_file|
      res = IO.popen('identify -format "%w,%h" -', 'wb+') do |subprocess|
        subprocess.write(uploaded_file.read)
        subprocess.close_write
        subprocess.read
      end
      next {} if res.blank?

      (width, height) = res.split(',')
      {
        width: width.to_i,
        height: height.to_i
      }
    end

    private

    def generate_location(_io, record:, **)
      File.join('stripo', record.key_plugin_id, record.key_email_id.to_s, super)
    end
  end
end
