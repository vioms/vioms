# frozen_string_literal: true

class EmailDigestDecorator < ApplicationDecorator
  delegate_all

  def footer(email_list)
    h.render partial: 'vioms_mailer/email_digest_footer', locals: { email_list: }
  end
end
