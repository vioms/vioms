# frozen_string_literal: true

class MailingDecorator < ApplicationDecorator
  delegate_all

  def web_view
    Mailing::View::Web.call(object.rendered_html).html_safe
  end

  def footer(email_list)
    h.render(partial: 'vioms_mailer/mailing_footer', locals: { mailing: object, email_list: },
             formats: %i[html])
  end
end
