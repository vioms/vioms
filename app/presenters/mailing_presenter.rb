# frozen_string_literal: true

class MailingPresenter
  def initialize(mailing)= @mailing = mailing

  def as_json(_options)
    {
      id: @mailing.id,
      subject: @mailing.subject,
      published_at: @mailing.deliver_at.utc
    }
  end
end
