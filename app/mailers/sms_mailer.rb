class SmsMailer < SmsActionMailer
  helper :application, :sms_mailer

  default from: 'sms@vioms.ru'

  def password_recovery(subscriber, raw_token)
    mail(send_sms_message('password_recovery', subscriber, raw_token))
  end

  private

  def send_sms_message(sms_message_name, subscriber, raw_token = nil)
    @subscriber = subscriber
    @message = view_context.get_message(sms_message_name, raw_token)
    {
      subject: view_context.subject_for_message(sms_message_name),
      template_name: :sms_message,
      to: SmtpSettingGroup.get_selected_email
    }
  end
end
