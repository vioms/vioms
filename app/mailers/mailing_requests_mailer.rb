# frozen_string_literal: true

class MailingRequestsMailer < ApplicationMailer
  helper :application

  default from: 'robot@vioms.ru'

  layout 'bootstrap-mailer'

  def new_mailing_request_notification(user, mailing_request)
    @user = user
    @mailing_request = mailing_request
    bootstrap_mail(subject: "[VIOMS] Новая заявка №#{mailing_request.id}", to: user.email)
  end

  def subscriber_notification(subscriber, mailing_request_message)
    @mailing_request_id = mailing_request_message.mailing_request_id
    @message = mailing_request_message.message
    @user = mailing_request_message.user
    @mailing_request_message = mailing_request_message
    bootstrap_mail(subject: "[VIOMS] Поступил ответ по Вашей заявке №#{@mailing_request_id}", to: subscriber.email)
  end

  def user_notification(user, mailing_request, mailing_request_message)
    @mailing_request = mailing_request
    @mailing_request_message = mailing_request_message
    @message = mailing_request_message.message
    @subscriber = @mailing_request.subscriber
    bootstrap_mail(subject: "[VIOMS] Новое сообщение по заявке №#{mailing_request.id}", to: user.email)
  end
end
