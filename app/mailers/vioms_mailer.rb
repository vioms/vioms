# frozen_string_literal: true

class ViomsMailer < ApplicationMailer
  default from: 'robot@vioms.ru'
  layout 'mailing'
  helper ApplicationHelper
  helper LayoutHelper
  helper EmailListsHelper

  def mailing(mailing:, email_list:, subscriber:, content:)
    add_email_headers(mailing.email_list, subscriber, mailing.id)
    mailing.email_attachments.each do |email_attachment|
      attachments[email_attachment.attributes['attachment']] = email_attachment.attachment.read
    end

    mail(from: "#{email_list.from_name} <#{email_list.from_email}>",
         to: subscriber.to_email,
         subject: mailing.subject,
         layout: nil) do |format|
      format.html { render html: content.html_safe }
    end
  end

  def email_digest(email_digest:, subscriber:, rendered_html:, rendered_subject:)
    email_list = email_digest.email_list
    add_email_headers(email_list, subscriber, email_digest.id, true)

    @skip_unsubscription_links = email_digest.email_list.skip_unsubscription_links
    content = EmailDigest::View::Email.call(
      email_digest:,
      email_list:,
      content: rendered_html,
      subscriber:
    )
    mail(from: "#{email_list.from_name} <#{email_list.from_email}>",
         to: subscriber.to_email,
         subject: rendered_subject) do |format|
      format.html { render html: content.html_safe }
    end
  end

  def support_message(contact)
    @name = contact[:name]
    @email = contact[:email]
    @phone = contact[:phone]
    @body_subject = contact[:subject]
    @question = contact[:question]
    mail(subject: "[вопрос] #{@body_subject}", from: @email, to: '<support@vioms.ru>')
  end

  def email_confirmation(subscriber)
    @email_confirmation_token = subscriber.email_confirmation_token
    @auth_token = subscriber.authentication_token
    @email = subscriber.email
    mail(subject: 'Подтверждение вашего ящика', to: subscriber.to_email)
  end

  def subscribed_notification(subscriber_id, email_list_id)
    @subscriber = Subscriber.select(%i[id name email]).find(subscriber_id)
    @email_list = EmailList.select(%i[name notify_email]).find(email_list_id)
    mail(subject: 'Человек подписался', to: "<#{@email_list.notify_email}>")
  end

  def unsubscribed_notification(subscriber_id, email_list_id)
    @subscriber = Subscriber.select(%i[id name email]).find(subscriber_id)
    @email_list = EmailList.select(%i[name notify_email]).find(email_list_id)
    mail(subject: 'Человек отписался', to: "<#{@email_list.notify_email}>")
  end

  def cannot_send_sms_notification(sms_sending, user)
    @sms_sending = sms_sending
    mail(subject: 'Не хватает средств для отправки sms', from: '<support@vioms.ru>', to: user.to_email)
  end

  def sms_account_low_notification(sms_account, user)
    @sms_account = sms_account
    mail(subject: 'Низкий баланс на Вашем счете', from: '<support@vioms.ru>', to: user.to_email)
  end

  private

  def add_email_headers(email_list, subscriber, mailing_id, is_digest = false)
    mailing_id = mailing_id.to_s
    headers['Return-Path'] = '<' + email_list.from_bounce + '>' if email_list.from_bounce
    headers['Reply-To'] = '<' + email_list.reply_to + '>' if email_list.reply_to
    headers['X-Postmaster-Msgtype'] = if is_digest
                                        if email_list.postmaster_msgtype.present?
                                          email_list.postmaster_msgtype + '_d' + mailing_id
                                        else
                                          'd' + mailing_id
                                        end
                                      else
                                        email_list.postmaster_msgtype.to_s + mailing_id
                                      end
    headers['List-Unsubscribe'] = '<' + new_unsubscription_url(email_list_id: email_list.id,
                                                               auth_token: subscriber.authentication_token, quick: 1,
                                                               email: subscriber.email,
                                                               protocol: 'http') + '>'
  end
end
