# frozen_string_literal: true

class SmsActionMailer < ApplicationMailer
  def mail(headers = {}, &)
    ActionMailer::Base.smtp_settings = SmtpSettingGroup.get_selected_group.to_yaml if Rails.env.production?
    begin
      ret = super
    rescue StandardError
    end
    ActionMailer::Base.smtp_settings = $email_settings if Rails.env.production?
    ret
  end
end
