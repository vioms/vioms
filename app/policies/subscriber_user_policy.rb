# frozen_string_literal: true

class SubscriberUserPolicy < ApplicationPolicy
  def manage?
    true
  end

  def permitted_attributes
    [
      :name, :email, :phone, { email_list_ids: [] }, :set_password, :current_password, :password,
      :password_confirmation, :accept_privacy_policy
    ]
  end
end
