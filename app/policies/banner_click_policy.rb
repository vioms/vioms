# frozen_string_literal: true

class BannerClickPolicy < ApplicationPolicy
  def new?
    true
  end
end
