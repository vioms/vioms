# frozen_string_literal: true

class EmailListPolicy < ApplicationPolicy
  def index?
    true
  end

  def show?
    super || EmailList.visible_by_subscriber(subscriber).pluck(:id).include?(record.id)
  end

  class Scope < Scope
    def resolve
      scope.visible_by_subscriber(subscriber)
    end
  end
end
