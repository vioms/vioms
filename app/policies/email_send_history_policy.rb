# frozen_string_literal: true

class EmailSendHistoryPolicy < ApplicationPolicy
  def show?
    true
  end
end
