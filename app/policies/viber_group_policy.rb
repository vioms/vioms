# frozen_string_literal: true

class ViberGroupPolicy < ApplicationPolicy
  def index?
    true
  end
end
