# frozen_string_literal: true

class SubscriberPolicy < SubscriberUserPolicy
  def destroy?
    true
  end

  def permitted_attributes
    [:name, :email, :phone, { email_list_ids: [] }]
  end
end
