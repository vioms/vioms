# frozen_string_literal: true

class MailingRequestMessagePolicy < ApplicationPolicy
  def index?
    true
  end

  def create?
    record.mailing_request.status != 'processed'
  end

  def permitted_attributes
    %i[message donation_method_id donated_at]
  end
end
