# frozen_string_literal: true

class VkGroupPolicy < ApplicationPolicy
  def index?
    true
  end
end
