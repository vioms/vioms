# frozen_string_literal: true

class ApplicationPolicy
  attr_accessor :user, :subscriber, :record

  def initialize(users, record)
    @user = users[:user]
    @subscriber = users[:subscriber]
    @record = record
  end

  def index?
    manage? || read?
  end

  def show?
    manage? || read?
  end

  def create?
    manage?
  end

  def new?
    create?
  end

  def update?
    manage?
  end

  def edit?
    update?
  end

  def destroy?
    manage?
  end

  def read?
    false
  end

  def manage?
    false
  end

  # Map custom actions to `manage?`
  def method_missing(method, *args, &)
    method.to_s.end_with?('?') ? manage? : super
  end

  def respond_to_missing?(method, *)
    method.to_s.end_with?('?') || super
  end

  private

  def policy(record)
    policies[record] ||= Pundit.policy!(user, record)
  end

  def policies
    @policies ||= {}
  end

  class Scope
    attr_accessor :user, :subscriber, :scope

    def initialize(user, scope)
      @user = user[:user]
      @subscriber = user[:subscriber]
      @scope = scope
    end

    def resolve
      scope.all
    end
  end
end
