# frozen_string_literal: true

class EmailAttachmentPolicy < ApplicationPolicy
  def read?
    true
  end
end
