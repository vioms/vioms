# frozen_string_literal: true

class FacebookGroupPolicy < ApplicationPolicy
  def index?
    true
  end
end
