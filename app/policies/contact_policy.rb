# frozen_string_literal: true

class ContactPolicy < ApplicationPolicy
  def create?
    true
  end

  def permitted_attributes
    %i[name email phone subject question]
  end
end
