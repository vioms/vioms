# frozen_string_literal: true

class TelegramGroupPolicy < ApplicationPolicy
  def index?
    true
  end

  class Scope < Scope
    def resolve
      scope.visible
    end
  end
end
