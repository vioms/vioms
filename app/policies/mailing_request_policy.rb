# frozen_string_literal: true

class MailingRequestPolicy < ApplicationPolicy
  def index?
    true
  end

  def create?
    true
  end

  def show?
    subscriber && record.subscriber_id == subscriber.id
  end

  def manage?
    show? && record.status.in?(%w[awaiting_subscriber_response awaiting_donation])
  end

  def destroy?
    !record.processed_status?
  end

  def permitted_attributes
    [
      :subject, :body, :deliver_at, :mailing_type, :donation_method_id, :donated_at, :comments,
      { images_attributes: %i[id image _destroy] }, { email_list_ids: [] }
    ]
  end

  class Scope < Scope
    def resolve
      scope.authorized(subscriber)
    end
  end
end
