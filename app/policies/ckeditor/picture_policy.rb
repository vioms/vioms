# frozen_string_literal: true

module Ckeditor
  class PicturePolicy
    attr_reader :user, :picture

    def initialize(user, picture)
      @user = user
      @picture = picture
    end

    def index?
      true
    end

    def create?
      true
    end

    def destroy?
      user.effective_admin? || picture.assetable_id == user.id
    end
  end
end
