# frozen_string_literal: true

module Ckeditor
  class AttachmentFilePolicy
    attr_reader :user, :attachment

    def initialize(user, attachment)
      @user = user
      @attachment = attachment
    end

    def index?
      true
    end

    def create?
      true
    end

    def destroy?
      user.effective_admin? || attachment.assetable_id == user.id
    end
  end
end
