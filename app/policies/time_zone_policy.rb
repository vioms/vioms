# frozen_string_literal: true

class TimeZonePolicy < ApplicationPolicy
  def index?
    true
  end
end
