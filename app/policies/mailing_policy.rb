# frozen_string_literal: true

class MailingPolicy < ApplicationPolicy
  def show?
    return false if !record.visible || record.expired?

    return true if record.email_list.visible

    email_list_ids = record.included_email_list_ids
    return true if email_list_ids.intersect?(EmailList.visible.ids)

    return false if subscriber.nil?

    subscriber.email_list_ids.include?(record.email_list_id) || subscriber.email_list_ids.intersect?(email_list_ids)
  end

  def full?
    show?
  end

  def track_email?
    true
  end

  class Scope < Scope
    def resolve
      Mailing.archive_visible(subscriber)
    end
  end
end
