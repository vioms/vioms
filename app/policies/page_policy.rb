# frozen_string_literal: true

class PagePolicy < ApplicationPolicy
  def read?
    true
  end

  def manage?
    user && (user.effective_admin? || user.is_pages?)
  end
end
