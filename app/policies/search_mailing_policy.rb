# frozen_string_literal: true

class SearchMailingPolicy < SubscriberUserPolicy
  def index?
    true
  end
end
