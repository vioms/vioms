# frozen_string_literal: true

class BannerPolicy < ApplicationPolicy
  def show?
    true
  end
end
