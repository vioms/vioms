# frozen_string_literal: true

class UnsubscriptionPolicy < ApplicationPolicy
  def create?
    subscriber.present?
  end
end
