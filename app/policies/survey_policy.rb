# frozen_string_literal: true

class SurveyPolicy < ApplicationPolicy
  def update?
    @subscriber && record.visible
  end
end
