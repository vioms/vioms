# frozen_string_literal: true

module Admin
  class EmailListVarNamePolicy < BasePolicy
    def index?
      true
    end
  end
end
