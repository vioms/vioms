# frozen_string_literal: true

module Admin
  class SubscriberCustomVarPolicy < BasePolicy
    class Scope < Scope
      def resolve
        scope.authorized(user)
      end
    end
  end
end
