# frozen_string_literal: true

module Admin
  class EmailProblemPolicy < BasePolicy
    def manage?
      true
    end

    def permitted_attributes
      %i[email_problem_name_id description solved]
    end
  end
end
