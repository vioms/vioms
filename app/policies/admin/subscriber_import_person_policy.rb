# frozen_string_literal: true

module Admin
  class SubscriberImportPersonPolicy < BasePolicy
    def manage?
      super || user.is_import?
    end

    def update?
      super && record.error_id.present?
    end

    def permitted_attributes
      %i[name email phone]
    end
  end
end
