# frozen_string_literal: true

module Admin
  class DonationMethodPolicy < BasePolicy
    def manage?
      super || user.effective_admin?
    end

    def permitted_attributes
      %i[name]
    end
  end
end
