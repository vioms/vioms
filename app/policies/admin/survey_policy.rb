# frozen_string_literal: true

module Admin
  class SurveyPolicy < BasePolicy
    def permitted_attributes
      %i[name title visible announce]
    end
  end
end
