# frozen_string_literal: true

module Admin
  class EmailListCustomVarPolicy < BasePolicy
    def manage?
      super || user.is_custom_vars?
    end
  end
end
