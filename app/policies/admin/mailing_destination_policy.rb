# frozen_string_literal: true

module Admin
  class MailingDestinationPolicy < BasePolicy
    def permitted_attributes
      [:name, { user_ids: [] }, { email_list_ids: [] }]
    end
  end
end
