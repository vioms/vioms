# frozen_string_literal: true

module Admin
  class TelegramGroupPolicy < BasePolicy
    def index?
      true
    end

    def manage?
      super || (record.is_a?(TelegramGroup) && user.allowed_telegram_group_id?(record.id))
    end

    def permitted_attributes
      attrs = %i[name visible description invite_link chat_id bot_token]
      attrs.push(user_ids: []) if user.effective_admin?
      attrs
    end

    class Scope < Scope
      def resolve
        scope.authorized(user)
      end
    end
  end
end
