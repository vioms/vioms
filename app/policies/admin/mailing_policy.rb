# frozen_string_literal: true

module Admin
  class MailingPolicy < BasePolicy
    def index?
      true
    end

    def new?
      true
    end

    def manage?
      super || user.allowed_email_list_id?(record.email_list_id) || record.email_list_id.nil?
    end

    def permitted_attributes
      [:email_list_id, :editor_type, :deliver_at, :subject, :stripo_template_id, :stripo_template_group_id,
       :html_content, :css, :visible, :digest, :digest_content, :digest_image, :digest_category_id, :email_digest_id,
       :digest_image_crop_x, :digest_image_crop_y, :digest_image_crop_w, :digest_image_crop_h, :enable_content_headers,
       { included_email_list_ids: [] },
       { excluded_email_list_ids: [] },
       { email_attachments_attributes: %i[id attachment _destroy] },
       { email_list_vars_attributes: %i[value email_list_var_name_id] }]
    end

    class Scope < Scope
      def resolve
        scope.authorized(user)
      end
    end
  end
end
