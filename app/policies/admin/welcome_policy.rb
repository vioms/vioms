# frozen_string_literal: true

module Admin
  class WelcomePolicy < BasePolicy
    def index?
      true
    end

    def toggle_become_admin?
      user.is_admin?
    end
  end
end
