# frozen_string_literal: true

module Admin
  class SurveyQuestionPolicy < BasePolicy
    def permitted_attributes
      %i[question question_type range_from range_to position]
    end
  end
end
