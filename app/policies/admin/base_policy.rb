# frozen_string_literal: true

module Admin
  class BasePolicy < ApplicationPolicy
    def initialize(user, record)
      @user = user
      @record = record
    end

    def policy(record)
      super([:admin, record])
    end

    # Allow all actions for admins and nothing for users by default
    def manage?
      user&.effective_admin?
    end

    private

    def admin?
      user.effective_admin?
    end

    def owner?
      record.user_id == user.id
    end

    class Scope < Scope
      def initialize(user, scope)
        @user = user
        @scope = scope
      end
    end
  end
end
