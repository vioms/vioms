# frozen_string_literal: true

module Admin
  class EmailListPolicy < BasePolicy
    def index?
      true
    end

    def show?
      update?
    end

    def update?
      super || allowed_email_list_id?
    end

    def permitted_attributes
      attrs = %i[name internal_name from_name description from_bounce from_email visible never_visible expiration_days
                 notify_email notify_subscribed notify_unsubscribed content_header content_footer postmaster_msgtype
                 digest_template digest_subject_template skip_unsubscription_links template default_editor_type
                 default_stripo_template_group_id default_stripo_template_id reply_to]
      if policy(EmailListCustomVar).index?
        attrs.push(:use_custom_vars, :import_link, :import_login, :import_password, :import_use)
      end
      attrs.push({ user_ids: [], user_exclusion_ability_ids: [] }, :group_priority) if user.effective_admin?
      attrs
    end

    private

    def allowed_email_list_id?
      user.allowed_email_list_id?(record.id)
    end

    class Scope < Scope
      def resolve
        scope.authorized(user)
      end
    end
  end
end
