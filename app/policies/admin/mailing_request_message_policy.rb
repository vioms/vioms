# frozen_string_literal: true

module Admin
  class MailingRequestMessagePolicy < BasePolicy
    def permitted_attributes
      %i[new_status message]
    end
  end
end
