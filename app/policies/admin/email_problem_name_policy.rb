# frozen_string_literal: true

module Admin
  class EmailProblemNamePolicy < BasePolicy
    def permitted_attributes
      %i[name problem_type]
    end
  end
end
