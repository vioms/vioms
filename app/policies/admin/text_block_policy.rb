# frozen_string_literal: true

module Admin
  class TextBlockPolicy < BasePolicy
    def permitted_attributes
      %i[content]
    end
  end
end
