# frozen_string_literal: true

module Admin
  class SurveyAnswerPolicy < BasePolicy
    def permitted_attributes
      %i[answer position]
    end
  end
end
