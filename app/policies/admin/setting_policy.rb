# frozen_string_literal: true

module Admin
  class SettingPolicy < BasePolicy
    def show?
      true
    end

    def permitted_attributes
      %i[value]
    end
  end
end
