# frozen_string_literal: true

module Admin
  class APIKeyPolicy < BasePolicy
    def index?
      true
    end

    def manage?
      super || record.user == user
    end

    def permitted_attributes
      %i[name]
    end

    class Scope < Scope
      def resolve
        APIKey.visible(user)
      end
    end
  end
end
