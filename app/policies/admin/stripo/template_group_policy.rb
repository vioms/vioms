# frozen_string_literal: true

module Admin
  module Stripo
    class TemplateGroupPolicy < BasePolicy
      def index?
        true
      end

      def show?
        admin? || root_owner? || root.public || allowed_by_email_list? || allowed_by_user?
      end

      def create?
        admin? || record.parent.nil? || root_owner?
      end

      def manage?
        admin? || root_owner?
      end

      def permitted_attributes
        attrs = []
        attrs.push(:name, :parent_id, { email_list_ids: [], user_ids: [] }) if manage? || create?
        attrs.push(:public, :user_id) if admin?
        attrs
      end

      private

      def allowed_by_email_list?
        root.email_list_ids.intersect?(user.email_list_ids)
      end

      def allowed_by_user?
        root.user_ids.include?(user.id)
      end

      def root_owner?
        root.user_id == user.id
      end

      def root
        @root ||= record.root
      end

      class Scope < Scope
        def resolve
          scope.authorized(user)
        end
      end
    end
  end
end
