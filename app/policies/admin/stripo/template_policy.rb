# frozen_string_literal: true

module Admin
  module Stripo
    class TemplatePolicy < BasePolicy
      def index?
        true
      end

      def show?
        admin? || root_group.public? || allowed_by_email_list?
      end

      # If template_group is nil, the validation will not allow to create a template.
      def manage?
        admin? || record.template_group.nil? || root_group_owner? || (allowed_by_user? && allowed_by_email_list?)
      end

      def permitted_attributes
        %i[name stripo_template_group_id html css preview mailing_id]
      end

      private

      def root_group_owner?
        root_group.user_id == user.id
      end

      def allowed_by_email_list?
        root_group.email_list_ids.intersect?(user.email_list_ids)
      end

      def allowed_by_user?
        root_group.user_ids.include?(user.id)
      end

      def root_group
        @root_group ||= record.template_group.root
      end
    end
  end
end
