# frozen_string_literal: true

module Admin
  class EmailDigestPolicy < BasePolicy
    def index?
      true
    end

    def new?
      true
    end

    def manage?
      super || user.allowed_email_list_id?(record.email_list_id)
    end

    class Scope < Scope
      def resolve
        scope.authorized(user)
      end
    end
  end
end
