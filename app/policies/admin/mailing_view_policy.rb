# frozen_string_literal: true

module Admin
  class MailingViewPolicy < BasePolicy
    class Scope < Scope
      def resolve
        scope.authorized(user)
      end
    end
  end
end
