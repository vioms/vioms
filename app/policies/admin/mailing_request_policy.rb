# frozen_string_literal: true

module Admin
  class MailingRequestPolicy < BasePolicy
    def permitted_attributes
      [
        :status, :subject, :body, :deliver_at, :mailing_type, :donation_method_id, :donated_at, :comments,
        { images_attributes: %i[id image _destroy] }, { email_list_ids: [] }
      ]
    end
  end
end
