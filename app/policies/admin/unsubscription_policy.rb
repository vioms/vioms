# frozen_string_literal: true

module Admin
  class UnsubscriptionPolicy < BasePolicy
    def index?
      true
    end

    def show?
      super || record.email_list_id.in?(user.email_list_ids)
    end

    class Scope < Scope
      def resolve
        scope.authorized(user)
      end
    end
  end
end
