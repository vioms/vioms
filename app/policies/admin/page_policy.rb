# frozen_string_literal: true

module Admin
  class PagePolicy < BasePolicy
    def permitted_attributes
      %i[name content position show_in_navbar]
    end
  end
end
