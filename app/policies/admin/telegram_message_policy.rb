# frozen_string_literal: true

module Admin
  class TelegramMessagePolicy < BasePolicy
    def index?
      true
    end

    def new?
      true
    end

    def recent_mailings?
      true
    end

    def manage?
      super || user.allowed_telegram_group_id?(record.telegram_group_id) || record.telegram_group_id.nil?
    end

    def permitted_attributes
      attrs = %i[message image remove_image telegram_group_id deliver_at]
      attrs.push({ included_telegram_group_ids: [] })
      attrs
    end

    class Scope < Scope
      def resolve
        scope.authorized(user)
      end
    end
  end
end
