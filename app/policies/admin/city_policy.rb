# frozen_string_literal: true

module Admin
  class CityPolicy < BasePolicy
    def manage?
      super || user.is_cities?
    end

    def permitted_attributes
      %i[name priority parent_id]
    end
  end
end
