# frozen_string_literal: true

module Admin
  class SubscriberImportPolicy < BasePolicy
    def index?
      can_import?
    end

    def new?
      can_import?
    end

    def manage?
      super || (user.is_import? && permitted_lists?)
    end

    def destroy?
      admin?
    end

    def permitted_attributes
      attrs = %i[description subscribers_data name_column first_name_column last_name_column]
      if admin? || user.email_list_ids.any?
        attrs.push(:email_column, { email_list_ids: [] }, :confirm_email, :phone_column)
      end
      attrs << :delete_old if admin? || user.is_import_delete_old?
      attrs
    end

    private

    def can_import?
      admin? || user.is_import?
    end

    def permitted_lists?
      permitted_email_lists? || record.email_list_ids.empty?
    end

    def permitted_email_lists?
      user.email_list_ids.any? && (user.email_list_ids & record.email_list_ids).sort == record.email_list_ids.sort
    end

    class Scope < Scope
      def resolve
        scope.authorized(user)
      end
    end
  end
end
