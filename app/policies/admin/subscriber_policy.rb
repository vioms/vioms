# frozen_string_literal: true

module Admin
  class SubscriberPolicy < BasePolicy
    def index?
      true
    end

    def create?
      true
    end

    def manage?
      super || (record.email_list_ids & user.email_list_ids).size > 0
    end

    def destroy?
      user.effective_admin?
    end

    class Scope < Scope
      def resolve
        scope.authorized(user)
      end
    end
  end
end
