# frozen_string_literal: true

class WhatsappGroupPolicy < ApplicationPolicy
  def index?
    true
  end
end
