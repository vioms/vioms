# frozen_string_literal: true

class EmailListsController < ApplicationController
  caches_page :show, if: proc { |c| @email_list.visible && (c.request.format.rss? || c.request.format.atom?) }
  before_action :authenticate_subscriber_user_from_token!

  def index
    @email_lists = authorize policy_scope(EmailList)
  end

  def show
    begin
      @email_list = authorize EmailList.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      redirect_to root_url, alert: 'Нет списка с таким номером'
      return
    end
    respond_to do |format|
      format.html do
        skip_authorization
        redirect_to email_list_mailings_url(params[:id])
      end
      format.rss { set_mailings }
      format.atom { set_mailings }
    end
  end

  private

  def pundit_not_authorized_message
    if current_subscriber_user
      'У Вас недостаточно прав для просмотра этого списка рассылки'
    else
      'Пожалуйста, авторизуйтесь в личном кабинете для просмотра этого списка рассылки'
    end
  end

  def set_mailings
    @mailings = @email_list.archive_mailings.feed_limit
  end
end
