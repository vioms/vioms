# frozen_string_literal: true

class VkGroupsController < ApplicationController
  def index
    authorize(nil, :index?, policy_class: VkGroupPolicy)
    skip_policy_scope
  end
end
