# frozen_string_literal: true

module CollectionLoaders
  extend ActiveSupport::Concern
  include ProblemTypesHelper::Types

  private

  def set_email_lists
    @email_lists = policy_scope(EmailList).ordered
  end

  def set_email_lists_for_mailing_requests
    @email_lists = EmailList.joins(:mailing_destinations).distinct.ordered.pluck(:name, :id)
  end

  def set_email_lists_with_visible
    @email_lists = policy_scope(EmailList).or(EmailList.visible).ordered
  end

  def load_email_inclusions_and_exclusions
    return unless @mailing

    @email_list_inclusions = @email_lists - [@mailing.email_list]
    @email_list_exclusions =
      (@email_list_inclusions.to_a + current_user.email_list_exclusions).uniq - [@mailing.email_list]
  end

  def set_telegram_groups
    @telegram_groups = policy_scope(TelegramGroup).ordered
  end

  def set_users
    @users = User.ordered
  end

  def set_problem_types
    @problem_types = [[t('problem_type.info'), PROBLEM_INFO],
                      [t('problem_type.temporary'), PROBLEM_TEMPORARY],
                      [t('problem_type.permanent'), PROBLEM_PERMANENT]]
  end

  def set_editor_types
    @editor_types = Mailing.editor_types.keys.map do |editor_type|
      [t("editor_types.#{editor_type}"), editor_type]
    end
  end

  def set_mailing_types
    @mailing_types = MailingRequest.mailing_types.keys.map do |mailing_type|
      [t("mailing_types.#{mailing_type}"), mailing_type]
    end
  end

  def set_mailing_request_statuses
    @mailing_request_statuses = %w[awaiting_subscriber_response awaiting_donation processed].map do |status|
      [I18n.t('mailing_request_statuses.' + status), status]
    end
  end

  # TODO: move to decorators
  def get_email_list_options(email_list_id, truncate_label: false)
    view_context.tag.option('Все email списки', value: 0) +
      view_context.options_from_collection_for_select(
        policy_scope(EmailList).ordered, :id, truncate_label ? :to_label_truncated : :to_label,
        email_list_id.to_i
      )
  end

  def get_telegram_group_options(telegram_group_id, truncate_label: false)
    view_context.tag.option('Все телеграм группы', value: 0) +
      view_context.options_from_collection_for_select(
        policy_scope(TelegramGroup).ordered, :id, truncate_label ? :to_label_truncated : :to_label,
        telegram_group_id.to_i
      )
  end
end
