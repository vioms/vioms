# frozen_string_literal: true

module Stripo
  module TemplateGroupsLoader
    extend ActiveSupport::Concern

    def set_stripo_template_groups_collection
      if current_user.effective_admin?
        @stripo_template_groups = ::SimpleFormTree::ArrangeAsArray.call(::Stripo::TemplateGroup)
        return
      end

      @stripo_template_groups = []
      ::Stripo::TemplateGroup.by_owner(current_user).ordered.each do |template_group|
        group = ::Stripo::TemplateGroup.find(template_group.id)
        @stripo_template_groups.push(group)
        @stripo_template_groups.concat(group.children.ordered)
      end
    end
  end
end
