module SendingCancellation
  extend ActiveSupport::Concern

  SENDING_CANCEL_PERIOD = 1.minute

  def cancel_sending_job(record:, redirect_link:)
    if record.deliver_at - SENDING_CANCEL_PERIOD > Time.zone.now || record.sent || record.send_failed || record.sending
      record.update_attribute(:published, false)
      redirect_to(redirect_link, notice: 'Отправка отменена')
      true
    else
      redirect_to(redirect_link, alert: 'Отправка уже началась, невозможно отменить')
      false
    end
  end
end
