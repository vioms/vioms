# frozen_string_literal: true

module SessionValues
  extend ActiveSupport::Concern

  def get_session_value(name)
    session[name] = params[name].nil? ? session[name] : params[name].to_i
  end
end
