# frozen_string_literal: true

class BannerClicksController < ApplicationController
  def new
    authorize BannerClick
    banner = authorize(Banner.find(params[:id]), :show?)
    banner.clicks.create!
    redirect_to banner.link, allow_other_host: true
  end
end
