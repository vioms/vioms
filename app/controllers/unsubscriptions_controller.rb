# frozen_string_literal: true

class UnsubscriptionsController < ApplicationController
  before_action :authenticate_subscriber_user_from_token!
  before_action :authenticate_subscriber_user!
  before_action :authorize_email_list

  def new
    @unsubscription = authorize Unsubscription.new(email_list_id: params[:email_list_id])
    return unless params[:quick] == '1'

    unsubscribe(@unsubscription.email_list_id)
  end

  def create
    @unsubscription = authorize Unsubscription.new(permitted_params)
    @unsubscription.subscriber_id = current_subscriber_user.id
    if @unsubscription.save
      unsubscribe(@unsubscription.email_list_id)
    else
      render :new
    end
  end

  private

  def authorize_email_list
    email_list_id = (permitted_params == {} ? params[:email_list_id] : permitted_params[:email_list_id]).to_i
    unless current_subscriber_user.email_subscriptions.map(&:email_list_id).include?(email_list_id)
      redirect_to root_url, alert: t('flash.unsubscription.not_subscribed') unless params[:quick] == '1'
      return false
    end
    true
  end

  def unsubscribe(email_list_id)
    EmailSubscription.where(email_list_id:, subscriber_id: current_subscriber_user.id).delete_all
    if params[:quick] == '1'
      render plain: 'OK'
    else
      redirect_to root_url, notice: t('flash.unsubscription.create')
    end
  end
end
