# frozen_string_literal: true

module API
  module Mobile
    class MobileBaseController < ActionController::API
      PER_PAGE = 15

      include Pundit::Authorization

      before_action :authenticate

      def pundit_user
        { user: nil, subscriber: @current_subscriber_user }
      end

      private

      def authenticate
        token = bearer_token
        return unless token

        payload = ::Mobile::Encoder.decode(token)
        @current_mobile_token = MobileToken.find(payload['id'])
        raise JWT::ExpiredSignature if payload['serial'] < @current_mobile_token.serial - 1

        @current_subscriber_user = @current_mobile_token.subscriber.becomes(SubscriberUser)
        headers['X-Next-Token'] = ::Mobile::Token.next!(@current_mobile_token, payload['serial'])
      rescue JWT::DecodeError, ActiveRecord::RecordNotFound
        head :unauthorized
      end

      def bearer_token
        request.headers['Authorization']&.split('Bearer ')&.last
      end
    end
  end
end
