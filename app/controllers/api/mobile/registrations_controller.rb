# frozen_string_literal: true

module API
  module Mobile
    class RegistrationsController < MobileBaseController
      def show
        return head :unauthorized unless @current_subscriber_user

        render json: {
          subscriber: @current_subscriber_user.as_json(only: %i[name email phone]),
          email_lists: ::Subscriber::Subscriptions.call(:email, @current_subscriber_user),
          time_zone_id: @current_subscriber_user.time_zone_id || TimeZone.first.id
        }
      end

      def create
        @subscriber_user = authorize SubscriberUser.new(sanitize_params(permitted_attributes(SubscriberUser.new)))
        Subscriber::ForgetHistory.call(@subscriber_user) if @subscriber_user.valid?
        if @subscriber_user.save
          token = ::Mobile::Token.create!(subscriber: @subscriber_user, device_token: params[:device_token],
                                          device_type: params[:device_type])
          render json: { token: }
        else
          render json: { error: ::Mobile::FormatErrors.call(@subscriber_user) }, status: :unprocessable_entity
        end
      end

      def update
        return head :unauthorized unless @current_subscriber_user

        if @current_subscriber_user.update(sanitize_params(permitted_attributes(@current_subscriber_user)))
          head :ok
        else
          render json: { error: ::Mobile::FormatErrors.call(@current_subscriber_user) }, status: :unprocessable_entity
        end
      end

      def destroy
        return head :unauthorized unless @current_subscriber_user

        @current_mobile_token.destroy
        @current_subscriber_user.destroy
        head :ok
      end

      private

      def sanitize_params(pars)
        ::Subscriber::ParamsSanitizer::ForSubscriber.call(pars)
      end
    end
  end
end
