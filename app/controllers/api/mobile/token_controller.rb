# frozen_string_literal: true

module API
  module Mobile
    class TokenController < MobileBaseController
      def create
        subscriber = SubscriberUser.find_by(email: params[:email])
        if subscriber
          if subscriber.valid_password?(params[:password])
            token = ::Mobile::Token.create!(subscriber:, device_token: params[:device_token],
                                            device_type: params[:device_type])
            render json: { token: }
          else
            render json: { error: 'Неверный пароль' }, status: :unauthorized
          end
        else
          render json: { error: 'Неверный email' }, status: :unauthorized
        end
      end

      def validate
        if @current_mobile_token
          head :ok
        else
          head :unauthorized
        end
      end

      def destroy
        if @current_mobile_token
          @current_mobile_token.destroy
          head :ok
        else
          head :unauthorized
        end
      end
    end
  end
end
