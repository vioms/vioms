# frozen_string_literal: true

module API
  module Mobile
    class TimeZonesController < MobileBaseController
      def index
        time_zones = authorize policy_scope(TimeZone)
        render json: time_zones.select(:id, :name)
      end
    end
  end
end
