# frozen_string_literal: true

module API
  module Mobile
    class MailingsController < MobileBaseController
      def index
        begin
          email_list = authorize EmailList.find(params[:email_list_id])
        rescue ActiveRecord::RecordNotFound
          render json: { error: 'Нет списка с таким номером' }, status: :not_found
          return
        end
        mailings = email_list.archive_mailings

        render json: mailings.map { |mailing| mailing.as_json(only: %i[id subject]) }
      end

      def show
        mailing = authorize Mailing.find(params[:id])
        mailing.rendered_html.sub!('[FOOTER]', '')
        render json: mailing.as_json(only: %i[id subject rendered_html])
      end
    end
  end
end
