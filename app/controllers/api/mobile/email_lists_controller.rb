# frozen_string_literal: true

module API
  module Mobile
    class EmailListsController < MobileBaseController
      def index
        email_lists = authorize policy_scope(EmailList)
        render json: email_lists.select(:id, :name)
      end
    end
  end
end
