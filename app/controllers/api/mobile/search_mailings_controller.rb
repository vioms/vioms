# frozen_string_literal: true

module API
  module Mobile
    class SearchMailingsController < MobileBaseController
      def index
        mailings = Mailing::Search.call(policy_scope(Mailing), params[:email_list], params[:search])
        mailings = authorize(mailings, policy_class: SearchMailingPolicy).page(params[:page]).per(PER_PAGE)

        render json: {
          mailings: mailings.map do |mailing|
            {
              id: mailing.id,
              subject: mailing.subject,
              email_list_id: mailing.email_list_id,
              email_list: mailing.email_list.name
            }
          end,
          last_page: mailings.count == 0 || mailings.last_page?
        }
      end
    end
  end
end
