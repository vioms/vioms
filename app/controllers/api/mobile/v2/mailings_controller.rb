# frozen_string_literal: true

module API
  module Mobile
    module V2
      class MailingsController < MobileBaseController
        def index
          begin
            email_list = authorize EmailList.find(params[:email_list_id])
          rescue ActiveRecord::RecordNotFound
            render json: { error: 'Нет списка с таким номером' }, status: :not_found
            return
          end
          mailings = email_list.archive_mailings.page(params[:page]).per(PER_PAGE)

          render json: {
            mailings: mailings.map { MailingPresenter.new(_1) },
            last_page: mailings.count == 0 || mailings.last_page?
          }
        end
      end
    end
  end
end
