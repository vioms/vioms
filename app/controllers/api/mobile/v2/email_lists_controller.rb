# frozen_string_literal: true

module API
  module Mobile
    module V2
      class EmailListsController < MobileBaseController
        def index
          if @current_subscriber_user
            render json: {
              subscribed: @current_subscriber_user.subscribed_email_lists.as_json(only: %i[id name]),
              unsubscribed: @current_subscriber_user.unsubscribed_email_lists.as_json(only: %i[id name])
            }
          else
            render json: {
              subscribed: [],
              unsubscribed: EmailList.visible.without_never_visible.as_json(only: %i[id name])
            }
          end
        end
      end
    end
  end
end
