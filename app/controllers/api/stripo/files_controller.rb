# frozen_string_literal: true

module API
  module Stripo
    class FilesController < APIBaseController
      include ActionController::HttpAuthentication::Basic::ControllerMethods

      before_action :http_authenticate

      rescue_from(ActiveRecord::RecordInvalid) { head(:bad_request) }
      rescue_from(ActiveRecord::RecordNotFound) { head(:not_found) }

      def list
        render json: ::Stripo::FileOperations.list
      end

      def info
        render json: ::Stripo::FileOperations.info(**permitted_params(:src))
      end

      def upload
        render json: ::Stripo::FileOperations.upload(**permitted_params(:key, :file))
      end

      def delete
        render json: ::Stripo::FileOperations.delete(**permitted_params(:url, :key))
      end

      private

      def http_authenticate
        authenticate_or_request_with_http_basic do |username, password|
          ActiveSupport::SecurityUtils.secure_compare(username, Setting.get_setting('stripo_username')) &
            ActiveSupport::SecurityUtils.secure_compare(password, Setting.get_setting('stripo_password'))
        end
      end

      def permitted_params(*)
        params.permit(*).to_h.symbolize_keys
      end
    end
  end
end
