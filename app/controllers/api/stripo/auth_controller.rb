# frozen_string_literal: true

module API
  module Stripo
    class AuthController < APIBaseController
      before_action :authenticate_user!

      def token
        render json: { token: ::Stripo::RequestToken.call }
      end
    end
  end
end
