# frozen_string_literal: true

class SurveySubscriberAnswersController < ApplicationController
  before_action :authenticate_subscriber_user_from_token!
  before_action :set_survey

  def edit; end

  def update
    permitted_params.each do |question_id, answer|
      question = SurveyQuestion.find(question_id)
      user_answer = SurveySubscriberAnswer.find_or_initialize_by(subscriber_id: current_subscriber_user.id,
                                                                 survey_question_id: question_id)
      case question.question_type.to_sym
      when :string, :text, :vote
        user_answer.value = answer
      when :options
        user_answer.survey_answer_id = answer
      end
      user_answer.save!
    end

    redirect_to root_url, notice: t('flash.survey_subscriber_answer.thank_you')
  end

  private

  def set_survey
    @survey = authorize Survey.find(params[:id])
  end

  def permitted_params
    question_ids = @survey.survey_questions.pluck(:id).map(&:to_s)
    params.require(:form).permit(question_ids)
  end
end
