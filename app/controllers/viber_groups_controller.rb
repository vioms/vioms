# frozen_string_literal: true

class ViberGroupsController < ApplicationController
  def index
    authorize(nil, :index?, policy_class: ViberGroupPolicy)
    skip_policy_scope
  end
end
