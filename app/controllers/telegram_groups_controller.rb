# frozen_string_literal: true

class TelegramGroupsController < ApplicationController
  def index
    @telegram_groups = authorize policy_scope(TelegramGroup).ordered
  end
end
