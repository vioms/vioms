# frozen_string_literal: true

class FacebookGroupsController < ApplicationController
  def index
    authorize(nil, :index?, policy_class: FacebookGroupPolicy)
    skip_policy_scope
  end
end
