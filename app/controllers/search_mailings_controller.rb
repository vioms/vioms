# frozen_string_literal: true

class SearchMailingsController < ApplicationController
  include CollectionLoaders

  def index
    @email_list_options = get_email_list_options(params[:email_list])
    @mailings = Mailing::Search.call(policy_scope(Mailing), params[:email_list], params[:search])
    @mailings = authorize(@mailings, policy_class: SearchMailingPolicy).page(params[:page])
  end
end
