# frozen_string_literal: true

class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  include Pundit::Authorization
  after_action :verify_authorized, if: :verify_pundit_authorized?
  after_action :verify_policy_scoped, only: :index, if: :verify_pundit_scoped?
  before_action :set_active_survey
  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  def pundit_user
    { user: current_user, subscriber: current_subscriber_user }
  end

  def memoize_permitted_params
    @permitted_params ||= PermittedParams.new(params, self, permitted_model_name)
  end

  def permitted_params
    memoize_permitted_params
    @permitted_params.permitted_params
  end

  def _permitted_attributes(model_name)
    memoize_permitted_params
    @permitted_params.permitted_attributes(model_name)
  end
  helper_method :_permitted_attributes

  def confirmation_controller?
    instance_of?(EmailConfirmationsController)
  end
  helper_method :confirmation_controller?

  private

  def verify_pundit_authorized?
    # TODO: we should not skip ckeditor controllers
    # https://github.com/galetahub/ckeditor/issues/609
    # https://github.com/galetahub/ckeditor/pull/809
    !devise_controller? && !is_a?(Admin::WikiPagesController) && !ckeditor_controller?
  end

  def verify_pundit_scoped?
    !devise_controller? && !is_a?(Admin::WikiPagesController) && !ckeditor_controller?
  end

  def ckeditor_controller?
    self.class.to_s.starts_with?('Ckeditor::')
  end

  def user_not_authorized(exception)
    policy_name = exception.policy.class.to_s.underscore
    flash[:error] = pundit_not_authorized_message ||
                    t("#{policy_name}.#{exception.query}", scope: pundit_i18n_scope, default: :default)
    redirect_to request.referer || pundit_unauthorized_redirect_url
  end

  def pundit_i18n_scope
    'pundit'
  end

  def pundit_unauthorized_redirect_url
    root_url
  end

  def pundit_not_authorized_message
    nil
  end

  def permitted_model_name
    params[:controller].split('/').last.singularize.to_sym
  end

  def namespace
    controller_name_segments = params[:controller].split('/')
    controller_name_segments.pop
    controller_name_segments.join('/').camelize
  end

  def authenticate_subscriber_user_from_token!
    return if subscriber_user_signed_in? || params[:email].blank?

    authenticator = Subscriber::AuthenticateFromToken.new(params[:email], params[:auth_token])
    if authenticator.call
      sign_in(authenticator.subscriber.becomes(SubscriberUser))
    else
      flash[:alert] = t('devise.failure.invalid_token') if is_flashing_format?
      redirect_to root_url
    end
  end

  def set_active_survey
    return if !current_subscriber_user || instance_of?(SurveySubscriberAnswersController) ||
              SurveySubscriberAnswer.exists?(subscriber: current_subscriber_user)

    @active_survey = Survey.visible.announce.first
  end
end
