# frozen_string_literal: true

class MailingsController < ApplicationController
  before_action :set_mailing, except: :index
  before_action :authenticate_subscriber_user_from_token!

  def index
    begin
      @email_list = authorize policy_scope(EmailList).find(params[:email_list_id])
    rescue ActiveRecord::RecordNotFound
      redirect_to root_url, alert: 'Нет списка с таким номером'
      return
    end
    @mailings = @email_list.archive_mailings.page(params[:page]).per(10)
  end

  def show
    @email_attachments = authorize @mailing.email_attachments, :show?
  end

  def full
    response.headers.except!('X-Frame-Options')
    src = Rails.env.production? ? 'https://app.vioms.ru' : 'http://lvh.me:19006'
    response.headers['Content-Security-Policy'] = 'frame-src ' + src
    render html: @mailing.decorate.web_view, layout: false
  end

  def track_email
    Mailing::Track::FromEmailAndToken.call(mailing: @mailing, email: params[:email], auth_token: params[:auth_token])
    send_file Mailing::Track.spacer_path, type: 'image/gif', disposition: 'inline'
  end

  private

  def set_mailing
    @mailing = authorize Mailing.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    redirect_to email_lists_url, alert: 'Нет рассылки с таким номером'
  end

  def pundit_not_authorized_message
    if current_subscriber_user
      'У Вас недостаточно прав для чтения этой рассылки'
    else
      'Пожалуйста, авторизуйтесь в личном кабинете для чтения рассылки'
    end
  end

  def pundit_unauthorized_redirect_url
    email_lists_url
  end
end
