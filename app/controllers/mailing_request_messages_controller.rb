# frozen_string_literal: true

class MailingRequestMessagesController < ApplicationController
  include CollectionLoaders

  before_action :set_mailing_request
  before_action :set_mailing_types, only: %i[create]

  def index
    @mailing_request_messages = authorize policy_scope(@mailing_request.messages).ordered
    @mailing_request_message = authorize @mailing_request.messages.build
    @mailing_request_message.donation_method_id = @mailing_request.donation_method_id
    @mailing_request_message.donated_at = @mailing_request.donated_at
  end

  def create
    @mailing_request_message =
      authorize @mailing_request.messages.build(permitted_attributes(MailingRequestMessage.new))

    MailingRequest::UpdateAttrsFromSubscriber.call(@mailing_request, @mailing_request_message)
    if @mailing_request_message.valid?
      MailingRequest::MaybeUpdateDonation.call(@mailing_request, @mailing_request_message)
      MailingRequestMessage.transaction do
        @mailing_request_message.save!
        @mailing_request.save!
      end
      MailingRequest::NotifyNewMailingMessage.call(@mailing_request, @mailing_request_message)
      redirect_to mailing_request_mailing_request_messages_url(@mailing_request),
                  notice: t('flash.mailing_request_message.create')
    else
      # raise @mailing_request_message.errors.inspect
      render :new
    end
  end

  private

  def set_mailing_request
    @mailing_request = authorize MailingRequest.find(params[:mailing_request_id]), :show?
  end
end
