# frozen_string_literal: true

module Admin
  class SettingsController < BaseController
    before_action :set_setting, only: %i[edit update]

    def index
      @settings = authorize policy_scope(Setting)
    end

    def show
      @setting = authorize Setting.find_by(name: params[:id])
    end

    def edit; end

    def update
      if ::Setting::Update.call(@setting, permitted_attributes(Setting))
        redirect_to admin_settings_url, notice: t('flash.setting.update')
      else
        render :edit
      end
    end

    private

    def set_setting
      @setting = authorize Setting.find(params[:id])
    end
  end
end
