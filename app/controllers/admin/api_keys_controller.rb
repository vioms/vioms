# frozen_string_literal: true

module Admin
  class APIKeysController < BaseController
    before_action :set_api_key, only: %i[edit update destroy]

    def index
      @api_keys = authorize policy_scope(APIKey).includes(:user)
    end

    def new
      @api_key = authorize APIKey.new
    end

    def edit; end

    def create
      @api_key = authorize APIKey.new(permitted_attributes(APIKey.new))
      @api_key.key = SecureRandom.uuid
      @api_key.user = current_user
      if @api_key.save
        render :create
      else
        render :new
      end
    end

    def update
      if @api_key.update(permitted_attributes(@api_key))
        redirect_to admin_api_keys_url, notice: t('flash.api_key.update')
      else
        render :edit
      end
    end

    def destroy
      @api_key.destroy
      redirect_to admin_api_keys_url, notice: t('flash.api_key.destroy')
    end

    private

    def set_api_key
      @api_key = authorize APIKey.find(params[:id])
    end
  end
end
