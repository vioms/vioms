# frozen_string_literal: true

module Admin
  class BlacklistEmailsController < BaseController
    before_action :set_blacklist_email, only: %i[edit update destroy]

    def index
      @blacklist_emails = authorize policy_scope(BlacklistEmail)
    end

    def new
      @blacklist_email = authorize BlacklistEmail.new
    end

    def edit; end

    def create
      @blacklist_email = authorize BlacklistEmail.new(permitted_params)
      if @blacklist_email.save
        redirect_to admin_blacklist_emails_path, notice: t('flash.blacklist_email.create')
      else
        render :new
      end
    end

    def update
      if @blacklist_email.update(permitted_params)
        redirect_to admin_blacklist_emails_path, notice: t('flash.blacklist_email.update')
      else
        render :edit
      end
    end

    def destroy
      @blacklist_email.destroy
      redirect_to admin_blacklist_emails_path, notice: t('flash.blacklist_email.destroy')
    end

    private

    def set_blacklist_email
      @blacklist_email = authorize BlacklistEmail.find(params[:id])
    end
  end
end
