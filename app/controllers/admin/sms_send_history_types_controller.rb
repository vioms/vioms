# frozen_string_literal: true

module Admin
  class SmsSendHistoryTypesController < BaseController
    before_action :set_sms_send_history_type, only: %i[edit update]

    def index
      @sms_send_history_types = authorize policy_scope(SmsSendHistoryType)
    end

    def edit; end

    def update
      if @sms_send_history_type.update(permitted_params)
        redirect_to admin_sms_send_history_types_url, notice: t('flash.sms_send_history_type.update')
      else
        render :edit
      end
    end

    private

    def set_sms_send_history_type
      @sms_send_history_type = authorize SmsSendHistoryType.find(params[:id])
    end
  end
end
