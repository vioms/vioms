# frozen_string_literal: true

module Admin
  class SmtpSettingsController < BaseController
    before_action :smtp_setting_group
    before_action :smtp_setting, only: %i[edit update destroy]

    def index
      @smtp_settings = authorize policy_scope(@smtp_setting_group.smtp_settings)
    end

    def new
      @smtp_setting = authorize @smtp_setting_group.smtp_settings.build
    end

    def edit; end

    def create
      @smtp_setting = authorize @smtp_setting_group.smtp_settings.build(permitted_params)
      if @smtp_setting.save
        redirect_to admin_smtp_setting_group_smtp_settings_url, notice: t('flash.smtp_setting.create')
      else
        render :new
      end
    end

    def update
      if @smtp_setting.update(permitted_params)
        redirect_to admin_smtp_setting_group_smtp_settings_url, notice: t('flash.smtp_setting.update')
      else
        render :edit
      end
    end

    def destroy
      @smtp_setting.destroy
      redirect_to admin_smtp_setting_group_smtp_settings_url, notice: t('flash.smtp_setting.destroy')
    end

    private

    def smtp_setting_group
      @smtp_setting_group = authorize SmtpSettingGroup.find(params[:smtp_setting_group_id])
    end

    def smtp_setting
      @smtp_setting = authorize @smtp_setting_group.smtp_settings.find(params[:id])
    end
  end
end
