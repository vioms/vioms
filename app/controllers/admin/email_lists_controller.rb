# frozen_string_literal: true

module Admin
  class EmailListsController < BaseController
    include CollectionLoaders

    before_action :set_email_list, only: %i[edit update destroy export]
    before_action :set_editor_types, only: %i[new create edit update]
    before_action :set_users, only: %i[new create edit update]
    before_action :set_stripo_templates, only: %i[new create edit update]

    def index
      @email_lists = authorize policy_scope(EmailList)
      respond_to do |format|
        format.html do
          @email_lists = @email_lists
                         .select(%i[id name internal_name from_name visible subscribers_count_cache])
                         .page(params[:page])
          @can_destroy = policy(EmailList).destroy?
        end
        format.json do
          render json: @email_lists.select(%i[id name])
        end
      end
    end

    def show
      email_list = authorize(
        EmailList
          .select(%i[id default_editor_type default_stripo_template_group_id default_stripo_template_id])
          .find(params[:id])
      )
      template_groups = ::Stripo::TemplateGroup::Usable.new(email_list:, user: current_user).call
      render json: { email_list:, template_groups: }
    end

    def new
      @email_list = authorize EmailList.new
    end

    def edit; end

    def create
      @email_list = authorize EmailList.new(permitted_attributes(EmailList.new))
      if @email_list.save
        redirect_to admin_email_lists_url, notice: t('flash.email_list.create')
      else
        render :new
      end
    end

    def update
      if @email_list.update(permitted_attributes(@email_list))
        redirect_to admin_email_lists_url, notice: t('flash.email_list.update')
      else
        render :edit
      end
    end

    def destroy
      @email_list.destroy
      redirect_to admin_email_lists_url, notice: t('flash.email_list.destroy')
    end

    def export
      subscribers = @email_list.subscribers.select(%i[name email])
      filename = Russian.translit(@email_list.name).to_url + '.xlsx'
      send_data ExcelExporter.call(subscribers.to_a), filename:, type: ExcelExporter.mime_type
    end

    private

    def set_email_list
      @email_list = authorize EmailList.find(params[:id])
    end

    def set_stripo_templates
      @stripo_templates =
        if @email_list&.default_stripo_template
          @email_list.default_stripo_template.template_group.templates.ordered
        else
          []
        end
    end
  end
end
