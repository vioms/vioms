# frozen_string_literal: true

module Admin
  class ExternalRedirectsController < BaseController
    before_action :set_external_redirect, only: %i[edit update destroy]

    def index
      @external_redirects = authorize policy_scope(ExternalRedirect).order(:url)
    end

    def new
      @external_redirect = authorize ExternalRedirect.new
    end

    def edit; end

    def create
      @external_redirect = authorize ExternalRedirect.new(permitted_params)
      if @external_redirect.save
        redirect_to admin_external_redirects_url, notice: t('flash.external_redirect.create')
      else
        render :new
      end
    end

    def update
      if @external_redirect.update(permitted_params)
        redirect_to admin_external_redirects_url, notice: t('flash.external_redirect.update')
      else
        render :edit
      end
    end

    def destroy
      @external_redirect.destroy
      redirect_to admin_external_redirects_url, notice: t('flash.external_redirect.destroy')
    end

    private

    def set_external_redirect
      @external_redirect = authorize ExternalRedirect.find(params[:id])
    end
  end
end
