# frozen_string_literal: true

module Admin
  class EmailListCustomVarsController < BaseController
    before_action :set_email_list
    before_action :set_email_list_custom_var, only: %i[edit update destroy]

    def index
      @email_list_custom_vars = authorize policy_scope(EmailListCustomVar).where(email_list_id: params[:email_list_id])
    end

    def new
      @email_list_custom_var = authorize @email_list.email_list_custom_vars.build
    end

    def edit; end

    def create
      @email_list_custom_var = authorize @email_list.email_list_custom_vars.build(permitted_params)
      if @email_list_custom_var.save
        redirect_to admin_email_list_email_list_custom_vars_url(@email_list),
                    notice: t('flash.email_list_custom_var.create')
      else
        render :new
      end
    end

    def update
      if @email_list_custom_var.update permitted_params
        redirect_to admin_email_list_email_list_custom_vars_url(@email_list),
                    notice: t('flash.email_list_custom_var.update')
      else
        render :edit
      end
    end

    def destroy
      @email_list_custom_var.destroy
      redirect_to admin_email_list_email_list_custom_vars_url(@email_list),
                  notice: t('flash.email_list_custom_var.destroy')
    end

    private

    def set_email_list
      @email_list = authorize EmailList.find(params[:email_list_id])
    end

    def set_email_list_custom_var
      @email_list_custom_var = authorize @email_list.email_list_custom_vars.find(params[:id])
    end
  end
end
