# frozen_string_literal: true

module Admin
  class UsersController < BaseController
    include CollectionLoaders

    before_action :set_user, only: %i[show edit update destroy]
    before_action :set_email_lists, only: %i[new create edit update]
    before_action :set_telegram_groups, only: %i[new create edit update]

    def index
      @users = authorize(
        policy_scope(User).select(%i[id name username email sms_phone current_sign_in_at roles_mask]).ordered
      )
    end

    def show; end

    def new
      @user = authorize User.new
    end

    def edit; end

    def create
      @user = authorize User.new(permitted_params)
      if @user.save
        redirect_to admin_users_url, notice: t('flash.user.create')
      else
        render :new
      end
    end

    def update
      if @user.update permitted_params
        redirect_to admin_users_url, notice: t('flash.user.update')
      else
        render :edit
      end
    end

    def destroy
      @user.destroy
      redirect_to admin_users_url, notice: t('flash.user.destroy')
    end

    private

    def set_user
      @user = authorize User.find(params[:id])
    end
  end
end
