# frozen_string_literal: true

module Admin
  class DonationMethodsController < BaseController
    before_action :set_donation_method, only: %i[edit update destroy]

    def index
      @donation_methods = authorize policy_scope(DonationMethod)
    end

    def new
      @donation_method = authorize DonationMethod.new
    end

    def edit; end

    def create
      @donation_method = authorize DonationMethod.new(permitted_attributes(DonationMethod.new))
      if @donation_method.save
        redirect_to admin_donation_methods_url, notice: t('flash.donation_method.create')
      else
        render :new
      end
    end

    def update
      if @donation_method.update(permitted_attributes(@donation_method))
        redirect_to admin_donation_methods_url, notice: t('flash.donation_method.update')
      else
        render :edit
      end
    end

    def destroy
      @donation_method.destroy
      redirect_to admin_donation_methods_url, notice: t('flash.donation_method.destroy')
    end

    private

    def set_donation_method
      @donation_method = authorize DonationMethod.find(params[:id])
    end
  end
end
