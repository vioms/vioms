# frozen_string_literal: true

module Admin
  class WikiPagesController < Admin::BaseController
    acts_as_wiki_pages_controller
  end
end
