# frozen_string_literal: true

module Admin
  class EmailProblemNamesController < BaseController
    include CollectionLoaders

    before_action :set_email_problem_name, only: %i[edit update destroy]
    before_action :set_problem_types, only: %i[new create edit update]

    def index
      @email_problem_names = authorize policy_scope(EmailProblemName)
    end

    def new
      @email_problem_name = authorize EmailProblemName.new
    end

    def edit; end

    def create
      @email_problem_name = authorize EmailProblemName.new(permitted_attributes(EmailProblemName.new))
      if @email_problem_name.save
        redirect_to admin_email_problem_names_url, notice: t('flash.email_problem_name.create')
      else
        render :new
      end
    end

    def update
      if @email_problem_name.update(permitted_attributes(@email_problem_name))
        redirect_to admin_email_problem_names_url, notice: t('flash.email_problem_name.update')
      else
        render :edit
      end
    end

    def destroy
      @email_problem_name.destroy
      redirect_to admin_email_problem_names_url, notice: t('flash.email_problem_name.destroy')
    end

    def sort
      params[:email_problem_name].each_with_index do |id, index|
        EmailProblemName.find(id).update_column(:position, index + 1)
      end
      render nothing: true
    end

    private

    def set_email_problem_name
      @email_problem_name = authorize EmailProblemName.find(params[:id])
    end
  end
end
