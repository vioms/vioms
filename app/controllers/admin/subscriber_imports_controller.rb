# frozen_string_literal: true

module Admin
  class SubscriberImportsController < BaseController
    before_action :set_subscriber_import, only: %i[edit update destroy]
    before_action :load_collections, only: %i[new create]

    def index
      @subscriber_imports = authorize(
        policy_scope(SubscriberImport)
          .select(%i[id description errors_count created_at])
          .distinct
      )
      @subscriber_imports = @subscriber_imports.page(params[:page])
    end

    def new
      @subscriber_import = authorize SubscriberImport.new
      @subscriber_import.name_column = 1
      @subscriber_import.email_column = 2 if @has_email_lists

      @subscriber_import.email_list_ids = [params[:email_list].to_i] if params[:email_list]
      @subscriber_import.sms_list_ids = [params[:sms_list].to_i] if params[:sms_list]
    end

    def edit; end

    def create
      @subscriber_import = authorize SubscriberImport.new(permitted_attributes(SubscriberImport.new))
      @subscriber_import.user = current_user
      if @subscriber_import.save
        ::Subscriber::Import.new(@subscriber_import).call
        flash_msg = if @subscriber_import.errors_count > 0
                      { alert: t('flash.subscriber_import.create_with_errors') }
                    else
                      { notice: t('flash.subscriber_import.create') }
                    end
        redirect_to admin_subscriber_imports_url, flash_msg
      else
        render :new
      end
    end

    def update
      @subscriber_import.update_attribute(:description, permitted_attributes(@subscriber_import)[:description])
      redirect_to admin_subscriber_imports_url, notice: t('flash.subscriber_import.update')
    end

    def destroy
      @subscriber_import.destroy
      redirect_to admin_subscriber_imports_url, notice: t('flash.subscriber_import.destroy')
    end

    private

    def set_subscriber_import
      @subscriber_import = authorize SubscriberImport.find(params[:id])
    end

    def load_collections
      @email_lists = policy_scope(EmailList).ordered
      @has_email_lists = @email_lists.any?
    end
  end
end
