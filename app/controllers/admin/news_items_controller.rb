# frozen_string_literal: true

module Admin
  class NewsItemsController < BaseController
    before_action :set_news_item, only: %i[edit update destroy]

    def index
      @news_items = authorize policy_scope(NewsItem)
    end

    def new
      @news_item = authorize NewsItem.new
    end

    def edit; end

    def create
      @news_item = authorize NewsItem.new(permitted_params)
      if @news_item.save
        redirect_to admin_news_items_url, notice: t('flash.news_item.create')
      else
        render :new
      end
    end

    def update
      if @news_item.update(permitted_params)
        redirect_to admin_news_items_url, notice: t('flash.news_item.update')
      else
        render :edit
      end
    end

    def destroy
      @news_item.destroy
      redirect_to admin_news_items_url, notice: t('flash.news_item.destroy')
    end

    private

    def set_news_item
      @news_item = authorize NewsItem.find(params[:id])
    end
  end
end
