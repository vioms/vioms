# frozen_string_literal: true

module Admin
  class BannersController < BaseController
    before_action :set_banner_order
    before_action :set_banner, only: %i[show edit update destroy]

    def index
      @banners = authorize policy_scope(@banner_order.banners)
    end

    def show; end

    def new
      @banner = @banner_order.banners.build
    end

    def edit; end

    def create
      @banner = authorize @banner_order.banners.build(permitted_params)
      if @banner.save
        redirect_to admin_banner_order_banners_url(@banner_order), notice: t('flash.banner.create')
      else
        render :new
      end
    end

    def update
      @banner = Banner.find(params[:id])
      if @banner.update(permitted_params)
        redirect_to admin_banner_order_banners_url(@banner_order), notice: t('flash.banner.update')
      else
        render :edit
      end
    end

    def destroy
      @banner.destroy
      redirect_to admin_banner_order_banners_url(@banner_order), notice: t('flash.banner.destroy')
    end

    private

    def set_banner_order
      @banner_order = authorize BannerOrder.find(params[:banner_order_id])
    end

    def set_banner
      @banner = authorize @banner_order.banners.find(params[:id])
    end
  end
end
