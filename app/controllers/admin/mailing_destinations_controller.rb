# frozen_string_literal: true

module Admin
  class MailingDestinationsController < BaseController
    before_action :set_mailing_destination, only: %i[edit update destroy]

    def index
      @mailing_destinations = authorize policy_scope(MailingDestination)
    end

    def new
      @mailing_destination = authorize MailingDestination.new
    end

    def edit; end

    def create
      @mailing_destination = authorize MailingDestination.new(permitted_attributes(MailingDestination.new))
      if @mailing_destination.save
        redirect_to admin_mailing_destinations_url, notice: t('flash.mailing_destination.create')
      else
        render :new
      end
    end

    def update
      if @mailing_destination.update(permitted_attributes(@mailing_destination))
        redirect_to admin_mailing_destinations_url, notice: t('flash.mailing_destination.update')
      else
        render :edit
      end
    end

    def destroy
      @mailing_destination.destroy
      redirect_to admin_mailing_destinations_url, notice: t('flash.mailing_destination.destroy')
    end

    private

    def set_mailing_destination
      @mailing_destination = authorize MailingDestination.find(params[:id])
    end
  end
end
