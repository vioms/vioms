# frozen_string_literal: true

module Admin
  class PollsController < BaseController
    before_action :set_poll, only: %i[edit update destroy]

    def index
      @polls = authorize policy_scope(Poll)
    end

    def new
      @poll = authorize Poll.new
    end

    def edit; end

    def create
      @poll = authorize Poll.new(permitted_params)
      if @poll.save
        redirect_to admin_polls_url, notice: t('flash.poll.create')
      else
        render :new
      end
    end

    def update
      @poll = Poll.find(params[:id])
      if @poll.update(permitted_params)
        redirect_to admin_polls_url, notice: t('flash.poll.update')
      else
        render :edit
      end
    end

    def destroy
      @poll.destroy
      redirect_to admin_polls_url, notice: t('flash.poll.destroy')
    end

    private

    def set_poll
      @poll = authorize Poll.find(params[:id])
    end
  end
end
