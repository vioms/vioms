# frozen_string_literal: true

module Admin
  class ImportEmailProblemsController < BaseController
    def new
      authorize EmailProblem
    end

    def create
      authorize EmailProblem
      importer = ImportProblemsService.new(contact_type: :email, user: current_user)
      importer.import!(input: params[:emails], problem_name_id: params[:problem_name])

      @emails = importer.not_imported_contacts.join("\n")
      fill_flash(@emails.empty?)

      render :new
    end

    private

    def fill_flash(success)
      if success
        flash.now[:notice] = t('flash.import_email_problem.create.success')
      else
        flash.now[:alert] = t('flash.import_email_problem.create.error')
      end
    end
  end
end
