# frozen_string_literal: true

module Admin
  class SubscriberImportPeopleController < BaseController
    before_action :set_subscriber_import
    before_action :set_subscriber_import_person, only: %i[edit update]

    def index
      @subscriber_import_people = authorize(policy_scope(@subscriber_import.subscriber_import_people))
                                  .page(params[:page])
    end

    def edit; end

    def update
      @subscriber_import_person.update(permitted_attributes(@subscriber_import_person))
      if Subscriber::Import::ImportOne.new(@subscriber_import_person).call
        @subscriber_import_person.subscriber_import.decrease_errors_count!
        flash_msg = { notice: t('flash.subscriber_import_person.update') }
      else
        flash_msg = { alert: t('flash.subscriber_import_person.update_failed') }
      end
      redirect_to admin_subscriber_import_subscriber_import_people_url(@subscriber_import), flash_msg
    end

    private

    def set_subscriber_import
      @subscriber_import = authorize SubscriberImport.find(params[:subscriber_import_id]), :manage?
    end

    def set_subscriber_import_person
      @subscriber_import_person = authorize @subscriber_import.subscriber_import_people.find(params[:id])
    end
  end
end
