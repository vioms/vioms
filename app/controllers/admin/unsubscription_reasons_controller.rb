# frozen_string_literal: true

module Admin
  class UnsubscriptionReasonsController < BaseController
    before_action :set_unsubscription_reason, only: %i[edit update destroy]

    def index
      @unsubscription_reasons = authorize policy_scope(UnsubscriptionReason)
    end

    def new
      @unsubscription_reason = authorize UnsubscriptionReason.new
    end

    def edit; end

    def create
      @unsubscription_reason = authorize UnsubscriptionReason.new(permitted_params)
      if @unsubscription_reason.save
        redirect_to admin_unsubscription_reasons_url, notice: t('flash.unsubscription_reason.create')
      else
        render :new
      end
    end

    def update
      @unsubscription_reason = UnsubscriptionReason.find(params[:id])
      if @unsubscription_reason.update(permitted_params)
        redirect_to admin_unsubscription_reasons_url, notice: t('flash.unsubscription_reason.update')
      else
        render :edit
      end
    end

    def destroy
      @unsubscription_reason.destroy
      redirect_to admin_unsubscription_reasons_url, notice: t('flash.unsubscription_reason.destroy')
    end

    def sort
      params[:unsubscription_reason].each_with_index do |id, index|
        UnsubscriptionReason.find(id).update_column(:position, index + 1)
      end
      render nothing: true
    end

    private

    def set_unsubscription_reason
      @unsubscription_reason = authorize UnsubscriptionReason.find(params[:id])
    end
  end
end
