# frozen_string_literal: true

module Admin
  class EmailProblemsController < BaseController
    before_action :set_subscriber
    before_action :set_email_problem, only: %i[edit update]

    def index
      @email_problems = authorize policy_scope(@subscriber.email_problems)
    end

    def new
      @email_problem = authorize @subscriber.email_problems.build
    end

    def edit; end

    def create
      @email_problem = authorize @subscriber.email_problems.build(permitted_attributes(EmailProblem.new))
      if @email_problem.save
        redirect_to admin_subscriber_email_problems_url(@subscriber), notice: t('flash.email_problem.create')
      else
        render :new
      end
    end

    def update
      if @email_problem.update(permitted_attributes(@email_problem))
        @subscriber.reconfirm_if_no_problems
        redirect_to admin_subscriber_email_problems_url(@subscriber), notice: t('flash.email_problem.update')
      else
        render :edit
      end
    end

    private

    def set_subscriber
      @subscriber = authorize Subscriber.find(params[:subscriber_id])
    end

    def set_email_problem
      @email_problem = authorize @subscriber.email_problems.find(params[:id])
    end
  end
end
