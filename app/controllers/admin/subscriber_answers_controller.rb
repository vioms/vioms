# frozen_string_literal: true

module Admin
  class SubscriberAnswersController < BaseController
    def index
      @poll = authorize Poll.find(params[:poll_id])
      @subscriber_answers =
        authorize(policy_scope(@poll.subscriber_answers)
                    .includes(:answer, :subscriber)
                    .order(updated_at: :desc))
        .page(params[:page])
    end
  end
end
