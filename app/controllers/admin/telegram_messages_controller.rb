# frozen_string_literal: true

module Admin
  class TelegramMessagesController < BaseController
    TIME_OFFSET = 5.minutes

    include CollectionLoaders
    include SessionValues

    before_action :set_telegram_message, only: %i[show edit update destroy copy send_message cancel_sending]
    before_action :set_telegram_groups, only: %i[new create edit update copy]

    def index
      telegram_group_id = get_session_value(:telegram_group)
      @telegram_messages = authorize(
        policy_scope(TelegramMessage)
          .search(telegram_group_id:, message: params[:message])
      )
      @telegram_messages = @telegram_messages.page(params[:page]).per(view_context.per_page_remember)
      @telegram_group_options = get_telegram_group_options(telegram_group_id)
    end

    def show; end

    def new
      @telegram_message = authorize TelegramMessage.new
      @telegram_message.deliver_at = TIME_OFFSET.from_now
      return unless params[:telegram_group].to_i > 0

      @telegram_group = authorize TelegramGroup.find(params[:telegram_group]), :show?
      @telegram_message.telegram_group = @telegram_group
    end

    def edit; end

    def create
      @telegram_message = authorize TelegramMessage.new(permitted_attributes(TelegramMessage.new))
      if @telegram_message.save
        redirect_to admin_telegram_messages_url, notice: t('flash.telegram_message.create')
      else
        render :new
      end
    end

    def update
      @telegram_message = TelegramMessage.find(params[:id])
      if @telegram_message.update(permitted_attributes(@telegram_message))
        redirect_to admin_telegram_messages_url, notice: t('flash.telegram_message.update')
      else
        render :edit
      end
    end

    def destroy
      @telegram_message.destroy
      redirect_to admin_telegram_messages_url, notice: t('flash.telegram_message.destroy')
    end

    def copy
      @telegram_message = @telegram_message.dup
      @telegram_message.deliver_at = TIME_OFFSET.from_now
      render :new
    end

    def send_message
      result = Telegram::PublishMessage.call(telegram_message: @telegram_message, user: current_user)
      redirect_from_interactor(result)
    end

    def cancel_sending
      result = Telegram::CancelMessage.call(telegram_message: @telegram_message)
      redirect_from_interactor(result)
    end

    def recent_mailings
      authorize TelegramMessage
      @mailings = policy_scope(Mailing)
      @mailings = @mailings.where(email_list_id: params[:email_list_id]) if params[:email_list_id].present?
      render json: @mailings.limit(5).order(updated_at: :desc).select(:id, :subject)
    end

    private

    def set_telegram_message
      @telegram_message = authorize TelegramMessage.find(params[:id])
    end

    def redirect_from_interactor(result)
      if result.success?
        redirect_to admin_telegram_messages_url, notice: result.message
      else
        redirect_to admin_telegram_messages_url, alert: result.message
      end
    end
  end
end
