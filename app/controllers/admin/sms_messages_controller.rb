# frozen_string_literal: true

module Admin
  class SmsMessagesController < BaseController
    before_action :set_sms_message, only: %i[edit update]

    def index
      @sms_messages = authorize policy_scope(SmsMessage).includes(:sms_send_history_type).order(:position)
    end

    def edit; end

    def update
      if @sms_message.update(permitted_params)
        redirect_to admin_sms_messages_url, notice: t('flash.sms_message.update')
      else
        render :edit
      end
    end

    private

    def set_sms_message
      @sms_message = authorize SmsMessage.find(params[:id])
    end
  end
end
