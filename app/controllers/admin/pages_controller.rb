# frozen_string_literal: true

module Admin
  class PagesController < BaseController
    before_action :set_page, only: %i[edit update destroy]

    def index
      @pages_in_navbar = authorize(policy_scope(Page)).where(show_in_navbar: true).order(:position)
      @pages_not_in_navbar = authorize(policy_scope(Page)).where(show_in_navbar: false).order(:position)
    end

    def new
      @page = authorize Page.new
    end

    def edit; end

    def create
      @page = authorize Page.new(permitted_attributes(Page.new))
      if @page.save
        redirect_to admin_pages_url, notice: t('flash.page.create')
      else
        render :new
      end
    end

    def update
      if @page.update(permitted_attributes(@page))
        redirect_to admin_pages_url, notice: t('flash.page.update')
      else
        render :edit
      end
    end

    def destroy
      @page.destroy
      redirect_to admin_pages_url, notice: t('flash.page.destroy')
    end

    private

    def set_page
      @page = authorize Page.find(params[:id])
    end
  end
end
