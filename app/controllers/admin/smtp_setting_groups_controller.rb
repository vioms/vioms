# frozen_string_literal: true

module Admin
  class SmtpSettingGroupsController < BaseController
    before_action :set_smtp_setting_group, only: %i[edit update destroy]

    def index
      @smtp_setting_groups = authorize policy_scope(SmtpSettingGroup)
    end

    def new
      @smtp_setting_group = authorize SmtpSettingGroup.new
    end

    def edit; end

    def create
      @smtp_setting_group = authorize SmtpSettingGroup.new(permitted_params)
      if @smtp_setting_group.save
        redirect_to admin_smtp_setting_groups_url, notice: t('flash.smtp_setting_group.create')
      else
        render :new
      end
    end

    def update
      if @smtp_setting_group.update(permitted_params)
        redirect_to admin_smtp_setting_groups_url, notice: t('flash.smtp_setting_group.update')
      else
        render :edit
      end
    end

    def destroy
      @smtp_setting_group.destroy
      redirect_to admin_smtp_setting_groups_url, notice: t('flash.smtp_setting_group.destroy')
    end

    def sort
      params[:smtp_setting_group].each_with_index do |id, index|
        SmtpSettingGroup.find(id).update_column(:position, index + 1)
      end
      render nothing: true
    end

    private

    def set_smtp_setting_group
      @smtp_setting_group = authorize SmtpSettingGroup.find(params[:id])
    end
  end
end
