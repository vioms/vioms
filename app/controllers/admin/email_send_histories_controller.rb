# frozen_string_literal: true

module Admin
  class EmailSendHistoriesController < BaseController
    def index
      @email_send_histories = authorize policy_scope(EmailSendHistory).search(email: params[:email])
      @email_send_histories = @email_send_histories.page(params[:page])
    end

    def destroy
      @email_send_history = authorize EmailSendHistory.find(params[:id])
      @email_send_history.destroy
      redirect_to admin_email_send_histories_url, notice: t('flash.email_send_history.destroy')
    end
  end
end
