# frozen_string_literal: true

module Admin
  class SurveyQuestionsController < BaseController
    before_action :set_survey
    before_action :set_survey_question, only: %i[edit update destroy]
    before_action :set_question_types, only: %i[new create edit update]

    def index
      @survey_questions = authorize @survey.survey_questions.ordered
      skip_policy_scope
    end

    def new
      @survey_question = authorize SurveyQuestion.new
    end

    def edit; end

    def create
      @survey_question = authorize @survey.survey_questions.build(permitted_attributes(SurveyQuestion.new))
      if @survey_question.save
        redirect_to admin_survey_survey_questions_url(@survey), notice: t('flash.survey_question.create')
      else
        render :new
      end
    end

    def update
      if @survey_question.update(permitted_attributes(@survey_question))
        redirect_to admin_survey_survey_questions_url(@survey), notice: t('flash.survey_question.update')
      else
        render :edit
      end
    end

    def destroy
      @survey_question.destroy
      redirect_to admin_survey_survey_questions_url(@survey), notice: t('flash.survey_question.destroy')
    end

    private

    def set_survey
      @survey = authorize Survey.find(params[:survey_id])
    end

    def set_survey_question
      @survey_question = authorize @survey.survey_questions.find(params[:id])
    end

    def set_question_types
      @question_types = SurveyQuestion.question_types.keys.map do |question_type|
        [t("admin_question_types.#{question_type}"), question_type]
      end
    end
  end
end
