# frozen_string_literal: true

module Admin
  class TelegramGroupsController < BaseController
    include CollectionLoaders

    before_action :set_telegram_group, only: %i[edit update destroy]
    before_action :set_users, only: %i[new create edit update]

    def index
      @telegram_groups = authorize(
        policy_scope(TelegramGroup).ordered
      )
      @telegram_groups = @telegram_groups.page(params[:page])
      @can_destroy = policy(TelegramGroup).destroy?
    end

    def new
      @telegram_group = authorize TelegramGroup.new
    end

    def edit; end

    def create
      @telegram_group = authorize TelegramGroup.new(permitted_attributes(TelegramGroup.new))
      if @telegram_group.save
        redirect_to admin_telegram_groups_url, notice: t('flash.telegram_group.create')
      else
        render :new
      end
    end

    def update
      if @telegram_group.update(permitted_attributes(@telegram_group))
        redirect_to admin_telegram_groups_url, notice: t('flash.telegram_group.update')
      else
        render :edit
      end
    end

    def destroy
      @telegram_group.destroy
      redirect_to admin_telegram_groups_url, notice: t('flash.telegram_group.destroy')
    end

    private

    def set_telegram_group
      @telegram_group = authorize TelegramGroup.find(params[:id])
    end
  end
end
