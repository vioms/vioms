# frozen_string_literal: true

module Admin
  class TextBlocksController < BaseController
    before_action :set_text_block, only: %i[edit update]

    def index
      @text_blocks = authorize policy_scope(TextBlock).ordered
    end

    def edit; end

    def update
      if @text_block.update(permitted_attributes(@text_block))
        redirect_to admin_text_blocks_url, notice: t('flash.text_block.update')
      else
        render :edit
      end
    end

    private

    def set_text_block
      @text_block = authorize TextBlock.find(params[:id])
    end
  end
end
