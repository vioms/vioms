# frozen_string_literal: true

module Admin
  class SubscribersController < BaseController
    include CollectionLoaders
    include SessionValues

    before_action :set_subscriber, only: %i[new create edit update destroy]
    before_action :set_email_lists_with_visible, only: %i[new create edit update]
    before_action :update_subscriber_attributes, only: %i[create update]
    after_action :nil_user, only: %i[create update]

    def index
      email_list_id = get_session_value(:email_list).to_i
      @subscribers = authorize(policy_scope(Subscriber).search(
                                 id: params[:id], name: params[:name], email: params[:email], phone: params[:phone],
                                 email_list_id:
                               ))
      @subscribers = @subscribers.page(params[:page]).per(view_context.per_page_remember)
      @email_list_options = get_email_list_options(email_list_id, truncate_label: true)
      @can_destroy = policy(Subscriber).destroy?
    end

    def new; end

    def edit
      return unless policy(EmailListCustomVar).index?

      @custom_vars =
        policy_scope(@subscriber.subscriber_custom_vars)
        .select('email_lists.name AS email_list_name, email_list_custom_vars.name, ' \
                'email_list_custom_vars.description, subscriber_custom_vars.value')
        .joins(email_list_custom_var: :email_list)
        .reorder('email_list_name, email_list_custom_vars.name')
    end

    def create
      if @subscriber.save
        redirect_to admin_subscribers_url, notice: 'Подписчик создан.'
      else
        render :new
      end
    end

    def update
      if @subscriber.save
        redirect_to admin_subscribers_url, notice: 'Подписчик обновлен.'
      else
        render :edit
      end
    end

    def destroy
      @subscriber.destroy
      redirect_to admin_subscribers_url, notice: 'Подписчик удален.'
    end

    private

    def set_subscriber
      @subscriber =
        case action_name
        when 'new'
          Subscriber.new
        when 'create'
          Subscriber.new(::Subscriber::ParamsSanitizer::ForAdmin.call(permitted_params, subscriber: Subscriber.new,
                                                                                        user: current_user))
        else
          Subscriber.find(params[:id])
        end
      authorize(@subscriber)
    end

    def update_subscriber_attributes
      @subscriber.attributes =
        ::Subscriber::ParamsSanitizer::ForAdmin.call(permitted_params, subscriber: @subscriber, user: current_user)
      @subscriber.user = current_user
    end

    def nil_user
      @subscriber.user = nil
    end
  end
end
