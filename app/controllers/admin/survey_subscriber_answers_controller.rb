# frozen_string_literal: true

module Admin
  class SurveySubscriberAnswersController < BaseController
    before_action :set_survey
    before_action :set_survey_question

    def index
      @survey_subscriber_answers = authorize @survey_question.survey_subscriber_answers.page(params[:page])
      skip_policy_scope
    end

    private

    def set_survey
      @survey = authorize Survey.find(params[:survey_id])
    end

    def set_survey_question
      @survey_question = authorize @survey.survey_questions.find(params[:survey_question_id])
    end
  end
end
