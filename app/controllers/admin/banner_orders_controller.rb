# frozen_string_literal: true

module Admin
  class BannerOrdersController < BaseController
    before_action :set_banner_order, only: %i[edit update destroy]

    def index
      @banner_orders = authorize policy_scope(BannerOrder).includes(:banner_owner)
    end

    def new
      @banner_order = authorize BannerOrder.new
    end

    def edit; end

    def create
      @banner_order = authorize BannerOrder.new(permitted_params)
      if @banner_order.save
        redirect_to admin_banner_orders_url, notice: t('flash.banner_order.create')
      else
        render :new
      end
    end

    def update
      if @banner_order.update(permitted_params)
        redirect_to admin_banner_orders_url, notice: t('flash.banner_order.update')
      else
        render :edit
      end
    end

    def destroy
      @banner_order.destroy
      redirect_to admin_banner_orders_url, notice: t('flash.banner_order.destroy')
    end

    private

    def set_banner_order
      @banner_order = authorize BannerOrder.find(params[:id])
    end
  end
end
