# frozen_string_literal: true

module Admin
  class SurveysController < BaseController
    before_action :set_survey, only: %i[edit update destroy]

    def index
      @surveys = authorize policy_scope(Survey)
    end

    def new
      @survey = authorize Survey.new
    end

    def edit; end

    def create
      @survey = authorize Survey.new(permitted_attributes(Survey.new))
      if @survey.save
        redirect_to admin_surveys_url, notice: t('flash.survey.create')
      else
        render :new
      end
    end

    def update
      if @survey.update(permitted_attributes(@survey))
        redirect_to admin_surveys_url, notice: t('flash.survey.update')
      else
        render :edit
      end
    end

    def destroy
      @survey.destroy
      redirect_to admin_surveys_url, notice: t('flash.survey.destroy')
    end

    private

    def set_survey
      @survey = authorize Survey.find(params[:id])
    end
  end
end
