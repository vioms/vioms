# frozen_string_literal: true

module Admin
  class MailingStatisticsController < BaseController
    before_action :set_mailing

    def index
      @mailing_views = policy_scope(MailingView).where(mailing_id: @mailing.id).page(params[:page])
    end

    private

    def set_mailing
      @mailing = authorize Mailing.find(params[:mailing_id]), :statistics?
    end
  end
end
