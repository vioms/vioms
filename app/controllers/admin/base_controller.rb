# frozen_string_literal: true

module Admin
  class BaseController < ApplicationController
    layout 'admin'

    before_action :authenticate_user!

    def authorize(record, query = nil, **)
      super([:admin, record], query, **)
    end

    def policy_scope(scope)
      super([:admin, scope])
    end

    def policy(record)
      super([:admin, record])
    end

    def pundit_user
      current_user
    end

    def pundit_i18n_scope
      'pundit.admin'
    end

    def pundit_unauthorized_redirect_url
      admin_welcome_url
    end

    private

    mattr_accessor :stripo_version
    def stripo_version
      self.class.stripo_version ||= ::Stripo::Version.call
    end
    helper_method :stripo_version
  end
end
