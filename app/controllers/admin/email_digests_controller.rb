# frozen_string_literal: true

module Admin
  class EmailDigestsController < BaseController
    include CollectionLoaders
    include SendingCancellation

    before_action :set_email_digest, only: %i[show edit update destroy send_digest cancel_sending]
    before_action :set_email_lists, only: %i[index new create edit update]

    def index
      @email_digests = authorize(policy_scope(EmailDigest)).page(params[:page])
    end

    def show
      content = EmailDigest::View::AdminPreview.call(content: EmailDigest::Render.call(@email_digest),
                                                     email_digest: @email_digest, email_list: @email_digest.email_list)
      render html: content.html_safe, layout: false
    end

    def new
      @email_digest = authorize EmailDigest.new
      @next_num = EmailDigest.next_num(params[:email_list]) if params['email_list'].present?
    end

    def edit
      redirect_to action: :show if @email_digest.published
    end

    def create
      @email_digest = authorize EmailDigest.new(permitted_params)
      if @email_digest.save
        redirect_to admin_email_digests_url, notice: t('flash.email_digest.create')
      else
        render :new
      end
    end

    def update
      if EmailDigest::Update.call(email_digest: @email_digest, params: permitted_params).success?
        redirect_to admin_email_digests_url, notice: t('flash.email_digest.update')
      else
        render :edit
      end
    end

    def destroy
      @email_digest.destroy
      redirect_to admin_email_digests_url, notice: t('flash.email_digest.destroy')
    end

    def send_digest
      @email_digest.published = true
      @email_digest.user_id = current_user.id
      @email_digest.save(validate: false)
      @email_digest.create_bg_job
      redirect_to admin_email_digests_url,
                  notice: "Email дайджест будет отправлен #{view_context.date_ru(@email_digest.deliver_at)}"
    end

    def cancel_sending
      return unless cancel_sending_job(record: @email_digest, redirect_link: admin_email_digests_url)

      @email_digest.email_digest_bg_job.destroy
    end

    private

    def set_email_digest
      @email_digest = authorize EmailDigest.find(params[:id])
    end
  end
end
