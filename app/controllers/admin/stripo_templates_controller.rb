# frozen_string_literal: true

module Admin
  class StripoTemplatesController < BaseController
    include ::Stripo::TemplateGroupsLoader

    before_action :set_stripo_template_group, except: %i[new_from_mailing create_from_mailing]
    before_action :set_stripo_template, only: %i[edit edit_stripo update destroy]
    before_action :set_stripo_template_groups_collection,
                  only: %i[new create new_from_mailing create_from_mailing edit update]

    def index
      scope = @stripo_template_group.templates
                                    .select(%i[id name preview_data stripo_template_group_id])
                                    .ordered
      @stripo_templates = authorize policy_scope(scope)
      respond_to do |format|
        format.html
        format.json do
          render json: @stripo_templates.map { |t| { id: t.id, name: t.name, preview_url: t.preview_url } }
        end
      end
    end

    def new
      @stripo_template = authorize ::Stripo::Template.new(template_group: @stripo_template_group)
    end

    def new_from_mailing
      ::Stripo::TemplateGroup::CreateFirstOwnGroup.call(current_user)
      @stripo_template = authorize ::Stripo::Template.new
    end

    def edit; end

    def edit_stripo; end

    def create
      result = ::Stripo::Template::Create.call(params: permitted_attributes(::Stripo::Template.new))
      @stripo_template = authorize result.template
      if result.success?
        redirect_to admin_stripo_template_group_stripo_templates_url(@stripo_template_group),
                    notice: t('flash.stripo_template.create')
      else
        render :new
      end
    end

    def create_from_mailing
      result = ::Stripo::Template::CreateFromMailing.call(params: permitted_attributes(::Stripo::Template.new))
      @stripo_template = authorize result.template
      if result.success?
        redirect_to admin_stripo_template_group_stripo_templates_url(@stripo_template.template_group),
                    notice: t('flash.stripo_template.create')
      else
        render :new_from_mailing
      end
    end

    def update
      if @stripo_template.update(permitted_attributes(@stripo_template))
        redirect_to admin_stripo_template_group_stripo_templates_url(@stripo_template_group),
                    notice: t('flash.stripo_template.update')
      else
        render :edit
      end
    end

    def destroy
      ::Stripo::Template::Destroy.call(template: @stripo_template)
      redirect_to admin_stripo_template_group_stripo_templates_url(@stripo_template_group),
                  notice: t('flash.stripo_template.destroy')
    end

    private

    def set_stripo_template_group
      @stripo_template_group = authorize ::Stripo::TemplateGroup.find(params[:stripo_template_group_id]), :show?
    end

    def set_stripo_template
      @stripo_template = authorize ::Stripo::Template.find(params[:id])
    end
  end
end
