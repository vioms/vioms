# frozen_string_literal: true

module Admin
  class MailingRequestMessagesController < BaseController
    include CollectionLoaders

    before_action :set_mailing_request
    before_action :set_mailing_types, only: %i[create]
    before_action :set_mailing_request_statuses, only: %i[index create]

    def index
      @mailing_request_messages = authorize policy_scope(@mailing_request.messages).ordered
      @mailing_request_message = authorize @mailing_request.messages.build
    end

    def create
      @mailing_request_message =
        authorize @mailing_request.messages.build(permitted_attributes(MailingRequestMessage.new))
      MailingRequest::UpdateAttrsFromAdmin.call(@mailing_request, @mailing_request_message, current_user)
      MailingRequest.transaction do
        if @mailing_request_message.save(validate: false)
          @mailing_request.save!
          MailingRequestsMailer.subscriber_notification(@mailing_request.subscriber,
                                                        @mailing_request_message).deliver_now
          redirect_to admin_mailing_requests_url, notice: t('flash.mailing_request_message.create')
        else
          render :new
        end
      end
    end

    private

    def set_mailing_request
      @mailing_request = authorize MailingRequest.find(params[:mailing_request_id])
    end
  end
end
