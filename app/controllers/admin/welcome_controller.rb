# frozen_string_literal: true

module Admin
  class WelcomeController < BaseController
    def index
      authorize(nil, :index?, policy_class: WelcomePolicy)
      skip_policy_scope
    end

    def toggle_become_admin
      authorize(nil, :toggle_become_admin?, policy_class: WelcomePolicy)
      current_user.toggle_become_admin
      redirect_to admin_welcome_url
    end
  end
end
