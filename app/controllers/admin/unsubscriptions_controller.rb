# frozen_string_literal: true

module Admin
  class UnsubscriptionsController < BaseController
    before_action :set_unsubscription, only: %i[show destroy]

    def index
      @unsubscriptions = authorize(
        policy_scope(Unsubscription).includes(:subscriber, :unsubscription_reason, :email_list)
      ).page(params[:page])
    end

    def show; end

    def destroy
      @unsubscription.destroy
      redirect_to admin_unsubscriptions_url, notice: t('flash.unsubscription.destroy')
    end

    private

    def set_unsubscription
      @unsubscription = authorize Unsubscription.find(params[:id])
    end
  end
end
