# frozen_string_literal: true

module Admin
  class BannerOwnersController < BaseController
    before_action :set_banner_owner, only: %i[edit update destroy]

    def index
      @banner_owners = authorize policy_scope(BannerOwner)
    end

    def new
      @banner_owner = authorize BannerOwner.new
    end

    def edit; end

    def create
      @banner_owner = authorize BannerOwner.new(permitted_params)
      if @banner_owner.save
        redirect_to admin_banner_owners_url, notice: t('flash.banner_owner.create')
      else
        render :new
      end
    end

    def update
      if @banner_owner.update(permitted_params)
        redirect_to admin_banner_owners_url, notice: t('flash.banner_owner.update')
      else
        render :edit
      end
    end

    def destroy
      @banner_owner.destroy
      redirect_to admin_banner_owners_url, notice: t('flash.banner_owner.destroy')
    end

    private

    def set_banner_owner
      @banner_owner = authorize BannerOwner.find(params[:id])
    end
  end
end
