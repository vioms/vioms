# frozen_string_literal: true

module Admin
  class EmailListVarNamesController < BaseController
    before_action :set_email_list
    before_action :set_email_list_var_name, only: %i[edit update destroy]

    def index
      @email_list_var_names = authorize policy_scope(@email_list.email_list_var_names)
    end

    def new
      @email_list_var_name = authorize @email_list.email_list_var_names.build
    end

    def edit; end

    def create
      @email_list_var_name = authorize @email_list.email_list_var_names.build(permitted_params)
      if @email_list_var_name.save
        redirect_to admin_email_list_email_list_var_names_url(@email_list),
                    notice: t('flash.email_list_var_name.create')
      else
        render :new
      end
    end

    def update
      if @email_list_var_name.update(permitted_params)
        redirect_to admin_email_list_email_list_var_names_url(@email_list),
                    notice: t('flash.email_list_var_name.update')
      else
        render :edit
      end
    end

    def destroy
      @email_list_var_name.destroy
      redirect_to admin_email_list_email_list_var_names_url(@email_list), notice: t('flash.email_list_var_name.destroy')
    end

    def sort
      params[:email_list_var_name].each_with_index do |id, index|
        EmailListVarName.find(id).update_column(:position, index + 1)
      end
      render nothing: true
    end

    private

    def set_email_list
      @email_list = authorize EmailList.find(params[:email_list_id])
    end

    def set_email_list_var_name
      @email_list_var_name = authorize @email_list.email_list_var_names.find(params[:id])
    end
  end
end
