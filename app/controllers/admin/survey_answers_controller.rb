# frozen_string_literal: true

module Admin
  class SurveyAnswersController < BaseController
    before_action :set_survey
    before_action :set_survey_question
    before_action :set_survey_answer, only: %i[edit update destroy]

    def index
      @survey_answers = authorize @survey_question.survey_answers.ordered
      skip_policy_scope
    end

    def new
      @survey_answer = authorize SurveyAnswer.new
    end

    def edit; end

    def create
      @survey_answer = authorize @survey_question.survey_answers.build(permitted_attributes(SurveyAnswer.new))
      if @survey_answer.save
        redirect_to admin_survey_survey_question_survey_answers_url(@survey, @survey_question),
                    notice: t('flash.survey_answer.create')
      else
        render :new
      end
    end

    def update
      if @survey_answer.update(permitted_attributes(@survey_answer))
        redirect_to admin_survey_survey_question_survey_answers_url(@survey, @survey_question),
                    notice: t('flash.survey_answer.update')
      else
        render :edit
      end
    end

    def destroy
      @survey_answer.destroy
      redirect_to admin_survey_survey_question_survey_answers_url(@survey, @survey_question),
                  notice: t('flash.survey_answer.destroy')
    end

    private

    def set_survey
      @survey = authorize Survey.find(params[:survey_id])
    end

    def set_survey_question
      @survey_question = authorize @survey.survey_questions.find(params[:survey_question_id])
    end

    def set_survey_answer
      @survey_answer = authorize @survey_question.survey_answers.find(params[:id])
    end
  end
end
