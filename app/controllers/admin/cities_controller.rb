# frozen_string_literal: true

module Admin
  class CitiesController < BaseController
    before_action :set_city, only: %i[edit update destroy]

    def index
      @city = authorize City.find(params[:city_id]) if params[:city_id]
      @cities = authorize policy_scope(City).ordered
      @cities = if params[:city_id]
                  @cities.children_of(params[:city_id])
                else
                  @cities.roots
                end
    end

    def new
      @city = authorize City.new
    end

    def edit; end

    def create
      @city = authorize City.new(permitted_attributes(City.new))
      if @city.save
        redirect_to admin_cities_url, notice: t('flash.city.create')
      else
        render :new
      end
    end

    def update
      if @city.update(permitted_attributes(@city))
        redirect_to admin_cities_url, notice: t('flash.city.update')
      else
        render :edit
      end
    end

    def destroy
      @city.destroy
      redirect_to admin_cities_url, notice: t('flash.city.destroy')
    end

    private

    def set_city
      @city = authorize City.find(params[:id])
    end
  end
end
