# frozen_string_literal: true

module Admin
  class MailingRequestsController < BaseController
    include CollectionLoaders

    before_action :set_mailing_request, only: %i[edit update destroy]
    before_action :set_mailing_types, only: %i[edit update]
    before_action :set_mailing_request_statuses, only: %i[edit update]
    before_action :set_email_lists_for_mailing_requests, only: %i[edit update]

    def index
      @mailing_requests = authorize policy_scope(MailingRequest).ordered
    end

    def edit; end

    def update
      if @mailing_request.update(permitted_attributes(@mailing_request))
        redirect_to admin_mailing_requests_url, notice: t('flash.mailing_request.update')
      else
        render :edit
      end
    end

    def destroy
      @mailing_request.destroy
      redirect_to admin_mailing_requests_url, notice: t('flash.mailing_request.destroy')
    end

    private

    def set_mailing_request
      @mailing_request = authorize MailingRequest.find(params[:id])
    end
  end
end
