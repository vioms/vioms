# frozen_string_literal: true

module Admin
  class SubscriberStatisticsController < BaseController
    before_action :set_subscriber

    def index
      @mailing_views = policy_scope(MailingView).where(subscriber_id: @subscriber.id).page(params[:page])
    end

    private

    def set_subscriber
      @subscriber = authorize Subscriber.find(params[:subscriber_id]), :statistics?
    end
  end
end
