# frozen_string_literal: true

module Admin
  class DigestCategoriesController < BaseController
    include CollectionLoaders

    before_action :set_digest_category, only: %i[edit update destroy]
    before_action :set_email_lists, only: %i[index new create edit update]

    def index
      @digest_categories = authorize policy_scope(DigestCategory)
      email_list_id = params[:email_list]
      return unless email_list_id

      @email_list = authorize EmailList.find(email_list_id)
      @digest_categories = @digest_categories.where(email_list_id:)
    end

    def new
      @digest_category = authorize DigestCategory.new
    end

    def edit; end

    def create
      @digest_category = authorize DigestCategory.new(permitted_params)
      if @digest_category.save
        redirect_to admin_digest_categories_url(email_list: @digest_category.email_list),
                    notice: t('flash.digest_category.create')
      else
        render :new
      end
    end

    def update
      if @digest_category.update(permitted_params)
        redirect_to admin_digest_categories_url(email_list: @digest_category.email_list),
                    notice: t('flash.digest_category.update')
      else
        render :edit
      end
    end

    def destroy
      @digest_category.destroy
      redirect_to admin_digest_categories_url(email_list: @digest_category.email_list),
                  notice: t('flash.digest_category.destroy')
    end

    private

    def set_digest_category
      @digest_category = authorize DigestCategory.find(params[:id])
    end
  end
end
