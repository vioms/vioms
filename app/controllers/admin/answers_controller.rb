# frozen_string_literal: true

module Admin
  class AnswersController < BaseController
    before_action :set_poll
    before_action :set_answer, only: %i[edit update destroy]

    def index
      @answers = authorize policy_scope(@poll.answers).order(:position)
    end

    def new
      @answer = @poll.answers.build
    end

    def edit; end

    def create
      @answer = @poll.answers.build(permitted_params)
      if @answer.save
        redirect_to admin_poll_answers_url(@poll), notice: t('flash.answer.create')
      else
        render :new
      end
    end

    def update
      @answer = Answer.find(params[:id])
      if @answer.update(permitted_params)
        redirect_to admin_poll_answers_url(@poll), notice: t('flash.answer.update')
      else
        render :edit
      end
    end

    def destroy
      @answer.destroy
      redirect_to admin_poll_answers_url(@poll), notice: t('flash.answer.destroy')
    end

    private

    def set_poll
      @poll = authorize Poll.find(params[:poll_id])
    end

    def set_answer
      @answer = authorize @poll.answers.find(params[:id])
    end
  end
end
