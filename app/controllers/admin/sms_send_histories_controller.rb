# frozen_string_literal: true

module Admin
  class SmsSendHistoriesController < BaseController
    def index
      @sms_send_histories = authorize(
        policy_scope(SmsSendHistory).search(phone: params[:phone], message_type: params[:message_type])
      )
      @sms_send_histories = @sms_send_histories.page(params[:page])
      @message_types = view_context.tag.option('Все типы', value: 0) +
                       view_context.options_from_collection_for_select(SmsSendHistoryType.all, :id, 'description',
                                                                       params[:message_type])
    end

    def destroy
      @sms_send_history = authorize SmsSendHistory.find(params[:id])
      @sms_send_history.destroy
      redirect_to admin_sms_send_histories_url, notice: t('flash.sms_send_history.destroy')
    end
  end
end
