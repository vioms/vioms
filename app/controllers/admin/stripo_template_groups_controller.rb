# frozen_string_literal: true

module Admin
  class StripoTemplateGroupsController < BaseController
    include CollectionLoaders
    include ::Stripo::TemplateGroupsLoader

    before_action :set_stripo_template_group, only: %i[edit update destroy]
    before_action :set_email_lists, only: %i[new create edit update]
    before_action :set_stripo_template_groups_collection, only: %i[new create edit update]

    def index
      root_groups = authorize policy_scope(::Stripo::TemplateGroup)
      if params[:stripo_template_group_id]
        @stripo_template_group = authorize ::Stripo::TemplateGroup.find(params[:stripo_template_group_id]), :show?
        @stripo_template_groups = @stripo_template_group.children
      else
        @stripo_template_groups = root_groups.roots
      end
      @stripo_template_groups = @stripo_template_groups.ordered
    end

    def new
      @stripo_template_group = authorize ::Stripo::TemplateGroup.new(parent_id: params[:stripo_template_group_id])
    end

    def edit; end

    def create
      pars = ::Stripo::TemplateGroup::ParamsSanitizer.call(permitted_attributes(::Stripo::TemplateGroup.new),
                                                           user: current_user)
      result = ::Stripo::TemplateGroup::Create.call(params: pars, user: current_user)
      @stripo_template_group = authorize result.stripo_template_group
      if result.success?
        redirect_to admin_stripo_template_groups_url, notice: t('flash.stripo_template_group.create')
      else
        render :new
      end
    end

    def update
      pars = ::Stripo::TemplateGroup::ParamsSanitizer.call(permitted_attributes(::Stripo::TemplateGroup.new),
                                                           user: current_user, template_group: @stripo_template_group)
      result = ::Stripo::TemplateGroup::Update.call(stripo_template_group: @stripo_template_group, params: pars,
                                                    user: current_user)
      if result.success?
        redirect_to admin_stripo_template_groups_url, notice: t('flash.stripo_template_group.update')
      else
        render :edit
      end
    end

    def destroy
      @stripo_template_group.destroy
      redirect_to admin_stripo_template_groups_url, notice: t('flash.stripo_template_group.destroy')
    end

    private

    def set_stripo_template_group
      @stripo_template_group = authorize ::Stripo::TemplateGroup.find(params[:id])
    end
  end
end
