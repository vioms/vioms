# frozen_string_literal: true

module Admin
  class MailingsController < BaseController
    include SendingCancellation
    include CollectionLoaders
    include SessionValues

    before_action :set_mailing, only: %i[show full edit frame update copy send_mailing cancel_sending destroy]
    before_action :set_editor_types, only: %i[new create]
    before_action :set_email_lists, only: %i[new create edit update copy]
    before_action :load_email_inclusions_and_exclusions, only: %i[edit update copy]
    before_action :set_max_attachments_size, only: %i[edit update copy]
    skip_before_action :verify_authenticity_token, only: :new, if: -> { request.format.json? }

    TIME_OFFSET = 5.minutes

    def index
      email_list_id = get_session_value(:email_list)
      @mailings = authorize policy_scope(Mailing).admin_search(email_list_id:, subject: params[:subject])
      @mailings = @mailings.page(params[:page]).per(view_context.per_page_remember)
      @email_list_options = get_email_list_options(email_list_id)
    end

    def show; end

    def full
      content = Mailing::View::AdminPreview.call(
        mailing: @mailing, email_list: @mailing.email_list, content: @mailing.rendered_html
      )
      render html: content.html_safe, layout: false
    end

    def new
      @mailing = authorize Mailing.new(creating: true)
      return unless params[:email_list].to_i > 0

      email_list = authorize EmailList.find(params[:email_list]), :show?
      @mailing.email_list = email_list
      @mailing.editor_type = email_list.default_editor_type
    end

    def edit
      @mailing.deliver_at ||= TIME_OFFSET.from_now
      if @mailing.email_list_vars.none? && @mailing.template_editor_type?
        EmailListVarName.where(email_list: @mailing.email_list).find_each do |email_list_var_name|
          @mailing.email_list_vars.build(email_list_var_name:)
        end
      end
      load_email_digests_and_digest_categories
      redirect_to action: :show if @mailing.published
    end

    def create
      result = Mailing::Create.call(params: permitted_attributes(Mailing.new))
      @mailing = authorize result.mailing
      if result.success?
        redirect_to edit_admin_mailing_url(@mailing)
      else
        render :new
      end
    end

    def frame
      render 'frame', layout: false
    end

    def update
      if Mailing::Update.call(mailing: @mailing, params: permitted_attributes(@mailing)).success?
        if params[:mailing][:digest_image].present?
          render :crop
        else
          redirect_to admin_mailings_url, notice: t('flash.mailing.update')
        end
      else
        load_email_digests_and_digest_categories
        render :edit
      end
    end

    def copy
      mailing = authorize Mailing::Copy.call(mailing: @mailing).mailing
      redirect_to edit_admin_mailing_url(mailing)
    end

    def send_mailing
      @mailing.published = true
      @mailing.user_id = current_user.id
      @mailing.save(validate: false)
      @mailing.create_bg_job unless @mailing.digest
      redirect_to admin_mailings_url,
                  notice: "Email рассылка будет отправлена #{view_context.date_ru(@mailing.deliver_at)}"
    end

    def cancel_sending
      return unless cancel_sending_job(record: @mailing, redirect_link: admin_mailings_url)

      @mailing.mailing_bg_job.destroy
    end

    def crop; end

    def destroy
      Mailing::Destroy.call(mailing: @mailing)
      redirect_to admin_mailings_url, notice: t('flash.mailing.destroy')
    end

    private

    def set_mailing
      @mailing = authorize Mailing.find(params[:id])
    end

    def set_max_attachments_size
      @mailing.max_attachments_size = current_user.max_attachments_size
    end

    def load_email_digests_and_digest_categories
      email_list = @mailing.email_list
      if email_list
        @email_digests = email_list.email_digests.non_published
        @digest_categories = email_list.digest_categories
      else
        @email_digests = []
        @digest_categories = []
      end
    end
  end
end
