# frozen_string_literal: true

class PagesController < ApplicationController
  before_action :set_page, only: %i[show edit update destroy]
  before_action :authenticate_user!, except: %i[index show]

  def index
    @page = authorize(policy_scope(Page).first)
    render :show
  end

  def show; end

  def new
    @page = authorize Page.new
  end

  def edit; end

  def create
    @page = authorize Page.new(permitted_params)
    if @page.save
      redirect_to @page, notice: t('flash.page.create')
    else
      render :new
    end
  end

  def update
    if @page.update(permitted_params)
      redirect_to @page, notice: t('flash.page.update')
    else
      render :edit
    end
  end

  def destroy
    @page.destroy
    redirect_to pages_url, notice: t('flash.page.destroy')
  end

  private

  def set_page
    @page = authorize Page.find(params[:id])
  end
end
