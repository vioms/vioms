# frozen_string_literal: true

class NewsItemsController < ApplicationController
  def index
    @news_items = authorize(policy_scope(NewsItem)).page(params[:page])
  end

  def show
    @news_item = authorize NewsItem.find(params[:id])
  end
end
