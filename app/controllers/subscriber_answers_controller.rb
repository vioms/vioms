# frozen_string_literal: true

# A subscriber follows the link to give his answer
class SubscriberAnswersController < ApplicationController
  def new
    # TODO: authorize subscriber
    skip_authorization
    subscriber = Subscriber.find(params[:subscriber_id])
    answer     = Answer.find(params[:answer_id])

    if VoteService.vote(subscriber:, answer:)
      redirect_to root_url, notice: t('flash.subscriber_answer.create')
    else
      redirect_to root_url
    end
  end
end
