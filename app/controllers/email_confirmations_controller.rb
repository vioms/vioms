# frozen_string_literal: true

class EmailConfirmationsController < ApplicationController
  before_action :authenticate_subscriber_user_from_token!
  before_action :authenticate_subscriber_user!
  before_action :skip_authorization

  def show
    Subscriber::SendEmailConfirmationInstructions.call(current_subscriber_user)
  end

  def confirm
    esh = EmailSendHistory.find_by(subscriber: current_subscriber_user, email: current_subscriber_user.email,
                                   last_value: params[:confirmation_token])
    subscriber = esh&.subscriber
    if esh && subscriber
      authorize esh, :show?
      ::Subscriber::ConfirmEmail.call(current_subscriber_user)
      redirect_to edit_my_settings_url, notice: t('flash.my_settings.email_confirmed')
    else
      redirect_to root_url, alert: t('flash.my_settings.bad_link')
    end
  end
end
