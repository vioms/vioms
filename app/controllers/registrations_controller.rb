# frozen_string_literal: true

class RegistrationsController < ApplicationController
  before_action :skip_authorization, only: %i[edit update]

  def new
    @subscriber_user = authorize SubscriberUser.new
  end

  def edit; end

  def create
    @subscriber_user = authorize SubscriberUser.new(sanitize_params(permitted_attributes(SubscriberUser.new)))
    Subscriber::ForgetHistory.call(@subscriber_user) if @subscriber_user.valid?
    if @subscriber_user.save
      Subscriber::SendEmailConfirmationInstructions.call(@subscriber_user)
      sign_in(@subscriber_user)
      redirect_to root_url, notice: t('simple_form.flash.subscriber_user.create')
    else
      render :new
    end
  end

  def update
    if ::Subscriber::ChangePassword.call(current_subscriber_user,
                                         sanitize_params(permitted_attributes(current_subscriber_user)))
      redirect_to root_url, notice: t('simple_form.flash.subscriber_user.update')
    else
      render :edit
    end
  end

  def destroy
    authorize current_subscriber_user
    subscriber_user = current_subscriber_user
    sign_out(current_subscriber_user)
    subscriber_user.destroy
    redirect_to root_url, notice: t('simple_form.flash.subscriber_user.destroy')
  end

  private

  def sanitize_params(pars)
    ::Subscriber::ParamsSanitizer::ForSubscriber.call(pars)
  end
end
