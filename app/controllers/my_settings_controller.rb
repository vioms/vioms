# frozen_string_literal: true

class MySettingsController < ApplicationController
  before_action :authenticate_subscriber_user_from_token!, only: %i[edit update]
  before_action :authenticate_subscriber_user!, only: %i[edit update]
  before_action :set_subscriber, only: %i[edit update]

  def edit; end

  def update
    if MySettings::Update.call(subscriber: @subscriber, params: permitted_attributes(@subscriber)).success?
      redirect_to edit_my_settings_url, notice: t('flash.my_settings.update')
    else
      render :edit
    end
  end

  private

  def set_subscriber
    @subscriber = authorize current_subscriber_user.becomes(Subscriber)
  end
end
