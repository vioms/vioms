# frozen_string_literal: true

class MailingRequestsController < ApplicationController
  include CollectionLoaders

  before_action :set_mailing_request, only: %i[edit update destroy]
  before_action :set_mailing_types, only: %i[new create edit update]
  before_action :set_email_lists_for_mailing_requests, only: %i[new create edit update]
  before_action :set_mailing_request_statuses, only: %i[new create edit update]

  def index
    @mailing_requests = authorize policy_scope(MailingRequest).ordered
  end

  def new
    @mailing_request = authorize MailingRequest.new
  end

  def edit; end

  def create
    @mailing_request = authorize MailingRequest.new(permitted_attributes(MailingRequest.new))
    @mailing_request.status = :awaiting_admin_response
    @mailing_request.subscriber_id = current_subscriber_user.id
    if @mailing_request.save
      MailingRequest::NotifyNewMailingRequest.call(@mailing_request)
      redirect_to mailing_requests_url, notice: t('flash.mailing_request.create')
    else
      render :new
    end
  end

  def update
    if @mailing_request.update(permitted_attributes(@mailing_request))
      redirect_to mailing_requests_url, notice: t('flash.mailing_request.update')
    else
      render :edit
    end
  end

  def destroy
    @mailing_request.destroy
    redirect_to mailing_requests_url, notice: t('flash.mailing_request.destroy')
  end

  private

  def set_mailing_request
    @mailing_request = authorize MailingRequest.find(params[:id])
  end
end
