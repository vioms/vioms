class Devise::Subscriber::PasswordsController < Devise::PasswordsController
  RESET_PASSWORD_FAILED_ATTEMPTS = 10
  RESET_PASSWORD_UNLOCK_TIME = 20.minutes

  # PUT /resource/password
  def update
    self.resource = resource_class.reset_password_by_token(resource_params)
    yield resource if block_given?

    if resource.reset_password_locked_at && resource.reset_password_locked_at < RESET_PASSWORD_UNLOCK_TIME.ago
      resource.reset_password_failed_attempts = 0
      resource.reset_password_locked_at = nil
      resource.save validate: false if resource.persisted?
    end

    if resource.reset_password_failed_attempts < RESET_PASSWORD_FAILED_ATTEMPTS
      if resource.errors.empty?
        SmsSendHistory.forget resource.phone
        flash_message = resource.active_for_authentication? ? :updated : :updated_not_active
        set_flash_message(:notice, flash_message) if is_flashing_format?
        sign_in(resource_name, resource)
        respond_with resource, location: after_sign_in_path_for(resource)
      else
        # PostgreSQL uses nil as the default value for integer columns set to 0
        # resource.reset_password_failed_attempts ||= 0
        resource.reset_password_failed_attempts += 1
        if resource.reset_password_failed_attempts == RESET_PASSWORD_FAILED_ATTEMPTS
          resource.reset_password_locked_at = Time.zone.now
        end
        resource.save validate: false if resource.persisted?
        respond_with resource
      end
    else
      set_flash_message(:alert, :failed_attempts) if is_flashing_format?
      respond_with resource
    end
  end

  protected

  def after_sending_reset_password_instructions_path_for(resource_name)
    if resource.email.present?
      new_session_path(resource_name) if is_navigational_format?
    elsif resource.phone.present?
      edit_password_path(resource_name, sms_recovery: 1) if is_navigational_format?
    end
  end

  def assert_reset_token_passed
    super unless params[:sms_recovery] == '1'
  end
end
