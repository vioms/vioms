# frozen_string_literal: true

class Devise::User::SessionsController < Devise::SessionsController
  def after_sign_in_path_for(_resource_or_scope)
    admin_welcome_url
  end
end
