# frozen_string_literal: true

class ContactController < ApplicationController
  def new
    @contact = authorize Contact.new
  end

  def create
    contact_params = permitted_attributes(Contact.new)
    @contact = authorize Contact.new(contact_params)
    if @contact.valid? && (current_subscriber_user || verify_recaptcha(model: @contact))
      ViomsMailer.support_message(contact_params.to_h).deliver
      redirect_to root_url, notice: 'Вопрос отправлен, в ближайшее время с Вами свяжутся'
    else
      render :new
    end
  end
end
