# frozen_string_literal: true

class WhatsappGroupsController < ApplicationController
  def index
    authorize(nil, :index?, policy_class: WhatsappGroupPolicy)
    skip_policy_scope
  end
end
