# frozen_string_literal: true

class AdminController < ApplicationController
  before_action :authenticate_user!

  def show
    skip_authorization
    skip_policy_scope
    redirect_to admin_welcome_url
  end
end
