# frozen_string_literal: true

# One contact imported
class SubscriberImportPerson < ApplicationRecord
  belongs_to :subscriber_import

  default_scope { order(Arel.sql('error_id IS NULL')) }

  def error_message
    SubscriberImport::ERRORS[error_id]
  end
end
