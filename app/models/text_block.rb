# frozen_string_literal: true

class TextBlock < ApplicationRecord
  validates :name, :description, presence: true
  validates :name, uniqueness: true

  scope :ordered, -> { order(:name) }

  def self.get(name)
    select(:content).find_by!(name:).content.html_safe
  rescue ActiveRecord::RecordNotFound
    nil
  end
end
