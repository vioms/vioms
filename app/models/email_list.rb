# frozen_string_literal: true

class EmailList < ApplicationRecord
  CKEDITOR_FIELDS = %i[description].freeze
  EDITOR_TYPES = { none: 1, ckeditor: 2, template: 3, stripo: 4 }.freeze

  has_many :email_subscriptions, dependent: :destroy
  has_many :subscribers, through: :email_subscriptions
  has_many :mailings, dependent: :nullify
  has_many :email_lists_mailings_inclusions, dependent: :delete_all
  has_many :email_lists_mailings_exclusions, dependent: :delete_all
  has_many :email_list_permissions, dependent: :destroy
  has_many :users, through: :email_list_permissions
  has_many :email_list_custom_vars, dependent: :destroy
  has_many :email_list_exclusion_permissions, dependent: :delete_all
  has_many :user_exclusion_abilities, through: :email_list_exclusion_permissions, source: 'user'
  has_many :unsubscriptions, dependent: :delete_all
  has_many :digest_categories
  has_many :email_digests
  has_many :email_list_var_names, dependent: :delete_all
  belongs_to :default_stripo_template_group, class_name: 'Stripo::TemplateGroup', optional: true
  belongs_to :default_stripo_template, class_name: 'Stripo::Template', optional: true
  has_and_belongs_to_many :mailing_destinations

  enum :default_editor_type, EDITOR_TYPES, suffix: true

  validates :name, :from_name, :from_email, :default_editor_type, presence: true
  validates :from_email, :from_bounce, :reply_to, :notify_email, email: { mx: { a_fallback: true } }, allow_blank: true
  validates :postmaster_msgtype, length: { maximum: 25 }
  validates :group_priority, numericality: { only_integer: true,
                                             greater_than_or_equal_to: 0, less_than_or_equal_to: 100 }

  before_destroy :expire_feed_caches
  after_save :expire_feed_caches
  after_save :reset_subscribers_counter_cache
  after_touch :expire_feed_caches

  default_scope { order(:name) }

  scope :visible, -> { where(visible: true) }
  scope :never_visible, -> { where(never_visible: true) }
  scope :without_never_visible, -> { where(never_visible: false) }
  scope :authorized, ->(user) { where(id: user.email_list_ids) unless user.effective_admin? }
  scope :visible_by_subscriber, lambda { |subscriber|
    if subscriber && subscriber.email_list_ids.any?
      where(id: subscriber.email_list_ids).or(EmailList.visible)
    else
      visible
    end
  }
  scope :priority_comparison, -> { select(%i[id from_name from_email group_priority]) }
  scope :ordered, -> { order(:name) }

  def to_label
    label = name
    label = label + ' (' + internal_name + ')' if internal_name.present?
    label
  end

  def to_label_truncated
    to_label.truncate(45)
  end

  def subscribers_count
    return subscribers_count_cache if subscribers_count_cache

    subscribers_count = subscribers.for_email_sending.count
    update_column(:subscribers_count_cache, subscribers_count)
    subscribers_count
  end

  def sync_json_contacts
    return unless is_json_import?

    uri = URI(import_link)
    req = Net::HTTP::Get.new(uri.request_uri)
    req.basic_auth import_login, import_password
    res = Net::HTTP.start(uri.hostname, uri.port) { |http| http.request(req) }

    add_subscribers JSON.parse(res.body)['users']
  end

  def archive_mailings
    archive_mailings = mailings.archive(self)
    included_mailing_ids = email_lists_mailings_inclusions.pluck(:mailing_id)
    if included_mailing_ids.any?
      Mailing.where(id: (archive_mailings.pluck(:id) + included_mailing_ids).uniq).archive(self)
    else
      archive_mailings
    end
  end

  def migrate_subscribers(email_list_id)
    subscribers << Subscriber.find(EmailList.find(email_list_id).subscriber_ids - subscriber_ids)
  end

  private

  def add_subscribers(subscribers)
    EmailList.transaction do
      subscribers.each { |subscriber| add_subscriber(subscriber) }
      unsubscribe_list subscribers.pluck('email')
    end
  end

  def add_subscriber(user_attributes)
    email = user_attributes['email']
    subscriber = Subscriber.all
    # subscriber = subscriber.includes(:subscriber_custom_vars) if self.use_custom_vars
    subscriber = subscriber.find_or_initialize_by(email:)
    return unless subscriber.valid?

    if subscriber.new_record?
      subscriber.confirmed_at = Time.zone.now
    elsif subscriber.confirmed_at.nil?
      subscriber.update_column(:confirmed_at, Time.zone.now)
    end
    subscribers << subscriber unless subscribers.include?(subscriber)

    return unless use_custom_vars

    email_list_custom_vars.each do |email_custom_var|
      subscriber_custom_var = SubscriberCustomVar.find_or_initialize_by(email_list_custom_var_id: email_custom_var.id,
                                                                        subscriber_id: subscriber.id)
      subscriber_custom_var.update_attribute(:value, user_attributes[email_custom_var.name])
    end
  end

  def unsubscribe_list(subscriber_emails)
    subscriber_ids = [0]
    subscribers.each do |subscriber|
      subscriber_ids << subscriber.id unless subscriber_emails.include? subscriber.email
    end
    EmailSubscription.where(email_list_id: id, subscriber_id: subscriber_ids).delete_all
  end

  def is_json_import?
    import_use
  end

  def expire_feed_caches
    %w[rss atom].each do |f|
      ActionController::Base.expire_page(Rails.application.routes.url_helpers.email_list_path(self, f))
    end
  end

  def reset_subscribers_counter_cache
    return if subscribers_count_cache.nil?

    self.subscribers_count_cache = nil
    save!(validate: false)
  end
end
