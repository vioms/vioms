# frozen_string_literal: true

class Subscriber < SubscriberBase
  after_save :do_email_confirmation_task, :reset_email_list_counter_cache

  scope(:authorized, lambda do |user|
    unless user.effective_admin?
      conditions = []
      if user.email_list_ids.any?
        conditions << 'subscribers.id IN (SELECT DISTINCT subscriber_id FROM email_subscriptions ' \
                      "WHERE email_list_id IN (#{user.email_list_ids.join(',')}))"
      end
      if conditions.any?
        where(conditions.join(' OR '))
      else
        none
      end
    end
  end)
  scope :find_id, ->(id) { where(id:) if id.present? }
  scope :find_name, ->(name) { like_param('name', name) }
  scope :find_email, ->(email) { like_param('email', email) }
  scope :find_phone, ->(phone) { like_param('phone', phone) }
  scope(:find_email_list, lambda do |email_list_id|
    joins(:email_subscriptions).where(email_subscriptions: { email_list_id: }) if email_list_id > 0
  end)
  scope :light, -> { select(%i[id phone]) }
  scope :without_email_problems, -> { where(email_problems_count: 0, raised_exception_at: nil) }
  scope :without_phone_problems, -> { where(phone_problems_count: 0) }
  scope :include_custom_vars, -> { includes(subscriber_custom_vars: :email_list_custom_var) }
  scope :for_email_sending, -> { without_email_problems.where.not(confirmed_at: nil) }
  scope(:for_email_sending_with_select, lambda do
    for_email_sending.select(%i[id name email authentication_token]).includes(:mobile_tokens)
  end)

  def self.search(params = {})
    select('subscribers.id, subscribers.name, email, phone, subscribers.created_at, confirmed_at, ' \
           'email_problems_count, phone_problems_count, raised_exception_at')
      .find_id(params[:id])
      .find_name(params[:name])
      .find_email(params[:email])
      .find_phone(params[:phone])
      .find_email_list(params[:email_list_id])
      .order(Arel.sql('subscribers.created_at DESC'))
  end

  def self.like_param(name, value)
    where("subscribers.#{name} LIKE ?", "%#{value}%") if value.present?
  end

  def reconfirm_if_no_problems
    return unless email_problems.all?(&:solved)

    update_attribute(:confirmed_at, Time.zone.now) if valid?
  end
end
