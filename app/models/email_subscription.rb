class EmailSubscription < ApplicationRecord
  belongs_to :email_list
  belongs_to :subscriber, optional: true # Rails bug, should not be optional
end
