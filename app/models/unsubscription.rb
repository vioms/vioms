class Unsubscription < ApplicationRecord
  belongs_to :subscriber
  belongs_to :email_list
  belongs_to :unsubscription_reason, optional: true

  default_scope { order(created_at: :desc) }

  scope(:authorized, lambda do |user|
    return if user.effective_admin?

    where(email_list_id: user.email_list_ids)
  end)
end
