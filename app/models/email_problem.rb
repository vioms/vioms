# frozen_string_literal: true

class EmailProblem < ApplicationRecord
  include ProblemTypesHelper::Types

  belongs_to :subscriber
  belongs_to :email_problem_name

  after_save :unconfirm_subscriber
  after_save :update_subscriber_problems_count

  scope :temporary_not_solved, lambda {
    joins(:email_problem_name).where(email_problem_names: { problem_type: PROBLEM_TEMPORARY }, solved: false)
  }
  scope :blocking, lambda {
    joins(:email_problem_name).where.not(email_problem_names: { problem_type: PROBLEM_INFO })
                              .where(solved: false)
  }

  private

  def unconfirm_subscriber
    return unless email_problem_name.problem_type.in?([PROBLEM_TEMPORARY, PROBLEM_PERMANENT])

    subscriber.update_attribute(:confirmed_at, nil)
  end

  def update_subscriber_problems_count
    subscriber.update_email_problems_count
    subscriber.save!(validate: false)
  end
end
