class EmailListCustomVar < ApplicationRecord
  belongs_to :email_list

  validates :name, :description, presence: true
end
