class TimeZone < ApplicationRecord
  has_many :subscribers, dependent: :nullify
  has_many :users, dependent: :nullify

  default_scope { order(priority: :desc, name: :asc) }

  validates :num, presence: true, uniqueness: true, numericality: true
  validates :name, :priority, presence: true
end
