# frozen_string_literal: true

class TelegramMessagesTelegramGroup < ApplicationRecord
  belongs_to :telegram_message
  belongs_to :telegram_group
end
