# frozen_string_literal: true

class SurveySubscriberAnswer < ApplicationRecord
  belongs_to :subscriber
  belongs_to :survey_question
  belongs_to :survey_answer, optional: true

  validates :subscriber_id, uniqueness: { scope: :survey_question_id }
end
