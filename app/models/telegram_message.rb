# frozen_string_literal: true

class TelegramMessage < ApplicationRecord
  belongs_to :telegram_group
  belongs_to :user, optional: true
  has_one :schedule, class_name: 'TelegramMessageSchedule', dependent: :destroy
  has_many :telegram_messages_telegram_groups, dependent: :delete_all
  has_many :included_telegram_groups, through: :telegram_messages_telegram_groups, source: 'telegram_group'

  scope :ordered, -> { order(deliver_at: :desc) }
  scope :by_message, ->(message) { where('message LIKE ?', "%#{message}%") }
  scope(:by_telegram_group, lambda do |telegram_group_id|
    return if telegram_group_id.to_i == 0

    where(telegram_group_id:)
  end)
  scope(:authorized, lambda do |user|
    return if user.effective_admin?

    where(telegram_group_id: user.telegram_group_ids)
  end)

  validates :message, presence: true, length: { maximum: 1024 }
  validate :delivery_time_in_the_future

  attr_accessor :image_destroy

  include ShrineUploaders::TelegramImage::Attachment(:image)

  def self.search(telegram_group_id:, message:)
    by_telegram_group(telegram_group_id)
      .by_message(message)
      .ordered
  end

  private

  def delivery_time_in_the_future
    return if sent || deliver_at > Time.zone.now

    errors.add(:deliver_at, :in_the_future)
  end
end
