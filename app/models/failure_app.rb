class FailureApp < Devise::FailureApp
  def i18n_message(default = nil)
    if flash[:alert].try(:match, 'devise.failure.invalid_token')
      flash.now[:alert] = I18n.t('devise.failure.invalid_token')
    else
      super
    end
  end
end
