class Poll < ApplicationRecord
  has_many :answers, -> { order(:position) }, dependent: :delete_all
  has_many :subscriber_answers, dependent: :delete_all

  validates :title, presence: true
end
