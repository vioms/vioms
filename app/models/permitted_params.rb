# frozen_string_literal: true

PermittedParams = Struct.new(:params, :controller, :model) do
  delegate :current_user, :policy, to: :controller

  def permitted_params
    if params[model].nil?
      {}
    else
      attrs = permitted_attributes(model)
      if attrs == :all
        params.require(model).permit!
      else
        allow_attrs = []
        attrs.map! do |attr|
          if attr.is_a?(Hash)
            new_hash = {}
            attr.each_pair do |nested_model, array|
              if array == :all
                allow_attrs << nested_model
              elsif array.empty?
                new_hash.merge!(add_associations(model, nested_model).last)
              else
                array.map! { |el| add_associations(nested_model, el) }.flatten!
                array.push(:_destroy, :id)
                new_hash[:"#{nested_model}_attributes"] = array
              end
            end
            new_hash
          else
            add_associations(model, attr)
          end
        end.flatten!
        p = params.require(model).permit(*attrs)
        allow_attrs.each do |attr|
          value = params[model][attr]
          p[attr] = value unless value.nil?
        end
        p
      end
    end
  end

  def add_associations(model, attr)
    klass = model.to_s.singularize.camelize.constantize
    reflection = klass.try(:reflect_on_association, attr)
    if reflection
      case reflection.macro
      when :belongs_to
        [attr, (reflection.respond_to?(:options) && reflection.options[:foreign_key]) || :"#{reflection.name}_id"]
      else
        [attr, { "#{reflection.name.to_s.singularize}_ids": [] }]
      end
    else
      attr
    end
  end

  def permitted_attributes(model)
    case model # rubocop:disable Style/HashLikeCase
    when :email_list_custom_var
      %i[name description]
    when :email_list_var_name
      %i[name description is_text with_editor skip_validation]
    when :digest_category
      %i[name template template_entry priority email_list]
    when :email_digest
      %i[num deliver_at email_list]
    when :subscriber
      [
        :name, :email, :blacklist_email, :blacklist_phone, :email_lists, :phone,
        :email_confirmation_task, { email_problems: :all }
      ]
    when :user
      %i[name username email login password password_confirmation remember_me email_lists
         email_list_exclusions telegram_groups is_admin is_import is_email_attachment
         is_cities is_pages is_custom_vars is_blocked
         max_attachments_size is_import_delete_old]
    when :sms_message
      %i[message subject_addition]
    when :time_zone
      %i[num priority name begin_at end_at zone_sign zone]
    when :smtp_setting_group
      %i[name email selected]
    when :smtp_setting
      %i[name value]
    when :sms_send_history_type
      %i[description attempts]
    when :page
      %i[name content show_in_navbar]
    when :unsubscription_reason
      %i[name]
    when :unsubscription
      %i[email_list_id unsubscription_reason comments]
    when :blacklist_email
      %i[email]
    when :news_item
      %i[title content date]
    when :banner_owner
      %i[name contact]
    when :banner_order
      %i[banner_name banner_owner start_on end_on]
    when :banner
      %i[banner_order image link placement priority]
    when :external_redirect
      %i[url is_relative is_simple param_name]
    when :poll
      %i[title]
    when :answer
      %i[title poll_id position]
    end
  end

  private

  # def get_model_name(object)
  #   object.class.to_s.underscore.to_sym
  # end

  def add_fields_on_create(fields, on_create)
    fields.push(on_create) if %w[new create copy].include?(params[:action])
    fields
  end
end
