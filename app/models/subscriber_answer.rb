# frozen_string_literal: true

class SubscriberAnswer < ApplicationRecord
  belongs_to :poll
  belongs_to :answer
  belongs_to :subscriber

  validates :subscriber_id, uniqueness: { scope: :poll_id }
end
