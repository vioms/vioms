# frozen_string_literal: true

class MailingRequestImage < ApplicationRecord
  belongs_to :mailing_request

  include ShrineUploaders::MailingRequestImage::Attachment(:image)
end
