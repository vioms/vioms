class EmailProblemName < ApplicationRecord
  include ProblemTypesHelper::Types
  include SimplePosition
  default_scope { order(:position) }

  has_many :email_problems, dependent: :delete_all

  validates :name, :problem_type, presence: true
end
