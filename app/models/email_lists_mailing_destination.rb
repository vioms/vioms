# frozen_string_literal: true

class EmailListsMailingDestination < ApplicationRecord
  belongs_to :email_list
  belongs_to :mailing_destination
end
