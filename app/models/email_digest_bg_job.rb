# frozen_string_literal: true

class EmailDigestBgJob < ApplicationRecord
  belongs_to :email_digest
end
