class SmsMessage < ApplicationRecord
  validates :message, presence: true

  belongs_to :sms_send_history_type
end
