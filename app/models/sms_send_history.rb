# frozen_string_literal: true

class SmsSendHistory < ApplicationRecord
  # TODO: check why optional
  belongs_to :subscriber, optional: true
  belongs_to :message_type, class_name: 'SmsSendHistoryType', foreign_key: 'sms_send_history_type_id'

  scope :find_phone, ->(*phone) { like_param('phone', phone.first) }
  scope :find_message_type, ->(*message_type) { where(message_type: message_type.first) if message_type.first.to_i > 0 }

  def self.search(phone:, message_type:)
    select(['sms_send_histories.id, sms_send_histories.phone, sms_send_histories.created_at', :subscriber_id,
            :sms_send_history_type_id, :expire_at])
      .includes(:subscriber)
      .find_phone(phone)
      .find_message_type(message_type)
      .order(Arel.sql('sms_send_histories.created_at DESC'))
  end

  def self.like_param(name, value)
    where("sms_send_histories.#{name} LIKE ?", "%#{value}%") if value.present?
  end

  def self.check(phone, subscriber_id, message_type, options = {})
    value = options[:value]
    expire_at = options[:expire_at]
    message_type_id = SmsSendHistoryType.message_type_id(message_type)

    history = find_or_initialize_by(phone:, sms_send_history_type_id: message_type_id)
    message_type = SmsSendHistoryType.find message_type_id
    if history.new_record? || history.expire_at.past? || history.attempts < message_type.attempts
      history.sms_send_history_type_id = message_type_id
      history.last_value = value
      if history.expire_at.past?
        history.attempts = 1
      else
        history.attempts += 1
      end
      ret = true
    else
      ret = if history.expire_at.past?
              history.attempts = 1
              true
            else
              false
            end
    end
    history.subscriber_id = subscriber_id
    history.expire_at = expire_at if expire_at
    history.save!
    ret
  end

  def self.query(phone, message_type)
    message_type_id = SmsSendHistoryType.message_type_id(message_type)
    history = find_by(phone:, message_type: message_type_id)
    if history && (history.expire_at.nil? || history.expire_at.future?)
      [false, history.last_value]
    else
      [true, nil]
    end
  end

  def self.forget(phone, message_type = nil)
    message_type_id = SmsSendHistoryType.message_type_id(message_type)
    conditions = { phone: }
    conditions[:message_type] = message_type_id if message_type_id
    where(sanitize_sql_for_conditions(conditions)).delete_all
  end
end
