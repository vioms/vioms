# frozen_string_literal: true

class EmailListVar < ApplicationRecord
  belongs_to :mailing
  belongs_to :email_list_var_name

  validates :value, presence: true, if: proc { |var| !var.email_list_var_name.skip_validation }
end
