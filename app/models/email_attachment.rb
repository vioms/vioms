class EmailAttachment < ApplicationRecord
  belongs_to :mailing

  mount_uploader :attachment, EmailAttachmentUploader
end
