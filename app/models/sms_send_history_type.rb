# frozen_string_literal: true

class SmsSendHistoryType < ApplicationRecord
  validates :attempts, numericality: { greater_than_or_equal_to: 1 }

  def self.message_type_id(message_type_name)
    select(:id).find_by(name: message_type_name).id if message_type_name
  end

  def self.message_type_name(message_type_id)
    select(:name).find_by(id: message_type_id).name if message_type_id
  end

  def self.get_description(message_name)
    select(:description).find_by(name: message_name).description
  end
end
