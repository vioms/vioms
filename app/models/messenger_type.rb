class MessengerType < ApplicationRecord
  validates :name, :char, presence: true
  validates :name, :char, uniqueness: true
  validates :char, length: { is: 1 }
end
