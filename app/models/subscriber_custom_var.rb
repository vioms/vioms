class SubscriberCustomVar < ApplicationRecord
  belongs_to :subscriber
  belongs_to :email_list_custom_var

  scope :authorized, ->(user) { where(email_list_id: user.email_list_ids) unless user.effective_admin? }
end
