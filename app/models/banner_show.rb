class BannerShow < ApplicationRecord
  belongs_to :banner, counter_cache: 'show_count'
end
