class BannerOrder < ApplicationRecord
  belongs_to :banner_owner
  has_many :banners

  validates :banner_name, :start_on, :end_on, presence: true

  def to_label
    banner_name
  end
end
