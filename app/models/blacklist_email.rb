class BlacklistEmail < ApplicationRecord
  validates :email, presence: true, uniqueness: true
end
