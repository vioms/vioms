# frozen_string_literal: true

class SurveyQuestion < ApplicationRecord
  QUESTION_TYPES = { string: 1, text: 2, options: 3, vote: 4 }.freeze

  enum :question_type, QUESTION_TYPES, suffix: true

  belongs_to :survey
  has_many :survey_answers, dependent: :delete_all
  has_many :survey_subscriber_answers, dependent: :delete_all

  validates :question, :question_type, presence: true
  validates :position, presence: true, uniqueness: { scope: :survey_id }

  scope :ordered, -> { order(:position) }
end
