# frozen_string_literal: true

class Mailing < ApplicationRecord
  include Admin::MailingsHelper
  include Validators::DeliveryTimeInFuture
  include PgSearch::Model

  CKEDITOR_FIELDS = %i[html_content].freeze

  attr_accessor :max_attachments_size, :digest_image_crop_x, :digest_image_crop_y, :digest_image_crop_w,
                :digest_image_crop_h, :stripo_template_group_id, :stripo_template_id, :creating

  belongs_to :email_list, optional: true, touch: true
  belongs_to :user, optional: true
  has_many :email_lists_mailings_inclusions
  has_many :included_email_lists, through: :email_lists_mailings_inclusions, source: 'email_list'
  has_many :email_lists_mailings_exclusions
  has_many :excluded_email_lists, through: :email_lists_mailings_exclusions, source: 'email_list'
  has_many :email_attachments, dependent: :destroy
  belongs_to :email_digest, optional: true
  belongs_to :digest_category, optional: true
  has_many :email_list_vars, dependent: :delete_all
  has_one :mailing_bg_job
  has_one :stripo_file_key, class_name: 'Stripo::FileKey', dependent: :nullify
  belongs_to :stripo_template_group, class_name: 'Stripo::TemplateGroup', optional: true
  belongs_to :stripo_template, class_name: 'Stripo::Template', optional: true
  has_many :mailing_views, dependent: :delete_all

  enum :editor_type, EmailList::EDITOR_TYPES, suffix: true

  accepts_nested_attributes_for :email_attachments, allow_destroy: true, reject_if: :all_blank
  accepts_nested_attributes_for :email_list_vars

  validates :email_list, :editor_type, presence: true
  validates :stripo_template_id, presence: true, if: proc { |m| m.stripo_editor_type? && m.creating }
  with_options unless: :creating do
    validates :deliver_at, :subject, presence: true
    validates :html_content, presence: true, if: proc { |mailing| !mailing.template_editor_type? }
    validates :digest_content, :digest_category, :email_digest, presence: true, if: :digest
    validates :subject, length: { maximum: 255 }
    validate :email_attachments_size
    validate :delivery_time
    validate :footer_var_present, if: :stripo_editor_type?
  end

  before_save :unpublish_digest
  after_update :crop_digest_image
  before_destroy :delete_mailing_bg_job

  scope :visible, -> { where(visible: true) }
  scope :authorized, ->(user) { where(email_list_id: user.email_list_ids) unless user.effective_admin? }
  scope(:find_subject, lambda do |*subject|
    subject = subject.first.to_s.strip
    Rails.logger.debug('%' + sanitize_sql_like(subject) + '%')
    where('subject LIKE ?', '%' + sanitize_sql_like(subject) + '%') if subject
  end)
  scope(:find_email_list, lambda do |*email_list_id|
    email_list_id = email_list_id.first.to_i
    where(email_list_id:) if email_list_id != 0
  end)
  scope(:archive, lambda do |*email_list|
    scope = select(%i[id subject deliver_at html_content email_list_id editor_type rendered_html])
      .visible
      .where('deliver_at<NOW() AND (published=true OR digest=true)')
      .order(deliver_at: :desc)
    if email_list&.first&.expiration_days
      scope.where(arel_table[:deliver_at].gt(email_list.first.expiration_days.days.ago))
    else
      scope
    end
  end)
  scope(:archive_visible, lambda do |subscriber_user|
    email_list_ids = EmailList
                       .visible_by_subscriber(subscriber_user)
                       .pluck(:id)
    if email_list_ids.present?
      archive.where(email_list_id: email_list_ids)
    else
      none
    end
  end)
  scope :feed_limit, -> { limit(100) }
  scope(:for_digest, lambda do |email_digest_id|
    where(digest: true, email_digest_id:)
  end)

  delegate(*Queries::Mailing.instance_methods(false), to: :queries)

  mount_uploader :digest_image, DigestImageUploader

  pg_search_scope :search_fulltext, against: { subject: 'A', plain_content: 'B' },
                                    using: { tsearch: { dictionary: 'russian', tsvector_column: 'searchable' } }

  def self.admin_search(subject:, email_list_id:)
    find_subject(subject)
      .find_email_list(email_list_id)
      .select(%i[id subject published deliver_at sent email_digest_id digest email_list_id sending send_failed
                 raised_exception_at editor_type])
      .order(deliver_at: :desc)
  end

  def create_bg_job
    create_mailing_bg_job!
    MailingJob.set(wait_until: deliver_at).perform_later(mailing_bg_job.id)
  end

  def resolve_external_redirects
    html_content_backup = html_content.dup

    ExternalRedirect.cache_records do
      Nokogiri.HTML(html_content).search('a')
              .pluck('href').uniq.each do |url|
        url_resolved = ExternalRedirect.replace_url(url)

        if url_resolved.present?
          html_content.gsub!(url, url_resolved)
          html_content.gsub!(url.gsub('&', '&amp;'), url_resolved)
        end
      end
    end

    self.html_content_backup = html_content_backup if html_content_backup != html_content
    save(validate: false)
  end

  private

  def queries
    @queries ||= Queries::Mailing.new(self)
  end

  def email_attachments_size
    total = email_attachments.map { |ea| ea.attachment.size }.sum
    return if total <= max_attachments_size.to_i * 1024

    size = total / 1024 - max_attachments_size
    errors.add(:email_attachment,
               "Размер прикрепленных файлов превышен на #{size} кб")
  end

  def delivery_time
    return if digest

    mailings = Mailing.all
    mailings = mailings.where.not(id:) if persisted?
    if mailings.where(deliver_at: (deliver_at - MINIMUM_INTERVAL..deliver_at + MINIMUM_INTERVAL)).any? ||
       EmailDigest.where(deliver_at: (deliver_at - MINIMUM_INTERVAL..deliver_at + MINIMUM_INTERVAL)).any?
      errors.add(:deliver_at, "Между соседними рассылками должен быть интервал не менее #{MINIMUM_INTERVAL / 60} минут")
    end
  end

  def footer_var_present
    errors.add(:html_content, :no_footer) unless html_content.include?('[FOOTER]')
  end

  def crop_digest_image
    digest_image.recreate_versions! if digest_image_crop_x.present?
  end

  def delete_mailing_bg_job
    mailing_bg_job.destroy if mailing_bg_job.present?
  end

  def unpublish_digest
    self.published = false if digest
    nil
  end
end
