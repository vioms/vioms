class Answer < ApplicationRecord
  belongs_to :poll
  has_many :subscriber_answers, dependent: :delete_all

  validates :title, presence: true
  validates :position, presence: true, uniqueness: { scope: :poll_id }

  before_save :set_position

  private

  def set_position
    self.position ||= 1 + self.class.maximum(:position).to_i
  end
end
