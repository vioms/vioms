class SubscriberImportEmailList < ApplicationRecord
  belongs_to :subscriber_import
  belongs_to :email_list
end
