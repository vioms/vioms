# frozen_string_literal: true

class MailingView < ApplicationRecord
  belongs_to :mailing
  belongs_to :subscriber

  validates :mailing_id, uniqueness: { scope: :subscriber_id }

  scope(:authorized, lambda do |user|
    return if user.effective_admin?

    joins(:mailing).where(mailings: { email_list_id: user.email_list_ids })
  end)
end
