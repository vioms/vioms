# frozen_string_literal: true

class EmailListVarName < ApplicationRecord
  belongs_to :email_list
  has_many :email_list_vars, dependent: :delete_all

  include SimplePosition
  default_scope { order(:position) }

  validates :name, :description, presence: true
  validates :is_text, presence: { message: I18n.t('errors.email_list_var_name.editor_without_text') }, if: :with_editor
end
