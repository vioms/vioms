class BannerClick < ApplicationRecord
  belongs_to :banner, counter_cache: 'click_count'
end
