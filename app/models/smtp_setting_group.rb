class SmtpSettingGroup < ApplicationRecord
  has_many :smtp_settings, dependent: :destroy

  validates :name, :email, presence: true
  validates :email, email: { mx: { a_fallback: true } }
  validate :one_group_is_selected

  include SimplePosition
  default_scope { order(:position) }

  scope :selected, -> { where(selected: true) }

  def to_yaml
    YAML.load("---\n" + smtp_settings.select(%i[name value]).map { |s| ":#{s.name}: #{s.value}" }.join("\n"))
  end

  def self.get_selected_email
    get_selected_group.email
  end

  def self.get_selected_group
    selected.first
  end

  protected

  def one_group_is_selected
    selected_groups = SmtpSettingGroup.selected
    return unless (selected_groups.size == 1) && (selected_groups.first.id == id) && (selected == false)

    errors.add(:selected, 'Хотя бы один сервер должен быть выбран')
  end
end
