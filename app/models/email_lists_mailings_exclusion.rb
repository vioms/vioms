# frozen_string_literal: true

class EmailListsMailingsExclusion < ApplicationRecord
  belongs_to :email_list
  belongs_to :mailing
end
