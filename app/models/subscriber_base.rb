# frozen_string_literal: true

class SubscriberBase < ApplicationRecord
  include ProblemTypesHelper::Types
  include ToEmail

  self.table_name = 'subscribers'

  has_many :email_subscriptions, foreign_key: 'subscriber_id'
  has_many :email_lists, through: :email_subscriptions
  has_many :cached_subscribers
  has_many :subscriber_custom_vars, foreign_key: 'subscriber_id'
  has_many :unsubscriptions, foreign_key: 'subscriber_id', dependent: :delete_all
  has_many :email_problems, foreign_key: 'subscriber_id', dependent: :delete_all
  has_many :subscriber_answers, foreign_key: 'subscriber_id', dependent: :nullify
  has_many :mailing_views, foreign_key: 'subscriber_id', dependent: :delete_all
  has_many :mobile_tokens, foreign_key: 'subscriber_id', dependent: :delete_all
  has_many :survey_subscriber_answers, foreign_key: 'subscriber_id', dependent: :nullify
  has_and_belongs_to_many :email_lists_invisible_checked, -> { where(visible: false, never_visible: false) },
                          class_name: 'EmailList', join_table: 'email_subscriptions', foreign_key: 'subscriber_id'

  validates :email, uniqueness: { message: 'такой адрес уже зарегистрирован', case_sensitive: false },
                    email: { mx: { a_fallback: true } }, allow_blank: true
  validates :email, length: { maximum: 255 }
  validates :phone, numericality: { only_integer: true }, length: { is: 10 },
                    uniqueness: { message: 'такой номер уже есть в базе' }, allow_blank: true
  validate :presence_of_email_or_phone
  validate :phone_validity

  before_validation :extract_phone
  before_save :convert_blank_email_and_phone_to_nil
  before_save :ensure_authentication_token
  before_destroy :notify_unsubscription_on_destroy

  attr_accessor :email_confirmation_task, :user, :phone_invalid, :email_list_ids_before

  delegate(*Queries::Subscriber.instance_methods(false), to: :queries)

  def convert_blank_email_and_phone_to_nil
    self.email = nil if email.blank?
    self.phone = nil if phone.blank?
  end

  def self.extract_email(email)
    email.to_s.strip.tr('<>', '')
  end

  def extract_phone
    extracted_phone = SubscriberBase.extract_phone(phone)
    self.phone = if extracted_phone == false
                   self.phone_invalid = true
                   nil
                 else
                   extracted_phone
                 end
  end

  def self.extract_phone(phone)
    return nil if phone.nil?
    return false if phone.match('@')

    phone = phone.gsub(/\D/, '')
    if phone.size == 11 && %w[7 8].include?(phone[0])
      phone[1..10]
    elsif phone.size == 10 && (phone[0] != '7' && phone[0] != '8')
      phone
    elsif phone.size == 12 && (phone[0, 2] == '38')
      phone[2..11]
    elsif phone.size == 0
      nil
    else
      false
    end
  end

  def self.is_login_email?(login)
    login =~ /@/ ? true : false
  end

  def name_or_email
    name.presence || email
  end

  def update_email_problems_count
    self.email_problems_count = email_problems.blocking.count
  end

  def do_email_confirmation_task
    email_confirmation_task = self.email_confirmation_task
    self.email_confirmation_task = nil
    case email_confirmation_task
    when '1'
      Subscriber::SendEmailConfirmationInstructions.call(self)
    when '2'
      Subscriber::ConfirmEmail.call(self)
    end
  end

  def notify_subscription
    return unless email_list_ids_before

    (email_list_ids - email_list_ids_before).each do |email_list_id|
      send_subscribed_notification(email_list_id)
    end
    (email_list_ids_before - email_list_ids).each do |email_list_id|
      send_unsubscribed_notification(email_list_id)
    end
  end

  private

  def presence_of_email_or_phone
    return if email.present? || phone.present?

    errors.add(:base, I18n.t('errors.subscriber.email_or_phone_required'))
  end

  def phone_validity
    errors.add(:phone, I18n.t('errors.subscriber.phone_invalid')) if phone_invalid
  end

  def notify_unsubscription_on_destroy
    email_list_ids.each do |email_list_id|
      send_unsubscribed_notification(email_list_id)
    end
  end

  def send_subscribed_notification(email_list_id)
    email_list = EmailList
                 .select(%i[id notify_subscribed notify_email])
                 .find(email_list_id)
    return unless email_list.notify_subscribed && email_list.notify_email.present?

    ViomsMailer.subscribed_notification(id, email_list_id).deliver!
  end

  def send_unsubscribed_notification(email_list_id)
    email_list = EmailList.select(%i[id notify_unsubscribed notify_email]).find(email_list_id)
    return unless email_list.notify_unsubscribed && email_list.notify_email.present?

    ViomsMailer.unsubscribed_notification(id, email_list_id).deliver!
  end

  def ensure_authentication_token
    return unless authentication_token.blank?

    self.authentication_token = generate_authentication_token
  end

  def generate_authentication_token
    loop do
      token = Devise.friendly_token
      break token unless SubscriberUser.where(authentication_token: token).first
    end
  end

  def reset_email_list_counter_cache
    email_lists.each do |email_list|
      next if email_list.subscribers_count_cache.blank?

      email_list.update_column(:subscribers_count_cache, nil)
    end
  end

  def queries
    @queries ||= Queries::Subscriber.new(self)
  end
end
