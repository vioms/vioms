class BannerOwner < ApplicationRecord
  validates :name, :contact, presence: true
end
