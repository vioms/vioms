# frozen_string_literal: true

class User < ApplicationRecord
  include ToEmail

  ROLES = {
    admin: 0,
    import: 1,
    cities: 4,
    pages: 6,
    custom_vars: 8,
    blocked: 9,
    email_attachment: 10,
    import_delete_old: 12
  }

  self.inheritance_column = 'dont_care'

  has_many :email_list_permissions
  has_many :email_lists, through: :email_list_permissions
  has_many :telegram_group_permissions
  has_many :telegram_groups, through: :telegram_group_permissions
  has_many :email_list_exclusion_permissions, dependent: :delete_all
  has_many :email_list_exclusions, through: :email_list_exclusion_permissions, source: 'email_list'
  has_and_belongs_to_many :sms_accounts, join_table: :sms_accounts_permissions
  has_and_belongs_to_many :mailing_destinations

  validates :name, :username, :email, presence: true, uniqueness: true
  validates :username, :email, length: { maximum: 255 }

  validates :password, :password_confirmation, presence: true, on: :create
  validates :password, confirmation: true

  validates :max_attachments_size, numericality: { greater_than: 0, if: :is_email_attachment? }

  attr_accessor :login

  devise :database_authenticatable, :recoverable, :rememberable, :trackable, :encryptable, encryptor: :md5hex,
                                                                                           authentication_keys: %i[login], reset_password_keys: %i[login]

  scope :ordered, -> { order(:name) }

  before_destroy :keep_last_admin_user

  delegate(*Queries::User.instance_methods(false), to: :queries)

  def active_for_authentication?
    super && !is_blocked?
  end

  def toggle_become_admin
    update_attribute(:become_admin, !become_admin)
  end

  def inactive_message
    if is_blocked?
      :blocked
    else
      super
    end
  end

  ROLES.keys.each do |role|
    class_eval(<<-EOS, __FILE__, __LINE__ + 1)
      def is_#{role}
        roles.is_role?(:#{role}, roles_mask)
      end
      alias_method :is_#{role}?, :is_#{role}

      def is_#{role}=(val)
        self.roles_mask = roles.set_role(:#{role}, roles_mask, val)
      end
    EOS
  end

  private

  def queries
    @queries ||= Queries::User.new(self)
  end

  def roles
    @roles ||= RolesService.new(ROLES)
  end

  def self.find_first_by_auth_conditions(warden_conditions)
    conditions = warden_conditions.dup
    if (login = conditions.delete(:login))
      where(conditions).where(['lower(username) = :value OR lower(email) = :value', { value: login.downcase }]).first
    else
      where(conditions).first
    end
  end

  def keep_last_admin_user
    raise 'Can\'t delete last user' if User.select(:roles_mask).to_a.keep_if(&:is_admin?).size == 1
  end

  def self.find_for_database_authentication(warden_conditions)
    conditions = warden_conditions.dup
    login = conditions.delete(:login)
    where(conditions).where(['username = :value OR email = :value', { value: login }]).first
  end
end
