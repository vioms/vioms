# frozen_string_literal: true

class EmailListsMailingsInclusion < ApplicationRecord
  belongs_to :email_list
  belongs_to :mailing
end
