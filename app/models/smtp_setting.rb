class SmtpSetting < ApplicationRecord
  belongs_to :smtp_setting_group

  validates :name, :value, presence: true
end
