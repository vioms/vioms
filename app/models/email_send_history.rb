# frozen_string_literal: true

class EmailSendHistory < ApplicationRecord
  belongs_to :subscriber

  scope :find_email, ->(*email) { like_param('email', email.first) }

  def self.search(email:)
    select(['email_send_histories.id, email_send_histories.email, email_send_histories.created_at', :subscriber_id])
      .includes(:subscriber).order(Arel.sql('email_send_histories.created_at DESC'))
      .find_email(email)
  end

  def self.like_param(name, value)
    where("email_send_histories.#{name} LIKE ?", "%#{value}%") if value.to_s.strip != ''
  end

  def self.check(email, subscriber_id, options = {})
    value = options[:value]

    history = find_or_initialize_by(email:)
    new_record = history.new_record?
    if new_record
      history.subscriber_id = subscriber_id
      history.last_value = value
    end
    history.save!
    new_record
  end

  def self.query(email)
    history = find_by(email:)
    if history
      [false, history.last_value]
    else
      [true, nil]
    end
  end

  def self.forget(email)
    where(email:).delete_all
  end
end
