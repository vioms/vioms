# frozen_string_literal: true

module Validators
  module DeliveryTimeInFuture
    extend ActiveSupport::Concern

    MINIMUM_INTERVAL = 2.minutes

    included do
      validate :deliver_at_time
    end

    private

    def deliver_at_time
      return if sent || try(:creating) || deliver_at > Time.zone.now

      errors.add(:deliver_at, 'Время отправки должно быть в будущем')
    end
  end
end
