# frozen_string_literal: true

class SurveyAnswer < ApplicationRecord
  belongs_to :survey_question

  validates :answer, presence: true
  validates :position, presence: true, uniqueness: { scope: :survey_question_id }

  scope :ordered, -> { order(:position) }
end
