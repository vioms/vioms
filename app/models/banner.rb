class Banner < ApplicationRecord
  belongs_to :banner_order
  has_many :clicks, class_name: 'BannerClick', dependent: :delete_all
  has_many :shows, class_name: 'BannerShow', dependent: :delete_all

  validates :image, :link, :priority, :placement, presence: true

  mount_uploader :image, BannerImageUploader

  BANNER_PLACEMENTS = {
    1 => 'Слева',
    2 => 'Справа',
    3 => 'Снизу'
  }

  scope :show, lambda { |placement|
    joins(:banner_order).where(placement:).where('start_on<=NOW() AND end_on>=NOW()').order(priority: :desc)
  }
end
