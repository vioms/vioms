module ToEmail
  extend ActiveSupport::Concern

  def to_email
    "#{name} <#{email}>"
  end
end
