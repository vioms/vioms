module SmsMailerHelper
  extend ActiveSupport::Concern

  include ApplicationHelper

  def wrap_sms_message(message)
    "РАМ***#{message}***".html_safe
  end

  def get_message(message_name, raw_token = nil)
    message_type_id = SmsSendHistoryType.message_type_id(message_name)
    message = SmsMessage.find_sole_by(sms_send_history_type_id: message_type_id).message
    message.sub!('%<reset_password_token>s', raw_token.present? ? raw_token.to_s : '')
    message
  end

  def subject_for_message(message_name)
    '16108' + build_time_zone_str + ' site' + build_subject_addition(message_name)
  end

  # "161089 site W01.09.2008 10:05 N00000001"
  def subject_for_sending(user, sms_sending)
    '16108' + build_time_zone_str(user) + ' site' +
      build_time_str(sms_sending) +
      build_report_str(user, sms_sending.sender_user) +
      build_num_str(sms_sending)
  end

  def build_time_zone_str(user = nil)
    user.nil? ? '0' : user.time_zone.num.to_s
  end

  def build_report_str(user, sender_user)
    user = sender_user if sender_user.present? && user.effective_admin?
    return '' if user.sms_phone.blank? || user.blank?

    ' R' + user.sms_phone
  end

  def build_time_str(sms_sending)
    return '' if sms_sending.nil?

    ' W' + sms_sending.deliver_at.to_fs(:rus_datetime)
  end

  def build_num_str(sms_sending)
    return '' if sms_sending.nil?

    ' N' + zerofill(sms_sending.num, 8).to_s
  end

  def find_subject_addition(message_name)
    message_type_id = SmsSendHistoryType.message_type_id(message_name)
    SmsMessage.find_sole_by(sms_send_history_type_id: message_type_id).subject_addition.to_s
  end

  def build_subject_addition(message_name)
    subject_addition = find_subject_addition(message_name)
    return '' if subject_addition.blank?

    ' ' + subject_addition
  end
end
