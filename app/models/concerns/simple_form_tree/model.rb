module SimpleFormTree
  module Model
    extend ActiveSupport::Concern

    included do
      has_ancestry
    end

    def name_for_selects
      NameWithDepthIndent.call(self)
    end
  end
end
