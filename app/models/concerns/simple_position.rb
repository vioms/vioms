# frozen_string_literal: true

module SimplePosition
  extend ActiveSupport::Concern

  included do
    before_save :set_position
  end

  private

  def set_position
    self.position ||= 1 + self.class.maximum(:position).to_i
  end
end
