class DigestCategory < ApplicationRecord
  has_many :mailings, dependent: :restrict_with_error
  belongs_to :email_list

  validates :name, :template, :template_entry, presence: true

  default_scope { order(priority: :desc) }

  scope :authorized, ->(user) { where(email_list_id: user.email_list_ids) unless user.effective_admin? }
end
