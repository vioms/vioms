# frozen_string_literal: true

class Contact
  include ActiveModel::Validations
  include ActiveModel::Conversion
  extend ActiveModel::Naming

  validates :name, :email, :phone, :question, presence: true
  validates :email, email: { mx: { a_fallback: true } }

  attr_accessor :name, :email, :phone, :subject, :question

  def initialize(attributes = {})
    attributes.each do |name, value|
      send("#{name}=", value)
    end
  end

  def persisted?
    false
  end
end
