class UnsubscriptionReason < ApplicationRecord
  include SimplePosition
  default_scope { order(:position) }

  has_many :unsubscriptions, dependent: :nullify

  validates :name, presence: true
end
