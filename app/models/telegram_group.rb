# frozen_string_literal: true

class TelegramGroup < ApplicationRecord
  validates :name, :chat_id, :bot_token, presence: true
  validates :chat_id, numericality: { only_integer: true, less_than: 0 }

  has_many :telegram_messages, dependent: :destroy
  has_many :telegram_group_permissions, dependent: :destroy
  has_many :users, through: :telegram_group_permissions

  scope :ordered, -> { order(:name) }
  scope :visible, -> { where(visible: true) }
  scope :authorized, ->(user) { where(id: user.telegram_group_ids) unless user.effective_admin? }

  def to_label
    name
  end

  def to_label_truncated
    to_label.truncate(45)
  end
end
