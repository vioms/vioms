class PermittedFormBuilder < SimpleForm::FormBuilder
  alias parent_input input

  attr_accessor :authorized_by_assoc, :parent_attribute_name

  def input(attribute_name, options = {}, &)
    return unless @authorized_by_assoc || options[:permitted] || permitted_field?(attribute_name)

    @authorized_by_assoc = nil
    super
  end

  def association(attribute_name, options = {}, &)
    return unless permitted_field?(attribute_name)

    @authorized_by_assoc = true
    super
  end

  def fields_for(attribute_name, *args, &)
    @parent_attribute_name = attribute_name
    ret = super if permitted_field?(attribute_name)
    @parent_attribute_name = nil
    ret
  end

  private

  def permitted_field?(attribute_name)
    collection = get_options[:permitted_attributes]
    return false if collection.nil?

    collection == :all || collection.include?(attribute_name) || test_hash_values(collection, attribute_name)
  end

  def test_hash_values(collection, attribute_name)
    collection.each do |el|
      next unless el.is_a?(Hash)

      parent_attribute_name = options[:parent_builder].try(:parent_attribute_name)
      if el[parent_attribute_name] == :all
        return true
      elsif parent_attribute_name && el.has_key?(parent_attribute_name)
        return el[parent_attribute_name].include?(attribute_name)
      else
        el.each_key { |key| return true if key == attribute_name }
      end
    end
    false
  end

  def get_options
    if options[:parent_builder]
      options[:parent_builder].options
    else
      options
    end
  end
end
