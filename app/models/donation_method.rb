# frozen_string_literal: true

class DonationMethod < ApplicationRecord
  validates :name, presence: true
end
