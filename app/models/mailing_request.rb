# frozen_string_literal: true

class MailingRequest < ApplicationRecord
  MAILING_STATUSES = { awaiting_admin_response: 1, awaiting_subscriber_response: 2, awaiting_donation: 3,
                       processed: 4 }.freeze
  MAILING_TYPES = { private: 1, commercial: 2 }.freeze

  has_many :images, class_name: 'MailingRequestImage', dependent: :destroy
  has_many :messages, class_name: 'MailingRequestMessage', dependent: :delete_all
  belongs_to :donation_method, optional: true
  belongs_to :subscriber
  has_and_belongs_to_many :email_lists
  belongs_to :mailing, optional: true

  scope :ordered, -> { order(created_at: :desc) }
  scope :authorized, ->(subscriber) { where(subscriber:) }

  validates :subject, :body, :mailing_type, presence: true

  accepts_nested_attributes_for :images, allow_destroy: true, reject_if: :all_blank

  enum :status, MAILING_STATUSES, suffix: true
  enum :mailing_type, MAILING_TYPES, suffix: true
end
