# frozen_string_literal: true

class MobileToken < ApplicationRecord
  belongs_to :subscriber

  enum :device_type, { android: 0, ios: 1 }

  validates :device_token, presence: true, if: -> { device_type.present? }
end
