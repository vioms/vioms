class City < ApplicationRecord
  ORDER_BY = 'priority DESC, name'

  validates :name, presence: true
  validates :priority, presence: true, numericality: true

  scope :ordered, -> { order(ORDER_BY) }

  include SimpleFormTree::Model
end
