require 'nested_form/builder_mixin'

class PermittedNestedFormBuilder < PermittedFormBuilder
  include ::NestedForm::BuilderMixin
end
