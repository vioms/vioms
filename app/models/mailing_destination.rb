# frozen_string_literal: true

class MailingDestination < ApplicationRecord
  has_and_belongs_to_many :email_lists
  has_and_belongs_to_many :users

  validates :name, presence: true
end
