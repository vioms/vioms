# frozen_string_literal: true

module Queries
  module Stripo
    class FileKey
      def initialize(file_key)
        @file_key = file_key
      end

      def stripo_key
        "pluginId_#{@file_key.plugin_id}_email_#{@file_key.email_id}"
      end
    end
  end
end
