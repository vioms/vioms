# frozen_string_literal: true

module Queries
  class Mailing
    def initialize(mailing)
      @mailing = mailing
    end

    def expired?
      return @expired unless @expired.nil?

      days_ago = @mailing.email_list.expiration_days&.days&.ago
      @expired = !days_ago.nil? && @mailing.deliver_at < days_ago
    end
  end
end
