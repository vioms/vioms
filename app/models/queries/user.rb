# frozen_string_literal: true

module Queries
  class User
    def initialize(user)
      @user = user
    end

    def allowed_email_list_id?(email_list_id)
      allowed_email_list_ids.include?(email_list_id)
    end

    def allowed_sms_list_id?(sms_list_id)
      @user.sms_list_ids.include?(sms_list_id)
    end

    def allowed_telegram_group_id?(telegram_group_id)
      @user.telegram_group_ids.include?(telegram_group_id)
    end

    def allowed_sms_account_id?(sms_account_id)
      @user.sms_account_ids.include?(sms_account_id)
    end

    def allowed_subscriber_email?(subscriber)
      effective_admin? || allowed_email_list_ids.intersect?(subscriber.email_list_ids)
    end

    def allowed_subscriber_phone?
      effective_admin?
    end

    def effective_admin?
      @user.is_admin? && @user.become_admin?
    end

    private

    def allowed_email_list_ids
      @allowed_email_list_ids ||= @user.email_list_ids
    end
  end
end
