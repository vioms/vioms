# frozen_string_literal: true

module Queries
  class Subscriber
    def initialize(subscriber)
      @subscriber = subscriber
    end

    def email_confirmation_token
      EmailSendHistory.select(:last_value).find_by(email: @subscriber.email).try(:last_value)
    end

    def subscribed_email_lists
      @subscribed_email_lists ||= @subscriber.email_lists
    end

    def unsubscribed_email_lists
      EmailList.visible.without_never_visible - subscribed_email_lists
    end
  end
end
