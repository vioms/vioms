# frozen_string_literal: true

class Survey < ApplicationRecord
  has_many :survey_questions, dependent: :destroy

  validates :title, presence: true

  scope :visible, -> { where(visible: true) }
  scope :announce, -> { where(announce: true) }
end
