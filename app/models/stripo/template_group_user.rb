# frozen_string_literal: true

module Stripo
  class TemplateGroupUser < ApplicationRecord
    self.table_name = 'stripo_template_group_users'

    belongs_to :template_group, class_name: 'Stripo::TemplateGroup', foreign_key: 'stripo_template_group_id'
    belongs_to :user
  end
end
