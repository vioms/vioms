# frozen_string_literal: true

module Stripo
  class File < ApplicationRecord
    self.table_name = 'stripo_files'

    belongs_to :key, class_name: 'Stripo::FileKey', foreign_key: 'stripo_file_key_id'

    validates :file, presence: true
    validates :cached_url, presence: true, if: :persisted?
    validates :cached_url, uniqueness: true

    delegate :stripo_key, to: :key
    delegate :plugin_id, :email_id, to: :key, prefix: true

    include ShrineUploaders::StripoFile::Attachment(:file)
  end
end
