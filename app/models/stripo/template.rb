# frozen_string_literal: true

module Stripo
  class Template < ApplicationRecord
    self.table_name = 'stripo_templates'

    attr_accessor :mailing_id

    has_one :stripo_file_key, class_name: 'Stripo::FileKey', foreign_key: 'stripo_template_id', dependent: :nullify
    belongs_to :template_group, class_name: 'Stripo::TemplateGroup', foreign_key: 'stripo_template_group_id'

    validates :name, :html, :css, presence: true

    scope :ordered, -> { order(:name) }

    include ShrineUploaders::StripoTemplate::Attachment(:preview)
  end
end
