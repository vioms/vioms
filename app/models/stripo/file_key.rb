# frozen_string_literal: true

module Stripo
  class FileKey < ApplicationRecord
    self.table_name = 'stripo_file_keys'

    validates :plugin_id, presence: true
    validates :email_id, uniqueness: { scope: :plugin_id }

    has_many :files, class_name: 'Stripo::File', foreign_key: 'stripo_file_key_id', dependent: :restrict_with_error
    belongs_to :mailing, optional: true
    belongs_to :stripo_template, class_name: 'Stripo::Template', optional: true

    delegate(*Queries::Stripo::FileKey.instance_methods(false), to: :queries)

    private

    def queries
      @queries ||= Queries::Stripo::FileKey.new(self)
    end
  end
end
