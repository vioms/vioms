# frozen_string_literal: true

module Stripo
  class TemplateGroup < ApplicationRecord
    ORDER_BY = :position

    self.table_name = 'stripo_template_groups'

    validates :name, presence: true
    validates :user, presence: true, if: proc { |group| group.parent.nil? && !group.public }

    include SimplePosition
    include SimpleFormTree::Model

    belongs_to :parent, class_name: 'Stripo::TemplateGroup', optional: true
    has_many :templates, class_name: 'Stripo::Template', foreign_key: 'stripo_template_group_id'
    belongs_to :user, -> { ordered }, optional: true
    has_many :email_list_permissions, class_name: 'Stripo::TemplateGroupEmailList',
                                      foreign_key: 'stripo_template_group_id',
                                      inverse_of: :template_group, dependent: :delete_all
    has_many :email_lists, through: :email_list_permissions
    has_many :user_permissions, class_name: 'Stripo::TemplateGroupUser', foreign_key: 'stripo_template_group_id',
                                inverse_of: :template_group, dependent: :delete_all
    has_many :users, through: :user_permissions

    scope :ordered, -> { order(ORDER_BY) }
    scope :authorized, lambda { |user|
      return if user.effective_admin?

      with_joined_tables.where(public: true)
                        .or(with_joined_tables.by_owner(user))
                        .or(by_authorized_email_lists(user))
                        .or(by_authorized_users(user))
                        .distinct
    }
    scope :by_owner, ->(user) { where(user_id: user.id) }
    scope :by_authorized_email_lists, lambda { |user|
      with_joined_tables.where(stripo_template_group_email_lists: { email_list_id: user.email_list_ids })
    }
    scope :by_authorized_users, ->(user) { with_joined_tables.where(stripo_template_group_users: { user_id: user.id }) }
    scope :with_joined_tables, lambda {
      left_outer_joins(:email_list_permissions)
        .left_outer_joins(:user_permissions)
    }
  end
end
