class NewsItem < ApplicationRecord
  default_scope { order(date: :desc, id: :desc) }

  validates :title, :content, :date, presence: true

  scope :latest, -> { limit(3) }

  extend FriendlyId
  friendly_id :title, use: %i[slugged finders]

  def normalize_friendly_id(string)
    Russian.translit(string).to_url
  end
end
