class ExternalRedirect < ApplicationRecord
  validates :url, presence: true, uniqueness: true
  validates :param_name, presence: true, if: :is_simple

  def self.cache_records
    raise 'Records already cached' unless @ers.nil?

    @ers = ExternalRedirect.all
    yield
    @ers = nil
  end

  def self.replace_url(replace_url)
    if @ers.nil?
      raise 'Records not cached. Please use ExternalRedirect.cache_records { ExternalRedirect.replace_url(url) }'
    end

    url_resolved = replace_url
    begin
      replaced = false
      @ers.each do |er|
        url_resolved_new = er.replace_url(url_resolved)
        next if url_resolved_new.blank?

        replaced = true
        url_resolved = url_resolved_new
        break
      end
    end while replaced

    if replace_url == url_resolved
      nil
    else
      url_resolved
    end
  end

  def replace_url(replace_url)
    url_escaped = Regexp.escape(url)
    if replace_url.blank? || !replace_url.match(%r{^(http)?s?(://)?.*?#{url_escaped}}i) ||
       (is_relative && !replace_url.match(/^#{url_escaped}/i))
      return nil
    end

    begin
      url_new = if is_simple
                  CGI.parse(URI.parse(replace_url).query)[param_name].first
                else
                  ExternalRedirect.internet_resolve(replace_url)
                end
    rescue Exception
      url_new = nil
    end

    url_new = nil if replace_url == url_new

    unless url_new.nil?
      begin
        url_new = url_new.encode(Encoding::UTF_8)
      rescue Encoding::UndefinedConversionError
        url_new = nil
      rescue Encoding::InvalidByteSequenceError
        url_new = nil
      end
    end

    if url_new.nil?
      increment(:unresolved_count)
    else
      increment(:resolved_count)
    end
    save(validate: false)

    url_new
  end

  def self.internet_resolve(uri_str, max_attempts = 5, timeout = 10, agent = 'curl/7.47.0')
    attempts = 0
    cookie = nil

    until attempts >= max_attempts
      url = URI.parse(uri_str)
      http = Net::HTTP.new(url.host, url.port)
      http.open_timeout = timeout
      http.read_timeout = timeout
      path = url.path
      path = '/' if path == ''
      path += '?' + url.query unless url.query.nil?
      path += '#' + url.fragment unless url.fragment.nil?

      params = { 'User-Agent' => agent, 'Accept' => '*/*' }
      params['Cookie'] = cookie unless cookie.nil?
      request = Net::HTTP::Get.new(path, params)

      if url.instance_of?(URI::HTTPS)
        http.use_ssl = true
        http.verify_mode = OpenSSL::SSL::VERIFY_NONE
      end
      begin
        response = http.request(request)
      rescue Net::OpenTimeout
        sleep(timeout)
        attempts += 1
        next
      rescue SocketError => e
        break if e.message == 'getaddrinfo: nodename nor servname provided, or not known'

        raise
      end

      case response
      when Net::HTTPSuccess then break     # 2xx
      when Net::HTTPClientError then break # 4xx
      when Net::HTTPServerError then break # 5xx
      when Net::HTTPRedirection            # 3xx
        location = response['Location']
        cookie = response['Set-Cookie']
        begin
          new_uri = URI.parse(location)
        rescue URI::InvalidURIError
          uri_str = location
          break
        end
        uri_str = if new_uri.relative?
                    url.to_s + location
                  else
                    location
                  end
        break
      else
        raise 'Unexpected response: ' + response.inspect
      end
    end
    raise 'Too many http redirects' if attempts == max_attempts

    uri_str
  end
end
