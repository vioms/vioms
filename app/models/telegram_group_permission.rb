# frozen_string_literal: true

class TelegramGroupPermission < ApplicationRecord
  belongs_to :telegram_group
  belongs_to :user
end
