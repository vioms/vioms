# frozen_string_literal: true

class MailingRequestMessage < ApplicationRecord
  belongs_to :user, optional: true
  belongs_to :mailing_request
  belongs_to :donation_method, optional: true

  scope :ordered, -> { order(created_at: :desc) }

  AUTHOR = { subscriber: 1, admin: 2 }.freeze

  enum :author, AUTHOR, suffix: true
  enum :old_status, MailingRequest::MAILING_STATUSES, suffix: true
  enum :new_status, MailingRequest::MAILING_STATUSES, suffix: true

  attr_accessor :donation_method_id, :donated_at

  validates :author, presence: true
  validates :message, presence: true, if: proc { |mrm| mrm.subscriber_author? }
  validates :donation_method_id, :donated_at, presence: true,
                                              if: proc { |mrm| mrm.mailing_request.awaiting_donation_status? }
end
