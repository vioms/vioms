# frozen_string_literal: true

class EmailDigest < ApplicationRecord
  include Validators::DeliveryTimeInFuture

  has_many :mailings, dependent: :nullify
  belongs_to :email_list
  belongs_to :user, optional: true
  has_one :email_digest_bg_job

  validates :num, :deliver_at, presence: true
  validates :num, uniqueness: { scope: %i[year email_list_id] }
  validate :delivery_time

  before_validation :set_year
  before_destroy :delete_email_digest_bg_job

  default_scope { order(deliver_at: :desc) }
  scope :non_published, -> { where(published: false) }
  scope :authorized, ->(user) { where(email_list_id: user.email_list_ids) unless user.effective_admin? }

  def to_s
    "№#{num} (#{year})"
  end

  def self.next_num(email_list)
    1 + where(email_list_id: email_list, year: Date.today.year).maximum(:num).to_i
  end

  def create_bg_job
    create_email_digest_bg_job!
    EmailDigestJob.set(wait_until: deliver_at).perform_later(email_digest_bg_job.id)
  end

  private

  def delivery_time
    email_digests = EmailDigest.all
    email_digests = email_digests.where.not(id:) if persisted?
    if Mailing.where(deliver_at: (deliver_at - MINIMUM_INTERVAL..deliver_at + MINIMUM_INTERVAL)).any? ||
       email_digests.where(deliver_at: (deliver_at - MINIMUM_INTERVAL..deliver_at + MINIMUM_INTERVAL)).any?
      errors.add(:deliver_at, "Между соседними рассылками должен быть интервал не менее #{MINIMUM_INTERVAL / 60} минут")
    end
  end

  def set_year
    self.year = deliver_at.year
  end

  def delete_email_digest_bg_job
    email_digest_bg_job.destroy if email_digest_bg_job.present?
  end
end
