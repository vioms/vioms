# frozen_string_literal: true

class EmailListPermission < ApplicationRecord
  belongs_to :email_list
  belongs_to :user
end
