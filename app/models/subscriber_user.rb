# frozen_string_literal: true

class SubscriberUser < SubscriberBase
  devise :database_authenticatable, :recoverable, :lockable, :rememberable, :trackable, :encryptable,
         encryptor: :md5hex, authentication_keys: %i[login], reset_password_keys: %i[login]

  validates :password, :password_confirmation, presence: true, if: proc { |s| s.set_password == '1' }
  validates :password, confirmation: true, if: proc { |s| s.set_password == '1' }
  validates :accept_privacy_policy, acceptance: true

  attr_accessor :login, :reset_password_token_raw, :set_password

  after_create :reset_email_list_counter_cache

  def authenticate(password)
    return self if valid_password?(password)

    false
  end

  def self.find_first_by_auth_conditions(warden_conditions)
    conditions = warden_conditions.dup
    if login = conditions.delete(:login)
      login = if Subscriber.is_login_email?(login)
                login.strip.downcase
              else
                Subscriber.extract_phone(login)
              end
      where(conditions).where(['lower(email) = :value OR phone = :value', { value: login }]).first
    else
      where(conditions).first
    end
  end

  def self.send_reset_password_instructions(attributes = {})
    subscriber = find_or_initialize_with_errors(reset_password_keys, attributes, :not_found)
    if subscriber.persisted?
      if Subscriber.is_login_email?(attributes[:login])
        subscriber.send_reset_password_instructions
      elsif SmsSendHistory.check(subscriber.phone, subscriber.id, 'password_recovery')
        raw, enc = loop do
          raw = SecureRandom.random_number(9_999_999_999)
          enc = Devise.token_generator.digest(SubscriberUser, :reset_password_token, raw)
          break [raw, enc] unless to_adapter.find_first({ reset_password_token: enc })
        end
        subscriber.reset_password_token_raw = raw
        subscriber.reset_password_token = enc
        subscriber.reset_password_sent_at = Time.now.utc
        subscriber.save validate: false
        # TODO: send SMS
        SmsMailer.password_recovery(subscriber, raw).deliver
      end
    end
    subscriber
  end
end
