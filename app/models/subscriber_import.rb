# frozen_string_literal: true

class SubscriberImport < ApplicationRecord
  attr_accessor :subscribers_data, :name_column, :first_name_column, :last_name_column, :email_column, :phone_column

  has_many :subscriber_import_email_lists, dependent: :delete_all
  has_many :email_lists, through: :subscriber_import_email_lists
  has_many :subscriber_import_people, dependent: :delete_all
  belongs_to :user

  default_scope { order(created_at: :desc) }
  scope :authorized, lambda { |user|
    return if user.effective_admin?

    with_joined_tables.where(subscriber_import_email_lists: { email_list_id: user.email_list_ids })
                      .or(with_joined_tables.where(subscriber_import_sms_lists: { sms_list_id: user.sms_list_ids }))
  }
  scope(:with_joined_tables, lambda do
    left_outer_joins(:subscriber_import_email_lists)
      .left_outer_joins(:subscriber_import_sms_lists)
  end)

  validates :subscribers_data, presence: true
  validates :name_column, :first_name_column, :last_name_column, :email_column, :phone_column,
            numericality: { only_integer: true, greater_than: 0 }, allow_blank: true
  validates :email_column, presence: true, if: proc { _1.phone_column.blank? }
  validates :phone_column, presence: true, if: proc { _1.email_column.blank? }
  validates :name_column, absence: true, if: proc { _1.first_name_column.present? }
  validates :name_column, absence: true, if: proc { _1.last_name_column.present? }
  validates :first_name_column, presence: true, if: proc { _1.last_name_column.present? }
  validates :last_name_column, presence: true, if: proc { _1.first_name_column.present? }
  validates :confirm_email, acceptance: true
  validate :columns_different
  validate :must_have_lists

  ERRORS = {
    1 => 'Некорректный телефон',
    2 => 'Склеивание невозможно, потому что списки накладываются', # Legacy, not used.
    3 => 'Найден по email, но телефон отличается', # Legacy, not used.
    4 => 'Не указан ни email, ни телефон',
    5 => 'Ошибка валидации'
  }.freeze

  ERROR_IDS = {
    bad_phone: 1,
    cannot_merge: 2,
    different_phone: 3,
    no_data: 4,
    not_valid: 5
  }

  def decrease_errors_count!
    update_attribute(:errors_count, errors_count - 1)
  end

  private

  def columns_different
    return if [name_column, first_name_column, last_name_column, email_column, phone_column].compact_blank!.uniq!.nil?

    errors.add(:name_column, 'Столбики должны быть разными')
  end

  def must_have_lists
    return unless email_lists.empty?

    errors.add(:base, 'Необходимо выбрать хотя бы один список')
  end
end
