# frozen_string_literal: true

class EmailListExclusionPermission < ApplicationRecord
  belongs_to :user
  belongs_to :email_list
end
