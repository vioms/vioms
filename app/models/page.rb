class Page < ApplicationRecord
  CKEDITOR_FIELDS = %i[content]

  validates :name, :content, presence: true
  validates :position, uniqueness: true

  extend FriendlyId
  friendly_id :name, use: %i[slugged finders]

  include SimplePosition

  scope :show_in_navbar, -> { select(:id, :name, :slug).where(show_in_navbar: true).order(:position) }

  def normalize_friendly_id(string)
    Russian.translit(string).to_url
  end
end
