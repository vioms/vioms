# frozen_string_literal: true

class Setting < ApplicationRecord
  validates :value, presence: true, if: proc { |s| s.value != false }

  def self.get_setting(setting_name)
    setting = Setting.find_by(name: setting_name)
    case setting.value_type
    when 'boolean'
      setting.value == '1'
    when 'integer'
      setting.value.to_i
    else
      setting.value
    end
  end

  def self.put_setting(name, value)
    setting = Setting.find_by(name:)
    if setting.value_type == 'boolean'
      value = value ? '1' : '0'
    end
    setting.update_attribute(:value, value)
  end
end
