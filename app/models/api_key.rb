# frozen_string_literal: true

class APIKey < ApplicationRecord
  belongs_to :user

  validates :key, presence: true, uniqueness: true

  scope :visible, lambda { |user|
    where(user:) unless user.effective_admin?
  }
end
