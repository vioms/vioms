class EmailDigestJob < ApplicationJob
  queue_as :email

  def perform(email_digest_bg_job_id)
    email_digest_bg_job = EmailDigestBgJob.find(email_digest_bg_job_id)
    email_digest = email_digest_bg_job.email_digest
    email_digest_bg_job.destroy

    EmailDigest::Deliver.call(email_digest)
  rescue ActiveRecord::RecordNotFound
    nil
  end
end
