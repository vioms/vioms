# frozen_string_literal: true

class TelegramMessageJob < ApplicationJob
  queue_as :default

  def perform(schedule_id)
    schedule = TelegramMessageSchedule.find(schedule_id)
    telegram_message = schedule.telegram_message
    schedule.destroy

    Telegram::Deliver.call(telegram_message)
  rescue ActiveRecord::RecordNotFound
    nil
  end
end
