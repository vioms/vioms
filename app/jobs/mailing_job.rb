# frozen_string_literal: true

class MailingJob < ApplicationJob
  queue_as :email

  def perform(mailing_bg_job_id)
    mailing_bg_job = MailingBgJob.find(mailing_bg_job_id)
    mailing = mailing_bg_job.mailing
    mailing_bg_job.destroy

    ::Mailing::Deliver.new(mailing:).call
  rescue ActiveRecord::RecordNotFound
    nil
  end
end
