# frozen_string_literal: true

class RenderDigestContentJob < ApplicationJob
  def perform(email_digest_id)
    email_digest = EmailDigest.find(email_digest_id)
    EmailDigest::Render.call(email_digest)
  rescue ActiveRecord::RecordNotFound
    nil
  end
end
