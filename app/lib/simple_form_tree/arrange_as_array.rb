# frozen_string_literal: true

module SimpleFormTree
  # https://github.com/stefankroes/ancestry/wiki/Creating-a-selectbox-for-a-form-using-ancestry
  module ArrangeAsArray
    module_function

    def call(klass, options = {}, hash = nil)
      hash ||= klass.arrange(options) unless hash.is_a?(Array)

      arr = []
      hash.each do |node, children|
        arr << node
        arr += call(klass, options, children) unless children.nil?
      end
      arr
    end

    def possible_parents(object, options = {})
      parents = call(object.class, options)
      object.new_record? ? parents : parents - object.subtree
    end
  end
end
