# frozen_string_literal: true

module SimpleFormTree
  module BuilderMethods
    def tree(attribute_name, klass, options = {})
      collection = options[:collection] || ArrangeAsArray.call(klass, sql_options(options))
      options = { collection:, label_method: :name_for_selects }.merge(options)
      association(attribute_name, options)
    end

    def parent(options = {})
      collection =
        options[:collection] ||
        ArrangeAsArray.call(@object.class, {}, ArrangeAsArray.possible_parents(@object, sql_options(options)))
      input(:parent_id, options) do
        collection_select(:parent_id, collection, :id, :name_for_selects, { include_blank: true },
                          { class: 'form-control' })
      end
    end

    def sql_options(options)
      options[:order].present? ? { order: options[:order] } : {}
    end
  end
end
