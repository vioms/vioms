# frozen_string_literal: true

module Ckeditor::AddFullUrl
  extend ActiveSupport::Concern

  included do
    before_validation :add_host_to_ckeditor_fields
  end

  def add_host_to_ckeditor_fields
    return unless self.class.const_defined?('CKEDITOR_FIELDS')

    url_options = Rails.application.config.action_mailer.default_url_options
    protocol = url_options[:protocol] || 'http://'
    host = url_options[:host]
    self.class::CKEDITOR_FIELDS.each do |field|
      send("#{field}=",
           attributes[field.to_s].try(:gsub, %r{(["'])/ckeditor_assets/}, "\\1#{protocol}#{host}/ckeditor_assets/"))
    end
  end
end
