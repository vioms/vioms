# frozen_string_literal: true

require 'digest/md5'

module Devise
  module Encryptable
    module Encryptors
      # Old salted md5 algorithm from Joomla
      class Md5hex < Base
        def self.digest(password, _stretches, salt, _pepper)
          Digest::MD5.hexdigest([password, salt].join)
        end
      end
    end
  end
end
