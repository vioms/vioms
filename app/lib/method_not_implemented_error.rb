# frozen_string_literal: true

class MethodNotImplementedError < StandardError; end
