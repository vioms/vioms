class PhoneTokenValidator < ActiveModel::EachValidator
  def validate_each(object, attribute, value)
    return if value == object.phone_confirmation_token

    object.errors.add(attribute, (options[:message] || I18n.t('activerecord.errors.messages.invalid_phone_token')))
  end
end
