# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby File.read('.ruby-version').strip

gem 'rails', '~> 7.2.0'

gem 'pg', '~> 1.1'

gem 'puma', '>= 5.0'

gem 'jbuilder'
gem 'sprockets-rails'

gem 'coffee-rails', '~> 4.2'

gem 'bootstrap', '~> 4.6'
gem 'ckeditor'
gem 'font-awesome-sass'
gem 'friendly_id'
gem 'haml'
gem 'icomoon-rails'
gem 'jquery-rails'
gem 'jquery-ui-rails'
gem 'kaminari'
gem 'material_icons'
gem 'russian'
gem 'simple_form'
gem 'stringex'

gem 'devise'
gem 'devise-encryptable'
gem 'jwt'

gem 'ancestry'
gem 'sidekiq', '~> 6.5'
gem 'spectator-validates_email', require: 'validates_email'

gem 'carrierwave'
gem 'image_processing', '~> 1.8'
gem 'jcrop-rails-v2'
gem 'marcel' # mime type detection
gem 'mini_magick'
gem 'shrine', '~> 3.0'

gem 'actionpack-page_caching'
gem 'activerecord-session_store'
gem 'bootsnap', require: false
gem 'bootstrap-email'
gem 'draper'
gem 'interactor'
gem 'irwi'
gem 'nested_form'
gem 'nokogiri'
gem 'pg_search'
gem 'pundit'
gem 'rack-cors'
gem 'rack-ssl-enforcer'
gem 'recaptcha'
gem 'record_tag_helper', '~> 1.0'
gem 'RedCloth'
gem 'rest-client'
gem 'ruby-progressbar', require: false
gem 'rubyXL'
gem 'sinatra', require: false

gem 'dry-monads'

group :development do
  gem 'web-console'

  gem 'better_errors'
  gem 'binding_of_caller'
  gem 'haml-rails'

  gem 'rubocop', require: false
  gem 'rubocop-factory_bot', require: false
  gem 'rubocop-rails', require: false
  gem 'rubocop-rspec', require: false

  gem 'license_finder'
end

group :development, :test do
  gem 'debug', platforms: %i[mri mingw x64_mingw]
  gem 'dotenv-rails'
  gem 'factory_bot_rails'
  gem 'rspec-rails'
end

group :test do
  gem 'capybara'
  gem 'cucumber-rails', require: false
  gem 'database_cleaner-active_record'
  gem 'faker'
  gem 'launchy'
  gem 'rails-controller-testing'
  gem 'simplecov', require: false
  gem 'test-prof', '~> 1.0'
  gem 'timecop'
  gem 'webmock'
end
