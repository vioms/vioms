# frozen_string_literal: true

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Page.create!(name: 'First page', content: '<h1>Demo page</h1>') unless Page.any?

unless User.any?
  User.create!(name: 'Admin', username: 'admin', email: 'admin@example.com',
               password: 'password', password_confirmation: 'password', is_admin: true)
end

unless Setting.any?
  Setting.create!(name: 'sms_max_chars', value: '670', value_type: 'integer',
                  description: 'Максимальное количество символов в SMS')
  Setting.create!(name: 'allow_send_sms', value: '1', value_type: 'boolean', description: 'Разрешить отправлять SMS')
  Setting.create!(name: 'support_phone', value: '8-987-50-22-108', value_type: 'string', description: 'Телефон в шапке')
  Setting.create!(name: 'use_crlf', value: '1', value_type: 'boolean', description: 'Заменять \\r\\n на \\n?')
  Setting.create!(name: 'stripo_username', value: 'stripo', value_type: 'string',
                  description: 'Имя пользователя для Stripo Email Plugin')
  Setting.create!(name: 'stripo_password', value: 'password', value_type: 'string',
                  description: 'Пароль для Stripo Email Plugin')
  Setting.create!(name: 'stripo_app_id', value: 'app_id', value_type: 'string',
                  description: 'ID приложения Stripo Email Plugin')
  Setting.create!(name: 'stripo_secret_key', value: 'secret', value_type: 'string',
                  description: 'Секретный ключ для Stripo Email Plugin')
  Setting.create!(name: 'stripo_version', value: 'latest', value_type: 'string',
                  description: 'Версия Stripo плагина')
end

SmsSendHistoryType.create!(name: 'password_recovery') unless SmsSendHistoryType.any?

unless SmtpSettingGroup.any?
  g = SmtpSettingGroup.create!(name: 'mailcatcher', email: 'smsgw@internal.vioms.ru', position: 1, selected: true)
  g.smtp_settings << SmtpSetting.new(name: 'address', value: 'localhost')
  g.smtp_settings << SmtpSetting.new(name: 'port', value: '1025')
end

unless TextBlock.any?
  TextBlock.create!(name: 'email_lists', description: 'Текст на странице "Email рассылки"', content: 'Демо текст')
  TextBlock.create!(name: 'telegram_groups', description: 'Текст на странице "Телеграм рассылки"',
                    content: 'Демо текст')
  TextBlock.create!(name: 'viber_groups', description: 'Текст на странице "Viber рассылки"',
                    content: 'Демо текст')
  TextBlock.create!(name: 'whatsapp_groups', description: 'Текст на странице "WhatsApp рассылки"',
                    content: 'Демо текст')
  TextBlock.create!(name: 'facebook_groups', description: 'Текст на странице "Facebook рассылки"',
                    content: 'Демо текст')
  TextBlock.create!(name: 'vk_groups', description: 'Текст на странице "ВКонтакте рассылки"',
                    content: 'Демо текст')
  TextBlock.create!(name: 'contact_new', description: 'Текст на странице "Задать вопрос в техподдержку"',
                    content: 'Демо текст')
  TextBlock.create!(name: 'register', description: 'Текст на странице "Зарегистрироваться"',
                    content: 'Демо текст')
  TextBlock.create!(name: 'widgets_code', description: 'Код для виджетов на сайте', content: '')
end
