SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: citext; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS citext WITH SCHEMA public;


--
-- Name: EXTENSION citext; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION citext IS 'data type for case-insensitive character strings';


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: answers; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.answers (
    id bigint NOT NULL,
    title character varying(255) DEFAULT ''::character varying NOT NULL,
    poll_id bigint,
    "position" bigint NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: answers_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.answers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: answers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.answers_id_seq OWNED BY public.answers.id;


--
-- Name: api_keys; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.api_keys (
    id bigint NOT NULL,
    name character varying,
    key character varying NOT NULL,
    user_id bigint,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: api_keys_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.api_keys_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: api_keys_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.api_keys_id_seq OWNED BY public.api_keys.id;


--
-- Name: ar_internal_metadata; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.ar_internal_metadata (
    key character varying NOT NULL,
    value character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: banner_clicks; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.banner_clicks (
    id bigint NOT NULL,
    banner_id bigint,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: banner_clicks_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.banner_clicks_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: banner_clicks_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.banner_clicks_id_seq OWNED BY public.banner_clicks.id;


--
-- Name: banner_orders; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.banner_orders (
    id bigint NOT NULL,
    banner_name character varying(255),
    banner_owner_id bigint,
    start_on date,
    end_on date,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: banner_orders_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.banner_orders_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: banner_orders_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.banner_orders_id_seq OWNED BY public.banner_orders.id;


--
-- Name: banner_owners; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.banner_owners (
    id bigint NOT NULL,
    name character varying(255),
    contact character varying(255),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: banner_owners_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.banner_owners_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: banner_owners_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.banner_owners_id_seq OWNED BY public.banner_owners.id;


--
-- Name: banner_shows; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.banner_shows (
    id bigint NOT NULL,
    banner_id bigint,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: banner_shows_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.banner_shows_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: banner_shows_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.banner_shows_id_seq OWNED BY public.banner_shows.id;


--
-- Name: banners; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.banners (
    id bigint NOT NULL,
    image character varying(255),
    link character varying(255),
    placement bigint NOT NULL,
    priority bigint DEFAULT 0 NOT NULL,
    show_count bigint DEFAULT 0 NOT NULL,
    click_count bigint DEFAULT 0 NOT NULL,
    banner_order_id bigint,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: banners_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.banners_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: banners_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.banners_id_seq OWNED BY public.banners.id;


--
-- Name: blacklist_emails; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.blacklist_emails (
    id bigint NOT NULL,
    email character varying(255),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: blacklist_emails_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.blacklist_emails_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: blacklist_emails_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.blacklist_emails_id_seq OWNED BY public.blacklist_emails.id;


--
-- Name: cached_subscribers; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.cached_subscribers (
    id bigint NOT NULL,
    phone character varying(255),
    sms_sending_id bigint,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: cached_subscribers_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.cached_subscribers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: cached_subscribers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.cached_subscribers_id_seq OWNED BY public.cached_subscribers.id;


--
-- Name: cities; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.cities (
    id bigint NOT NULL,
    name character varying(255),
    priority bigint DEFAULT 0 NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    ancestry character varying(255)
);


--
-- Name: cities_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.cities_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: cities_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.cities_id_seq OWNED BY public.cities.id;


--
-- Name: ckeditor_assets; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.ckeditor_assets (
    id bigint NOT NULL,
    data_file_name character varying(255) NOT NULL,
    data_content_type character varying(255),
    data_file_size bigint,
    type character varying(30),
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: ckeditor_assets_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.ckeditor_assets_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: ckeditor_assets_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.ckeditor_assets_id_seq OWNED BY public.ckeditor_assets.id;


--
-- Name: digest_categories; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.digest_categories (
    id bigint NOT NULL,
    name character varying(255),
    template text,
    template_entry text,
    priority bigint DEFAULT 0 NOT NULL,
    email_list_id bigint,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: digest_categories_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.digest_categories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: digest_categories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.digest_categories_id_seq OWNED BY public.digest_categories.id;


--
-- Name: donation_methods; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.donation_methods (
    id bigint NOT NULL,
    name character varying NOT NULL,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL
);


--
-- Name: donation_methods_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.donation_methods_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: donation_methods_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.donation_methods_id_seq OWNED BY public.donation_methods.id;


--
-- Name: email_attachments; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.email_attachments (
    id bigint NOT NULL,
    mailing_id bigint,
    attachment character varying(255),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: email_attachments_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.email_attachments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: email_attachments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.email_attachments_id_seq OWNED BY public.email_attachments.id;


--
-- Name: email_digest_bg_jobs; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.email_digest_bg_jobs (
    id bigint NOT NULL,
    email_digest_id bigint,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: email_digest_bg_jobs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.email_digest_bg_jobs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: email_digest_bg_jobs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.email_digest_bg_jobs_id_seq OWNED BY public.email_digest_bg_jobs.id;


--
-- Name: email_digests; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.email_digests (
    id bigint NOT NULL,
    num bigint NOT NULL,
    deliver_at timestamp without time zone NOT NULL,
    year bigint NOT NULL,
    email_list_id bigint NOT NULL,
    user_id bigint,
    published boolean DEFAULT false NOT NULL,
    sending boolean DEFAULT false NOT NULL,
    sent boolean DEFAULT false NOT NULL,
    send_failed boolean DEFAULT false NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    rendered_html text
);


--
-- Name: email_digests_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.email_digests_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: email_digests_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.email_digests_id_seq OWNED BY public.email_digests.id;


--
-- Name: email_list_custom_vars; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.email_list_custom_vars (
    id bigint NOT NULL,
    name character varying(255),
    description character varying(255),
    email_list_id bigint
);


--
-- Name: email_list_custom_vars_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.email_list_custom_vars_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: email_list_custom_vars_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.email_list_custom_vars_id_seq OWNED BY public.email_list_custom_vars.id;


--
-- Name: email_list_exclusion_permissions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.email_list_exclusion_permissions (
    id bigint NOT NULL,
    user_id bigint NOT NULL,
    email_list_id bigint NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: email_list_exclusion_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.email_list_exclusion_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: email_list_exclusion_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.email_list_exclusion_permissions_id_seq OWNED BY public.email_list_exclusion_permissions.id;


--
-- Name: email_list_permissions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.email_list_permissions (
    id bigint NOT NULL,
    user_id bigint,
    email_list_id bigint,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: email_list_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.email_list_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: email_list_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.email_list_permissions_id_seq OWNED BY public.email_list_permissions.id;


--
-- Name: email_list_var_names; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.email_list_var_names (
    id bigint NOT NULL,
    name character varying(255),
    description character varying(255),
    email_list_id bigint,
    "position" bigint NOT NULL,
    is_text boolean DEFAULT false NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    skip_validation boolean DEFAULT false NOT NULL,
    with_editor boolean DEFAULT false NOT NULL
);


--
-- Name: email_list_var_names_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.email_list_var_names_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: email_list_var_names_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.email_list_var_names_id_seq OWNED BY public.email_list_var_names.id;


--
-- Name: email_list_vars; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.email_list_vars (
    id bigint NOT NULL,
    value text NOT NULL,
    mailing_id bigint,
    email_list_var_name_id bigint,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: email_list_vars_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.email_list_vars_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: email_list_vars_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.email_list_vars_id_seq OWNED BY public.email_list_vars.id;


--
-- Name: email_lists; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.email_lists (
    id bigint NOT NULL,
    name character varying(255),
    description text,
    from_name character varying(255),
    from_email character varying(255),
    from_bounce character varying(255),
    layout text,
    subscribe_message text,
    unsubscribe_message text,
    unsubscribe_send boolean DEFAULT true,
    html boolean DEFAULT true,
    visible boolean DEFAULT false,
    notification boolean DEFAULT false,
    user_id bigint,
    footer boolean DEFAULT true,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    notify_subscribed boolean DEFAULT false NOT NULL,
    notify_unsubscribed boolean DEFAULT false NOT NULL,
    notify_email character varying(255),
    solve_sync_method bigint DEFAULT 0 NOT NULL,
    solve_contacts_synced_at timestamp without time zone,
    solve_custom_formula character varying(255),
    use_custom_vars boolean DEFAULT false NOT NULL,
    import_link character varying(255),
    import_login character varying(255),
    import_password character varying(255),
    import_use boolean DEFAULT false NOT NULL,
    never_visible boolean DEFAULT false NOT NULL,
    content_header text,
    content_footer text,
    postmaster_msgtype character varying(255),
    group_priority bigint DEFAULT 0 NOT NULL,
    digest_subject_template character varying(255),
    digest_template text,
    skip_unsubscription_links boolean DEFAULT false NOT NULL,
    use_template boolean DEFAULT false NOT NULL,
    template text,
    internal_name character varying(255),
    subscribers_count_cache bigint,
    expiration_days integer,
    default_editor_type integer,
    default_stripo_template_id bigint,
    default_stripo_template_group_id bigint,
    reply_to character varying
);


--
-- Name: email_lists_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.email_lists_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: email_lists_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.email_lists_id_seq OWNED BY public.email_lists.id;


--
-- Name: email_lists_mailing_destinations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.email_lists_mailing_destinations (
    id bigint NOT NULL,
    mailing_destination_id bigint NOT NULL,
    email_list_id bigint NOT NULL
);


--
-- Name: email_lists_mailing_destinations_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.email_lists_mailing_destinations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: email_lists_mailing_destinations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.email_lists_mailing_destinations_id_seq OWNED BY public.email_lists_mailing_destinations.id;


--
-- Name: email_lists_mailing_requests; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.email_lists_mailing_requests (
    mailing_request_id bigint NOT NULL,
    email_list_id bigint NOT NULL
);


--
-- Name: email_lists_mailings_exclusions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.email_lists_mailings_exclusions (
    id bigint NOT NULL,
    email_list_id bigint,
    mailing_id bigint,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: email_lists_mailings_exclusions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.email_lists_mailings_exclusions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: email_lists_mailings_exclusions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.email_lists_mailings_exclusions_id_seq OWNED BY public.email_lists_mailings_exclusions.id;


--
-- Name: email_lists_mailings_inclusions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.email_lists_mailings_inclusions (
    id bigint NOT NULL,
    email_list_id bigint,
    mailing_id bigint,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: email_lists_mailings_inclusions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.email_lists_mailings_inclusions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: email_lists_mailings_inclusions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.email_lists_mailings_inclusions_id_seq OWNED BY public.email_lists_mailings_inclusions.id;


--
-- Name: email_problem_names; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.email_problem_names (
    id bigint NOT NULL,
    name character varying(255),
    "position" bigint DEFAULT 0 NOT NULL,
    problem_type bigint NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: email_problem_names_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.email_problem_names_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: email_problem_names_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.email_problem_names_id_seq OWNED BY public.email_problem_names.id;


--
-- Name: email_problems; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.email_problems (
    id bigint NOT NULL,
    description character varying(255),
    subscriber_id bigint,
    email_problem_name_id bigint,
    solved boolean DEFAULT false NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: email_problems_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.email_problems_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: email_problems_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.email_problems_id_seq OWNED BY public.email_problems.id;


--
-- Name: email_send_histories; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.email_send_histories (
    id bigint NOT NULL,
    email character varying(255),
    subscriber_id bigint,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    last_value character varying(255)
);


--
-- Name: email_send_histories_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.email_send_histories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: email_send_histories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.email_send_histories_id_seq OWNED BY public.email_send_histories.id;


--
-- Name: email_subscriptions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.email_subscriptions (
    id bigint NOT NULL,
    subscriber_id bigint,
    email_list_id bigint,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: email_subscriptions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.email_subscriptions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: email_subscriptions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.email_subscriptions_id_seq OWNED BY public.email_subscriptions.id;


--
-- Name: external_redirects; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.external_redirects (
    id bigint NOT NULL,
    url character varying(255) NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    is_simple boolean DEFAULT false NOT NULL,
    param_name character varying(255),
    is_relative boolean DEFAULT false NOT NULL,
    resolved_count bigint DEFAULT 0 NOT NULL,
    unresolved_count bigint DEFAULT 0 NOT NULL
);


--
-- Name: external_redirects_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.external_redirects_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: external_redirects_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.external_redirects_id_seq OWNED BY public.external_redirects.id;


--
-- Name: mailing_bg_jobs; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.mailing_bg_jobs (
    id bigint NOT NULL,
    mailing_id bigint,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: mailing_bg_jobs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.mailing_bg_jobs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: mailing_bg_jobs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.mailing_bg_jobs_id_seq OWNED BY public.mailing_bg_jobs.id;


--
-- Name: mailing_destinations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.mailing_destinations (
    id bigint NOT NULL,
    name character varying,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL
);


--
-- Name: mailing_destinations_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.mailing_destinations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: mailing_destinations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.mailing_destinations_id_seq OWNED BY public.mailing_destinations.id;


--
-- Name: mailing_destinations_users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.mailing_destinations_users (
    mailing_destination_id bigint NOT NULL,
    user_id bigint NOT NULL
);


--
-- Name: mailing_request_images; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.mailing_request_images (
    id bigint NOT NULL,
    image_data jsonb,
    mailing_request_id bigint NOT NULL,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL
);


--
-- Name: mailing_request_images_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.mailing_request_images_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: mailing_request_images_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.mailing_request_images_id_seq OWNED BY public.mailing_request_images.id;


--
-- Name: mailing_request_messages; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.mailing_request_messages (
    id bigint NOT NULL,
    mailing_request_id bigint NOT NULL,
    user_id bigint,
    author integer NOT NULL,
    old_status integer NOT NULL,
    new_status integer NOT NULL,
    message text,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL
);


--
-- Name: mailing_request_messages_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.mailing_request_messages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: mailing_request_messages_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.mailing_request_messages_id_seq OWNED BY public.mailing_request_messages.id;


--
-- Name: mailing_requests; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.mailing_requests (
    id bigint NOT NULL,
    subject character varying NOT NULL,
    body text NOT NULL,
    status integer NOT NULL,
    mailing_type integer,
    donation_method_id bigint,
    donated_at character varying,
    deliver_at character varying,
    comments text,
    mailing_id bigint,
    subscriber_id bigint NOT NULL,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL
);


--
-- Name: mailing_requests_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.mailing_requests_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: mailing_requests_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.mailing_requests_id_seq OWNED BY public.mailing_requests.id;


--
-- Name: mailing_views; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.mailing_views (
    id bigint NOT NULL,
    mailing_id bigint,
    subscriber_id bigint,
    count integer DEFAULT 0 NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: mailing_views_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.mailing_views_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: mailing_views_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.mailing_views_id_seq OWNED BY public.mailing_views.id;


--
-- Name: mailings; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.mailings (
    id bigint NOT NULL,
    email_list_id bigint,
    issue_nb bigint,
    subject public.citext,
    html_content public.citext,
    plain_content public.citext,
    attachments text,
    images text,
    deliver_at timestamp without time zone,
    sent boolean DEFAULT false,
    visible boolean DEFAULT true,
    html boolean DEFAULT true,
    published boolean DEFAULT false,
    user_id bigint,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    send_failed boolean DEFAULT false NOT NULL,
    sending boolean DEFAULT false NOT NULL,
    digest boolean DEFAULT false NOT NULL,
    digest_content text,
    digest_image character varying(255),
    email_digest_id bigint,
    digest_category_id bigint,
    disable_editor boolean DEFAULT false NOT NULL,
    raised_exception_at timestamp without time zone,
    html_content_backup text,
    use_template boolean DEFAULT false NOT NULL,
    editor_type integer,
    css text,
    rendered_html text,
    deliveries_count integer DEFAULT 0 NOT NULL,
    delivery_errors_count integer DEFAULT 0 NOT NULL,
    enable_content_headers boolean DEFAULT false NOT NULL,
    searchable tsvector GENERATED ALWAYS AS ((setweight(to_tsvector('russian'::regconfig, (COALESCE(subject, ''::public.citext))::text), 'A'::"char") || setweight(to_tsvector('russian'::regconfig, (COALESCE(plain_content, ''::public.citext))::text), 'B'::"char"))) STORED
);


--
-- Name: mailings_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.mailings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: mailings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.mailings_id_seq OWNED BY public.mailings.id;


--
-- Name: messenger_types; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.messenger_types (
    id bigint NOT NULL,
    name character varying(255) DEFAULT ''::character varying NOT NULL,
    "char" character varying(255) DEFAULT ''::character varying NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: messenger_types_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.messenger_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: messenger_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.messenger_types_id_seq OWNED BY public.messenger_types.id;


--
-- Name: mobile_tokens; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.mobile_tokens (
    id bigint NOT NULL,
    serial integer DEFAULT 0 NOT NULL,
    subscriber_id bigint,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    device_token character varying,
    device_type integer
);


--
-- Name: mobile_tokens_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.mobile_tokens_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: mobile_tokens_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.mobile_tokens_id_seq OWNED BY public.mobile_tokens.id;


--
-- Name: news_items; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.news_items (
    id bigint NOT NULL,
    title character varying(255),
    content text,
    date date NOT NULL,
    slug character varying(255),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: news_items_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.news_items_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: news_items_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.news_items_id_seq OWNED BY public.news_items.id;


--
-- Name: pages; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.pages (
    id bigint NOT NULL,
    name character varying(255),
    content text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    slug character varying(255),
    show_in_navbar boolean DEFAULT false NOT NULL,
    "position" bigint NOT NULL
);


--
-- Name: pages_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.pages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: pages_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.pages_id_seq OWNED BY public.pages.id;


--
-- Name: phone_prefixes; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.phone_prefixes (
    id bigint NOT NULL,
    sms_operator_id bigint,
    prefix_start character varying(255),
    prefix_end character varying(255),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: phone_prefixes_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.phone_prefixes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: phone_prefixes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.phone_prefixes_id_seq OWNED BY public.phone_prefixes.id;


--
-- Name: phone_problem_names; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.phone_problem_names (
    id bigint NOT NULL,
    name character varying(255),
    "position" bigint DEFAULT 0 NOT NULL,
    problem_type bigint NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: phone_problem_names_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.phone_problem_names_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: phone_problem_names_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.phone_problem_names_id_seq OWNED BY public.phone_problem_names.id;


--
-- Name: phone_problems; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.phone_problems (
    id bigint NOT NULL,
    description character varying(255),
    subscriber_id bigint,
    phone_problem_name_id bigint,
    solved boolean DEFAULT false NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: phone_problems_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.phone_problems_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: phone_problems_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.phone_problems_id_seq OWNED BY public.phone_problems.id;


--
-- Name: polls; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.polls (
    id bigint NOT NULL,
    title character varying(255) DEFAULT ''::character varying NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: polls_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.polls_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: polls_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.polls_id_seq OWNED BY public.polls.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.schema_migrations (
    version character varying NOT NULL
);


--
-- Name: sessions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sessions (
    id bigint NOT NULL,
    session_id character varying(255) NOT NULL,
    data text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: sessions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.sessions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: sessions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.sessions_id_seq OWNED BY public.sessions.id;


--
-- Name: settings; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.settings (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    value character varying(255),
    value_type character varying(255) NOT NULL,
    description character varying(255),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: settings_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.settings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: settings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.settings_id_seq OWNED BY public.settings.id;


--
-- Name: sms_account_actions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sms_account_actions (
    id bigint NOT NULL,
    amount numeric(12,3),
    balance numeric(12,3),
    comments text,
    sms_account_id bigint,
    user_id bigint,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    sms_sending_id bigint,
    reserved boolean DEFAULT false NOT NULL,
    reservable_amount numeric(12,3),
    cancelled boolean DEFAULT false NOT NULL
);


--
-- Name: sms_account_actions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.sms_account_actions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: sms_account_actions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.sms_account_actions_id_seq OWNED BY public.sms_account_actions.id;


--
-- Name: sms_accounts; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sms_accounts (
    id bigint NOT NULL,
    name character varying(255),
    user_id bigint,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    is_admin boolean DEFAULT false NOT NULL,
    low_balance_threshold numeric(12,3) DEFAULT 0.0 NOT NULL,
    low_balance_notification_sent_at timestamp without time zone
);


--
-- Name: sms_accounts_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.sms_accounts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: sms_accounts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.sms_accounts_id_seq OWNED BY public.sms_accounts.id;


--
-- Name: sms_accounts_permissions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sms_accounts_permissions (
    id bigint NOT NULL,
    sms_account_id bigint,
    user_id bigint
);


--
-- Name: sms_accounts_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.sms_accounts_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: sms_accounts_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.sms_accounts_permissions_id_seq OWNED BY public.sms_accounts_permissions.id;


--
-- Name: sms_list_permissions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sms_list_permissions (
    id bigint NOT NULL,
    user_id bigint,
    sms_list_id bigint,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: sms_list_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.sms_list_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: sms_list_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.sms_list_permissions_id_seq OWNED BY public.sms_list_permissions.id;


--
-- Name: sms_lists; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sms_lists (
    id bigint NOT NULL,
    name character varying(255),
    description text,
    visible boolean,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    karmi boolean DEFAULT false NOT NULL,
    public boolean DEFAULT false NOT NULL,
    never_visible boolean DEFAULT false NOT NULL,
    linked_email_list_id bigint,
    ancestry character varying(255)
);


--
-- Name: sms_lists_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.sms_lists_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: sms_lists_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.sms_lists_id_seq OWNED BY public.sms_lists.id;


--
-- Name: sms_lists_sms_sendings; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sms_lists_sms_sendings (
    id bigint NOT NULL,
    sms_list_id bigint NOT NULL,
    sms_sending_id bigint NOT NULL
);


--
-- Name: sms_lists_sms_sendings_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.sms_lists_sms_sendings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: sms_lists_sms_sendings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.sms_lists_sms_sendings_id_seq OWNED BY public.sms_lists_sms_sendings.id;


--
-- Name: sms_messages; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sms_messages (
    id bigint NOT NULL,
    message text,
    karmi boolean DEFAULT false NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    "position" bigint NOT NULL,
    subject_addition character varying(255),
    sms_send_history_type_id bigint
);


--
-- Name: sms_messages_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.sms_messages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: sms_messages_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.sms_messages_id_seq OWNED BY public.sms_messages.id;


--
-- Name: sms_operators; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sms_operators (
    id bigint NOT NULL,
    code character varying(255),
    name character varying(255),
    gate_name character varying(255),
    sms_sending_id bigint,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: sms_operators_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.sms_operators_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: sms_operators_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.sms_operators_id_seq OWNED BY public.sms_operators.id;


--
-- Name: sms_send_histories; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sms_send_histories (
    id bigint NOT NULL,
    phone character varying(255),
    subscriber_id bigint,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    sms_send_history_type_id bigint DEFAULT 1 NOT NULL,
    last_value character varying(255),
    expire_at timestamp without time zone,
    attempts bigint DEFAULT 0 NOT NULL
);


--
-- Name: sms_send_histories_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.sms_send_histories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: sms_send_histories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.sms_send_histories_id_seq OWNED BY public.sms_send_histories.id;


--
-- Name: sms_send_history_types; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sms_send_history_types (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    description character varying(255),
    attempts bigint DEFAULT 1 NOT NULL
);


--
-- Name: sms_send_history_types_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.sms_send_history_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: sms_send_history_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.sms_send_history_types_id_seq OWNED BY public.sms_send_history_types.id;


--
-- Name: sms_sending_bg_jobs; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sms_sending_bg_jobs (
    id bigint NOT NULL,
    sms_sending_id bigint,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: sms_sending_bg_jobs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.sms_sending_bg_jobs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: sms_sending_bg_jobs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.sms_sending_bg_jobs_id_seq OWNED BY public.sms_sending_bg_jobs.id;


--
-- Name: sms_sendings; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sms_sendings (
    id bigint NOT NULL,
    message text,
    deliver_at timestamp without time zone,
    visible boolean DEFAULT true NOT NULL,
    user_id bigint,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    num bigint,
    sent boolean DEFAULT false NOT NULL,
    published boolean DEFAULT false NOT NULL,
    pay_from_sms_account_id bigint,
    cached_subject character varying(255),
    sender_user_id bigint,
    sending boolean DEFAULT false NOT NULL,
    subscriber_id bigint,
    email_subject character varying(255),
    copy_to_email boolean DEFAULT false NOT NULL,
    no_money boolean DEFAULT false NOT NULL
);


--
-- Name: sms_sendings_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.sms_sendings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: sms_sendings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.sms_sendings_id_seq OWNED BY public.sms_sendings.id;


--
-- Name: sms_subscriptions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sms_subscriptions (
    id bigint NOT NULL,
    subscriber_id bigint,
    sms_list_id bigint,
    confirmed boolean DEFAULT true NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: sms_subscriptions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.sms_subscriptions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: sms_subscriptions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.sms_subscriptions_id_seq OWNED BY public.sms_subscriptions.id;


--
-- Name: smtp_setting_groups; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.smtp_setting_groups (
    id bigint NOT NULL,
    name character varying(255),
    email character varying(255),
    "position" bigint NOT NULL,
    selected boolean DEFAULT false NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: smtp_setting_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.smtp_setting_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: smtp_setting_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.smtp_setting_groups_id_seq OWNED BY public.smtp_setting_groups.id;


--
-- Name: smtp_settings; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.smtp_settings (
    id bigint NOT NULL,
    name character varying(255),
    value character varying(255),
    smtp_setting_group_id bigint,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: smtp_settings_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.smtp_settings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: smtp_settings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.smtp_settings_id_seq OWNED BY public.smtp_settings.id;


--
-- Name: stripo_file_keys; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.stripo_file_keys (
    id bigint NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    plugin_id character varying(32) NOT NULL,
    email_id integer,
    mailing_id integer,
    reusable boolean DEFAULT false NOT NULL,
    stripo_template_id bigint
);


--
-- Name: stripo_file_keys_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.stripo_file_keys_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: stripo_file_keys_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.stripo_file_keys_id_seq OWNED BY public.stripo_file_keys.id;


--
-- Name: stripo_files; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.stripo_files (
    id bigint NOT NULL,
    file_data jsonb,
    stripo_file_key_id bigint,
    cached_url character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: stripo_files_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.stripo_files_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: stripo_files_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.stripo_files_id_seq OWNED BY public.stripo_files.id;


--
-- Name: stripo_template_group_email_lists; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.stripo_template_group_email_lists (
    id bigint NOT NULL,
    stripo_template_group_id bigint,
    email_list_id bigint
);


--
-- Name: stripo_template_group_email_lists_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.stripo_template_group_email_lists_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: stripo_template_group_email_lists_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.stripo_template_group_email_lists_id_seq OWNED BY public.stripo_template_group_email_lists.id;


--
-- Name: stripo_template_group_users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.stripo_template_group_users (
    id bigint NOT NULL,
    stripo_template_group_id bigint,
    user_id bigint
);


--
-- Name: stripo_template_group_users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.stripo_template_group_users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: stripo_template_group_users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.stripo_template_group_users_id_seq OWNED BY public.stripo_template_group_users.id;


--
-- Name: stripo_template_groups; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.stripo_template_groups (
    id bigint NOT NULL,
    name character varying NOT NULL,
    "position" integer NOT NULL,
    ancestry character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    public boolean,
    user_id bigint
);


--
-- Name: stripo_template_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.stripo_template_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: stripo_template_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.stripo_template_groups_id_seq OWNED BY public.stripo_template_groups.id;


--
-- Name: stripo_templates; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.stripo_templates (
    id bigint NOT NULL,
    name character varying NOT NULL,
    stripo_template_group_id bigint,
    html text NOT NULL,
    css text NOT NULL,
    preview_data jsonb,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: stripo_templates_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.stripo_templates_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: stripo_templates_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.stripo_templates_id_seq OWNED BY public.stripo_templates.id;


--
-- Name: subscriber_answers; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.subscriber_answers (
    id bigint NOT NULL,
    poll_id bigint,
    answer_id bigint,
    subscriber_id bigint,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: subscriber_answers_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.subscriber_answers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: subscriber_answers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.subscriber_answers_id_seq OWNED BY public.subscriber_answers.id;


--
-- Name: subscriber_custom_vars; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.subscriber_custom_vars (
    id bigint NOT NULL,
    subscriber_id bigint,
    email_list_custom_var_id bigint,
    value character varying(255)
);


--
-- Name: subscriber_custom_vars_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.subscriber_custom_vars_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: subscriber_custom_vars_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.subscriber_custom_vars_id_seq OWNED BY public.subscriber_custom_vars.id;


--
-- Name: subscriber_import_email_lists; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.subscriber_import_email_lists (
    id bigint NOT NULL,
    subscriber_import_id bigint,
    email_list_id bigint
);


--
-- Name: subscriber_import_email_lists_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.subscriber_import_email_lists_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: subscriber_import_email_lists_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.subscriber_import_email_lists_id_seq OWNED BY public.subscriber_import_email_lists.id;


--
-- Name: subscriber_import_people; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.subscriber_import_people (
    id bigint NOT NULL,
    name character varying(255),
    email character varying(255),
    phone character varying(255),
    subscriber_import_id bigint,
    error_id bigint
);


--
-- Name: subscriber_import_people_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.subscriber_import_people_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: subscriber_import_people_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.subscriber_import_people_id_seq OWNED BY public.subscriber_import_people.id;


--
-- Name: subscriber_import_sms_lists; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.subscriber_import_sms_lists (
    id bigint NOT NULL,
    subscriber_import_id bigint,
    sms_list_id bigint
);


--
-- Name: subscriber_import_sms_lists_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.subscriber_import_sms_lists_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: subscriber_import_sms_lists_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.subscriber_import_sms_lists_id_seq OWNED BY public.subscriber_import_sms_lists.id;


--
-- Name: subscriber_imports; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.subscriber_imports (
    id bigint NOT NULL,
    description character varying(255),
    user_id bigint,
    errors_count bigint DEFAULT 0 NOT NULL,
    time_zone_id bigint,
    confirm_email boolean DEFAULT false NOT NULL,
    confirm_phone boolean DEFAULT false NOT NULL,
    send_phone_confirmation_instructions boolean DEFAULT false NOT NULL,
    send_confirmation_notificaton_sms boolean DEFAULT false NOT NULL,
    delete_old boolean DEFAULT false NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: subscriber_imports_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.subscriber_imports_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: subscriber_imports_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.subscriber_imports_id_seq OWNED BY public.subscriber_imports.id;


--
-- Name: subscribers; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.subscribers (
    id bigint NOT NULL,
    email public.citext DEFAULT ''::character varying,
    encrypted_password character varying(128) DEFAULT ''::character varying NOT NULL,
    password_salt character varying(255) DEFAULT ''::character varying NOT NULL,
    authentication_token character varying(255),
    confirmed_at timestamp without time zone,
    reset_password_token character varying(255),
    remember_token character varying(255),
    remember_created_at timestamp without time zone,
    sign_in_count bigint DEFAULT 0,
    current_sign_in_at timestamp without time zone,
    last_sign_in_at timestamp without time zone,
    current_sign_in_ip character varying(255),
    last_sign_in_ip character varying(255),
    failed_attempts bigint DEFAULT 0,
    unlock_token character varying(255),
    locked_at timestamp without time zone,
    user_id bigint,
    name character varying(255),
    receive_html boolean DEFAULT true,
    email_problems_count bigint DEFAULT 0 NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    phone character varying(10),
    city_id bigint,
    time_zone_id bigint,
    sms_send_begin_at time without time zone,
    sms_send_end_at time without time zone,
    phone_confirmed_at timestamp without time zone,
    sms_event_type bigint,
    phone_confirmation_attempts bigint DEFAULT 0 NOT NULL,
    reset_password_failed_attempts bigint DEFAULT 0,
    reset_password_locked_at timestamp without time zone,
    email_confirmation_attempts bigint DEFAULT 0 NOT NULL,
    phone_blocked_at timestamp without time zone,
    send_sms boolean DEFAULT true NOT NULL,
    reset_password_sent_at timestamp without time zone,
    phone_problems_count bigint DEFAULT 0 NOT NULL,
    sms_operator_id bigint,
    raised_exception_at timestamp without time zone,
    messenger_type_id bigint
);


--
-- Name: subscribers_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.subscribers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: subscribers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.subscribers_id_seq OWNED BY public.subscribers.id;


--
-- Name: survey_answers; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.survey_answers (
    id bigint NOT NULL,
    answer character varying,
    "position" integer NOT NULL,
    survey_question_id bigint NOT NULL,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL
);


--
-- Name: survey_answers_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.survey_answers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: survey_answers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.survey_answers_id_seq OWNED BY public.survey_answers.id;


--
-- Name: survey_questions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.survey_questions (
    id bigint NOT NULL,
    question character varying NOT NULL,
    question_type integer NOT NULL,
    "position" integer NOT NULL,
    survey_id bigint NOT NULL,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL
);


--
-- Name: survey_questions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.survey_questions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: survey_questions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.survey_questions_id_seq OWNED BY public.survey_questions.id;


--
-- Name: survey_subscriber_answers; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.survey_subscriber_answers (
    id bigint NOT NULL,
    value text,
    subscriber_id bigint,
    survey_question_id bigint NOT NULL,
    survey_answer_id bigint,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL
);


--
-- Name: survey_subscriber_answers_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.survey_subscriber_answers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: survey_subscriber_answers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.survey_subscriber_answers_id_seq OWNED BY public.survey_subscriber_answers.id;


--
-- Name: surveys; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.surveys (
    id bigint NOT NULL,
    name character varying,
    title character varying NOT NULL,
    visible boolean DEFAULT false NOT NULL,
    announce boolean DEFAULT false NOT NULL,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL
);


--
-- Name: surveys_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.surveys_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: surveys_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.surveys_id_seq OWNED BY public.surveys.id;


--
-- Name: telegram_group_permissions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.telegram_group_permissions (
    id bigint NOT NULL,
    telegram_group_id bigint,
    user_id bigint,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: telegram_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.telegram_group_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: telegram_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.telegram_group_permissions_id_seq OWNED BY public.telegram_group_permissions.id;


--
-- Name: telegram_groups; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.telegram_groups (
    id bigint NOT NULL,
    name character varying NOT NULL,
    description text,
    chat_id bigint NOT NULL,
    bot_token character varying NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    invite_link character varying,
    visible boolean DEFAULT true NOT NULL
);


--
-- Name: telegram_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.telegram_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: telegram_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.telegram_groups_id_seq OWNED BY public.telegram_groups.id;


--
-- Name: telegram_message_schedules; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.telegram_message_schedules (
    id bigint NOT NULL,
    telegram_message_id bigint,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: telegram_message_schedules_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.telegram_message_schedules_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: telegram_message_schedules_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.telegram_message_schedules_id_seq OWNED BY public.telegram_message_schedules.id;


--
-- Name: telegram_messages; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.telegram_messages (
    id bigint NOT NULL,
    message text NOT NULL,
    telegram_group_id bigint,
    user_id bigint,
    deliver_at timestamp without time zone NOT NULL,
    published boolean DEFAULT false NOT NULL,
    sending boolean DEFAULT false NOT NULL,
    sent boolean DEFAULT false NOT NULL,
    send_failed boolean DEFAULT false NOT NULL,
    raised_exception_at timestamp without time zone,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    image_data text
);


--
-- Name: telegram_messages_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.telegram_messages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: telegram_messages_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.telegram_messages_id_seq OWNED BY public.telegram_messages.id;


--
-- Name: telegram_messages_telegram_groups; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.telegram_messages_telegram_groups (
    id bigint NOT NULL,
    telegram_message_id bigint,
    telegram_group_id bigint,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: telegram_messages_telegram_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.telegram_messages_telegram_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: telegram_messages_telegram_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.telegram_messages_telegram_groups_id_seq OWNED BY public.telegram_messages_telegram_groups.id;


--
-- Name: text_blocks; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.text_blocks (
    id bigint NOT NULL,
    name character varying,
    description character varying,
    content text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: text_blocks_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.text_blocks_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: text_blocks_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.text_blocks_id_seq OWNED BY public.text_blocks.id;


--
-- Name: time_zones; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.time_zones (
    id bigint NOT NULL,
    name character varying(255),
    begin_at time without time zone,
    end_at time without time zone,
    zone time without time zone,
    zone_sign boolean DEFAULT true NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    num bigint NOT NULL,
    priority bigint DEFAULT 0 NOT NULL
);


--
-- Name: time_zones_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.time_zones_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: time_zones_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.time_zones_id_seq OWNED BY public.time_zones.id;


--
-- Name: unsubscription_reasons; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.unsubscription_reasons (
    id bigint NOT NULL,
    name character varying(255),
    "position" bigint NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: unsubscription_reasons_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.unsubscription_reasons_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: unsubscription_reasons_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.unsubscription_reasons_id_seq OWNED BY public.unsubscription_reasons.id;


--
-- Name: unsubscriptions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.unsubscriptions (
    id bigint NOT NULL,
    subscriber_id bigint,
    email_list_id bigint,
    unsubscription_reason_id bigint,
    comments text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: unsubscriptions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.unsubscriptions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: unsubscriptions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.unsubscriptions_id_seq OWNED BY public.unsubscriptions.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.users (
    id bigint NOT NULL,
    name character varying(255),
    username public.citext,
    type character varying(255),
    email public.citext DEFAULT ''::character varying NOT NULL,
    encrypted_password character varying(128) DEFAULT ''::character varying NOT NULL,
    password_salt character varying(255) DEFAULT ''::character varying NOT NULL,
    reset_password_token character varying(255),
    remember_token character varying(255),
    remember_created_at timestamp without time zone,
    sign_in_count bigint DEFAULT 0,
    current_sign_in_at timestamp without time zone,
    last_sign_in_at timestamp without time zone,
    current_sign_in_ip character varying(255),
    last_sign_in_ip character varying(255),
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    roles_mask bigint DEFAULT 0,
    time_zone_id bigint,
    sms_phone character varying(255),
    reset_password_sent_at timestamp without time zone,
    max_attachments_size bigint DEFAULT 0 NOT NULL,
    become_admin boolean DEFAULT true NOT NULL,
    notify_cannot_send_sms boolean DEFAULT false NOT NULL,
    notify_low_balance boolean DEFAULT false NOT NULL
);


--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- Name: wiki_page_versions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.wiki_page_versions (
    id bigint NOT NULL,
    page_id bigint NOT NULL,
    updator_id bigint,
    number bigint,
    comment character varying(255),
    path character varying(255),
    title character varying(255),
    content text,
    updated_at timestamp without time zone
);


--
-- Name: wiki_page_versions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.wiki_page_versions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: wiki_page_versions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.wiki_page_versions_id_seq OWNED BY public.wiki_page_versions.id;


--
-- Name: wiki_pages; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.wiki_pages (
    id bigint NOT NULL,
    creator_id bigint,
    updator_id bigint,
    path character varying(255),
    title character varying(255),
    content text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: wiki_pages_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.wiki_pages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: wiki_pages_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.wiki_pages_id_seq OWNED BY public.wiki_pages.id;


--
-- Name: answers id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.answers ALTER COLUMN id SET DEFAULT nextval('public.answers_id_seq'::regclass);


--
-- Name: api_keys id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.api_keys ALTER COLUMN id SET DEFAULT nextval('public.api_keys_id_seq'::regclass);


--
-- Name: banner_clicks id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.banner_clicks ALTER COLUMN id SET DEFAULT nextval('public.banner_clicks_id_seq'::regclass);


--
-- Name: banner_orders id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.banner_orders ALTER COLUMN id SET DEFAULT nextval('public.banner_orders_id_seq'::regclass);


--
-- Name: banner_owners id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.banner_owners ALTER COLUMN id SET DEFAULT nextval('public.banner_owners_id_seq'::regclass);


--
-- Name: banner_shows id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.banner_shows ALTER COLUMN id SET DEFAULT nextval('public.banner_shows_id_seq'::regclass);


--
-- Name: banners id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.banners ALTER COLUMN id SET DEFAULT nextval('public.banners_id_seq'::regclass);


--
-- Name: blacklist_emails id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.blacklist_emails ALTER COLUMN id SET DEFAULT nextval('public.blacklist_emails_id_seq'::regclass);


--
-- Name: cached_subscribers id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.cached_subscribers ALTER COLUMN id SET DEFAULT nextval('public.cached_subscribers_id_seq'::regclass);


--
-- Name: cities id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.cities ALTER COLUMN id SET DEFAULT nextval('public.cities_id_seq'::regclass);


--
-- Name: ckeditor_assets id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.ckeditor_assets ALTER COLUMN id SET DEFAULT nextval('public.ckeditor_assets_id_seq'::regclass);


--
-- Name: digest_categories id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.digest_categories ALTER COLUMN id SET DEFAULT nextval('public.digest_categories_id_seq'::regclass);


--
-- Name: donation_methods id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.donation_methods ALTER COLUMN id SET DEFAULT nextval('public.donation_methods_id_seq'::regclass);


--
-- Name: email_attachments id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.email_attachments ALTER COLUMN id SET DEFAULT nextval('public.email_attachments_id_seq'::regclass);


--
-- Name: email_digest_bg_jobs id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.email_digest_bg_jobs ALTER COLUMN id SET DEFAULT nextval('public.email_digest_bg_jobs_id_seq'::regclass);


--
-- Name: email_digests id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.email_digests ALTER COLUMN id SET DEFAULT nextval('public.email_digests_id_seq'::regclass);


--
-- Name: email_list_custom_vars id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.email_list_custom_vars ALTER COLUMN id SET DEFAULT nextval('public.email_list_custom_vars_id_seq'::regclass);


--
-- Name: email_list_exclusion_permissions id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.email_list_exclusion_permissions ALTER COLUMN id SET DEFAULT nextval('public.email_list_exclusion_permissions_id_seq'::regclass);


--
-- Name: email_list_permissions id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.email_list_permissions ALTER COLUMN id SET DEFAULT nextval('public.email_list_permissions_id_seq'::regclass);


--
-- Name: email_list_var_names id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.email_list_var_names ALTER COLUMN id SET DEFAULT nextval('public.email_list_var_names_id_seq'::regclass);


--
-- Name: email_list_vars id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.email_list_vars ALTER COLUMN id SET DEFAULT nextval('public.email_list_vars_id_seq'::regclass);


--
-- Name: email_lists id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.email_lists ALTER COLUMN id SET DEFAULT nextval('public.email_lists_id_seq'::regclass);


--
-- Name: email_lists_mailing_destinations id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.email_lists_mailing_destinations ALTER COLUMN id SET DEFAULT nextval('public.email_lists_mailing_destinations_id_seq'::regclass);


--
-- Name: email_lists_mailings_exclusions id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.email_lists_mailings_exclusions ALTER COLUMN id SET DEFAULT nextval('public.email_lists_mailings_exclusions_id_seq'::regclass);


--
-- Name: email_lists_mailings_inclusions id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.email_lists_mailings_inclusions ALTER COLUMN id SET DEFAULT nextval('public.email_lists_mailings_inclusions_id_seq'::regclass);


--
-- Name: email_problem_names id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.email_problem_names ALTER COLUMN id SET DEFAULT nextval('public.email_problem_names_id_seq'::regclass);


--
-- Name: email_problems id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.email_problems ALTER COLUMN id SET DEFAULT nextval('public.email_problems_id_seq'::regclass);


--
-- Name: email_send_histories id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.email_send_histories ALTER COLUMN id SET DEFAULT nextval('public.email_send_histories_id_seq'::regclass);


--
-- Name: email_subscriptions id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.email_subscriptions ALTER COLUMN id SET DEFAULT nextval('public.email_subscriptions_id_seq'::regclass);


--
-- Name: external_redirects id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.external_redirects ALTER COLUMN id SET DEFAULT nextval('public.external_redirects_id_seq'::regclass);


--
-- Name: mailing_bg_jobs id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.mailing_bg_jobs ALTER COLUMN id SET DEFAULT nextval('public.mailing_bg_jobs_id_seq'::regclass);


--
-- Name: mailing_destinations id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.mailing_destinations ALTER COLUMN id SET DEFAULT nextval('public.mailing_destinations_id_seq'::regclass);


--
-- Name: mailing_request_images id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.mailing_request_images ALTER COLUMN id SET DEFAULT nextval('public.mailing_request_images_id_seq'::regclass);


--
-- Name: mailing_request_messages id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.mailing_request_messages ALTER COLUMN id SET DEFAULT nextval('public.mailing_request_messages_id_seq'::regclass);


--
-- Name: mailing_requests id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.mailing_requests ALTER COLUMN id SET DEFAULT nextval('public.mailing_requests_id_seq'::regclass);


--
-- Name: mailing_views id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.mailing_views ALTER COLUMN id SET DEFAULT nextval('public.mailing_views_id_seq'::regclass);


--
-- Name: mailings id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.mailings ALTER COLUMN id SET DEFAULT nextval('public.mailings_id_seq'::regclass);


--
-- Name: messenger_types id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.messenger_types ALTER COLUMN id SET DEFAULT nextval('public.messenger_types_id_seq'::regclass);


--
-- Name: mobile_tokens id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.mobile_tokens ALTER COLUMN id SET DEFAULT nextval('public.mobile_tokens_id_seq'::regclass);


--
-- Name: news_items id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.news_items ALTER COLUMN id SET DEFAULT nextval('public.news_items_id_seq'::regclass);


--
-- Name: pages id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.pages ALTER COLUMN id SET DEFAULT nextval('public.pages_id_seq'::regclass);


--
-- Name: phone_prefixes id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.phone_prefixes ALTER COLUMN id SET DEFAULT nextval('public.phone_prefixes_id_seq'::regclass);


--
-- Name: phone_problem_names id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.phone_problem_names ALTER COLUMN id SET DEFAULT nextval('public.phone_problem_names_id_seq'::regclass);


--
-- Name: phone_problems id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.phone_problems ALTER COLUMN id SET DEFAULT nextval('public.phone_problems_id_seq'::regclass);


--
-- Name: polls id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.polls ALTER COLUMN id SET DEFAULT nextval('public.polls_id_seq'::regclass);


--
-- Name: sessions id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sessions ALTER COLUMN id SET DEFAULT nextval('public.sessions_id_seq'::regclass);


--
-- Name: settings id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.settings ALTER COLUMN id SET DEFAULT nextval('public.settings_id_seq'::regclass);


--
-- Name: sms_account_actions id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sms_account_actions ALTER COLUMN id SET DEFAULT nextval('public.sms_account_actions_id_seq'::regclass);


--
-- Name: sms_accounts id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sms_accounts ALTER COLUMN id SET DEFAULT nextval('public.sms_accounts_id_seq'::regclass);


--
-- Name: sms_accounts_permissions id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sms_accounts_permissions ALTER COLUMN id SET DEFAULT nextval('public.sms_accounts_permissions_id_seq'::regclass);


--
-- Name: sms_list_permissions id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sms_list_permissions ALTER COLUMN id SET DEFAULT nextval('public.sms_list_permissions_id_seq'::regclass);


--
-- Name: sms_lists id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sms_lists ALTER COLUMN id SET DEFAULT nextval('public.sms_lists_id_seq'::regclass);


--
-- Name: sms_lists_sms_sendings id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sms_lists_sms_sendings ALTER COLUMN id SET DEFAULT nextval('public.sms_lists_sms_sendings_id_seq'::regclass);


--
-- Name: sms_messages id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sms_messages ALTER COLUMN id SET DEFAULT nextval('public.sms_messages_id_seq'::regclass);


--
-- Name: sms_operators id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sms_operators ALTER COLUMN id SET DEFAULT nextval('public.sms_operators_id_seq'::regclass);


--
-- Name: sms_send_histories id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sms_send_histories ALTER COLUMN id SET DEFAULT nextval('public.sms_send_histories_id_seq'::regclass);


--
-- Name: sms_send_history_types id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sms_send_history_types ALTER COLUMN id SET DEFAULT nextval('public.sms_send_history_types_id_seq'::regclass);


--
-- Name: sms_sending_bg_jobs id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sms_sending_bg_jobs ALTER COLUMN id SET DEFAULT nextval('public.sms_sending_bg_jobs_id_seq'::regclass);


--
-- Name: sms_sendings id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sms_sendings ALTER COLUMN id SET DEFAULT nextval('public.sms_sendings_id_seq'::regclass);


--
-- Name: sms_subscriptions id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sms_subscriptions ALTER COLUMN id SET DEFAULT nextval('public.sms_subscriptions_id_seq'::regclass);


--
-- Name: smtp_setting_groups id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.smtp_setting_groups ALTER COLUMN id SET DEFAULT nextval('public.smtp_setting_groups_id_seq'::regclass);


--
-- Name: smtp_settings id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.smtp_settings ALTER COLUMN id SET DEFAULT nextval('public.smtp_settings_id_seq'::regclass);


--
-- Name: stripo_file_keys id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.stripo_file_keys ALTER COLUMN id SET DEFAULT nextval('public.stripo_file_keys_id_seq'::regclass);


--
-- Name: stripo_files id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.stripo_files ALTER COLUMN id SET DEFAULT nextval('public.stripo_files_id_seq'::regclass);


--
-- Name: stripo_template_group_email_lists id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.stripo_template_group_email_lists ALTER COLUMN id SET DEFAULT nextval('public.stripo_template_group_email_lists_id_seq'::regclass);


--
-- Name: stripo_template_group_users id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.stripo_template_group_users ALTER COLUMN id SET DEFAULT nextval('public.stripo_template_group_users_id_seq'::regclass);


--
-- Name: stripo_template_groups id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.stripo_template_groups ALTER COLUMN id SET DEFAULT nextval('public.stripo_template_groups_id_seq'::regclass);


--
-- Name: stripo_templates id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.stripo_templates ALTER COLUMN id SET DEFAULT nextval('public.stripo_templates_id_seq'::regclass);


--
-- Name: subscriber_answers id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.subscriber_answers ALTER COLUMN id SET DEFAULT nextval('public.subscriber_answers_id_seq'::regclass);


--
-- Name: subscriber_custom_vars id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.subscriber_custom_vars ALTER COLUMN id SET DEFAULT nextval('public.subscriber_custom_vars_id_seq'::regclass);


--
-- Name: subscriber_import_email_lists id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.subscriber_import_email_lists ALTER COLUMN id SET DEFAULT nextval('public.subscriber_import_email_lists_id_seq'::regclass);


--
-- Name: subscriber_import_people id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.subscriber_import_people ALTER COLUMN id SET DEFAULT nextval('public.subscriber_import_people_id_seq'::regclass);


--
-- Name: subscriber_import_sms_lists id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.subscriber_import_sms_lists ALTER COLUMN id SET DEFAULT nextval('public.subscriber_import_sms_lists_id_seq'::regclass);


--
-- Name: subscriber_imports id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.subscriber_imports ALTER COLUMN id SET DEFAULT nextval('public.subscriber_imports_id_seq'::regclass);


--
-- Name: subscribers id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.subscribers ALTER COLUMN id SET DEFAULT nextval('public.subscribers_id_seq'::regclass);


--
-- Name: survey_answers id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.survey_answers ALTER COLUMN id SET DEFAULT nextval('public.survey_answers_id_seq'::regclass);


--
-- Name: survey_questions id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.survey_questions ALTER COLUMN id SET DEFAULT nextval('public.survey_questions_id_seq'::regclass);


--
-- Name: survey_subscriber_answers id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.survey_subscriber_answers ALTER COLUMN id SET DEFAULT nextval('public.survey_subscriber_answers_id_seq'::regclass);


--
-- Name: surveys id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.surveys ALTER COLUMN id SET DEFAULT nextval('public.surveys_id_seq'::regclass);


--
-- Name: telegram_group_permissions id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.telegram_group_permissions ALTER COLUMN id SET DEFAULT nextval('public.telegram_group_permissions_id_seq'::regclass);


--
-- Name: telegram_groups id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.telegram_groups ALTER COLUMN id SET DEFAULT nextval('public.telegram_groups_id_seq'::regclass);


--
-- Name: telegram_message_schedules id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.telegram_message_schedules ALTER COLUMN id SET DEFAULT nextval('public.telegram_message_schedules_id_seq'::regclass);


--
-- Name: telegram_messages id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.telegram_messages ALTER COLUMN id SET DEFAULT nextval('public.telegram_messages_id_seq'::regclass);


--
-- Name: telegram_messages_telegram_groups id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.telegram_messages_telegram_groups ALTER COLUMN id SET DEFAULT nextval('public.telegram_messages_telegram_groups_id_seq'::regclass);


--
-- Name: text_blocks id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.text_blocks ALTER COLUMN id SET DEFAULT nextval('public.text_blocks_id_seq'::regclass);


--
-- Name: time_zones id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.time_zones ALTER COLUMN id SET DEFAULT nextval('public.time_zones_id_seq'::regclass);


--
-- Name: unsubscription_reasons id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.unsubscription_reasons ALTER COLUMN id SET DEFAULT nextval('public.unsubscription_reasons_id_seq'::regclass);


--
-- Name: unsubscriptions id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.unsubscriptions ALTER COLUMN id SET DEFAULT nextval('public.unsubscriptions_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Name: wiki_page_versions id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.wiki_page_versions ALTER COLUMN id SET DEFAULT nextval('public.wiki_page_versions_id_seq'::regclass);


--
-- Name: wiki_pages id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.wiki_pages ALTER COLUMN id SET DEFAULT nextval('public.wiki_pages_id_seq'::regclass);


--
-- Name: answers answers_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.answers
    ADD CONSTRAINT answers_pkey PRIMARY KEY (id);


--
-- Name: api_keys api_keys_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.api_keys
    ADD CONSTRAINT api_keys_pkey PRIMARY KEY (id);


--
-- Name: ar_internal_metadata ar_internal_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.ar_internal_metadata
    ADD CONSTRAINT ar_internal_metadata_pkey PRIMARY KEY (key);


--
-- Name: banner_clicks banner_clicks_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.banner_clicks
    ADD CONSTRAINT banner_clicks_pkey PRIMARY KEY (id);


--
-- Name: banner_orders banner_orders_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.banner_orders
    ADD CONSTRAINT banner_orders_pkey PRIMARY KEY (id);


--
-- Name: banner_owners banner_owners_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.banner_owners
    ADD CONSTRAINT banner_owners_pkey PRIMARY KEY (id);


--
-- Name: banner_shows banner_shows_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.banner_shows
    ADD CONSTRAINT banner_shows_pkey PRIMARY KEY (id);


--
-- Name: banners banners_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.banners
    ADD CONSTRAINT banners_pkey PRIMARY KEY (id);


--
-- Name: blacklist_emails blacklist_emails_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.blacklist_emails
    ADD CONSTRAINT blacklist_emails_pkey PRIMARY KEY (id);


--
-- Name: cached_subscribers cached_subscribers_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.cached_subscribers
    ADD CONSTRAINT cached_subscribers_pkey PRIMARY KEY (id);


--
-- Name: cities cities_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.cities
    ADD CONSTRAINT cities_pkey PRIMARY KEY (id);


--
-- Name: ckeditor_assets ckeditor_assets_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.ckeditor_assets
    ADD CONSTRAINT ckeditor_assets_pkey PRIMARY KEY (id);


--
-- Name: digest_categories digest_categories_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.digest_categories
    ADD CONSTRAINT digest_categories_pkey PRIMARY KEY (id);


--
-- Name: donation_methods donation_methods_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.donation_methods
    ADD CONSTRAINT donation_methods_pkey PRIMARY KEY (id);


--
-- Name: email_attachments email_attachments_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.email_attachments
    ADD CONSTRAINT email_attachments_pkey PRIMARY KEY (id);


--
-- Name: email_digest_bg_jobs email_digest_bg_jobs_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.email_digest_bg_jobs
    ADD CONSTRAINT email_digest_bg_jobs_pkey PRIMARY KEY (id);


--
-- Name: email_digests email_digests_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.email_digests
    ADD CONSTRAINT email_digests_pkey PRIMARY KEY (id);


--
-- Name: email_list_custom_vars email_list_custom_vars_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.email_list_custom_vars
    ADD CONSTRAINT email_list_custom_vars_pkey PRIMARY KEY (id);


--
-- Name: email_list_exclusion_permissions email_list_exclusion_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.email_list_exclusion_permissions
    ADD CONSTRAINT email_list_exclusion_permissions_pkey PRIMARY KEY (id);


--
-- Name: email_list_permissions email_list_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.email_list_permissions
    ADD CONSTRAINT email_list_permissions_pkey PRIMARY KEY (id);


--
-- Name: email_list_var_names email_list_var_names_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.email_list_var_names
    ADD CONSTRAINT email_list_var_names_pkey PRIMARY KEY (id);


--
-- Name: email_list_vars email_list_vars_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.email_list_vars
    ADD CONSTRAINT email_list_vars_pkey PRIMARY KEY (id);


--
-- Name: email_lists_mailing_destinations email_lists_mailing_destinations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.email_lists_mailing_destinations
    ADD CONSTRAINT email_lists_mailing_destinations_pkey PRIMARY KEY (id);


--
-- Name: email_lists_mailings_exclusions email_lists_mailings_exclusions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.email_lists_mailings_exclusions
    ADD CONSTRAINT email_lists_mailings_exclusions_pkey PRIMARY KEY (id);


--
-- Name: email_lists_mailings_inclusions email_lists_mailings_inclusions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.email_lists_mailings_inclusions
    ADD CONSTRAINT email_lists_mailings_inclusions_pkey PRIMARY KEY (id);


--
-- Name: email_lists email_lists_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.email_lists
    ADD CONSTRAINT email_lists_pkey PRIMARY KEY (id);


--
-- Name: email_problem_names email_problem_names_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.email_problem_names
    ADD CONSTRAINT email_problem_names_pkey PRIMARY KEY (id);


--
-- Name: email_problems email_problems_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.email_problems
    ADD CONSTRAINT email_problems_pkey PRIMARY KEY (id);


--
-- Name: email_send_histories email_send_histories_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.email_send_histories
    ADD CONSTRAINT email_send_histories_pkey PRIMARY KEY (id);


--
-- Name: email_subscriptions email_subscriptions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.email_subscriptions
    ADD CONSTRAINT email_subscriptions_pkey PRIMARY KEY (id);


--
-- Name: external_redirects external_redirects_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.external_redirects
    ADD CONSTRAINT external_redirects_pkey PRIMARY KEY (id);


--
-- Name: mailing_bg_jobs mailing_bg_jobs_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.mailing_bg_jobs
    ADD CONSTRAINT mailing_bg_jobs_pkey PRIMARY KEY (id);


--
-- Name: mailing_destinations mailing_destinations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.mailing_destinations
    ADD CONSTRAINT mailing_destinations_pkey PRIMARY KEY (id);


--
-- Name: mailing_request_images mailing_request_images_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.mailing_request_images
    ADD CONSTRAINT mailing_request_images_pkey PRIMARY KEY (id);


--
-- Name: mailing_request_messages mailing_request_messages_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.mailing_request_messages
    ADD CONSTRAINT mailing_request_messages_pkey PRIMARY KEY (id);


--
-- Name: mailing_requests mailing_requests_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.mailing_requests
    ADD CONSTRAINT mailing_requests_pkey PRIMARY KEY (id);


--
-- Name: mailing_views mailing_views_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.mailing_views
    ADD CONSTRAINT mailing_views_pkey PRIMARY KEY (id);


--
-- Name: mailings mailings_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.mailings
    ADD CONSTRAINT mailings_pkey PRIMARY KEY (id);


--
-- Name: messenger_types messenger_types_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.messenger_types
    ADD CONSTRAINT messenger_types_pkey PRIMARY KEY (id);


--
-- Name: mobile_tokens mobile_tokens_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.mobile_tokens
    ADD CONSTRAINT mobile_tokens_pkey PRIMARY KEY (id);


--
-- Name: news_items news_items_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.news_items
    ADD CONSTRAINT news_items_pkey PRIMARY KEY (id);


--
-- Name: pages pages_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.pages
    ADD CONSTRAINT pages_pkey PRIMARY KEY (id);


--
-- Name: phone_prefixes phone_prefixes_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.phone_prefixes
    ADD CONSTRAINT phone_prefixes_pkey PRIMARY KEY (id);


--
-- Name: phone_problem_names phone_problem_names_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.phone_problem_names
    ADD CONSTRAINT phone_problem_names_pkey PRIMARY KEY (id);


--
-- Name: phone_problems phone_problems_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.phone_problems
    ADD CONSTRAINT phone_problems_pkey PRIMARY KEY (id);


--
-- Name: polls polls_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.polls
    ADD CONSTRAINT polls_pkey PRIMARY KEY (id);


--
-- Name: schema_migrations schema_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.schema_migrations
    ADD CONSTRAINT schema_migrations_pkey PRIMARY KEY (version);


--
-- Name: sessions sessions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sessions
    ADD CONSTRAINT sessions_pkey PRIMARY KEY (id);


--
-- Name: settings settings_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.settings
    ADD CONSTRAINT settings_pkey PRIMARY KEY (id);


--
-- Name: sms_account_actions sms_account_actions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sms_account_actions
    ADD CONSTRAINT sms_account_actions_pkey PRIMARY KEY (id);


--
-- Name: sms_accounts_permissions sms_accounts_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sms_accounts_permissions
    ADD CONSTRAINT sms_accounts_permissions_pkey PRIMARY KEY (id);


--
-- Name: sms_accounts sms_accounts_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sms_accounts
    ADD CONSTRAINT sms_accounts_pkey PRIMARY KEY (id);


--
-- Name: sms_list_permissions sms_list_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sms_list_permissions
    ADD CONSTRAINT sms_list_permissions_pkey PRIMARY KEY (id);


--
-- Name: sms_lists sms_lists_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sms_lists
    ADD CONSTRAINT sms_lists_pkey PRIMARY KEY (id);


--
-- Name: sms_lists_sms_sendings sms_lists_sms_sendings_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sms_lists_sms_sendings
    ADD CONSTRAINT sms_lists_sms_sendings_pkey PRIMARY KEY (id);


--
-- Name: sms_messages sms_messages_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sms_messages
    ADD CONSTRAINT sms_messages_pkey PRIMARY KEY (id);


--
-- Name: sms_operators sms_operators_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sms_operators
    ADD CONSTRAINT sms_operators_pkey PRIMARY KEY (id);


--
-- Name: sms_send_histories sms_send_histories_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sms_send_histories
    ADD CONSTRAINT sms_send_histories_pkey PRIMARY KEY (id);


--
-- Name: sms_send_history_types sms_send_history_types_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sms_send_history_types
    ADD CONSTRAINT sms_send_history_types_pkey PRIMARY KEY (id);


--
-- Name: sms_sending_bg_jobs sms_sending_bg_jobs_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sms_sending_bg_jobs
    ADD CONSTRAINT sms_sending_bg_jobs_pkey PRIMARY KEY (id);


--
-- Name: sms_sendings sms_sendings_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sms_sendings
    ADD CONSTRAINT sms_sendings_pkey PRIMARY KEY (id);


--
-- Name: sms_subscriptions sms_subscriptions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sms_subscriptions
    ADD CONSTRAINT sms_subscriptions_pkey PRIMARY KEY (id);


--
-- Name: smtp_setting_groups smtp_setting_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.smtp_setting_groups
    ADD CONSTRAINT smtp_setting_groups_pkey PRIMARY KEY (id);


--
-- Name: smtp_settings smtp_settings_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.smtp_settings
    ADD CONSTRAINT smtp_settings_pkey PRIMARY KEY (id);


--
-- Name: stripo_file_keys stripo_file_keys_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.stripo_file_keys
    ADD CONSTRAINT stripo_file_keys_pkey PRIMARY KEY (id);


--
-- Name: stripo_files stripo_files_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.stripo_files
    ADD CONSTRAINT stripo_files_pkey PRIMARY KEY (id);


--
-- Name: stripo_template_group_email_lists stripo_template_group_email_lists_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.stripo_template_group_email_lists
    ADD CONSTRAINT stripo_template_group_email_lists_pkey PRIMARY KEY (id);


--
-- Name: stripo_template_group_users stripo_template_group_users_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.stripo_template_group_users
    ADD CONSTRAINT stripo_template_group_users_pkey PRIMARY KEY (id);


--
-- Name: stripo_template_groups stripo_template_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.stripo_template_groups
    ADD CONSTRAINT stripo_template_groups_pkey PRIMARY KEY (id);


--
-- Name: stripo_templates stripo_templates_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.stripo_templates
    ADD CONSTRAINT stripo_templates_pkey PRIMARY KEY (id);


--
-- Name: subscriber_answers subscriber_answers_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.subscriber_answers
    ADD CONSTRAINT subscriber_answers_pkey PRIMARY KEY (id);


--
-- Name: subscriber_custom_vars subscriber_custom_vars_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.subscriber_custom_vars
    ADD CONSTRAINT subscriber_custom_vars_pkey PRIMARY KEY (id);


--
-- Name: subscriber_import_email_lists subscriber_import_email_lists_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.subscriber_import_email_lists
    ADD CONSTRAINT subscriber_import_email_lists_pkey PRIMARY KEY (id);


--
-- Name: subscriber_import_people subscriber_import_people_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.subscriber_import_people
    ADD CONSTRAINT subscriber_import_people_pkey PRIMARY KEY (id);


--
-- Name: subscriber_import_sms_lists subscriber_import_sms_lists_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.subscriber_import_sms_lists
    ADD CONSTRAINT subscriber_import_sms_lists_pkey PRIMARY KEY (id);


--
-- Name: subscriber_imports subscriber_imports_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.subscriber_imports
    ADD CONSTRAINT subscriber_imports_pkey PRIMARY KEY (id);


--
-- Name: subscribers subscribers_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.subscribers
    ADD CONSTRAINT subscribers_pkey PRIMARY KEY (id);


--
-- Name: survey_answers survey_answers_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.survey_answers
    ADD CONSTRAINT survey_answers_pkey PRIMARY KEY (id);


--
-- Name: survey_questions survey_questions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.survey_questions
    ADD CONSTRAINT survey_questions_pkey PRIMARY KEY (id);


--
-- Name: survey_subscriber_answers survey_subscriber_answers_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.survey_subscriber_answers
    ADD CONSTRAINT survey_subscriber_answers_pkey PRIMARY KEY (id);


--
-- Name: surveys surveys_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.surveys
    ADD CONSTRAINT surveys_pkey PRIMARY KEY (id);


--
-- Name: telegram_group_permissions telegram_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.telegram_group_permissions
    ADD CONSTRAINT telegram_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: telegram_groups telegram_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.telegram_groups
    ADD CONSTRAINT telegram_groups_pkey PRIMARY KEY (id);


--
-- Name: telegram_message_schedules telegram_message_schedules_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.telegram_message_schedules
    ADD CONSTRAINT telegram_message_schedules_pkey PRIMARY KEY (id);


--
-- Name: telegram_messages telegram_messages_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.telegram_messages
    ADD CONSTRAINT telegram_messages_pkey PRIMARY KEY (id);


--
-- Name: telegram_messages_telegram_groups telegram_messages_telegram_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.telegram_messages_telegram_groups
    ADD CONSTRAINT telegram_messages_telegram_groups_pkey PRIMARY KEY (id);


--
-- Name: text_blocks text_blocks_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.text_blocks
    ADD CONSTRAINT text_blocks_pkey PRIMARY KEY (id);


--
-- Name: time_zones time_zones_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.time_zones
    ADD CONSTRAINT time_zones_pkey PRIMARY KEY (id);


--
-- Name: unsubscription_reasons unsubscription_reasons_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.unsubscription_reasons
    ADD CONSTRAINT unsubscription_reasons_pkey PRIMARY KEY (id);


--
-- Name: unsubscriptions unsubscriptions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.unsubscriptions
    ADD CONSTRAINT unsubscriptions_pkey PRIMARY KEY (id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: wiki_page_versions wiki_page_versions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.wiki_page_versions
    ADD CONSTRAINT wiki_page_versions_pkey PRIMARY KEY (id);


--
-- Name: wiki_pages wiki_pages_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.wiki_pages
    ADD CONSTRAINT wiki_pages_pkey PRIMARY KEY (id);


--
-- Name: admin_subscriber_answer; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX admin_subscriber_answer ON public.survey_subscriber_answers USING btree (subscriber_id, survey_question_id);


--
-- Name: admin_surveys_position; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX admin_surveys_position ON public.survey_answers USING btree ("position", survey_question_id);


--
-- Name: email_list_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX email_list_id ON public.stripo_template_group_email_lists USING btree (email_list_id);


--
-- Name: idx_subscriber_custom_vars_subscriber_email_list_custom_var; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX idx_subscriber_custom_vars_subscriber_email_list_custom_var ON public.subscriber_custom_vars USING btree (subscriber_id, email_list_custom_var_id);


--
-- Name: index_answers_on_poll_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_answers_on_poll_id ON public.answers USING btree (poll_id);


--
-- Name: index_answers_on_position_and_poll_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_answers_on_position_and_poll_id ON public.answers USING btree ("position", poll_id);


--
-- Name: index_api_keys_on_key; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_api_keys_on_key ON public.api_keys USING btree (key);


--
-- Name: index_api_keys_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_api_keys_on_user_id ON public.api_keys USING btree (user_id);


--
-- Name: index_banner_clicks_on_banner_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_banner_clicks_on_banner_id ON public.banner_clicks USING btree (banner_id);


--
-- Name: index_banner_orders_on_banner_owner_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_banner_orders_on_banner_owner_id ON public.banner_orders USING btree (banner_owner_id);


--
-- Name: index_banner_shows_on_banner_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_banner_shows_on_banner_id ON public.banner_shows USING btree (banner_id);


--
-- Name: index_banners_on_banner_order_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_banners_on_banner_order_id ON public.banners USING btree (banner_order_id);


--
-- Name: index_banners_on_placement; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_banners_on_placement ON public.banners USING btree (placement);


--
-- Name: index_banners_on_priority; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_banners_on_priority ON public.banners USING btree (priority);


--
-- Name: index_blacklist_emails_on_email; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_blacklist_emails_on_email ON public.blacklist_emails USING btree (email);


--
-- Name: index_cached_subscribers_on_sms_sending_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_cached_subscribers_on_sms_sending_id ON public.cached_subscribers USING btree (sms_sending_id);


--
-- Name: index_cities_on_ancestry; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_cities_on_ancestry ON public.cities USING btree (ancestry);


--
-- Name: index_cities_on_priority; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_cities_on_priority ON public.cities USING btree (priority);


--
-- Name: index_digest_categories_on_email_list_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_digest_categories_on_email_list_id ON public.digest_categories USING btree (email_list_id);


--
-- Name: index_digest_categories_on_priority; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_digest_categories_on_priority ON public.digest_categories USING btree (priority);


--
-- Name: index_email_attachments_on_mailing_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_email_attachments_on_mailing_id ON public.email_attachments USING btree (mailing_id);


--
-- Name: index_email_digest_bg_jobs_on_email_digest_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_email_digest_bg_jobs_on_email_digest_id ON public.email_digest_bg_jobs USING btree (email_digest_id);


--
-- Name: index_email_digests_on_deliver_at; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_email_digests_on_deliver_at ON public.email_digests USING btree (deliver_at);


--
-- Name: index_email_digests_on_num_and_year_and_email_list_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_email_digests_on_num_and_year_and_email_list_id ON public.email_digests USING btree (num, year, email_list_id);


--
-- Name: index_email_list_custom_vars_on_email_list_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_email_list_custom_vars_on_email_list_id ON public.email_list_custom_vars USING btree (email_list_id);


--
-- Name: index_email_list_exclusion_permissions_on_email_list_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_email_list_exclusion_permissions_on_email_list_id ON public.email_list_exclusion_permissions USING btree (email_list_id);


--
-- Name: index_email_list_exclusion_permissions_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_email_list_exclusion_permissions_on_user_id ON public.email_list_exclusion_permissions USING btree (user_id);


--
-- Name: index_email_list_permissions_on_user_id_and_email_list_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_email_list_permissions_on_user_id_and_email_list_id ON public.email_list_permissions USING btree (user_id, email_list_id);


--
-- Name: index_email_list_var_names_on_email_list_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_email_list_var_names_on_email_list_id ON public.email_list_var_names USING btree (email_list_id);


--
-- Name: index_email_list_var_names_on_position; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_email_list_var_names_on_position ON public.email_list_var_names USING btree ("position");


--
-- Name: index_email_list_vars_on_email_list_var_name_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_email_list_vars_on_email_list_var_name_id ON public.email_list_vars USING btree (email_list_var_name_id);


--
-- Name: index_email_list_vars_on_mailing_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_email_list_vars_on_mailing_id ON public.email_list_vars USING btree (mailing_id);


--
-- Name: index_email_lists_mailing_requests_on_email_list_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_email_lists_mailing_requests_on_email_list_id ON public.email_lists_mailing_requests USING btree (email_list_id);


--
-- Name: index_email_lists_mailing_requests_on_mailing_request_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_email_lists_mailing_requests_on_mailing_request_id ON public.email_lists_mailing_requests USING btree (mailing_request_id);


--
-- Name: index_email_lists_mailings_exclusions_on_email_list_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_email_lists_mailings_exclusions_on_email_list_id ON public.email_lists_mailings_exclusions USING btree (email_list_id);


--
-- Name: index_email_lists_mailings_exclusions_on_mailing_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_email_lists_mailings_exclusions_on_mailing_id ON public.email_lists_mailings_exclusions USING btree (mailing_id);


--
-- Name: index_email_lists_mailings_inclusions_on_email_list_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_email_lists_mailings_inclusions_on_email_list_id ON public.email_lists_mailings_inclusions USING btree (email_list_id);


--
-- Name: index_email_lists_mailings_inclusions_on_mailing_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_email_lists_mailings_inclusions_on_mailing_id ON public.email_lists_mailings_inclusions USING btree (mailing_id);


--
-- Name: index_email_lists_on_group_priority; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_email_lists_on_group_priority ON public.email_lists USING btree (group_priority);


--
-- Name: index_email_lists_on_name; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_email_lists_on_name ON public.email_lists USING btree (name);


--
-- Name: index_email_lists_on_never_visible; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_email_lists_on_never_visible ON public.email_lists USING btree (never_visible);


--
-- Name: index_email_lists_on_visible; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_email_lists_on_visible ON public.email_lists USING btree (visible);


--
-- Name: index_email_problem_names_on_position; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_email_problem_names_on_position ON public.email_problem_names USING btree ("position");


--
-- Name: index_email_problems_on_solved; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_email_problems_on_solved ON public.email_problems USING btree (solved);


--
-- Name: index_email_problems_on_subscriber_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_email_problems_on_subscriber_id ON public.email_problems USING btree (subscriber_id);


--
-- Name: index_email_send_histories_on_email; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_email_send_histories_on_email ON public.email_send_histories USING btree (email);


--
-- Name: index_email_send_histories_on_subscriber_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_email_send_histories_on_subscriber_id ON public.email_send_histories USING btree (subscriber_id);


--
-- Name: index_email_subscriptions_on_subscriber_id_and_email_list_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_email_subscriptions_on_subscriber_id_and_email_list_id ON public.email_subscriptions USING btree (subscriber_id, email_list_id);


--
-- Name: index_external_redirects_on_url; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_external_redirects_on_url ON public.external_redirects USING btree (url);


--
-- Name: index_mailing_bg_jobs_on_mailing_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_mailing_bg_jobs_on_mailing_id ON public.mailing_bg_jobs USING btree (mailing_id);


--
-- Name: index_mailing_request_images_on_mailing_request_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_mailing_request_images_on_mailing_request_id ON public.mailing_request_images USING btree (mailing_request_id);


--
-- Name: index_mailing_request_messages_on_mailing_request_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_mailing_request_messages_on_mailing_request_id ON public.mailing_request_messages USING btree (mailing_request_id);


--
-- Name: index_mailing_request_messages_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_mailing_request_messages_on_user_id ON public.mailing_request_messages USING btree (user_id);


--
-- Name: index_mailing_requests_on_donation_method_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_mailing_requests_on_donation_method_id ON public.mailing_requests USING btree (donation_method_id);


--
-- Name: index_mailing_requests_on_mailing_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_mailing_requests_on_mailing_id ON public.mailing_requests USING btree (mailing_id);


--
-- Name: index_mailing_requests_on_subscriber_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_mailing_requests_on_subscriber_id ON public.mailing_requests USING btree (subscriber_id);


--
-- Name: index_mailing_views_on_mailing_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_mailing_views_on_mailing_id ON public.mailing_views USING btree (mailing_id);


--
-- Name: index_mailing_views_on_subscriber_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_mailing_views_on_subscriber_id ON public.mailing_views USING btree (subscriber_id);


--
-- Name: index_mailings_on_created_at; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_mailings_on_created_at ON public.mailings USING btree (created_at);


--
-- Name: index_mailings_on_deliver_at; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_mailings_on_deliver_at ON public.mailings USING btree (deliver_at);


--
-- Name: index_mailings_on_digest; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_mailings_on_digest ON public.mailings USING btree (digest);


--
-- Name: index_mailings_on_email_list_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_mailings_on_email_list_id ON public.mailings USING btree (email_list_id);


--
-- Name: index_mailings_on_issue_nb; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_mailings_on_issue_nb ON public.mailings USING btree (issue_nb);


--
-- Name: index_mailings_on_published; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_mailings_on_published ON public.mailings USING btree (published);


--
-- Name: index_mailings_on_raised_exception_at; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_mailings_on_raised_exception_at ON public.mailings USING btree (raised_exception_at);


--
-- Name: index_mailings_on_searchable; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_mailings_on_searchable ON public.mailings USING gin (searchable);


--
-- Name: index_mailings_on_send_failed; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_mailings_on_send_failed ON public.mailings USING btree (send_failed);


--
-- Name: index_mailings_on_sending; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_mailings_on_sending ON public.mailings USING btree (sending);


--
-- Name: index_mailings_on_sent; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_mailings_on_sent ON public.mailings USING btree (sent);


--
-- Name: index_mailings_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_mailings_on_user_id ON public.mailings USING btree (user_id);


--
-- Name: index_mailings_on_visible; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_mailings_on_visible ON public.mailings USING btree (visible);


--
-- Name: index_mdel_email_list_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_mdel_email_list_id ON public.email_lists_mailing_destinations USING btree (email_list_id);


--
-- Name: index_mdel_mailing_destination_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_mdel_mailing_destination_id ON public.email_lists_mailing_destinations USING btree (mailing_destination_id);


--
-- Name: index_mdu_mailing_destination_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_mdu_mailing_destination_id ON public.mailing_destinations_users USING btree (mailing_destination_id);


--
-- Name: index_mdu_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_mdu_user_id ON public.mailing_destinations_users USING btree (user_id);


--
-- Name: index_messenger_types_on_char; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_messenger_types_on_char ON public.messenger_types USING btree ("char");


--
-- Name: index_messenger_types_on_name; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_messenger_types_on_name ON public.messenger_types USING btree (name);


--
-- Name: index_mobile_tokens_on_subscriber_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_mobile_tokens_on_subscriber_id ON public.mobile_tokens USING btree (subscriber_id);


--
-- Name: index_news_items_on_date; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_news_items_on_date ON public.news_items USING btree (date);


--
-- Name: index_news_items_on_slug; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_news_items_on_slug ON public.news_items USING btree (slug);


--
-- Name: index_pages_on_position; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_pages_on_position ON public.pages USING btree ("position");


--
-- Name: index_pages_on_show_in_navbar; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_pages_on_show_in_navbar ON public.pages USING btree (show_in_navbar);


--
-- Name: index_pages_on_slug; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_pages_on_slug ON public.pages USING btree (slug);


--
-- Name: index_phone_prefixes_on_sms_operator_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_phone_prefixes_on_sms_operator_id ON public.phone_prefixes USING btree (sms_operator_id);


--
-- Name: index_phone_problem_names_on_position; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_phone_problem_names_on_position ON public.phone_problem_names USING btree ("position");


--
-- Name: index_phone_problems_on_solved; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_phone_problems_on_solved ON public.phone_problems USING btree (solved);


--
-- Name: index_phone_problems_on_subscriber_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_phone_problems_on_subscriber_id ON public.phone_problems USING btree (subscriber_id);


--
-- Name: index_sessions_on_session_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_sessions_on_session_id ON public.sessions USING btree (session_id);


--
-- Name: index_sessions_on_updated_at; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_sessions_on_updated_at ON public.sessions USING btree (updated_at);


--
-- Name: index_settings_on_name; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_settings_on_name ON public.settings USING btree (name);


--
-- Name: index_sms_account_actions_on_cancelled; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_sms_account_actions_on_cancelled ON public.sms_account_actions USING btree (cancelled);


--
-- Name: index_sms_account_actions_on_reserved; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_sms_account_actions_on_reserved ON public.sms_account_actions USING btree (reserved);


--
-- Name: index_sms_account_actions_on_sms_account_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_sms_account_actions_on_sms_account_id ON public.sms_account_actions USING btree (sms_account_id);


--
-- Name: index_sms_accounts_on_is_admin; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_sms_accounts_on_is_admin ON public.sms_accounts USING btree (is_admin);


--
-- Name: index_sms_accounts_on_name; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_sms_accounts_on_name ON public.sms_accounts USING btree (name);


--
-- Name: index_sms_accounts_permissions_on_sms_account_id_and_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_sms_accounts_permissions_on_sms_account_id_and_user_id ON public.sms_accounts_permissions USING btree (sms_account_id, user_id);


--
-- Name: index_sms_list_permissions_on_user_id_and_sms_list_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_sms_list_permissions_on_user_id_and_sms_list_id ON public.sms_list_permissions USING btree (user_id, sms_list_id);


--
-- Name: index_sms_lists_on_ancestry; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_sms_lists_on_ancestry ON public.sms_lists USING btree (ancestry);


--
-- Name: index_sms_lists_on_name; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_sms_lists_on_name ON public.sms_lists USING btree (name);


--
-- Name: index_sms_lists_on_never_visible; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_sms_lists_on_never_visible ON public.sms_lists USING btree (never_visible);


--
-- Name: index_sms_lists_on_public; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_sms_lists_on_public ON public.sms_lists USING btree (public);


--
-- Name: index_sms_lists_on_visible; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_sms_lists_on_visible ON public.sms_lists USING btree (visible);


--
-- Name: index_sms_lists_sms_sendings_on_sms_list_id_and_sms_sending_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_sms_lists_sms_sendings_on_sms_list_id_and_sms_sending_id ON public.sms_lists_sms_sendings USING btree (sms_list_id, sms_sending_id);


--
-- Name: index_sms_messages_on_position; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_sms_messages_on_position ON public.sms_messages USING btree ("position");


--
-- Name: index_sms_messages_on_sms_send_history_type_id_and_karmi; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_sms_messages_on_sms_send_history_type_id_and_karmi ON public.sms_messages USING btree (sms_send_history_type_id, karmi);


--
-- Name: index_sms_operators_on_name; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_sms_operators_on_name ON public.sms_operators USING btree (name);


--
-- Name: index_sms_operators_on_sms_sending_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_sms_operators_on_sms_sending_id ON public.sms_operators USING btree (sms_sending_id);


--
-- Name: index_sms_send_histories_on_phone_and_sms_send_history_type_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_sms_send_histories_on_phone_and_sms_send_history_type_id ON public.sms_send_histories USING btree (phone, sms_send_history_type_id);


--
-- Name: index_sms_send_histories_on_subscriber_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_sms_send_histories_on_subscriber_id ON public.sms_send_histories USING btree (subscriber_id);


--
-- Name: index_sms_send_histories_on_updated_at; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_sms_send_histories_on_updated_at ON public.sms_send_histories USING btree (updated_at);


--
-- Name: index_sms_send_history_types_on_name; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_sms_send_history_types_on_name ON public.sms_send_history_types USING btree (name);


--
-- Name: index_sms_sending_bg_jobs_on_sms_sending_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_sms_sending_bg_jobs_on_sms_sending_id ON public.sms_sending_bg_jobs USING btree (sms_sending_id);


--
-- Name: index_sms_sendings_on_deliver_at; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_sms_sendings_on_deliver_at ON public.sms_sendings USING btree (deliver_at);


--
-- Name: index_sms_sendings_on_no_money; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_sms_sendings_on_no_money ON public.sms_sendings USING btree (no_money);


--
-- Name: index_sms_sendings_on_num; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_sms_sendings_on_num ON public.sms_sendings USING btree (num);


--
-- Name: index_sms_sendings_on_pay_from_sms_account_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_sms_sendings_on_pay_from_sms_account_id ON public.sms_sendings USING btree (pay_from_sms_account_id);


--
-- Name: index_sms_sendings_on_published; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_sms_sendings_on_published ON public.sms_sendings USING btree (published);


--
-- Name: index_sms_sendings_on_sending; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_sms_sendings_on_sending ON public.sms_sendings USING btree (sending);


--
-- Name: index_sms_sendings_on_sent; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_sms_sendings_on_sent ON public.sms_sendings USING btree (sent);


--
-- Name: index_sms_sendings_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_sms_sendings_on_user_id ON public.sms_sendings USING btree (user_id);


--
-- Name: index_sms_subscriptions_on_subscriber_id_and_sms_list_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_sms_subscriptions_on_subscriber_id_and_sms_list_id ON public.sms_subscriptions USING btree (subscriber_id, sms_list_id);


--
-- Name: index_smtp_setting_groups_on_position; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_smtp_setting_groups_on_position ON public.smtp_setting_groups USING btree ("position");


--
-- Name: index_smtp_setting_groups_on_selected; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_smtp_setting_groups_on_selected ON public.smtp_setting_groups USING btree (selected);


--
-- Name: index_smtp_settings_on_smtp_setting_group_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_smtp_settings_on_smtp_setting_group_id ON public.smtp_settings USING btree (smtp_setting_group_id);


--
-- Name: index_stripo_file_keys_on_plugin_id_and_email_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_stripo_file_keys_on_plugin_id_and_email_id ON public.stripo_file_keys USING btree (plugin_id, email_id);


--
-- Name: index_stripo_file_keys_on_stripo_template_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_stripo_file_keys_on_stripo_template_id ON public.stripo_file_keys USING btree (stripo_template_id);


--
-- Name: index_stripo_files_on_stripo_file_key_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_stripo_files_on_stripo_file_key_id ON public.stripo_files USING btree (stripo_file_key_id);


--
-- Name: index_stripo_template_group_users_on_stripo_template_group_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_stripo_template_group_users_on_stripo_template_group_id ON public.stripo_template_group_users USING btree (stripo_template_group_id);


--
-- Name: index_stripo_template_group_users_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_stripo_template_group_users_on_user_id ON public.stripo_template_group_users USING btree (user_id);


--
-- Name: index_stripo_template_groups_on_ancestry; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_stripo_template_groups_on_ancestry ON public.stripo_template_groups USING btree (ancestry);


--
-- Name: index_stripo_template_groups_on_position; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_stripo_template_groups_on_position ON public.stripo_template_groups USING btree ("position");


--
-- Name: index_stripo_template_groups_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_stripo_template_groups_on_user_id ON public.stripo_template_groups USING btree (user_id);


--
-- Name: index_stripo_templates_on_stripo_template_group_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_stripo_templates_on_stripo_template_group_id ON public.stripo_templates USING btree (stripo_template_group_id);


--
-- Name: index_subscriber_answers_on_answer_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_subscriber_answers_on_answer_id ON public.subscriber_answers USING btree (answer_id);


--
-- Name: index_subscriber_answers_on_poll_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_subscriber_answers_on_poll_id ON public.subscriber_answers USING btree (poll_id);


--
-- Name: index_subscriber_answers_on_subscriber_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_subscriber_answers_on_subscriber_id ON public.subscriber_answers USING btree (subscriber_id);


--
-- Name: index_subscriber_import_email_lists_on_subscriber_import_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_subscriber_import_email_lists_on_subscriber_import_id ON public.subscriber_import_email_lists USING btree (subscriber_import_id);


--
-- Name: index_subscriber_import_people_on_error_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_subscriber_import_people_on_error_id ON public.subscriber_import_people USING btree (error_id);


--
-- Name: index_subscriber_import_people_on_subscriber_import_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_subscriber_import_people_on_subscriber_import_id ON public.subscriber_import_people USING btree (subscriber_import_id);


--
-- Name: index_subscriber_import_sms_lists_on_subscriber_import_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_subscriber_import_sms_lists_on_subscriber_import_id ON public.subscriber_import_sms_lists USING btree (subscriber_import_id);


--
-- Name: index_subscriber_imports_on_created_at; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_subscriber_imports_on_created_at ON public.subscriber_imports USING btree (created_at);


--
-- Name: index_subscribers_on_city_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_subscribers_on_city_id ON public.subscribers USING btree (city_id);


--
-- Name: index_subscribers_on_confirmed_at; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_subscribers_on_confirmed_at ON public.subscribers USING btree (confirmed_at);


--
-- Name: index_subscribers_on_email_lower; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_subscribers_on_email_lower ON public.subscribers USING btree (lower((email)::text));


--
-- Name: index_subscribers_on_locked_at; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_subscribers_on_locked_at ON public.subscribers USING btree (locked_at);


--
-- Name: index_subscribers_on_name; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_subscribers_on_name ON public.subscribers USING btree (name);


--
-- Name: index_subscribers_on_phone; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_subscribers_on_phone ON public.subscribers USING btree (phone);


--
-- Name: index_subscribers_on_phone_blocked_at; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_subscribers_on_phone_blocked_at ON public.subscribers USING btree (phone_blocked_at);


--
-- Name: index_subscribers_on_phone_confirmed_at; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_subscribers_on_phone_confirmed_at ON public.subscribers USING btree (phone_confirmed_at);


--
-- Name: index_subscribers_on_phone_problems_count; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_subscribers_on_phone_problems_count ON public.subscribers USING btree (phone_problems_count);


--
-- Name: index_subscribers_on_raised_exception_at; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_subscribers_on_raised_exception_at ON public.subscribers USING btree (raised_exception_at);


--
-- Name: index_subscribers_on_reset_password_token; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_subscribers_on_reset_password_token ON public.subscribers USING btree (reset_password_token);


--
-- Name: index_subscribers_on_send_sms; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_subscribers_on_send_sms ON public.subscribers USING btree (send_sms);


--
-- Name: index_subscribers_on_sms_operator_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_subscribers_on_sms_operator_id ON public.subscribers USING btree (sms_operator_id);


--
-- Name: index_subscribers_on_time_zone_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_subscribers_on_time_zone_id ON public.subscribers USING btree (time_zone_id);


--
-- Name: index_subscribers_on_unlock_token; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_subscribers_on_unlock_token ON public.subscribers USING btree (unlock_token);


--
-- Name: index_survey_answers_on_survey_question_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_survey_answers_on_survey_question_id ON public.survey_answers USING btree (survey_question_id);


--
-- Name: index_survey_questions_on_position_and_survey_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_survey_questions_on_position_and_survey_id ON public.survey_questions USING btree ("position", survey_id);


--
-- Name: index_survey_questions_on_survey_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_survey_questions_on_survey_id ON public.survey_questions USING btree (survey_id);


--
-- Name: index_survey_subscriber_answers_on_subscriber_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_survey_subscriber_answers_on_subscriber_id ON public.survey_subscriber_answers USING btree (subscriber_id);


--
-- Name: index_survey_subscriber_answers_on_survey_answer_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_survey_subscriber_answers_on_survey_answer_id ON public.survey_subscriber_answers USING btree (survey_answer_id);


--
-- Name: index_survey_subscriber_answers_on_survey_question_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_survey_subscriber_answers_on_survey_question_id ON public.survey_subscriber_answers USING btree (survey_question_id);


--
-- Name: index_telegram_group_permissions_on_telegram_group_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_telegram_group_permissions_on_telegram_group_id ON public.telegram_group_permissions USING btree (telegram_group_id);


--
-- Name: index_telegram_group_permissions_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_telegram_group_permissions_on_user_id ON public.telegram_group_permissions USING btree (user_id);


--
-- Name: index_telegram_groups_on_visible; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_telegram_groups_on_visible ON public.telegram_groups USING btree (visible);


--
-- Name: index_telegram_message_schedules_on_telegram_message_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_telegram_message_schedules_on_telegram_message_id ON public.telegram_message_schedules USING btree (telegram_message_id);


--
-- Name: index_telegram_messages_on_deliver_at; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_telegram_messages_on_deliver_at ON public.telegram_messages USING btree (deliver_at);


--
-- Name: index_telegram_messages_on_published; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_telegram_messages_on_published ON public.telegram_messages USING btree (published);


--
-- Name: index_telegram_messages_on_telegram_group_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_telegram_messages_on_telegram_group_id ON public.telegram_messages USING btree (telegram_group_id);


--
-- Name: index_telegram_messages_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_telegram_messages_on_user_id ON public.telegram_messages USING btree (user_id);


--
-- Name: index_telegram_messages_telegram_groups_on_telegram_group_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_telegram_messages_telegram_groups_on_telegram_group_id ON public.telegram_messages_telegram_groups USING btree (telegram_group_id);


--
-- Name: index_telegram_messages_telegram_groups_on_telegram_message_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_telegram_messages_telegram_groups_on_telegram_message_id ON public.telegram_messages_telegram_groups USING btree (telegram_message_id);


--
-- Name: index_text_blocks_on_name; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_text_blocks_on_name ON public.text_blocks USING btree (name);


--
-- Name: index_time_zones_on_name; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_time_zones_on_name ON public.time_zones USING btree (name);


--
-- Name: index_time_zones_on_priority; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_time_zones_on_priority ON public.time_zones USING btree (priority);


--
-- Name: index_unsubscription_reasons_on_position; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_unsubscription_reasons_on_position ON public.unsubscription_reasons USING btree ("position");


--
-- Name: index_unsubscriptions_on_created_at; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_unsubscriptions_on_created_at ON public.unsubscriptions USING btree (created_at);


--
-- Name: index_unsubscriptions_on_email_list_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_unsubscriptions_on_email_list_id ON public.unsubscriptions USING btree (email_list_id);


--
-- Name: index_unsubscriptions_on_subscriber_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_unsubscriptions_on_subscriber_id ON public.unsubscriptions USING btree (subscriber_id);


--
-- Name: index_unsubscriptions_on_unsubscription_reason_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_unsubscriptions_on_unsubscription_reason_id ON public.unsubscriptions USING btree (unsubscription_reason_id);


--
-- Name: index_users_on_email; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_users_on_email ON public.users USING btree (email);


--
-- Name: index_users_on_reset_password_token; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_users_on_reset_password_token ON public.users USING btree (reset_password_token);


--
-- Name: index_users_on_username; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_users_on_username ON public.users USING btree (username);


--
-- Name: index_wiki_page_versions_on_page_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_wiki_page_versions_on_page_id ON public.wiki_page_versions USING btree (page_id);


--
-- Name: index_wiki_page_versions_on_updator_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_wiki_page_versions_on_updator_id ON public.wiki_page_versions USING btree (updator_id);


--
-- Name: index_wiki_pages_on_creator_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_wiki_pages_on_creator_id ON public.wiki_pages USING btree (creator_id);


--
-- Name: index_wiki_pages_on_path; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_wiki_pages_on_path ON public.wiki_pages USING btree (path);


--
-- Name: stripo_template_group_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX stripo_template_group_id ON public.stripo_template_group_email_lists USING btree (stripo_template_group_id);


--
-- Name: stripo_template_group_email_lists fk_rails_006293ae1a; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.stripo_template_group_email_lists
    ADD CONSTRAINT fk_rails_006293ae1a FOREIGN KEY (stripo_template_group_id) REFERENCES public.stripo_template_groups(id);


--
-- Name: stripo_template_group_users fk_rails_097d0908fd; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.stripo_template_group_users
    ADD CONSTRAINT fk_rails_097d0908fd FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: stripo_templates fk_rails_245277ecbd; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.stripo_templates
    ADD CONSTRAINT fk_rails_245277ecbd FOREIGN KEY (stripo_template_group_id) REFERENCES public.stripo_template_groups(id);


--
-- Name: telegram_messages fk_rails_24f15a85e7; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.telegram_messages
    ADD CONSTRAINT fk_rails_24f15a85e7 FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: stripo_template_groups fk_rails_2ae0fa53b1; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.stripo_template_groups
    ADD CONSTRAINT fk_rails_2ae0fa53b1 FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: api_keys fk_rails_32c28d0dc2; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.api_keys
    ADD CONSTRAINT fk_rails_32c28d0dc2 FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: telegram_group_permissions fk_rails_4c6b77e442; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.telegram_group_permissions
    ADD CONSTRAINT fk_rails_4c6b77e442 FOREIGN KEY (telegram_group_id) REFERENCES public.telegram_groups(id);


--
-- Name: mailing_request_images fk_rails_5410b68f0a; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.mailing_request_images
    ADD CONSTRAINT fk_rails_5410b68f0a FOREIGN KEY (mailing_request_id) REFERENCES public.mailing_requests(id);


--
-- Name: survey_subscriber_answers fk_rails_5f823a6411; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.survey_subscriber_answers
    ADD CONSTRAINT fk_rails_5f823a6411 FOREIGN KEY (survey_question_id) REFERENCES public.survey_questions(id);


--
-- Name: mailing_request_messages fk_rails_8d929b57d2; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.mailing_request_messages
    ADD CONSTRAINT fk_rails_8d929b57d2 FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: mailing_views fk_rails_92eda38a09; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.mailing_views
    ADD CONSTRAINT fk_rails_92eda38a09 FOREIGN KEY (mailing_id) REFERENCES public.mailings(id);


--
-- Name: telegram_message_schedules fk_rails_9ae96ef653; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.telegram_message_schedules
    ADD CONSTRAINT fk_rails_9ae96ef653 FOREIGN KEY (telegram_message_id) REFERENCES public.telegram_messages(id);


--
-- Name: mailing_requests fk_rails_a14dc35653; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.mailing_requests
    ADD CONSTRAINT fk_rails_a14dc35653 FOREIGN KEY (mailing_id) REFERENCES public.mailings(id);


--
-- Name: telegram_messages fk_rails_a409e099c8; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.telegram_messages
    ADD CONSTRAINT fk_rails_a409e099c8 FOREIGN KEY (telegram_group_id) REFERENCES public.telegram_groups(id);


--
-- Name: mailing_request_messages fk_rails_cc5e842fe7; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.mailing_request_messages
    ADD CONSTRAINT fk_rails_cc5e842fe7 FOREIGN KEY (mailing_request_id) REFERENCES public.mailing_requests(id);


--
-- Name: mailing_views fk_rails_cd19d981c3; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.mailing_views
    ADD CONSTRAINT fk_rails_cd19d981c3 FOREIGN KEY (subscriber_id) REFERENCES public.subscribers(id);


--
-- Name: survey_questions fk_rails_d0558bfd89; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.survey_questions
    ADD CONSTRAINT fk_rails_d0558bfd89 FOREIGN KEY (survey_id) REFERENCES public.surveys(id);


--
-- Name: email_lists_mailing_destinations fk_rails_d09b149ac6; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.email_lists_mailing_destinations
    ADD CONSTRAINT fk_rails_d09b149ac6 FOREIGN KEY (email_list_id) REFERENCES public.email_lists(id);


--
-- Name: survey_answers fk_rails_d5508fe8d0; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.survey_answers
    ADD CONSTRAINT fk_rails_d5508fe8d0 FOREIGN KEY (survey_question_id) REFERENCES public.survey_questions(id);


--
-- Name: stripo_template_group_email_lists fk_rails_db6c85d978; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.stripo_template_group_email_lists
    ADD CONSTRAINT fk_rails_db6c85d978 FOREIGN KEY (email_list_id) REFERENCES public.email_lists(id);


--
-- Name: telegram_group_permissions fk_rails_e544c96484; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.telegram_group_permissions
    ADD CONSTRAINT fk_rails_e544c96484 FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: email_lists_mailing_destinations fk_rails_e91e777705; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.email_lists_mailing_destinations
    ADD CONSTRAINT fk_rails_e91e777705 FOREIGN KEY (mailing_destination_id) REFERENCES public.mailing_destinations(id);


--
-- Name: mobile_tokens fk_rails_eaf4c0d761; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.mobile_tokens
    ADD CONSTRAINT fk_rails_eaf4c0d761 FOREIGN KEY (subscriber_id) REFERENCES public.subscribers(id);


--
-- Name: survey_subscriber_answers fk_rails_ee4d37a71e; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.survey_subscriber_answers
    ADD CONSTRAINT fk_rails_ee4d37a71e FOREIGN KEY (subscriber_id) REFERENCES public.subscribers(id);


--
-- Name: mailing_requests fk_rails_ef869bee5b; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.mailing_requests
    ADD CONSTRAINT fk_rails_ef869bee5b FOREIGN KEY (subscriber_id) REFERENCES public.subscribers(id);


--
-- Name: mailing_requests fk_rails_efafd63c37; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.mailing_requests
    ADD CONSTRAINT fk_rails_efafd63c37 FOREIGN KEY (donation_method_id) REFERENCES public.donation_methods(id);


--
-- Name: stripo_files fk_rails_f2dc46b5ed; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.stripo_files
    ADD CONSTRAINT fk_rails_f2dc46b5ed FOREIGN KEY (stripo_file_key_id) REFERENCES public.stripo_file_keys(id);


--
-- Name: stripo_template_group_users fk_rails_fb20334e55; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.stripo_template_group_users
    ADD CONSTRAINT fk_rails_fb20334e55 FOREIGN KEY (stripo_template_group_id) REFERENCES public.stripo_template_groups(id);


--
-- PostgreSQL database dump complete
--

SET search_path TO "$user", public;

INSERT INTO "schema_migrations" (version) VALUES
('20200722141907'),
('20200729180556'),
('20201015173844'),
('20210131195337'),
('20211017150121'),
('20220406065145'),
('20220614065810'),
('20230302074750'),
('20230302074751'),
('20230302075026'),
('20230402051403'),
('20230416145617'),
('20230416150804'),
('20230425070902'),
('20230425074144'),
('20230425075753'),
('20230427183400'),
('20230606072324'),
('20230606072325'),
('20230606072914'),
('20230823073713'),
('20231013073809'),
('20231115062429'),
('20240802063038');


