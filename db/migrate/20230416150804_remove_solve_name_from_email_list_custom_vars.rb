class RemoveSolveNameFromEmailListCustomVars < ActiveRecord::Migration[5.2]
  def up
    remove_column :email_list_custom_vars, :solve_name
  end
end
