class AddEnableContentHeadersToMailings < ActiveRecord::Migration[5.2]
  def up
    add_column :mailings, :enable_content_headers, :boolean, null: false, default: false
    Mailing.reset_column_information
    Mailing.where(editor_type: %i[ckeditor template]).update_all(enable_content_headers: true)
    Mailing.where(editor_type: %i[none stripo]).update_all(enable_content_headers: false)
  end

  def down
    remove_column :mailings, :enable_content_headers
  end
end
