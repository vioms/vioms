class CreateMailingRequestImages < ActiveRecord::Migration[7.0]
  def change
    create_table :mailing_request_images do |t|
      t.jsonb :image_data
      t.belongs_to :mailing_request, null: false, foreign_key: true

      t.timestamps
    end
  end
end
