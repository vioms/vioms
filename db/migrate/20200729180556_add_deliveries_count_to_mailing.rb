class AddDeliveriesCountToMailing < ActiveRecord::Migration[5.2]
  def change
    add_column :mailings, :deliveries_count, :integer, null: false, default: 0
    add_column :mailings, :delivery_errors_count, :integer, null: false, default: 0
  end
end
