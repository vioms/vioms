# frozen_string_literal: true

class RemoveAssetableFromCkeditorAssets < ActiveRecord::Migration[5.2]
  def up
    change_table :ckeditor_assets, bulk: true do |t|
      t.remove :assetable_id
      t.remove :assetable_type
    end
  end

  def down
    change_table :ckeditor_assets, bulk: true do |t|
      t.bigint :assetable_id
      t.string :assetable_type, limit: 30
    end

    add_index :ckeditor_assets, %i[assetable_type assetable_id]
    add_index :ckeditor_assets, %i[assetable_type type assetable_id], name: 'idx_ckeditor_assets_assetable_type_type_and_assetable'
  end
end
