class CreateSurveyQuestions < ActiveRecord::Migration[7.0]
  def change
    create_table :survey_questions do |t|
      t.string :question, null: false
      t.integer :question_type, null: false
      t.integer :position, null: false
      t.belongs_to :survey, null: false, foreign_key: true

      t.timestamps
    end
    add_index :survey_questions, %i[position survey_id], unique: true
  end
end
