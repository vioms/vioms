class CreateMailingRequestMessages < ActiveRecord::Migration[7.0]
  def change
    create_table :mailing_request_messages do |t|
      t.belongs_to :mailing_request, foreign_key: true, null: false
      t.belongs_to :user, foreign_key: true
      t.integer :author, null: false
      t.integer :old_status, null: false
      t.integer :new_status, null: false
      t.text :message

      t.timestamps
    end
  end
end
