class AddSearchableColumnToMailings < ActiveRecord::Migration[5.2]
  def up
    execute <<-SQL.squish
      ALTER TABLE mailings
      ADD COLUMN searchable tsvector GENERATED ALWAYS AS (
        setweight(to_tsvector('russian', coalesce(subject, '')), 'A') ||
        setweight(to_tsvector('russian', coalesce(plain_content,'')), 'B')
      ) STORED;
    SQL
  end

  def down
    remove_column :mailings, :searchable
  end
end
