class CreateMailingDestinations < ActiveRecord::Migration[7.0]
  def change
    create_table :mailing_destinations do |t|
      t.string :name
      t.timestamps
    end

    create_table :email_lists_mailing_destinations do |t|
      t.references :mailing_destination, foreign_key: true, null: false,
                                         index: { name: 'index_mdel_mailing_destination_id' }
      t.references :email_list, foreign_key: true, null: false, index: { name: 'index_mdel_email_list_id' }
    end

    create_join_table :mailing_destinations, :users do |t|
      t.index :mailing_destination_id, name: 'index_mdu_mailing_destination_id'
      t.index :user_id, name: 'index_mdu_user_id'
    end
  end
end
