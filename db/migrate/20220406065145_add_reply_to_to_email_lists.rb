class AddReplyToToEmailLists < ActiveRecord::Migration[5.2]
  def change
    add_column :email_lists, :reply_to, :string
  end
end
