class CreateMailingRequests < ActiveRecord::Migration[7.0]
  def change
    create_table :mailing_requests do |t|
      t.string :subject, null: false
      t.text :body, null: false
      t.integer :status, null: false
      t.integer :mailing_type
      t.belongs_to :donation_method, foreign_key: true
      t.string :donated_at
      t.string :deliver_at
      t.text :comments
      t.belongs_to :mailing, foreign_key: true
      t.belongs_to :subscriber, foreign_key: true, null: false

      t.timestamps
    end
  end
end
