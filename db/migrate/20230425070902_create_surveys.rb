class CreateSurveys < ActiveRecord::Migration[7.0]
  def change
    create_table :surveys do |t|
      t.string :name
      t.string :title, null: false
      t.boolean :visible, null: false, default: false
      t.boolean :announce, null: false, default: false

      t.timestamps
    end
  end
end
