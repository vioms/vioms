class CreateMailingViews < ActiveRecord::Migration[5.2]
  def change
    create_table :mailing_views do |t|
      t.references :mailing, foreign_key: true
      t.references :subscriber, foreign_key: true
      t.integer :count, default: 0, null: false
      t.timestamps
    end
  end
end
