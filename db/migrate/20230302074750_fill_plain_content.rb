class FillPlainContent < ActiveRecord::Migration[5.2]
  def up
    Mailing.find_each do |mailing|
      next if mailing.plain_content.present?

      mailing.update_column(:plain_content, ActionController::Base.helpers.strip_tags(mailing.rendered_html))
    end
  end
end
