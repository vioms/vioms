class CreateMailingRequestsEmailLists < ActiveRecord::Migration[7.0]
  def change
    create_join_table :mailing_requests, :email_lists do |t|
      t.index :mailing_request_id
      t.index :email_list_id
    end
  end
end
