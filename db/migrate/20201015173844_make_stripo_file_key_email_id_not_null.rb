# frozen_string_literal: true

class MakeStripoFileKeyEmailIdNotNull < ActiveRecord::Migration[5.2]
  def change
    change_column_null(:stripo_file_keys, :email_id, true)
  end
end
