class CreateAPIKeys < ActiveRecord::Migration[5.2]
  def change
    create_table :api_keys do |t|
      t.string :name
      t.string :key, null: false
      t.belongs_to :user, foreign_key: true
      t.timestamps
    end
    add_index :api_keys, :key, unique: true
  end
end
