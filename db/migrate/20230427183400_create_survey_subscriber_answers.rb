class CreateSurveySubscriberAnswers < ActiveRecord::Migration[7.0]
  def change
    create_table :survey_subscriber_answers do |t|
      t.text :value
      t.belongs_to :subscriber, foreign_key: true
      t.belongs_to :survey_question, null: false, foreign_key: true
      t.belongs_to :survey_answer

      t.timestamps
    end
    add_index :survey_subscriber_answers, %i[subscriber_id survey_question_id], unique: true,
                                                                                name: 'admin_subscriber_answer'
  end
end
