class AddDeviceFieldsToMobileTokens < ActiveRecord::Migration[7.0]
  def change
    change_table :mobile_tokens, bulk: true do |t|
      t.string :device_token
      t.integer :device_type
    end
  end
end
