class CreateMobileTokens < ActiveRecord::Migration[5.2]
  def change
    create_table :mobile_tokens do |t|
      t.integer :serial, null: false, default: 0
      t.references :subscriber, foreign_key: true
      t.timestamps
    end
  end
end
