class DropSolve360Tables < ActiveRecord::Migration[5.2]
  def up
    drop_table :solve_syncs
    drop_table :solve_categories
  end
end
