class CreateSurveyAnswers < ActiveRecord::Migration[7.0]
  def change
    create_table :survey_answers do |t|
      t.string :answer
      t.integer :position, null: false
      t.belongs_to :survey_question, null: false, foreign_key: true

      t.timestamps
    end
    add_index :survey_answers, %i[position survey_question_id], unique: true, name: 'admin_surveys_position'
  end
end
