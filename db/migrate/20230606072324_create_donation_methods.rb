class CreateDonationMethods < ActiveRecord::Migration[7.0]
  def change
    create_table :donation_methods do |t|
      t.string :name, null: false

      t.timestamps
    end
  end
end
