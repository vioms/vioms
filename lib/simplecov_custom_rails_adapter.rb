# frozen_string_literal: true

if ENV['COVERAGE']
  require 'simplecov'
  SimpleCov.adapters.define 'custom_rails' do
    load_profile 'rails'
    add_filter 'vendor'
    add_group 'Decorators', 'app/decorators'
    add_group 'Libs', 'app/lib'
    add_group 'Mailers', 'app/mailers'
    add_group 'Policies', 'app/policies'
    add_group 'Queries', 'app/queries'
    add_group 'Services', 'app/services'
    add_group 'Uploaders', 'app/uploaders'
  end
end
