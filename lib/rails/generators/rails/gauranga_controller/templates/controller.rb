# frozen_string_literal: true

class <%= controller_class_name %>Controller < ApplicationController
  before_action :set_<%= singular_table_name %>, only: %i[edit update destroy]

  def index
    @<%= plural_table_name %> = authorize policy_scope(<%= class_name %>)
  end

  <%- if options.show? -%>
  def show
  end

  <%- end -%>
  def new
    @<%= singular_table_name %> = authorize <%= orm_class.build(class_name) %>
  end

  def edit; end

  def create
    @<%= singular_table_name %> = authorize <%= orm_class.build(class_name) %>(permitted_attributes(<%= orm_class.build(class_name) %>))
    if @<%= orm_instance.save %>
        redirect_to <%= redirect_to_url %>, notice: t('flash.<%= singular_table_name %>.create')
    else
      render :new
    end
  end

  def update
    if @<%= singular_table_name %>.update(permitted_attributes(@<%= singular_table_name %>))
      redirect_to <%= redirect_to_url %>, notice: t('flash.<%= singular_table_name %>.update')
    else
      render :edit
    end
  end

  def destroy
    @<%= orm_instance.destroy %>
    redirect_to <%= redirect_to_url %>, notice: t('flash.<%= singular_table_name %>.destroy')
  end

  private

  def set_<%= singular_table_name %>
    @<%= singular_table_name %> = authorize <%= orm_class.find(class_name, "params[:id]") %>
  end
end
