# frozen_string_literal: true

namespace :vioms do
  task recreate_bg_jobs: :environment do
    Mailing.transaction do
      MailingBgJob.includes(:mailing).find_each do |mailing_bg_job|
        mailing = mailing_bg_job.mailing
        MailingJob
          .set(wait_until: mailing.deliver_at)
          .perform_later(mailing_bg_job.id)
      end
      TelegramMessageSchedule.includes(:telegram_message).find_each do |telegram_message_schedule|
        telegram_message = telegram_message_schedule.telegram_message
        TelegramMessageJob
          .set(wait_until: telegram_message.deliver_at)
          .perform_later(telegram_message_schedule.id)
      end
    end
  end
end
