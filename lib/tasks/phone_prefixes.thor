class Vioms < Thor
  desc :import_phone_prefixes, 'Import phone prefixes'
  def import_phone_prefixes
    require File.expand_path 'config/environment'
    require 'csv'

    ActiveRecord::Base.connection.execute('TRUNCATE `sms_operators`')
    ActiveRecord::Base.connection.execute('TRUNCATE `phone_prefixes`')
    CSV.foreach(Rails.root.join('data', 'gates.csv')) do |name, _v, prefix_start, prefix_end|
      if prefix_start.match(/^7/)
        (prefix_start, prefix_end) = [prefix_start, prefix_end].map { |p| p = p[1, p.length] }
        sms_operator = SmsOperator.find_or_initialize_by gate_name: name
        sms_operator.name = name if sms_operator.name.blank?
        sms_operator.save!
        PhonePrefix.create!(sms_operator:, prefix_start:, prefix_end:)
      end
    end
  end

  desc :detect_prefixes_for_all_subscribers, 'Detect prefixes for all subscribers'
  def detect_prefixes_for_all_subscribers
    require File.expand_path 'config/environment'

    Subscriber.transaction do
      subscribers = Subscriber.where("phone!='' AND sms_operator_id IS NULL").all
      size = subscribers.size
      i = 1
      subscribers.each do |subscriber|
        puts "#{i}/#{size}"
        subscriber.update_attribute(:sms_operator_id, SmsOperator.detect_operator(subscriber.phone))
        i += 1
      end
    end
  end
end
