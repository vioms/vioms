class Vioms < Thor
  desc :find_not_prolonged_subscribers, 'Find not prolonged subscribers'
  method_options include_sms_lists: :string, exclude_sms_lists: :string, exclude_karmi: false,
                 encoding: 'utf-8', from_datetime: :string
  def find_not_prolonged_subscribers
    require 'iconv'
    require File.expand_path 'config/environment'

    iconv = Iconv.new(options[:encoding], 'utf-8')

    sms_lists = SmsList.scoped
    sms_lists = sms_lists.where { karmi == false } if options[:exclude_karmi]
    sms_lists = sms_lists.where { id >> options[:include_sms_lists].split(',') } if options[:include_sms_lists]
    sms_lists = sms_lists.where { id << options[:exclude_sms_lists].split(',') } if options[:exclude_sms_lists]

    sms_lists.each do |sms_list|
      puts iconv.iconv(sms_list.name) + "\r\n"
      subscribers = if options[:from_datetime]
                      sms_list.subscribers.where do
                        (sms_expire_at < Time.zone.now) & (sms_expire_at >= options[:from_datetime].to_time(:local))
                      end
                    else
                      sms_list.subscribers.where { (sms_expire_at < Time.zone.now) | (sms_expire_at.nil?) }
                    end
      subscribers.each do |s|
        puts "8#{s.phone}\r\n" if s.phone.present?
      end
      puts "--------\r\n\r\n"
    end
  end

  desc :reset_default_passwords, 'Reset default passwords'
  def reset_default_passwords
    require File.expand_path 'config/environment'

    SubscriberUser.find_each do |subscriber_user|
      next unless subscriber_user.valid_password?('keges6heK')

      subscriber = Subscriber.find(subscriber_user.id)
      subscriber.encrypted_password = ''
      subscriber.password_salt = ''
      subscriber.save validate: false
      puts 'one'
    end
  end
end
