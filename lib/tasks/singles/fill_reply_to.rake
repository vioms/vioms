# frozen_string_literal: true

namespace :vioms do
  namespace :singles do
    task fill_reply_to: :environment do
      EmailList.transaction do
        EmailList.where('from_email!=from_bounce').update_all('reply_to=from_bounce')
        EmailList.where('from_email=from_bounce').update_all('from_bounce=NULL')
      end
    end
  end
end
