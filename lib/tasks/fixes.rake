namespace :vioms do
  desc 'Fix broken encodings for subscribers'
  task fix_encoding: :environment do
    Subscriber.find_each do |subscriber|
      name = subscriber.name
      next unless name.present? && !name.match('[a-zA-Zа-яА-Я]')

      begin
        name = Iconv.iconv('ISO-8859-2//IGNORE', 'UTF-8', name).to_s
      rescue StandardError
        # puts "#{subscriber.id}|#{subscriber.email}|#{name}"
      end
      # puts "#{subscriber.id}|#{subscriber.email}|#{name}" #if name.size == 64
      begin
        subscriber.update_attribute(:name, name)
        subscriber.save!
      rescue StandardError
        puts "#{subscriber.id}|#{subscriber.email}|#{name}" # if name.size == 64
      end
    end
  end

  desc 'Find invalid emails'
  task find_invalid_emails: :environment do
    Subscriber.find_each do |subscriber|
      puts "#{subscriber.id}|#{subscriber.email}" unless subscriber.valid?
    end
  end

  desc 'Clean double email list vars'
  task clean_double_email_list_vars: :environment do
    Mailing.transaction do
      mailing_ids = EmailListVar.distinct.pluck(:mailing_id)
      Mailing.find(mailing_ids).each do |mailing|
        vars = mailing.email_list_vars
        uniq_name_ids = vars.map(&:email_list_var_name_id).uniq
        next if vars.length == uniq_name_ids.length

        uniq_name_ids.each do |name_id|
          duplicated_vars = vars.where(email_list_var_name_id: name_id).order(:id).to_a
          first_value = duplicated_vars.shift.value
          if duplicated_vars.map(&:value).any? { |v| v != first_value }
            puts "Different values for mailing_id: #{mailing.id}, skipping"
          else
            EmailListVar.where(id: duplicated_vars.map(&:id)).delete_all
          end
        end
      end
    end
  end

  desc 'Render mailings content'
  task render_mailings_content: :environment do
    require 'ruby-progressbar'

    scope = Mailing.where(rendered_html: nil, draft: false).where.not(email_list_id: nil)
    progressbar = ProgressBar.create(total: scope.count, format: '[%B] %a %e')
    scope.find_each do |mailing|
      mailing.update_column(:rendered_html, Mailing::Content.call(mailing))
      progressbar.increment
    end
  end

  desc 'Render email digests content'
  task render_email_digests_content: :environment do
    require 'ruby-progressbar'

    scope = EmailDigest.where(rendered_html: nil)
    progressbar = ProgressBar.create(total: scope.count, format: '[%B] %a %e')
    scope.find_each do |email_digest|
      email_digest.update_column(:rendered_html, EmailDigest::Content.call(email_digest))
      progressbar.increment
    end
  end

  desc 'Replace mailings title with subject'
  task replace_mailings_title: :environment do
    require 'ruby-progressbar'

    scope = Mailing.where.not(rendered_html: nil)
    progressbar = ProgressBar.create(total: scope.count, format: '[%B] %a %e')
    scope.find_each do |mailing|
      mailing.update_column(:rendered_html, Mailing::Content.replace_title(mailing.rendered_html, mailing.subject))
      progressbar.increment
    end
  end
end
