namespace :vioms do
  desc 'Migrate data from Joomla'
  task migrate_data: :environment do
    ActiveRecord::Base.logger = Logger.new(STDOUT)

    time_now = Time.zone.now

    class OldSubscriber < ActiveRecord::Base; set_table_name('jos_acajoom_subscribers'); end
    Subscriber.transaction do
      Subscriber.delete_all
      OldSubscriber.find_each do |old_subscriber|
        Subscriber.create do |subscriber|
          Subscriber.columns_hash.each_key do |key|
            subscriber[key] = if %w[created_at updated_at].include?(key)
                                old_subscriber['subscribe_date']
                              else
                                old_subscriber[key]
                              end
          end
          subscriber.password = 'keges6heK'
          subscriber.confirmed_at = time_now if old_subscriber['confirmed']
        end
      end
    end

    class OldMailList < ActiveRecord::Base; set_table_name('jos_acajoom_lists'); end
    MailList.transaction do
      MailList.delete_all
      OldMailList.find_each do |old_maillist|
        MailList.create do |maillist|
          MailList.columns_hash.each_key do |key|
            maillist[key] = if key == 'name'
                              old_maillist['list_name']
                            elsif key == 'visible'
                              old_maillist['hidden']
                            elsif key == 'description'
                              old_maillist['list_desc']
                            elsif key == 'from_name'
                              old_maillist['sendername']
                            elsif key == 'from_email'
                              old_maillist['senderemail']
                            elsif key == 'from_bounce'
                              old_maillist['bounceadres']
                            elsif key == 'subscribe_message'
                              old_maillist['subscribemessage']
                            elsif key == 'unsubscribe_message'
                              old_maillist['unsubscribemessage']
                            elsif key == 'unsubscribe_send'
                              old_maillist['unsubscribesend']
                            elsif %w[created_at updated_at].include?(key)
                              old_maillist['createdate']
                            else
                              old_maillist[key]
                            end
          end
        end
      end
    end

    class Subscription < ActiveRecord::Base; end

    class OldSubscription < ActiveRecord::Base; set_table_name('jos_acajoom_queue'); end
    Subscription.transaction do
      Subscription.delete_all
      OldSubscription.all(select: 'DISTINCT `subscriber_id`, `list_id`').find_each do |old_subscription|
        Subscription.create do |subscription|
          Subscription.columns_hash.each_key do |key|
            subscription[key] = if key == 'email_list_id'
                                  old_subscription['list_id']
                                else
                                  old_subscription[key]
                                end
          end
        end
      end
    end

    class OldMailing < ActiveRecord::Base; set_table_name('jos_acajoom_mailings'); end
    Mailing.transaction do
      Mailing.delete_all
      OldMailing.find_each do |old_mailing|
        Mailing.create do |mailing|
          Mailing.columns_hash.each_key do |key|
            mailing[key] = if key == 'email_list_id'
                             old_mailing['list_id']
                           elsif key == 'from_name'
                             old_mailing['fromname']
                           elsif key == 'from_email'
                             old_mailing['fromemail']
                           elsif key == 'from_bounce'
                             old_mailing['frombounce']
                           elsif key == 'html_content'
                             old_mailing['htmlcontent']
                           elsif key == 'text_only'
                             old_mailing['textonly']
                           elsif key == 'user_id'
                             old_mailing['author_id']
                           else
                             old_mailing[key]
                           end
            mailing['sent'] = true
          end
        end
      end
    end

    class OldUser < ActiveRecord::Base; set_table_name 'jos_users'; end

    # We need this class to bypass Devise magic and work directly with password_* fields
    class User2 < ActiveRecord::Base
      set_table_name('users')
      set_inheritance_column :dont_care
    end

    User2.transaction do
      User2.delete_all
      OldUser.find_each do |old_user|
        User2.create do |user|
          User2.columns_hash.each_key do |key|
            user[key] = if key == 'login'
                          old_user['username']
                        elsif key == 'type'
                          old_user['usertype']
                        elsif %w[created_at updated_at].include?(key)
                          old_user['registerDate']
                        elsif key == 'current_sign_in_at'
                          old_user['lastvisitDate']
                        else
                          old_user[key]
                        end
          end
          user['encrypted_password'], user['password_salt'] = old_user['password'].split ':'
          user['sign_in_count'] = 0
        end
      end
    end
  end
end
