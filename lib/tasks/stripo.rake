# frozen_string_literal: true

namespace :vioms do
  namespace :stripo do
    desc 'Import stripo templates'
    task :import_templates, %i[group_id path] => :environment do |_t, args|
      path = args[:path]

      group = Stripo::TemplateGroup.find(args[:group_id])
      Dir["#{path}/*"].each do |dir|
        html = nil
        css = nil
        preview = nil
        Dir["#{dir}/*"].each do |file|
          html = File.read(file) if file.end_with?('html')
          css = File.read(file) if file.end_with?('css')
          preview = File.open(file) if file.end_with?('png')
        end
        params = { name: dir.split('/').last, html:, css:, preview:, template_group: group }
        Stripo::Template::Create::CreateTemplate.call(params:)
      end
    end
  end
end
