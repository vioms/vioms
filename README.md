# VIOMS

## Development environment on Docker

### Setup

```bash
docker compose build --build-arg uid=`id -u`
docker compose run --rm rails dockerize -wait tcp://db:5432 bundle exec rake db:setup
docker compose down
```

### Run

```bash
docker compose up -d
```

Use `http://lvh.me:3000` for Rails and `http://lvh.me:1080` mailcatcher.

### Stop and cleanup

```bash
docker compose down
```

Your volumes are not destroyed and kept in `docker/volumes`.
