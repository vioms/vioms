Feature: Manage subscriber
  Scenario Outline: Login subscriber with valid and invalid passwords
    Given I have sms send history types
    And I have pages
    And I have sms messages
    And I have an smtp setting group
    And I have settings
    And I have the following subscribers:
      | email                   | phone      | password  |
      | subscriber1@example.com |            | password1 |
      |                         | 9091112233 | password2 |
      | subscriber2@example.com | 9091112234 | password3 |
    And I am not authenticated as a subscriber

    When I go to the home page
    And I fill in "subscriber_user_login" with "<login>" within the subscriber login form
    And I fill in "subscriber_user_password" with "<password>" within the subscriber login form
    And I press "subscriber_user_submit" within the subscriber login form
    Then I should see "<should_see>" within the flash message

    Examples:
      | login                   | password     | should_see |
      | subscriber1@example.com | password1    | выполнен   |
      | subscriber1@example.com | bad_password | Неверный   |
      | 9091112233              | password2    | выполнен   |
      | 9091112233              | bad_password | Неверный   |
      | subscriber2@example.com | password3    | выполнен   |
      | subscriber2@example.com | bad_password | Неверный   |
      | 9091112234              | password3    | выполнен   |
      | 9091112234              | bad_password | Неверный   |

  Scenario Outline: Login subscriber with authentication token
    Given I have sms send history types
    And I have pages
    And I have sms messages
    And I have an smtp setting group
    And I have settings
    And I have the following subscribers:
      | email                   | phone      | password  | authentication_token |
      | subscriber1@example.com |            | password1 | mkCtrhNxPAqU8GH4Jpqz |
      | subscriber2@example.com | 9091112234 | password3 | 8kK4ohHgA5PEgpPySWeL |
    And I am not authenticated as a subscriber

    When I follow the link with "<token>" auth token and "<email>" email
    Then I should see "<should_see>" within <see_within>

    Examples:
      | token                | email                   | should_see    | see_within        |
      | mkCtrhNxPAqU8GH4Jpqz | subscriber1@example.com | Мои настройки | the page title    |
      | 8kK4ohHgA5PEgpPySWeL | subscriber2@example.com | Мои настройки | the page title    |
      | bydzrvY1JWY3xztyuQUF | subscriber1@example.com | Неверный      | the flash message |

  Scenario Outline: Register new subscriber with email
    Given I am not authenticated as a subscriber
    And I have pages
    And I have settings
    And I have an empty Inbox

    When I go to the subscriber registration page
    And I fill in "subscriber_user_email" with "<email>" within the subscriber registration form
    And I press "subscriber_user_submit" within the subscriber registration form
    Then I should see "<should_see>" within the flash message
    And I should have 1 message in my Inbox

    Examples:
      | email                   | should_see                    |
      | subscriber1@example.com | Вы успешно зарегистрировались |

  Scenario Outline: Register new subscriber with phone
    Given I am not authenticated as a subscriber
    And I have pages
    And I have an empty Inbox
    And I have sms send history types
    And I have sms messages
    And I have an smtp setting group
    And I have settings
    And I have the following time zones:
      | name |
      | tz1  |
      | tz2  |
      | tz3  |

    When I go to the subscriber registration page
    And I fill in "subscriber_user_email" with "<email>" within the subscriber registration form
    And I fill in "subscriber_user_phone" with "<phone>" within the subscriber registration form
    And I select "tz2" from "subscriber_user_time_zone_id" within the subscriber registration form
    And I press "subscriber_user_submit" within the subscriber registration form
    Then I should see "<should_see>" within the flash message
    And I should have <messages> message in my Inbox

    Examples:
      | email                   | phone      | should_see                    | messages |
      |                         | 9091112233 | Вы успешно зарегистрировались | 1        |
      | subscriber1@example.com | 9091112234 | Вы успешно зарегистрировались | 2        |

  Scenario Outline: I am a registered subscriber and I request a link to change my password
    Given I am not authenticated as a subscriber
    And I have pages
    And I have sms send history types
    And I have sms messages
    And I have an smtp setting group
    And I have settings
    And I have the following subscribers:
      | email                   | phone      | password  |
      | subscriber1@example.com |            | password1 |
      |                         | 9091112233 | password2 |
      | subscriber2@example.com | 9091112234 | password3 |
    And I have an empty Inbox

    When I go to the subscriber forgot password page
    And I fill in "subscriber_user_login" with "<login>" within the subscriber forgot password form
    And I press "subscriber_user_submit" within the subscriber forgot password form
    Then I should see "<should_see>" within <see_within>
    And I should have <messages> messages in my Inbox

    Examples:
      | login                   | should_see                        | messages | see_within        |
      | subscriber1@example.com | вы получите письмо с инструкциями | 1        | the flash message |
      | 9091112233              | вы получите письмо с инструкциями | 1        | the flash message |
      | subscriber2@example.com | вы получите письмо с инструкциями | 1        | the flash message |
      | 9091112234              | вы получите письмо с инструкциями | 1        | the flash message |
      | subscriber3@example.com | не найдена                        | 0        | the input error   |
      | 9091112235              | не найдена                        | 0        | the input error   |

  Scenario Outline: I try to change my password within email
    Given I am not authenticated as a subscriber
    And I have pages
    And I have the following subscribers:
      | email                   | phone      | password  |
      | subscriber1@example.com |            | password1 |
    And I have sms send history types
    And I have sms messages
    And I have settings
    And I have an empty Inbox

    When I go to the subscriber forgot password page
    And I fill in "subscriber_user_login" with "subscriber1@example.com" within the subscriber forgot password form
    And I press "subscriber_user_submit" within the subscriber forgot password form
    Then I follow the change subscriber password link in my email message
    And I fill in "subscriber_user_password" with "<password>" within the subscriber change password form
    And I fill in "subscriber_user_password_confirmation" with "<password_confirmation>" within the subscriber change password form
    And I press "subscriber_user_submit" within the subscriber change password form
    Then I should see "<should_see>" within <see_within>

    Examples:
      | password      | password_confirmation | should_see                 | see_within        |
      | new_password  | new_password          | Ваш пароль успешно изменён | the flash message |
      | new_password1 | new_password2         | не совпадает               | the input error   |

  Scenario Outline: I try to change my password within phone
    Given I am not authenticated as a subscriber
    And I have pages
    And I have sms send history types
    And I have sms messages
    And I have an smtp setting group
    And I have settings
    And I have the following subscriber:
      | email                   | phone      | password  |
      |                         | 9091112233 | password2 |
    And I have an empty Inbox

    When I go to the subscriber forgot password page
    And I fill in "subscriber_user_login" with "9091112233" within the subscriber forgot password form
    And I press "subscriber_user_submit" within the subscriber forgot password form
    Then I should have the correct reset token in my sms message

    When I fill in "subscriber_user_login" with "9091112233" within the subscriber change password form
    And I fill in "subscriber_user_reset_password_token" with the reset password token within the subscriber change password form
    And I fill in "subscriber_user_password" with "<password>" within the subscriber change password form
    And I fill in "subscriber_user_password_confirmation" with "<password_confirmation>" within the subscriber change password form
    And I press "subscriber_user_submit" within the subscriber change password form
    Then I should see "<should_see>" within <see_within>

    Examples:
      | password      | password_confirmation | should_see                 | see_within        |
      | new_password  | new_password          | Ваш пароль успешно изменён | the flash message |
      | new_password1 | new_password2         | не совпадает               | the input error   |
