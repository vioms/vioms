Feature: Manage user
  Scenario Outline: I try to change my password within email
    Given I am not authenticated as a user
    And I have the following users:
      | email                   | password  |
      | subscriber1@example.com | password1 |
    And I have pages
    And I have sms send history types
    And I have sms messages
    And I have settings
    And I have an empty Inbox

    When I go to the user forgot password page
    And I fill in "user_login" with "subscriber1@example.com" within the user forgot password form
    And I press "user_submit" within the user forgot password form
    Then I follow the change user password link in my email message
    And I fill in "user_password" with "<password>" within the user change password form
    And I fill in "user_password_confirmation" with "<password_confirmation>" within the user change password form
    And I press "user_submit" within the user change password form
    Then I should see "<should_see>" within <see_within>

    Examples:
      | password      | password_confirmation | should_see                 | see_within        |
      | new_password  | new_password          | Ваш пароль успешно изменён | the flash message |
      | new_password1 | new_password2         | не совпадает               | the input error   |
