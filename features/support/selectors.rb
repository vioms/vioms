module HtmlSelectorsHelpers
  # Maps a name to a selector. Used primarily by the
  #
  #   When /^(.+) within (.+)$/ do |step, scope|
  #
  # step definitions in web_steps.rb
  #
  def selector_for(locator)
    case locator

    when 'the page'
      'html > body'

    when 'the user login form'
      'form#new_user'

    when 'the user forgot password form'
      'form#new_user'

    when 'the user change password form'
      'form#new_user'

    when 'the subscriber login form'
      'form#top_login_form'

    when 'the subscriber registration form'
      'form#new_subscriber_user'

    when 'the subscriber forgot password form'
      'form#new_subscriber_user'

    when 'the subscriber change password form'
      'form#new_subscriber_user'

    when 'the my settings edit page'
      'form.edit_my_settings'

    when 'the flash message'
      '.flash'

    when 'the sms list checkboxes'
      '#sms_lists_checkboxes'

    when 'the sms list request checkboxes'
      '#sms_lists_request_checkboxes'

    when 'the input error'
      'div.invalid-feedback'

    when 'the page title'
      '#page_title'

      # Add more mappings here.
      # Here is an example that pulls values out of the Regexp:
      #
      #  when /^the (notice|error|info) flash$/
      #    ".flash.#{$1}"

      # You can also return an array to use a different selector
      # type, like:
      #
      #  when /the header/
      #    [:xpath, "//header"]

      # This allows you to provide a quoted selector as the scope
      # for "within" steps as was previously the default for the
      # web steps:
    when /^"(.+)"$/
      ::Regexp.last_match(1)

    else
      raise "Can't find mapping from \"#{locator}\" to a selector.\n" +
            "Now, go and add a mapping in #{__FILE__}"
    end
  end
end

World(HtmlSelectorsHelpers)
