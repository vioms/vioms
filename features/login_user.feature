Feature: Login user
  Background:
    Given I have one user with email "user@example.com" and password "password"
    And I have pages
    And I have settings
  Scenario Outline: Login user with valid and invalid passwords
    Given I am not authenticated as a user

    When I go to the user login page
    And I fill in "user_login" with "<email>" within the user login form
    And I fill in "user_password" with "<password>" within the user login form
    And I press "user_submit" within the user login form
    Then I should see "<should_see>" within the flash message

    Examples:
      | email            | password     | should_see |
      | user@example.com | password     | выполнен   |
      | user@example.com | bad_password | Неверный   |
