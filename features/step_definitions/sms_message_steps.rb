When(/^I have sms messages$/) do
  create_sms_messages
end

When(/^I have sms send history types$/) do
  create_sms_send_history_types
end

When(/^I have settings$/) do
  create_settings
end

When(/^I have an smtp setting group$/) do
  create_smtp_setting_group
end

World(CreateSmsMessages)
World(CreateSmsSendHistoryTypes)
World(CreateSettings)
World(CreateSmtpSettingGroup)
