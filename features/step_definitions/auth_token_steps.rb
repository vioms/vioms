Then(/^I follow the link with "([^"]*)" auth token and "([^"]*)" email$/) do |auth_token, email|
  visit edit_my_settings_url(auth_token:, email:)
end
