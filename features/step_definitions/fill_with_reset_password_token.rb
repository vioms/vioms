When(/^I fill in "([^"]*)" with the reset password token$/) do |field|
  subscriber = Subscriber.first
  subscriber.update_attribute(:reset_password_token, '927449facbad4d4d02e9c81b826e07d0163dac51352c983da40670eb55ae31be')
  step %(I fill in "#{field}" with "1234567890")
end
