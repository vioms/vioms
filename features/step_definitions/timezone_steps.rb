When(/^I have the following time zones?:$/) do |fields|
  fields.hashes.each { |hash| create(:time_zone, hash) }
end
