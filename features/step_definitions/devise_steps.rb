Given(/^I am not authenticated as a user$/) do
  delete('/user/logout')
end

Given(/^I am not authenticated as a subscriber/) do
  delete('/subscriber_user/logout')
end

Given(/^I have one admin user with email "([^"]*)" and password "([^"]*)"$/) do |email, password|
  create(:user, :admin, email:, password:)
end

Given(/^I have one user with email "([^"]*)" and password "([^"]*)"$/) do |email, password|
  create(:user, email:, password:)
end

Given(/^I have the following subscribers?:$/) do |fields|
  fields.hashes.each do |hash|
    if hash['phone']
      create(:subscriber_user_with_phone, hash)
    else
      create(:subscriber_user, hash)
    end
  end
end

Given(/^I have the following users?:$/) do |fields|
  fields.hashes.each do |hash|
    create(:user, hash)
  end
end

Given(/^I have a subscriber with visible and invisible subscriptions$/) do
  create_subscriber_with_subscriptions
end

World(CreateSubscriberWithSubscriptions)
