Given(/^I have an empty Inbox$/) do
  ActionMailer::Base.deliveries = []
end

Then(/^I should have (\d+|no) messages? in my Inbox$/) do |n|
  n = 0 if n == 'no'
  ActionMailer::Base.deliveries.size.should == n.to_i
end

Then(/^I follow the change (subscriber|user) password link in my email message$/) do |model|
  step %(I should have 1 message in my Inbox)
  mail = ActionMailer::Base.deliveries.first
  if model == 'subscriber'
    expect(mail.body).to match "https://www.vioms.ru#{edit_subscriber_user_password_path}"
  elsif model == 'user'
    expect(mail.body).to match "https://www.vioms.ru#{edit_user_password_path}"
  end
  (email, reset_password_token) = mail.body.match(/login=(.+)&amp;reset_password_token=(.{20})/).to_a.drop(1)
  email = CGI.unescape(email)
  if model == 'subscriber'
    visit edit_subscriber_user_password_url(login: email, reset_password_token:)
  elsif model == 'user'
    visit edit_user_password_url(login: email, reset_password_token:)
  end
end

Then(/^I should have the correct reset token in my sms message$/) do
  step %(I should have 1 message in my Inbox)
  mail = ActionMailer::Base.deliveries.first
  token = Devise.token_generator.digest(SubscriberUser, :reset_password_token, mail.body.match(/\d+/))
  Subscriber.to_adapter.find_first({ reset_password_token: token }).should be_kind_of(Subscriber)
end
