Feature: Manage my settings
  Scenario: Check if subscriber can see different email and sms lists and saving does not break anything
    Given I have sms send history types
    And I have pages
    And I have sms messages
    And I have an smtp setting group
    And I have settings
    And I have a subscriber with visible and invisible subscriptions
    And I am not authenticated as a subscriber

    When I go to the home page
    And I fill in "subscriber_user_login" with "subscriber_with_subscriptions@example.com" within the subscriber login form
    And I fill in "subscriber_user_password" with "password" within the subscriber login form
    And I press "subscriber_user_submit" within the subscriber login form
    Then I should see "выполнен" within the flash message

    When I go to the my settings page
    Then I should not see "NeverVisibleEmailList"
    Then I should see "Visible Checked email list"
    Then I should see "Visible not checked email list"
    Then I should see "Invisible Checked email list"
    Then I should not see "Invisible not checked email list"

    Then I should not see "NeverVisibleSmsList"
    Then I should not see "Visible not public Checked sms list not confirmed" within the sms list checkboxes
    Then I should see "Visible Public Checked sms list not confirmed" within the sms list checkboxes
    Then I should see "Visible not public Checked sms list Confirmed" within the sms list checkboxes
    Then I should see "Visible Public Checked sms list Confirmed" within the sms list checkboxes
    Then I should not see "Visible not public not checked sms list" within the sms list checkboxes
    Then I should see "Visible Public not checked sms list" within the sms list checkboxes
    Then I should not see "Invisible not public Checked sms list not confirmed" within the sms list checkboxes
    Then I should not see "Invisible Public Checked sms list not confirmed" within the sms list checkboxes
    Then I should see "Invisible not public Checked sms list Confirmed" within the sms list checkboxes
    Then I should see "Invisible Public Checked sms list Confirmed" within the sms list checkboxes
    Then I should not see "Invisible not public not checked sms list" within the sms list checkboxes
    Then I should not see "Invisible Public not checked sms list" within the sms list checkboxes

    Then I should see "Visible not public Checked sms list not confirmed" within the sms list request checkboxes
    Then I should not see "Visible Public Checked sms list not confirmed" within the sms list request checkboxes
    Then I should not see "Visible not public Checked sms list Confirmed" within the sms list request checkboxes
    Then I should not see "Visible Public Checked sms list Confirmed" within the sms list request checkboxes
    Then I should see "Visible not public not checked sms list" within the sms list request checkboxes
    Then I should not see "Visible Public not checked sms list" within the sms list request checkboxes
    Then I should not see "Invisible not public Checked sms list not confirmed" within the sms list request checkboxes
    Then I should not see "Invisible Public Checked sms list not confirmed" within the sms list request checkboxes
    Then I should not see "Invisible not public Checked sms list Confirmed" within the sms list request checkboxes
    Then I should not see "Invisible Public Checked sms list Confirmed" within the sms list request checkboxes
    Then I should not see "Invisible not public not checked sms list" within the sms list request checkboxes
    Then I should not see "Invisible Public not checked sms list" within the sms list request checkboxes

    When I press "my_settings_submit" within the my settings edit page
    Then I should see "Ваши настройки успешно сохранены" within the flash message

    # Repeat tests after saving. Nothing should be changed.
    Then I should not see "NeverVisibleEmailList"
    Then I should see "Visible Checked email list"
    Then I should see "Visible not checked email list"
    Then I should see "Invisible Checked email list"
    Then I should not see "Invisible not checked email list"

    Then I should not see "NeverVisibleSmsList"
    Then I should not see "Visible not public Checked sms list not confirmed" within the sms list checkboxes
    Then I should see "Visible Public Checked sms list not confirmed" within the sms list checkboxes
    Then I should see "Visible not public Checked sms list Confirmed" within the sms list checkboxes
    Then I should see "Visible Public Checked sms list Confirmed" within the sms list checkboxes
    Then I should not see "Visible not public not checked sms list" within the sms list checkboxes
    Then I should see "Visible Public not checked sms list" within the sms list checkboxes
    Then I should not see "Invisible not public Checked sms list not confirmed" within the sms list checkboxes
    Then I should not see "Invisible Public Checked sms list not confirmed" within the sms list checkboxes
    Then I should see "Invisible not public Checked sms list Confirmed" within the sms list checkboxes
    Then I should see "Invisible Public Checked sms list Confirmed" within the sms list checkboxes
    Then I should not see "Invisible not public not checked sms list" within the sms list checkboxes
    Then I should not see "Invisible Public not checked sms list" within the sms list checkboxes

    Then I should see "Visible not public Checked sms list not confirmed" within the sms list request checkboxes
    Then I should not see "Visible Public Checked sms list not confirmed" within the sms list request checkboxes
    Then I should not see "Visible not public Checked sms list Confirmed" within the sms list request checkboxes
    Then I should not see "Visible Public Checked sms list Confirmed" within the sms list request checkboxes
    Then I should see "Visible not public not checked sms list" within the sms list request checkboxes
    Then I should not see "Visible Public not checked sms list" within the sms list request checkboxes
    Then I should not see "Invisible not public Checked sms list not confirmed" within the sms list request checkboxes
    Then I should not see "Invisible Public Checked sms list not confirmed" within the sms list request checkboxes
    Then I should not see "Invisible not public Checked sms list Confirmed" within the sms list request checkboxes
    Then I should not see "Invisible Public Checked sms list Confirmed" within the sms list request checkboxes
    Then I should not see "Invisible not public not checked sms list" within the sms list request checkboxes
    Then I should not see "Invisible Public not checked sms list" within the sms list request checkboxes