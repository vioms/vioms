# frozen_string_literal: true

require 'rails_helper'

RSpec.describe EmailDigestBgJob do
  before do
    DatabaseCleaner.clean
    ActionMailer::Base.deliveries = []
  end

  it 'sends email digest' do
    subscribers = []
    subscribers[0] = create(:subscriber_with_email)
    subscribers[1] = create(:subscriber_with_confirmed_email)
    subscribers[2] = create(:subscriber_with_email_and_problems)
    subscribers[3] = create(:subscriber_with_email_and_problems, confirmed_at: Time.zone.now)
    subscribers[4] = create(:subscriber_with_email)
    subscribers[5] = create(:subscriber_with_confirmed_email)
    subscribers[6] = create(:subscriber_with_email_and_problems)
    subscribers[7] = create(:subscriber_with_email_and_problems, confirmed_at: Time.zone.now)
    subscribers.each { |subscriber| subscriber.update_column(:authentication_token, SecureRandom.hex(16)) }

    email_lists = []
    email_lists[0] = create(:email_list_with_digest, subscribers: subscribers[4..6])
    email_lists[1] = create(:email_list_with_digest, subscribers: subscribers[4..6])
    email_lists[2] = create(:email_list_with_digest, subscribers: subscribers[0..6])

    email_digest = create(:email_digest, email_list: email_lists[0], published: true, rendered_html: nil)
    email_digest.create_email_digest_bg_job!
    expect(email_digest.sent).to be(false)

    Timecop.freeze(email_digest.deliver_at + 1.second) do
      EmailDigestJob.perform_later(email_digest.email_digest_bg_job.id)
    end

    email_digest.reload
    expect(email_digest.sent).to be(true)
    expect(email_digest.send_failed).to be(false)

    expect(ActionMailer::Base.deliveries.size).to eq(1)
    emails_received = []
    ActionMailer::Base.deliveries.each do |delivery|
      authentication_token = delivery.body.encoded[/auth_token=(.{32})/, 1]
      expect(authentication_token).not_to be_blank
      expect(subscribers.map(&:authentication_token)).to include(authentication_token)
      delivery.header.fields.each do |field|
        emails_received << field.to_s[/<(.*)>/, 1] if field.name == 'To'
      end
    end

    expect(emails_received).to include subscribers[5].email
    ((0..4).to_a + [7]).each { |i| expect(emails_received).not_to include(subscribers[i].email) }
  end

  it 'raises exception' do
    subscriber = create(:subscriber_with_confirmed_email)
    email_list = create(:email_list_with_digest, subscribers: [subscriber])

    email_digest = create(:email_digest, email_list:, published: true)
    email_digest.create_email_digest_bg_job!
    email_digest.sent.should be_falsey

    expect_any_instance_of(ViomsMailer).to receive(:email_digest).and_raise(StandardError, 'Testing error')
    Timecop.freeze(email_digest.deliver_at + 1.second) do
      EmailDigestJob.perform_later(email_digest.email_digest_bg_job.id)
    end
    subscriber.reload.raised_exception_at.should_not be_nil
  end

  it 'handles deleted email digest' do
    email_digest = create(:email_digest)
    email_digest_id = email_digest.id
    email_digest.destroy
    expect { EmailDigestJob.perform_later(email_digest_id) }.not_to raise_error
  end
end
