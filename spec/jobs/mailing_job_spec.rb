# frozen_string_literal: true

require 'rails_helper'

RSpec.describe MailingJob do
  before do
    DatabaseCleaner.clean
    ActionMailer::Base.deliveries = []
  end

  it 'sends mailing' do
    subscribers = []
    subscribers[0] = create(:subscriber_with_email)
    subscribers[1] = create(:subscriber_with_confirmed_email)
    subscribers[2] = create(:subscriber_with_email_and_problems)
    subscribers[3] = create(:subscriber_with_email_and_problems, confirmed_at: Time.zone.now)
    subscribers[4] = create(:subscriber_with_email)
    subscribers[5] = create(:subscriber_with_confirmed_email)
    subscribers[6] = create(:subscriber_with_confirmed_email, receive_html: false)
    subscribers[7] = create(:subscriber_with_email_and_problems)
    subscribers[8] = create(:subscriber_with_email_and_problems, confirmed_at: Time.zone.now)
    subscribers.each do |subscriber|
      subscriber.update_column(:authentication_token, SecureRandom.hex(16))
      subscriber.reload
    end

    email_lists = []
    email_lists[0] = create(:email_list, subscribers: subscribers[4..7])
    email_lists[1] = create(:email_list, subscribers: subscribers[4..7])
    email_lists[2] = create(:email_list, subscribers: subscribers[0..7])

    mailing = create(:mailing, email_list: email_lists[0], published: true)
    Mailing::Update.call(mailing:, params: [])
    mailing.create_mailing_bg_job!
    expect(mailing.sent).to be(false)

    expect do
      Timecop.freeze(mailing.deliver_at + 1.second) do
        MailingJob.perform_later(mailing.mailing_bg_job.id)
      end
    end.to(change { mailing.email_list.reload.updated_at })

    mailing.reload
    expect(mailing.sent).to be(true)
    expect(mailing.send_failed).to be(false)
    expect(mailing.deliveries_count).to eq(2)
    expect(mailing.delivery_errors_count).to eq(0)

    expect(ActionMailer::Base.deliveries.size).to eq(2)
    emails_received = []
    ActionMailer::Base.deliveries.each do |delivery|
      authentication_token = delivery.body.encoded[/auth_token=(.{32})/, 1]
      expect(authentication_token).to be_present
      expect(subscribers.map(&:authentication_token)).to include(authentication_token)
      delivery.header.fields.each do |field|
        emails_received << field.to_s[/<(.*)>/, 1] if field.name == 'To'
      end
    end

    (5..6).to_a.each { |i| expect(emails_received).to include subscribers[i].email }
    ((0..4).to_a + (7..8).to_a).each { |i| expect(emails_received).not_to include(subscribers[i].email) }
  end

  it 'sets correct From: field based on group priority' do
    subscribers = []
    6.times { |i| subscribers[i] = create(:subscriber_with_confirmed_email) }

    email_lists = []
    email_lists[0] = create(:email_list, from_name: 'List 1', subscribers: subscribers[0..0])
    email_lists[1] = create(:email_list, from_name: 'List 2', subscribers: subscribers[1..1], group_priority: 1)
    email_lists[2] = create(:email_list, from_name: 'List 3', subscribers: subscribers[0..2], group_priority: 2)
    email_lists[3] = create(:email_list, from_name: 'List 4', subscribers: subscribers[2..3], group_priority: 3)
    email_lists[4] = create(:email_list, from_name: 'List 5', subscribers: subscribers[3..3], group_priority: 4)
    email_lists[5] = create(:email_list, from_name: 'List 6', subscribers: subscribers[4..5], group_priority: 5)

    mailing = create(:mailing, email_list: email_lists[0], included_email_lists: email_lists[1..4], published: true)
    mailing.create_mailing_bg_job!
    expect(mailing.sent).to be(false)

    MailingJob.perform_later(mailing.mailing_bg_job.id)

    mailing.reload
    expect(mailing.sent).to be(true)
    expect(mailing.send_failed).to be(false)
    expect(mailing.deliveries_count).to eq(4)
    expect(mailing.delivery_errors_count).to eq(0)

    expect(ActionMailer::Base.deliveries.size).to eq(4)
    emails_received = {}
    ActionMailer::Base.deliveries.each do |delivery|
      from = nil
      to = nil
      delivery.header.fields.each do |field|
        from = field.to_s[/^(.*) </, 1] if field.name == 'From'
        to = field.to_s[/<(.*)>/, 1] if field.name == 'To'
      end
      emails_received[to] = from
    end

    expect(emails_received[subscribers[0].email]).to eq('List 1')
    expect(emails_received[subscribers[1].email]).to eq('List 3')
    expect(emails_received[subscribers[2].email]).to eq('List 4')
    expect(emails_received[subscribers[3].email]).to eq('List 5')
  end

  describe 'exceptions' do
    it 'raises for mailing' do
      subscriber = create(:subscriber_with_confirmed_email)
      email_list = create(:email_list, subscribers: [subscriber])

      mailing = create(:mailing, email_list:, published: true)
      mailing.create_mailing_bg_job!
      expect(mailing.sent).to be(false)

      expect_any_instance_of(EmailList).to receive(:sync_json_contacts).and_raise(StandardError, 'Testing error')
      Timecop.freeze(mailing.deliver_at + 1.second) do
        MailingJob.perform_later(mailing.mailing_bg_job.id)
      end
      mailing.reload
      expect(mailing.raised_exception_at).to be_present
      expect(mailing.deliveries_count).to eq(0)
      expect(mailing.delivery_errors_count).to eq(0)
    end

    it 'raises for subscriber' do
      subscriber = create(:subscriber_with_confirmed_email)
      email_list = create(:email_list, subscribers: [subscriber])

      mailing = create(:mailing, email_list:, published: true)
      mailing.create_mailing_bg_job!
      expect(mailing.sent).to be(false)

      expect_any_instance_of(ViomsMailer).to receive(:mailing).and_raise(StandardError, 'Testing error')
      Timecop.freeze(mailing.deliver_at + 1.second) do
        MailingJob.perform_later(mailing.mailing_bg_job.id)
      end
      expect(subscriber.reload.raised_exception_at).to be_present
      mailing.reload
      expect(mailing.deliveries_count).to eq(0)
      expect(mailing.delivery_errors_count).to eq(1)
    end
  end

  it 'handles deleted mailing' do
    mailing = create(:mailing)
    mailing_id = mailing.id
    mailing.destroy
    expect { MailingJob.perform_later(mailing_id) }.not_to raise_error
  end
end
