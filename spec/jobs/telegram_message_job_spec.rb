# frozen_string_literal: true

require 'rails_helper'

RSpec.describe TelegramMessageJob do
  subject { described_class.perform_now(telegram_message_schedule.id) }

  let(:telegram_message) { create(:telegram_message, :published) }
  let!(:telegram_message_schedule) { create(:telegram_message_schedule, telegram_message:) }

  before do
    expect(Telegram::Deliver).to receive(:call).with(telegram_message)
  end

  it 'destroys the schedule' do
    expect { subject }.to change(TelegramMessageSchedule, :count).by(-1)
  end
end
