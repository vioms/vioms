require 'rails_helper'

describe ViomsMailer do
  let(:email_list) { create(:email_list) }
  let(:rendered_html) { 'rendered html' }

  it 'sends support_message' do
    mail = ViomsMailer.support_message(attributes_for(:contact))
    mail.subject.should
    mail.body.encoded.should match('my name')
    mail.body.encoded.should match('my contact')
    mail.body.encoded.should match('my subject')
    mail.body.encoded.should match('my question')
  end

  it 'sets email from address' do
    mail = ViomsMailer.support_message(attributes_for(:contact, email: 'valid@gmail.com'))
    mail.subject.should
    mail.from.first.should == 'valid@gmail.com'
  end

  it 'sends mailing with attachments' do
    subscriber = create(:subscriber_user_with_email, email_lists: [email_list])
    mail = ViomsMailer.mailing(
      mailing: create(:mailing, email_list:,
                                email_attachments: create_list(:email_attachment, 2)),
      email_list:,
      subscriber:,
      content: rendered_html
    )
    expect(mail.body.parts.size).to eq(3)
  end

  describe 'X-Postmaster-Msgtype header' do
    subject { mail.header['X-Postmaster-Msgtype'].value }

    let(:subscriber) { create(:subscriber_with_email, email_lists: [email_list]) }

    describe 'for mailing' do
      let(:email_list) { create(:email_list, postmaster_msgtype:) }
      let(:mailing) { create(:mailing, email_list:) }
      let(:mail) do
        ViomsMailer.mailing(mailing:, email_list:, subscriber:, content: 'content')
      end

      context 'without msgtype' do
        let(:postmaster_msgtype) { nil }

        it { is_expected.to eq(mailing.id.to_s) }
      end

      context 'with msgtype' do
        let(:postmaster_msgtype) { 'vioms_mail_test' }

        it { is_expected.to eq('vioms_mail_test' + mailing.id.to_s) }
      end
    end

    describe 'for digest' do
      let(:email_list) { create(:email_list_with_digest, postmaster_msgtype:) }
      let(:email_digest) { create(:email_digest, email_list:) }
      let(:mail) do
        ViomsMailer.email_digest(email_digest:, subscriber:, rendered_html: 'html',
                                 rendered_subject: 'Subject')
      end

      context 'without msgtype' do
        let(:postmaster_msgtype) { nil }

        it { is_expected.to eq('d' + email_digest.id.to_s) }
      end

      context 'with msgtype' do
        let(:postmaster_msgtype) { 'vioms_mail_test' }

        it { is_expected.to eq('vioms_mail_test_d' + email_digest.id.to_s) }
      end
    end
  end

  it 'adds List-Unsubscribe header' do
    subscriber = create(:subscriber_with_email, email_lists: [email_list])
    mail = ViomsMailer.mailing(mailing: create(:mailing, email_list:), email_list:,
                               subscriber:, content: rendered_html)
    expect(mail.header['List-Unsubscribe'].value)
      .to eq("<http://www.vioms.ru/unsubscriptions/new?auth_token=#{subscriber.authentication_token}&" \
             "email=#{u(subscriber.email)}&email_list_id=#{email_list.id}&quick=1>")
  end

  it 'sends email_confirmation' do
    subscriber = create(:subscriber_with_email)
    expect_any_instance_of(Subscriber).to receive(:email_confirmation_token).and_return('123456')
    mail = ViomsMailer.email_confirmation(subscriber)
    expect(mail.subject).to eq('Подтверждение вашего ящика')
    expect(mail.body.encoded).to match('123456')
    expect(mail.body.encoded).to match(subscriber.authentication_token)
    expect(mail.body.encoded).to match(CGI.escape(subscriber.email))
  end

  it 'sends subscribed notification' do
    subscriber = create(:subscriber_with_email)
    email_list = create(:email_list, notify_email: generate(:email))
    mail = ViomsMailer.subscribed_notification(subscriber.id, email_list.id)
    mail.subject.should
    mail.body.encoded.should match(subscriber.name)
    mail.body.encoded.should match(subscriber.email)
    mail.body.encoded.should match(email_list.name)
    mail.to.should == [email_list.notify_email]
  end

  it 'sends unsubscribed notification' do
    subscriber = create(:subscriber_with_email)
    email_list = create(:email_list, notify_email: generate(:email))
    mail = ViomsMailer.unsubscribed_notification(subscriber.id, email_list.id)
    mail.subject.should
    mail.body.encoded.should match(subscriber.name)
    mail.body.encoded.should match(subscriber.email)
    mail.body.encoded.should match(email_list.name)
    mail.to.should == [email_list.notify_email]
  end
end
