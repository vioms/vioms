# frozen_string_literal: true

require 'rails_helper'

describe SmsMailer do
  before(:all) do
    create_sms_send_history_types
    create_sms_messages
    create_smtp_setting_group
    Timecop.travel(Time.gm(2008, 9, 1, 6, 5, 0))
  end

  before do
    stub_sms_settings
  end

  after(:all) do
    Timecop.return
  end

  describe 'template replacements' do
    before(:all) do
      @subscriber = create(:subscriber_with_phone, phone: '9091112233')
    end

    it 'sends password_recovery' do
      mail = SmsMailer.password_recovery(@subscriber, 123_456)
      expect(mail.subject).to eq('161080 site')
      expect(mail.body.encoded).to eq("РАМ***password_recovery 123456***\r\n89091112233\r\n")
    end

    it 'adds custom field' do
      password_recovery = SmsMessage.find_by(sms_send_history_type_id: 1002)
      password_recovery.update_attribute(:subject_addition, 'add this')
      mail = SmsMailer.password_recovery(@subscriber, 123_456)
      expect(mail.subject).to eq('161080 site add this')
      expect(mail.body.encoded).to eq("РАМ***password_recovery 123456***\r\n89091112233\r\n")
    end
  end
end
