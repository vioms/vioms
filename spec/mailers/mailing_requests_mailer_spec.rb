# frozen_string_literal: true

require 'rails_helper'

describe MailingRequestsMailer do
  let(:user) { create(:user) }
  let(:subscriber) { create(:subscriber_with_email) }
  let(:mailing_request) { create(:mailing_request, subscriber:) }
  let(:mailing_request_message) do
    create(:mailing_request_message, message: 'The request message', mailing_request:, user:)
  end

  specify 'new_mailing_request_notification' do
    body = MailingRequestsMailer.new_mailing_request_notification(user, mailing_request).body.encoded
    expect(body).to include(subscriber.email)
    expect(body).to include(subscriber.name)
  end

  specify 'subscriber_notification' do
    body = MailingRequestsMailer.subscriber_notification(subscriber, mailing_request_message).body.encoded
    expect(body).to include(user.name)
    expect(body).to include(mailing_request_message.message)
  end

  specify 'user_notification' do
    body = MailingRequestsMailer.user_notification(user, mailing_request, mailing_request_message).body.encoded
    expect(body).to include(subscriber.email)
    expect(body).to include(subscriber.name)
    expect(body).to include(mailing_request_message.message)
  end
end
