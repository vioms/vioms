FactoryBot.define do
  factory :page do
    name { 'Page name' }
    content { 'some text...' }

    trait :invalid do
      name { nil }
      content { nil }
    end
  end
end
