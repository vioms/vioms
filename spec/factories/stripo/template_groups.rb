# frozen_string_literal: true

FactoryBot.define do
  factory :stripo_template_group, class: 'Stripo::TemplateGroup' do
    name { 'MyString' }
    sequence(:position) { |i| i }
    public { true }

    trait :invalid do
      name { nil }
    end

    trait :with_owner do
      public { false }
      user
    end

    trait :with_parent do
      public { nil }
      parent factory: %i[stripo_template_group]
    end
  end
end
