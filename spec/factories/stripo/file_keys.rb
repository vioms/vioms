# frozen_string_literal: true

FactoryBot.define do
  factory :stripo_file_key, class: 'Stripo::FileKey' do
    plugin_id { 'MyString' }
    sequence(:email_id) { |n| n }
    mailing

    trait :with_files do
      after(:create) do |stripo_file_key|
        create_list(:stripo_file, 2, key: stripo_file_key)
      end
    end
  end
end
