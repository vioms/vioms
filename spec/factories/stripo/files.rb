# frozen_string_literal: true

FactoryBot.define do
  factory :stripo_file, class: 'Stripo::File' do
    file_data { ShrineTestData.image_data }
    key factory: %i[stripo_file_key]
    sequence(:cached_url) { |i| "memory://key1/#{i}.gif" }
  end
end
