# frozen_string_literal: true

FactoryBot.define do
  factory :stripo_template, class: 'Stripo::Template' do
    name { 'MyString' }
    html { 'MyText' }
    css { 'MyText' }
    association :template_group, factory: :stripo_template_group, strategy: :create
    stripo_file_key

    trait :invalid do
      name { nil }
    end
  end
end
