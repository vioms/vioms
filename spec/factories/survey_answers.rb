# frozen_string_literal: true

FactoryBot.define do
  factory :survey_answer do
    answer { 'MyString' }
    survey_question
    sequence(:position) { |n| n }
  end
end
