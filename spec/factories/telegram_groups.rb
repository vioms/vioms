# frozen_string_literal: true

FactoryBot.define do
  factory :telegram_group do
    name { Faker::Lorem.word }
    description { Faker::Lorem.paragraph }
    chat_id { Faker::Number.between(from: -1, to: -1_000_000) }
    bot_token { Faker::Lorem.characters(number: 40) }

    trait :invalid do
      name { nil }
      description { nil }
      chat_id { nil }
      bot_token { nil }
    end

    trait :without_description do
      description { nil }
    end

    trait :invisible do
      visible { false }
    end
  end
end
