FactoryBot.define do
  factory :email_attachment do
    mailing
    attachment { Rails.root.join('spec', 'support', 'images', 'spacer.gif').open }
  end
end
