FactoryBot.define do
  factory :email_list_var do
    value { 'MyString' }
    mailing
    email_list_var_name
  end
end
