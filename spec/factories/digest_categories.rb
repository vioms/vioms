FactoryBot.define do
  factory :digest_category do
    name { 'Name' }
    template { 'Big template [MAILINGS]' }
    template_entry { 'Template entry [MAILING_NAME] [MAILING_TEXT]' }
    priority { 0 }
    email_list

    factory :digest_category_with_mailings do
      mailings { [build(:mailing, :for_digest)] }
    end
  end
end
