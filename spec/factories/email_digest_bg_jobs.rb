# frozen_string_literal: true

FactoryBot.define do
  factory :email_digest_bg_job do
    email_digest
  end
end
