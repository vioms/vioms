FactoryBot.define do
  factory :email_list_custom_var do
    name { 'name' }
    description { 'description' }
    email_list
  end
end
