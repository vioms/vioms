FactoryBot.define do
  factory :news_item do
    title { 'MyString' }
    content { 'MyText' }
    date { '2015-08-10' }
  end
end
