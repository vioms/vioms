FactoryBot.define do
  factory :banner do
    association :banner_order, strategy: :create
    image { Rails.root.join('spec', 'support', 'images', 'spacer.gif').open }
    link { 'MyString' }
    placement { 1 }
    priority { 0 }
  end
end
