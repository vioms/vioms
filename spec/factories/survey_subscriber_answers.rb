# frozen_string_literal: true

FactoryBot.define do
  factory :survey_subscriber_answer do
    value { 'MyText' }
    subscriber factory: %i[subscriber_with_email]
    survey_question
  end
end
