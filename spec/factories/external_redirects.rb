FactoryBot.define do
  factory :external_redirect do
    sequence(:url) { |n| "http://example.com/url#{n}" }
  end
end
