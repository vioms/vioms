# frozen_string_literal: true

FactoryBot.define do
  factory :mailing_request_image do
    image { Rails.root.join('spec/support/images/spacer.gif').open }
    mailing_request
  end
end
