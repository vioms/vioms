# frozen_string_literal: true

FactoryBot.define do
  factory :mailing_request_message do
    old_status { :awaiting_admin_response }
    new_status { :processed }
    message { 'MyString' }
    mailing_request
    author { :subscriber }
  end
end
