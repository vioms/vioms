# frozen_string_literal: true

FactoryBot.define do
  factory :survey do
    name { 'MyString' }
    title { 'MyString' }
  end
end
