# frozen_string_literal: true

FactoryBot.define do
  factory :sms_message do
    message { 'some text' }
    sms_send_history_type
    sequence(:position) { |n| n }
  end
end
