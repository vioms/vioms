FactoryBot.define do
  factory :user do
    sequence(:name) { |n| "User #{n}" }
    sequence(:username) { |n| "user#{n}" }
    password { 'password' }
    password_confirmation { |u| u.password }
    sequence(:email) { |n| "user#{n}@example.com" }

    trait :with_email_lists do
      after(:create) { |user| user.email_lists << [build(:email_list), build(:email_list)] }
    end

    trait :with_sms_lists do
      after(:create) { |user| user.sms_lists << [build(:sms_list), build(:sms_list)] }
    end

    trait :with_email_and_sms_lists do
      after(:create) { |user| user.email_lists << [build(:email_list), build(:email_list)] }

      after(:create) { |user| user.sms_lists << [build(:sms_list), build(:sms_list)] }
    end

    trait :admin do
      is_admin { true }
    end
  end

  User::ROLES.each_key do |role|
    eval(<<-EOS, nil, __FILE__, __LINE__ + 1)
      factory :user_with_#{role}_role, parent: :user do
        is_#{role} { true }
      end
    EOS
  end
end
