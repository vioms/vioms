# frozen_string_literal: true

FactoryBot.define do
  factory :api_key do
    name { 'MyString' }
    key { SecureRandom.uuid }
    user
  end
end
