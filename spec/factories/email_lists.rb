FactoryBot.define do
  factory :email_list do
    name { 'Email list name' }
    default_editor_type { 'ckeditor' }
    from_name { 'From name' }
    from_email { 'from@example.com' }

    trait :visible do
      visible { true }
    end

    trait :invisible do
      visible { false }
    end

    trait :never_visible do
      visible { false }
      never_visible { true }
    end

    factory :email_list_with_digest do
      digest_template { 'body' }
      digest_subject_template { 'subject' }
      after(:create) { |model| model.digest_categories << [build(:digest_category)] }
    end

    factory :email_list_with_template do
      default_editor_type { 'template' }
      template { 'template' }
    end
  end
end
