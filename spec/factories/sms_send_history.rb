# frozen_string_literal: true

# == Schema Information
#
# Table name: sms_send_history
#
#  id            :integer          not null, primary key
#  phone         :string(255)
#  subscriber_id :integer
#  created_at    :datetime
#  updated_at    :datetime
#  message_type  :integer          default(1), not null
#  last_value    :string(255)
#  expire_at     :datetime
#  attempts      :integer          default(0), not null
#

FactoryBot.define do
  factory :sms_send_history do
    subscriber factory: %i[subscriber_with_phone]
    message_type factory: %i[sms_send_history_type]
  end
end
