# frozen_string_literal: true

FactoryBot.define do
  factory :subscriber_import do
    subscribers_data { 'subscribers data' }
    email_lists { [build(:email_list)] }
    confirm_email { true }
    user

    trait :valid do
      email_column { 1 }
    end
  end
end
