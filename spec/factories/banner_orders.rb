FactoryBot.define do
  factory :banner_order do
    banner_name { 'MyString' }
    association :banner_owner, strategy: :create
    start_on { '2015-09-17' }
    end_on { '2015-09-17' }
  end
end
