# frozen_string_literal: true

FactoryBot.define do
  factory :mailing_request do
    subject { 'MyString' }
    body { 'MyText' }
    comments { 'MyText' }
    mailing_type { :private }
    status { :awaiting_subscriber_response }
    association :subscriber, factory: :subscriber_with_email
  end
end
