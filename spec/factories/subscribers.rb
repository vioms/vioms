# frozen_string_literal: true

FactoryBot.define do
  factory :subscriber do
    name { 'Joe Doe' }
  end

  factory :subscriber_user do
    name { 'Joe Doe' }
    password { 'password' }
    password_confirmation(&:password)
  end

  trait :with_legacy_mobile_token do
    after(:create) { _1.mobile_tokens << build(:mobile_token) }
  end

  trait :with_android_mobile_token do
    after(:create) { _1.mobile_tokens << build(:mobile_token, :android) }
  end

  trait :with_ios_mobile_token do
    after(:create) { _1.mobile_tokens << build(:mobile_token, :ios) }
  end

  %w[subscriber subscriber_user].each do |name|
    eval(<<-EOS, nil, __FILE__, __LINE__ + 1)
      factory :#{name}_with_email, parent: :#{name} do
        email

        factory :#{name}_with_confirmed_email do
          confirmed_at { '2011-01-15 04:30:40' }
        end

        factory :#{name}_with_email_and_problems do
          after(:create) { |subscriber| create(:email_problem, subscriber:) }
        end
      end

      factory :#{name}_with_phone, parent: :#{name} do
        phone
      end

      factory :#{name}_with_email_and_phone, parent: :#{name}_with_phone do
        email
      end
    EOS
  end
end
