FactoryBot.define do
  factory :text_block do
    sequence(:name) { |n| "#{Faker::Lorem.word}[#{n}]" }
    description { Faker::Lorem.word }
    content { Faker::Lorem.paragraph }

    trait :invalid do
      name { nil }
    end
  end
end
