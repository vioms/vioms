FactoryBot.define do
  factory :banner_owner do
    name { 'MyString' }
    contact { 'MyString' }
  end
end
