FactoryBot.define do
  factory :answer do
    title { 'Answer' }
    association :poll, strategy: :create
    sequence(:position) { |n| n }
  end
end
