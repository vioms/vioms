# frozen_string_literal: true

FactoryBot.define do
  factory :mobile_token do
    serial { 0 }
    subscriber factory: %i[subscriber_with_email]

    trait :android do
      device_type { :android }
      device_token do
        'dS1-za1vFts:APA91bHsJzjTtZ5uOwT52VLe1F3QfXPbK2a29VXvOj1PyZfiJz6V4zW5AwOp_qbUrmhR1GHfYKjDR5h3qxs_pOzN3g7h-TShCdUogmhZzUzwFS1gJjRl5Kx5Q3bq-32yH4dEdDh7-yZ'
      end
    end

    trait :ios do
      device_type { :ios }
      device_token do
        'eX7y_aGh123:APA91bHbQPiNpZg8XKjQ8vQW8iSMLGp4nRNd4qZJH8s2YJ7m3UkJqH6Bh3_fHp2G3gMvC0s7Q3L9Fs4_RkQq3LXq0dV5UzAb7KhUnlF8Pc2Zxr3p9tYsZy_jD7l8pEtCyDQf9OPfRuTX'
      end
    end
  end
end
