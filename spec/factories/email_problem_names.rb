FactoryBot.define do
  factory :email_problem_name do
    name { 'MyString' }
    problem_type { 2 }
  end
end
