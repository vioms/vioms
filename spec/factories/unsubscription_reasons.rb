FactoryBot.define do
  factory :unsubscription_reason do
    name { 'MyString' }
    sequence(:position) { |n| n }
  end
end
