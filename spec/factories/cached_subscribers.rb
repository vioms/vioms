FactoryBot.define do
  factory :cached_subscriber do
    phone { 'MyString' }
    sms_sending_id { 1 }
  end
end
