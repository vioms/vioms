# frozen_string_literal: true

FactoryBot.define do
  factory :email_send_history do
    subscriber factory: %i[subscriber_with_email]
  end
end
