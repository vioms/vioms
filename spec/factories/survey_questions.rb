# frozen_string_literal: true

FactoryBot.define do
  factory :survey_question do
    question { 'MyString' }
    question_type { :string }
    sequence(:position) { |n| n }
    survey

    trait :text do
      question_type { :text }
    end

    trait :options do
      question_type { :options }
    end

    trait :vote do
      question_type { :vote }
    end
  end
end
