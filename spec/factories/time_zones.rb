FactoryBot.define do
  factory :time_zone do
    sequence(:name) { |n| "Timezone #{n}" }
    priority { 0 }
    sequence(:begin_at) { |n| Time.zone.parse('2001-01-01 07:00:00 +0300') + (n * 5).minutes }
    sequence(:end_at)   { |n| Time.zone.parse('2001-01-01 21:00:00 +0300') + (n * 5).minutes }
    sequence(:zone)     { |n| Time.zone.parse('2001-01-01 00:00:00 +0300') + n.hours }
    zone_sign { true }
    sequence(:num) { |n| n }
  end
end
