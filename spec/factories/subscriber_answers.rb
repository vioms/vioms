FactoryBot.define do
  factory :subscriber_answer do
    poll
    answer
    subscriber factory: %i[subscriber_with_email]
  end
end
