# frozen_string_literal: true

FactoryBot.define do
  factory :email_problem do
    subscriber
    association :email_problem_name, strategy: :create
    description { 'MyString' }

    factory :email_problem_solved do
      solved { true }
    end
  end
end
