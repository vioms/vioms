# frozen_string_literal: true

FactoryBot.define do
  factory :mailing_destination do
    name { 'MyString' }
  end
end
