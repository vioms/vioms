FactoryBot.define do
  factory :email_digest do
    sequence(:num) { |n| n }
    deliver_at { get_next_delivery_time }
    email_list factory: %i[email_list_with_digest], strategy: :create
    rendered_html { 'rendered html' }

    trait :published do
      published { true }
    end

    factory :email_digest_with_mailings do
      transient do
        mailings_count { 1 }
      end

      after(:create) do |email_digest, evaluator|
        build_list(:mailing, :for_digest, evaluator.mailings_count, email_digest:)
      end
    end
  end
end
