# frozen_string_literal: true

FactoryBot.define do
  factory :mailing_bg_job do
    mailing
  end
end
