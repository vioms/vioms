# frozen_string_literal: true

FactoryBot.define do
  factory :mailing_view do
    mailing
    subscriber factory: :subscriber_with_email
  end
end
