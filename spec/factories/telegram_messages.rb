FactoryBot.define do
  factory :telegram_message do
    message { Faker::Lorem.paragraph }
    association :telegram_group, strategy: :create
    deliver_at { 1.hour.from_now }
  end

  trait :invalid do
    deliver_at { 1.hour.ago }
  end

  trait :with_image do
    image { Rails.root.join('spec', 'support', 'images', 'spacer.gif').open }
  end

  trait :published do
    published { true }
    user
  end

  trait :with_schedule do
    association :schedule, factory: :telegram_message_schedule, strategy: :build
  end
end
