FactoryBot.define do
  factory :mailing do
    subject { 'Subject' }

    editor_type { 'ckeditor' }
    html_content { 'content' }
    rendered_html { 'rendered html' }
    deliver_at { get_next_delivery_time }
    association :email_list, strategy: :create
    max_attachments_size { 128 }

    trait :published do
      deliver_at { 1.day.ago }
      published { true }
      sent { true }
    end

    trait :published_not_sent do
      deliver_at { 1.day.from_now }
      published { true }
    end

    trait :for_creating do
      creating { true }
      subject { nil }

      html_content { nil }
      disable_editor { nil }
      deliver_at { nil }
      max_attachments_size { nil }
    end

    trait :for_stripo do
      editor_type { 'stripo' }
      css { 'css' }
      html_content { 'content [FOOTER]' }
      stripo_template_id { 1 }
    end

    trait :for_stripo_invalid do
      editor_type { 'stripo' }
      css { 'css' }
      html_content { 'content' }
    end

    trait :for_digest do
      digest { true }
      digest_content { 'Content' }
      digest_category
      email_digest
    end
  end
end
