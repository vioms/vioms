FactoryBot.define do
  factory :smtp_setting do
    name { 'MyString' }
    value { 'MyString' }
    smtp_setting_group
  end
end
