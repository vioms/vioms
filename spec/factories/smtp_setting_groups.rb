FactoryBot.define do
  factory :smtp_setting_group do
    name { 'MyString' }
    email
    position { 1 }
    selected { false }
  end
end
