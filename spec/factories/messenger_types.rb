FactoryBot.define do
  factory :messenger_type do
    sequence(:name) { |n| "Messenger Type #{n}" }
    sequence(:char) { |n| (64 + n).chr }
  end
end
