FactoryBot.define do
  factory :email_list_var_name do
    name { 'MyString' }
    description { 'Description' }
    email_list
  end
end
