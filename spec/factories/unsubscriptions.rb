FactoryBot.define do
  factory :unsubscription do
    subscriber factory: %i[subscriber_with_email]
    email_list
    unsubscription_reason
  end
end
