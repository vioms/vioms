# Read about factories at https://github.com/thoughtbot/factory_bot

FactoryBot.define do
  factory :wiki do
    name { 'MyString' }
  end
end
