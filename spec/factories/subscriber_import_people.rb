# frozen_string_literal: true

FactoryBot.define do
  factory :subscriber_import_person do
    name { 'Name' }
    email { 'email@example.com' }
    phone { '9001002030' }
    subscriber_import factory: %i[subscriber_import valid]
  end
end
