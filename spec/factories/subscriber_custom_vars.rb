FactoryBot.define do
  factory :subscriber_custom_var do
    subscriber factory: %i[subscriber_with_email]
    email_list_custom_var
  end
end
