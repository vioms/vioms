FactoryBot.define do
  factory :contact do
    name { 'my name' }
    email { 'test@example.com' }
    phone { 'my contact' }
    subject { 'my subject' }

    question { 'my question' }
  end
end
