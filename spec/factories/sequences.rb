FactoryBot.define do
  sequence :email do |n|
    "subscriber#{n}@example.com"
  end

  sequence :phone do |n|
    "909111#{format('%04d', n)}".to_i
  end
end
