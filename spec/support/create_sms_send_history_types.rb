# frozen_string_literal: true

module CreateSmsSendHistoryTypes
  def create_sms_send_history_types
    create(:sms_send_history_type, id: 1002, name: 'password_recovery')
    create(:sms_send_history_type, id: 1004, name: 'test_all_variables')
    create(:sms_send_history_type, id: 1005, name: '3_times', attempts: 3)
  end
end

begin
  RSpec.configure do |config|
    config.include CreateSmsSendHistoryTypes
  end
rescue StandardError
end
