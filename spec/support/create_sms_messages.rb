# frozen_string_literal: true

module CreateSmsMessages
  def create_sms_messages
    create(:sms_message, sms_send_history_type_id: 1002, message: 'password_recovery %<reset_password_token>s')
  end
end

begin
  RSpec.configure do |config|
    config.include CreateSmsMessages
  end
rescue StandardError
end
