# frozen_string_literal: true

module ControllerHelpers
  def sign_in_user(user, model_class)
    # @request is available only in controller tests. This line is not required for request tests.
    @request.env['devise.mapping'] = Devise.mappings[:user] if @request
    sign_in(user)
    stub_user(model_class)
  end

  def sign_in_subscriber_user(subscriber_user, model_class)
    @request.env['devise.mapping'] = Devise.mappings[:subscriber_user] if @request
    sign_in(subscriber_user)
    stub_user(model_class)
  end

  def stub_user(model_class)
    return unless model_class

    allow_any_instance_of(model_class).to receive(:user_id).and_return(subject.current_user.id)
    allow_any_instance_of(model_class).to receive(:user).and_return(subject.current_user)
  end
end
