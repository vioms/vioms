module StubSmsSettings
  def stub_sms_settings
    allow(Setting).to receive('get_setting').with('sms_max_chars').and_return(678)
    allow(Setting).to receive('get_setting').with('use_crlf').and_return(true)
    allow(Setting).to receive('get_setting').with('allow_send_sms').and_return(true)
  end
end

begin
  RSpec.configure do |config|
    config.include StubSmsSettings
  end
rescue StandardError
end
