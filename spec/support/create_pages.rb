# frozen_string_literal: true

module CreatePages
  def create_pages
    create(:page)
  end
end

begin
  RSpec.configure do |config|
    config.include CreatePages
  end
rescue StandardError
end
