# frozen_string_literal: true

module ControllerMacros
  def login_user(model_class = nil)
    before { sign_in_user(create(:user), model_class) }
  end

  def login_admin_user(model_class = nil)
    before { sign_in_user(create(:user, :admin), model_class) }
  end

  User::ROLES.each_key do |role|
    module_eval(<<-EOS, __FILE__, __LINE__ + 1)
      def login_user_with_#{role}_role model_class = nil
        before(:each) { self.sign_in_user(create(:user_with_#{role}_role), model_class) }
      end
    EOS
  end

  def login_subscriber_user(factory_name, model_class = nil)
    before { sign_in_subscriber_user(create(factory_name), model_class) }
  end

  def it_should_require_admin_user_for_actions(_controller, model, actions = {})
    actions.each do |action, method|
      it "#{action} action (with method #{method}) should require admin" do
        send(method, action, params: { id: model.first.id })
        response.should redirect_to(new_user_session_path)
        flash[:alert].should == 'Вам необходимо войти в систему или зарегистрироваться.'
      end
    end
  end

  def it_should_require_subscriber_user_for_actions(_controller, model, actions = {})
    actions.each do |action, method|
      it "#{action} action (with method #{method}) should require admin" do
        send(method, action, params: { id: model.first.id })
        response.should redirect_to(new_subscriber_user_session_path)
        flash[:alert].should == 'Вам необходимо войти в систему или зарегистрироваться.'
      end
    end
  end
end
