module CreateSubscriberWithSubscriptions
  def create_subscriber_with_subscriptions
    subscriber = create(:subscriber_user_with_email_and_phone, email: 'subscriber_with_subscriptions@example.com', password: 'password',
                                                               email_lists: [
                                                                 build(:email_list, name: 'Visible Checked email list',
                                                                                    visible: true, never_visible: false),
                                                                 build(:email_list, name: 'Invisible Checked email list', visible: false,
                                                                                    never_visible: false),
                                                                 build(:email_list, name: 'NeverVisibleEmailList Visible Checked email list', visible: true,
                                                                                    never_visible: true),
                                                                 build(:email_list, name: 'NeverVisibleEmailList Invisible Checked email list',
                                                                                    visible: false, never_visible: true)
                                                               ])

    subscriber.sms_subscriptions.create!(sms_list: build(:sms_list, name: 'Visible not public Checked sms list not confirmed', visible: true, never_visible: false),
                                         confirmed: false)
    subscriber.sms_subscriptions.create!(sms_list: build(:sms_list, name: 'Invisible not public Checked sms list not confirmed', visible: false, never_visible: false),
                                         confirmed: false)
    subscriber.sms_subscriptions.create!(sms_list: build(:sms_list, name: 'Visible Public Checked sms list not confirmed', visible: true, public: true, never_visible: false),
                                         confirmed: false)
    subscriber.sms_subscriptions.create!(sms_list: build(:sms_list, name: 'Invisible Public Checked sms list not confirmed', visible: false, public: true, never_visible: false),
                                         confirmed: false)

    subscriber.sms_subscriptions.create!(sms_list: build(:sms_list, name: 'NeverVisibleSmsList Visible not public Checked sms list not confirmed', visible: true, never_visible: true),
                                         confirmed: false)
    subscriber.sms_subscriptions.create!(sms_list: build(:sms_list, name: 'NeverVisibleSmsList Invisible not public Checked sms list not confirmed', visible: false, never_visible: true),
                                         confirmed: false)
    subscriber.sms_subscriptions.create!(sms_list: build(:sms_list, name: 'NeverVisibleSmsList Visible Public Checked sms list not confirmed', visible: true, public: true, never_visible: true),
                                         confirmed: false)
    subscriber.sms_subscriptions.create!(sms_list: build(:sms_list, name: 'NeverVisibleSmsList Invisible Public Checked sms list not confirmed', visible: false, public: true, never_visible: true),
                                         confirmed: false)

    subscriber.sms_subscriptions.create!(sms_list: build(:sms_list,
                                                         name: 'Visible not public Checked sms list Confirmed', visible: true, never_visible: false))
    subscriber.sms_subscriptions.create!(sms_list: build(:sms_list,
                                                         name: 'Invisible not public Checked sms list Confirmed', visible: false, never_visible: false))
    subscriber.sms_subscriptions.create!(sms_list: build(:sms_list, name: 'Visible Public Checked sms list Confirmed',
                                                                    visible: true, public: true, never_visible: false))
    subscriber.sms_subscriptions.create!(sms_list: build(:sms_list,
                                                         name: 'Invisible Public Checked sms list Confirmed', visible: false, public: true, never_visible: false))
    subscriber.sms_subscriptions.create!(sms_list: build(:sms_list,
                                                         name: 'NeverVisibleSmsList Visible not public Checked sms list Confirmed', visible: true, never_visible: true))
    subscriber.sms_subscriptions.create!(sms_list: build(:sms_list,
                                                         name: 'NeverVisibleSmsList Invisible not public Checked sms list Confirmed', visible: false, never_visible: true))
    subscriber.sms_subscriptions.create!(sms_list: build(:sms_list,
                                                         name: 'NeverVisibleSmsList Visible Public Checked sms list Confirmed', visible: true, public: true, never_visible: true))
    subscriber.sms_subscriptions.create!(sms_list: build(:sms_list,
                                                         name: 'NeverVisibleSmsList Invisible Public Checked sms list Confirmed', visible: false, public: true, never_visible: true))

    create(:email_list, name: 'Visible not checked email list', visible: true, never_visible: false)
    create(:email_list, name: 'Invisible not checked email list', visible: false, never_visible: false)
    create(:email_list, name: 'NeverVisibleEmailList Visible not checked email list', visible: true,
                        never_visible: true)
    create(:email_list, name: 'NeverVisibleEmailList Invisible not checked email list', visible: false,
                        never_visible: true)

    create(:sms_list, name: 'Visible not public not checked sms list', visible: true, never_visible: false)
    create(:sms_list, name: 'Invisible not public not checked sms list', visible: false, never_visible: false)
    create(:sms_list, name: 'Visible Public not checked sms list', visible: true, public: true, never_visible: false)
    create(:sms_list, name: 'Invisible Public not checked sms list', visible: false, public: true, never_visible: false)
    create(:sms_list, name: 'NeverVisibleSmsList Visible not public not checked sms list', visible: true,
                      never_visible: true)
    create(:sms_list, name: 'NeverVisibleSmsList Invisible not public not checked sms list', visible: false,
                      never_visible: true)
    create(:sms_list, name: 'NeverVisibleSmsList Visible Public not checked sms list', visible: true, public: true,
                      never_visible: true)
    create(:sms_list, name: 'NeverVisibleSmsList Invisible Public not checked sms list', visible: false, public: true,
                      never_visible: true)
  end
end

begin
  RSpec.configure { |config| config.include CreateSubscriberWithSubscriptions }
rescue StandardError
end
