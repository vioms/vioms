RSpec.configure do |config|
  config.before do
    allow(Setting).to receive('get_setting').with('stripo_version').and_return('latest')
  end
end
