# frozen_string_literal: true

module CreateSmtpSettingGroup
  def create_smtp_setting_group
    create(:smtp_setting_group, selected: true)
  end
end

begin
  RSpec.configure { |config| config.include CreateSmtpSettingGroup }
rescue StandardError
end
