# frozen_string_literal: true

module FactoryBotModelHelpers
  def reset_next_delivery_time
    $delivery_at_count = 1 # rubocop:disable Style/GlobalVars
  end
end

RSpec.configure do |config|
  config.include FactoryBotModelHelpers, type: :model
end
