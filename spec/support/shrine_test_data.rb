# frozen_string_literal: true

module ShrineTestData
  module_function

  def image_data
    attacher = Shrine::Attacher.new
    attacher.set(uploaded_image)

    attacher.data
  end

  def uploaded_image
    file = File.open('spec/support/images/spacer.gif', binmode: true)

    uploaded_file = Shrine.upload(file, :store, metadata: false)
    uploaded_file.metadata.merge!(
      'size' => File.size(file.path),
      'mime_type' => 'image/gif',
      'filename' => 'spacer.gif'
    )

    uploaded_file
  end
end
