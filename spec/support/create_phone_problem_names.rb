# frozen_string_literal: true

module CreatePhoneProblemNames
  include ProblemTypesHelper::Types

  def create_phone_problem_names
    create(:phone_problem_name, id: 1001, name: 'Info', problem_type: PROBLEM_INFO)
    create(:phone_problem_name, id: 1002, name: 'Temporarily unavailable', problem_type: PROBLEM_TEMPORARY)
    create(:phone_problem_name, id: 1003, name: 'Permanently unavailable', problem_type: PROBLEM_PERMANENT)
    create(:phone_problem_name, id: 1004, name: 'Reported spam', problem_type: PROBLEM_PERMANENT)
  end
end

begin
  RSpec.configure { |config| config.include CreatePhoneProblemNames }
rescue StandardError
end
