RSpec.configure do |config|
  config.before do
    allow(Setting).to receive('get_setting').with('support_phone').and_return('1234567890')
  end
end
