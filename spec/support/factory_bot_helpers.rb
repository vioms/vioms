# frozen_string_literal: true

module FactoryBotHelpers
  def get_next_delivery_time
    $delivery_at_count ||= 1
    deliver_at = ((EmailDigest::MINIMUM_INTERVAL + 1.minute) * 5 * $delivery_at_count).seconds.from_now
    $delivery_at_count += 1
    deliver_at
  end
end

FactoryBot::SyntaxRunner.include FactoryBotHelpers
