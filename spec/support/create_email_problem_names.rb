# frozen_string_literal: true

module CreateEmailProblemNames
  include ProblemTypesHelper::Types

  def create_email_problem_names
    create(:email_problem_name, id: 1001, name: 'Info', problem_type: PROBLEM_INFO)
    create(:email_problem_name, id: 1002, name: 'Over quota', problem_type: PROBLEM_TEMPORARY)
    create(:email_problem_name, id: 1003, name: 'Invalid mailbox', problem_type: PROBLEM_PERMANENT)
    create(:email_problem_name, id: 1004, name: 'Reported spam', problem_type: PROBLEM_PERMANENT)
  end
end

begin
  RSpec.configure { |config| config.include CreateEmailProblemNames }
rescue StandardError
end
