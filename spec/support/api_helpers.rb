# frozen_string_literal: true

module ApiHelpers
  def json_body
    JSON.parse(response.body, symbolize_names: true)
  end

  def auth_header(subscriber)
    mobile_token = subscriber.mobile_tokens.first_or_create!
    token = ::Mobile::Token.encoded_token(mobile_token)
    { Authorization: "Bearer #{token}" }
  end
end

RSpec.configure do |config|
  config.include ApiHelpers, type: :controller
  config.include ApiHelpers, type: :request
end
