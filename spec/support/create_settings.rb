# frozen_string_literal: true

module CreateSettings
  def create_settings
    create(:setting, name: 'sms_max_chars', value: 678, value_type: 'integer')
    create(:setting, name: 'use_crlf', value: true, value_type: 'boolean')
    create(:setting, name: 'allow_send_sms', value: true, value_type: 'boolean')

    # TODO: these are required for cucumber tests only
    create(:setting, name: 'support_phone', value: '+79001234567', value_type: 'string')
    create(:setting, name: 'stripo_version', value: 'latest', value_type: 'string')
  end
end

begin
  RSpec.configure do |config|
    config.include CreateSettings
  end
rescue StandardError
end
