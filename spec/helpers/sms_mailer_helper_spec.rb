require 'rails_helper'

describe SmsMailerHelper do
  before(:all) do
    create_sms_send_history_types
    create_sms_messages
    create_smtp_setting_group
  end

  it 'wraps sms message' do
    expect(helper.wrap_sms_message('текст')).to eq('РАМ***текст***')
  end

  it 'gets message for subscriber' do
    message = helper.get_message('password_recovery')
    expect(message).to eq('password_recovery ')
  end

  it 'does not raise exception for real model' do
    expect do
      lambda {
        helper.get_message('password_recovery')
      }
    end.not_to raise_error
  end

  it 'handles blank variables' do
    message = helper.get_message('password_recovery')
    expect(message).to eq('password_recovery ')
  end
end
