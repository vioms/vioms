require 'rails_helper'

describe Admin::MailingsHelper do
  it 'detects Mobile browser' do
    helper
      .is_mobile_browser?('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_2) AppleWebKit/534.51.22 (KHTML, like Gecko) Version/5.1.1 Safari/534.51.22')
      .should be_falsey
    helper
      .is_mobile_browser?('Mozilla/5.0 (Android; Linux armv7l; rv:8.0) Gecko/20111011 Firefox/8.0 Fennec/8.0')
      .should be_truthy
  end
end
