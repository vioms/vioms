# frozen_string_literal: true

require 'rails_helper'

describe Admin::SubscribersHelper do
  before(:all) { create_sms_send_history_types }

  it 'has email confirmation token' do
    subscriber = create(:subscriber_with_email)
    expect(helper.has_email_confirmation_token?(subscriber)).to be(false)
    create(:email_send_history, email: subscriber.email, subscriber_id: subscriber.id, last_value: '123456')
    subscriber.reload
    expect(helper.has_email_confirmation_token?(subscriber)).to be(true)
    subscriber.update_attribute(:email, generate(:email))
    expect(helper.has_email_confirmation_token?(subscriber)).to be(false)
  end
end
