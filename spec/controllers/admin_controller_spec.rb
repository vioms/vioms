# frozen_string_literal: true

require 'rails_helper'

describe AdminController do
  context 'as admin' do
    login_admin_user

    it 'redirects to welcome' do
      get :show
      expect(response).to redirect_to(admin_welcome_url)
    end
  end

  context 'as user' do
    login_user

    it 'redirects to welcome' do
      get :show
      expect(response).to redirect_to(admin_welcome_url)
    end
  end

  context 'as nobody' do
    it 'redirects to login page' do
      get :show
      expect(response).to redirect_to(new_user_session_url)
    end
  end
end
