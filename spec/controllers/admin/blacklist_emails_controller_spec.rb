require 'rails_helper'

describe Admin::BlacklistEmailsController do
  render_views
  before(:all) do
    create_list(:blacklist_email, 2)
  end

  context 'as admin user' do
    login_admin_user

    it 'index action should render index template' do
      get :index
      response.should render_template(:index)
    end

    it 'new action should render new template' do
      get :new
      response.should render_template(:new)
    end

    it 'create action should render new template when model is invalid' do
      expect_any_instance_of(BlacklistEmail).to receive(:valid?).and_return(false)
      post :create, params: { blacklist_email: params_for(:blacklist_email) }
      response.should render_template(:new)
    end

    it 'create action should redirect when model is valid' do
      expect_any_instance_of(BlacklistEmail).to receive(:valid?).and_return(true)
      post :create, params: { blacklist_email: params_for(:blacklist_email) }
      response.should redirect_to(admin_blacklist_emails_path)
    end

    it 'edit action should render edit template' do
      get :edit, params: { id: BlacklistEmail.first }
      response.should render_template(:edit)
    end

    it 'update action should render edit template when model is invalid' do
      expect_any_instance_of(BlacklistEmail).to receive(:valid?).and_return(false)
      put :update, params: { id: BlacklistEmail.first, blacklist_email: params_for(:blacklist_email) }
      response.should render_template(:edit)
    end

    it 'update action should redirect when model is valid' do
      expect_any_instance_of(BlacklistEmail).to receive(:valid?).and_return(true)
      params_for = params_for(:blacklist_email)
      put :update, params: { id: BlacklistEmail.first, blacklist_email: params_for }
      response.should redirect_to(admin_blacklist_emails_path)
    end

    it 'destroy action should destroy model and redirect to index action' do
      blacklist_email = BlacklistEmail.first
      delete :destroy, params: { id: blacklist_email }
      response.should redirect_to(admin_blacklist_emails_path)
      BlacklistEmail.exists?(blacklist_email.id).should be_falsey
    end
  end
end
