require 'rails_helper'

describe Admin::SmsSendHistoryTypesController do
  render_views
  before(:all) do
    create_sms_send_history_types
    create_sms_messages
  end

  context 'as admin user' do
    login_admin_user

    it 'index action should render index template' do
      get :index
      response.should render_template(:index)
    end

    it 'edit action should render edit template' do
      get :edit, params: { id: SmsSendHistoryType.first }
      response.should render_template(:edit)
    end

    it 'update action should render edit template when model is invalid' do
      expect_any_instance_of(SmsSendHistoryType).to receive(:valid?).and_return(false)
      put :update, params: { id: SmsSendHistoryType.first, sms_send_history_type: params_for(:sms_send_history_type) }
      response.should render_template(:edit)
    end

    it 'update action should redirect when model is valid' do
      expect_any_instance_of(SmsSendHistoryType).to receive(:valid?).and_return(true)
      put :update, params: { id: SmsSendHistoryType.first, sms_send_history_type: params_for(:sms_send_history_type) }
      response.should redirect_to(admin_sms_send_history_types_path)
    end
  end
end
