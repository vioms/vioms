# frozen_string_literal: true

require 'rails_helper'

describe Admin::EmailListsController do
  render_views
  before(:all) { create_list(:email_list, 2) }

  context 'as admin user' do
    login_admin_user

    it 'index action renders index template' do
      get :index
      expect(response).to render_template(:index)
    end

    specify 'index json action' do
      get :index, format: :json
      expect(response.parsed_body).to be_an_instance_of(Array)
    end

    specify 'show json action' do
      get :show, params: { id: EmailList.first }, format: :json
      expect(JSON.parse(response.body, symbolize_names: true))
        .to eq({ email_list:
                   { id: EmailList.first.id, default_editor_type: 'ckeditor', default_stripo_template_group_id: nil,
                     default_stripo_template_id: nil },
                 template_groups: [] })
    end

    it 'new action renders new template' do
      get :new
      expect(response).to render_template(:new)
    end

    it 'create action renders new template when model is invalid' do
      expect_any_instance_of(EmailList).to receive(:valid?).and_return(false)
      post :create, params: { email_list: params_for(:email_list) }
      expect(response).to render_template(:new)
    end

    it 'create action redirects when model is valid' do
      expect_any_instance_of(EmailList).to receive(:valid?).and_return(true)
      post :create, params: { email_list: params_for(:email_list) }
      expect(response).to redirect_to(admin_email_lists_url)
    end

    it 'edit action renders edit template' do
      get :edit, params: { id: EmailList.first }
      expect(response).to render_template(:edit)
    end

    it 'update action renders edit template when model is invalid' do
      expect_any_instance_of(EmailList).to receive(:valid?).and_return(false)
      put :update, params: { id: EmailList.first, email_list: params_for(:email_list) }
      expect(response).to render_template(:edit)
    end

    it 'update action redirects when model is valid' do
      expect_any_instance_of(EmailList).to receive(:valid?).and_return(true)
      put :update, params: { id: EmailList.first, email_list: params_for(:email_list) }
      expect(response).to redirect_to(admin_email_lists_url)
    end

    it 'destroy action destroys model and redirects to index action' do
      email_list = EmailList.first
      delete :destroy, params: { id: email_list }
      expect(response).to redirect_to(admin_email_lists_url)
      EmailList.exists?(email_list.id).should be_falsey
    end

    it 'export' do
      get :export, params: { id: EmailList.first }
      expect(response.header['Content-Type']).to eq('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
      expect(response.body).to be_present
    end
  end
end
