# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Admin::TelegramGroupsController do
  let(:valid_attributes) do
    attributes_for(:telegram_group)
  end

  let(:invalid_attributes) do
    attributes_for(:telegram_group, :invalid)
  end

  login_admin_user
  render_views

  describe 'GET #index' do
    it 'returns a success response' do
      TelegramGroup.create! valid_attributes
      get :index, params: {}
      expect(response).to be_successful
    end
  end

  describe 'GET #new' do
    it 'returns a success response' do
      get :new, params: {}
      expect(response).to be_successful
    end
  end

  describe 'GET #edit' do
    it 'returns a success response' do
      telegram_group = TelegramGroup.create! valid_attributes
      get :edit, params: { id: telegram_group.to_param }
      expect(response).to be_successful
    end
  end

  describe 'POST #create' do
    context 'with valid params' do
      it 'creates a new TelegramGroup' do
        expect do
          post :create, params: { telegram_group: valid_attributes }
        end.to change(TelegramGroup, :count).by(1)
      end

      it 'redirects to the created telegram_group' do
        post :create, params: { telegram_group: valid_attributes }
        expect(response).to redirect_to(admin_telegram_groups_url)
      end
    end

    context 'with invalid params' do
      it "returns a success response (i.e. to display the 'new' template)" do
        post :create, params: { telegram_group: invalid_attributes }
        expect(response).to be_successful
      end
    end
  end

  describe 'PUT #update' do
    context 'with valid params' do
      it 'redirects to the telegram_group' do
        telegram_group = TelegramGroup.create! valid_attributes
        put :update, params: { id: telegram_group.to_param, telegram_group: valid_attributes }
        expect(response).to redirect_to(admin_telegram_groups_url)
      end
    end

    context 'with invalid params' do
      it "returns a success response (i.e. to display the 'edit' template)" do
        telegram_group = TelegramGroup.create! valid_attributes
        put :update, params: { id: telegram_group.to_param, telegram_group: invalid_attributes }
        expect(response).to be_successful
      end
    end
  end

  describe 'DELETE #destroy' do
    it 'destroys the requested telegram_group' do
      telegram_group = TelegramGroup.create! valid_attributes
      expect do
        delete :destroy, params: { id: telegram_group.to_param }
      end.to change(TelegramGroup, :count).by(-1)
    end

    it 'redirects to the telegram_groups list' do
      telegram_group = TelegramGroup.create! valid_attributes
      delete :destroy, params: { id: telegram_group.to_param }
      expect(response).to redirect_to(admin_telegram_groups_url)
    end
  end
end
