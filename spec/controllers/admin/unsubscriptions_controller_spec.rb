require 'rails_helper'

describe Admin::UnsubscriptionsController do
  render_views
  before(:all) do
    create_list(:unsubscription, 2)
  end

  context 'as admin user' do
    login_admin_user

    it 'index action should render index template' do
      get :index
      response.should render_template(:index)
    end

    it 'show action should render show template' do
      get :show, params: { id: Unsubscription.first }
      response.should render_template(:show)
    end

    it 'destroy action should destroy model and redirect to index action' do
      unsubscription = Unsubscription.first
      delete :destroy, params: { id: unsubscription }
      response.should redirect_to(admin_unsubscriptions_path)
      Unsubscription.exists?(unsubscription.id).should be_falsey
    end
  end
end
