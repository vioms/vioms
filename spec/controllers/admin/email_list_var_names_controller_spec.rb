require 'rails_helper'

describe Admin::EmailListVarNamesController do
  render_views
  before(:all) do
    @email_list = create(:email_list)
    create_list(:email_list_var_name, 2, email_list: @email_list)
  end

  context 'as user with email_list_var_names role' do
    login_admin_user

    it 'index action should render index template' do
      get :index, params: { email_list_id: @email_list }
      response.should render_template(:index)
    end

    it 'new action should render new template' do
      get :new, params: { email_list_id: @email_list }
      response.should render_template(:new)
    end

    it 'create action should render new template when model is invalid' do
      expect_any_instance_of(EmailListVarName).to receive(:valid?).and_return(false)
      post :create, params: { email_list_id: @email_list, email_list_var_name: params_for(:email_list_var_name) }
      response.should render_template(:new)
    end

    it 'create action should redirect when model is valid' do
      expect_any_instance_of(EmailListVarName).to receive(:valid?).and_return(true)
      post :create, params: { email_list_id: @email_list, email_list_var_name: params_for(:email_list_var_name) }
      response.should redirect_to(admin_email_list_email_list_var_names_path(@email_list))
    end

    it 'edit action should render edit template' do
      get :edit, params: { id: EmailListVarName.first, email_list_id: @email_list }
      response.should render_template(:edit)
    end

    it 'update action should render edit template when model is invalid' do
      expect_any_instance_of(EmailListVarName).to receive(:valid?).and_return(false)
      put :update,
          params: { id: EmailListVarName.first, email_list_id: @email_list,
                    email_list_var_name: params_for(:email_list_var_name) }
      response.should render_template(:edit)
    end

    it 'update action should redirect when model is valid' do
      expect_any_instance_of(EmailListVarName).to receive(:valid?).and_return(true)
      put :update,
          params: { id: EmailListVarName.first, email_list_id: @email_list,
                    email_list_var_name: params_for(:email_list_var_name) }
      response.should redirect_to(admin_email_list_email_list_var_names_path(@email_list))
    end

    it 'destroy action should destroy model and redirect to index action' do
      email_list_var_name = EmailListVarName.first
      delete :destroy, params: { id: email_list_var_name, email_list_id: @email_list }
      response.should redirect_to(admin_email_list_email_list_var_names_path(@email_list))
      EmailListVarName.exists?(email_list_var_name.id).should be_falsey
    end
  end
end
