# frozen_string_literal: true

require 'rails_helper'

describe Admin::ImportEmailProblemsController do
  render_views

  context 'as admin user' do
    login_admin_user

    it 'new action should render new template' do
      get :new
      expect(response).to have_rendered(:new)
    end

    it 'create action should render new template when model is invalid' do
      post :create, params: { emails: 'one@example.com' }
      expect(response).to have_rendered(:new)
    end
  end
end
