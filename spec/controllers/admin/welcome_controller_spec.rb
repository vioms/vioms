# frozen_string_literal: true

require 'rails_helper'

describe Admin::WelcomeController do
  render_views

  context 'as admin user' do
    login_admin_user

    it 'index action should render index template' do
      get :index
      expect(response).to have_rendered(:index)
    end
  end

  context 'as user' do
    login_user

    it 'index action should render index template' do
      get :index
      expect(response).to have_rendered(:index)
    end
  end
end
