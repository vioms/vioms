# frozen_string_literal: true

require 'rails_helper'

describe Admin::BannerOrdersController do
  render_views
  before(:all) do
    create_list(:banner_order, 2)
  end

  context 'as admin user' do
    login_admin_user

    it 'index action should render index template' do
      get :index
      response.should render_template(:index)
    end

    it 'new action should render new template' do
      get :new
      response.should render_template(:new)
    end

    it 'create action should render new template when model is invalid' do
      expect_any_instance_of(BannerOrder).to receive(:valid?).and_return(false)
      post :create, params: { banner_order: params_for(:banner_order) }
      response.should render_template(:new)
    end

    it 'create action should redirect when model is valid' do
      expect_any_instance_of(BannerOrder).to receive(:valid?).and_return(true)
      post :create, params: { banner_order: params_for(:banner_order) }
      response.should redirect_to(admin_banner_orders_path)
    end

    it 'edit action should render edit template' do
      get :edit, params: { id: BannerOrder.first }
      response.should render_template(:edit)
    end

    it 'update action should render edit template when model is invalid' do
      expect_any_instance_of(BannerOrder).to receive(:valid?).and_return(false)
      put :update, params: { id: BannerOrder.first, banner_order: params_for(:banner_order) }
      response.should render_template(:edit)
    end

    it 'update action should redirect when model is valid' do
      expect_any_instance_of(BannerOrder).to receive(:valid?).and_return(true)
      put :update, params: { id: BannerOrder.first, banner_order: params_for(:banner_order) }
      response.should redirect_to(admin_banner_orders_path)
    end

    it 'destroy action should destroy model and redirect to index action' do
      banner_order = BannerOrder.first
      delete :destroy, params: { id: banner_order }
      response.should redirect_to(admin_banner_orders_path)
      BannerOrder.exists?(banner_order.id).should be_falsey
    end
  end
end
