require 'rails_helper'

describe Admin::UnsubscriptionReasonsController do
  render_views
  before(:all) do
    create_list(:unsubscription_reason, 2)
  end

  context 'as admin user' do
    login_admin_user

    it 'index action should render index template' do
      get :index
      response.should render_template(:index)
    end

    it 'new action should render new template' do
      get :new
      response.should render_template(:new)
    end

    it 'create action should render new template when model is invalid' do
      expect_any_instance_of(UnsubscriptionReason).to receive(:valid?).and_return(false)
      post :create, params: { unsubscription_reason: params_for(:unsubscription_reason) }
      response.should render_template(:new)
    end

    it 'create action should redirect when model is valid' do
      expect_any_instance_of(UnsubscriptionReason).to receive(:valid?).and_return(true)
      post :create, params: { unsubscription_reason: params_for(:unsubscription_reason) }
      response.should redirect_to(admin_unsubscription_reasons_path)
    end

    it 'edit action should render edit template' do
      get :edit, params: { id: UnsubscriptionReason.first }
      response.should render_template(:edit)
    end

    it 'update action should render edit template when model is invalid' do
      expect_any_instance_of(UnsubscriptionReason).to receive(:valid?).and_return(false)
      put :update, params: { id: UnsubscriptionReason.first, unsubscription_reason: params_for(:unsubscription_reason) }
      response.should render_template(:edit)
    end

    it 'update action should redirect when model is valid' do
      expect_any_instance_of(UnsubscriptionReason).to receive(:valid?).and_return(true)
      put :update, params: { id: UnsubscriptionReason.first, unsubscription_reason: params_for(:unsubscription_reason) }
      response.should redirect_to(admin_unsubscription_reasons_path)
    end

    it 'destroy action should destroy model and redirect to index action' do
      unsubscription_reason = UnsubscriptionReason.first
      delete :destroy, params: { id: unsubscription_reason }
      response.should redirect_to(admin_unsubscription_reasons_path)
      UnsubscriptionReason.exists?(unsubscription_reason.id).should be_falsey
    end
  end
end
