require 'rails_helper'

describe Admin::SubscriberImportsController do
  before(:all) do
    create_list(:subscriber_import, 2, :valid)
  end

  render_views

  context 'as admin user' do
    login_admin_user SubscriberImport

    it 'index action should render index template' do
      get :index
      response.should render_template(:index)
    end

    it 'new action should render new template' do
      get :new
      response.should render_template(:new)
    end

    it 'create action should render new template when model is invalid' do
      expect_any_instance_of(SubscriberImport).to receive(:valid?).and_return(false)
      post :create, params: { subscriber_import: params_for(:subscriber_import, {}, { subscribers_data: 'name' }) }
      response.should render_template(:new)
    end

    it 'create action should redirect when model is valid' do
      expect_any_instance_of(SubscriberImport).to receive(:valid?).and_return(true)
      post :create, params: { subscriber_import: params_for(:subscriber_import, {}, { subscribers_data: 'name' }) }
      response.should redirect_to(admin_subscriber_imports_path)
    end

    it 'edit action should render edit template' do
      get :edit, params: { id: SubscriberImport.first }
      response.should render_template(:edit)
    end

    it 'update action should redirect' do
      put :update, params: { id: SubscriberImport.first, subscriber_import: params_for(
        :subscriber_import, {}, { subscribers_data: 'name' }
      ) }
      response.should redirect_to(admin_subscriber_imports_path)
    end

    it 'destroy action should destroy model and redirect to index action' do
      import = SubscriberImport.first
      delete :destroy, params: { id: import }
      response.should redirect_to(admin_subscriber_imports_path)
      SubscriberImport.exists?(import.id).should be_falsey
    end
  end
end
