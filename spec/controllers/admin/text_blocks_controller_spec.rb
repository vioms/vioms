# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Admin::TextBlocksController do
  let(:valid_attributes) do
    attributes_for(:text_block)
  end

  login_admin_user
  render_views

  describe 'GET #index' do
    it 'returns a success response' do
      TextBlock.create! valid_attributes
      get :index, params: {}
      expect(response).to be_successful
    end
  end

  describe 'GET #edit' do
    it 'returns a success response' do
      text_block = TextBlock.create! valid_attributes
      get :edit, params: { id: text_block.to_param }
      expect(response).to be_successful
    end
  end

  describe 'PUT #update' do
    context 'with valid params' do
      it 'redirects to the text_block' do
        text_block = TextBlock.create! valid_attributes
        put :update, params: { id: text_block.to_param, text_block: valid_attributes }
        expect(response).to redirect_to(admin_text_blocks_url)
      end
    end
  end
end
