# frozen_string_literal: true

require 'rails_helper'

describe Admin::SmsSendHistoriesController do
  render_views
  before(:all) do
    create_list(:sms_send_history, 2)
    create_sms_send_history_types
    create_sms_messages
  end

  context 'as admin user' do
    login_admin_user

    it 'index action should render index template' do
      get :index
      expect(response).to render_template(:index)
    end

    it 'destroy action should destroy model and redirect to index action' do
      manage_sms_send_history = SmsSendHistory.first
      delete :destroy, params: { id: manage_sms_send_history }
      expect(response).to redirect_to(admin_sms_send_histories_url)
      expect(SmsSendHistory.exists?(manage_sms_send_history.id)).to be(false)
    end
  end
end
