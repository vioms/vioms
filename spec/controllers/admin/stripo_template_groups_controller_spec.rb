# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Admin::StripoTemplateGroupsController do
  let(:valid_attributes) do
    attributes_for(:stripo_template_group)
  end

  let(:invalid_attributes) do
    attributes_for(:stripo_template_group, :invalid)
  end

  login_admin_user
  render_views

  describe 'GET #index' do
    it 'returns a success response' do
      Stripo::TemplateGroup.create! valid_attributes
      get :index, params: {}
      expect(response).to be_successful
    end
  end

  describe 'GET #new' do
    it 'returns a success response' do
      get :new, params: {}
      expect(response).to be_successful
    end
  end

  describe 'GET #edit' do
    it 'returns a success response' do
      stripo_template_group = Stripo::TemplateGroup.create! valid_attributes
      get :edit, params: { id: stripo_template_group.to_param }
      expect(response).to be_successful
    end
  end

  describe 'POST #create' do
    context 'with valid params' do
      it 'creates a new Stripo::TemplateGroup' do
        expect do
          post :create, params: { stripo_template_group: valid_attributes }
        end.to change(Stripo::TemplateGroup, :count).by(1)
      end

      it 'redirects to the created stripo_template_group' do
        post :create, params: { stripo_template_group: valid_attributes }
        expect(response).to redirect_to(admin_stripo_template_groups_url)
      end
    end

    context 'with invalid params' do
      it "returns a success response (i.e. to display the 'new' template)" do
        post :create, params: { stripo_template_group: invalid_attributes }
        expect(response).to be_successful
      end
    end
  end

  describe 'PUT #update' do
    context 'with valid params' do
      it 'redirects to the stripo_template_group' do
        stripo_template_group = Stripo::TemplateGroup.create! valid_attributes
        put :update, params: { id: stripo_template_group.to_param, stripo_template_group: valid_attributes }
        expect(response).to redirect_to(admin_stripo_template_groups_url)
      end
    end

    context 'with invalid params' do
      it "returns a success response (i.e. to display the 'edit' template)" do
        stripo_template_group = Stripo::TemplateGroup.create! valid_attributes
        put :update, params: { id: stripo_template_group.to_param, stripo_template_group: invalid_attributes }
        expect(response).to be_successful
      end
    end
  end

  describe 'DELETE #destroy' do
    it 'destroys the requested stripo_template_group' do
      stripo_template_group = Stripo::TemplateGroup.create! valid_attributes
      expect do
        delete :destroy, params: { id: stripo_template_group.to_param }
      end.to change(Stripo::TemplateGroup, :count).by(-1)
    end

    it 'redirects to the stripo_template_groups list' do
      stripo_template_group = Stripo::TemplateGroup.create! valid_attributes
      delete :destroy, params: { id: stripo_template_group.to_param }
      expect(response).to redirect_to(admin_stripo_template_groups_url)
    end
  end
end
