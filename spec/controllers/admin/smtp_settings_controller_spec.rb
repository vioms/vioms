require 'rails_helper'

describe Admin::SmtpSettingsController do
  render_views
  before(:all) do
    @smtp_setting_group = create(:smtp_setting_group)
    create_list(:smtp_setting, 2, smtp_setting_group: @smtp_setting_group)
  end

  login_admin_user

  it 'index action should render index template' do
    get :index, params: { smtp_setting_group_id: @smtp_setting_group }
    response.should render_template(:index)
  end

  it 'new action should render new template' do
    get :new, params: { smtp_setting_group_id: @smtp_setting_group }
    response.should render_template(:new)
  end

  it 'create action should render new template when model is invalid' do
    expect_any_instance_of(SmtpSetting).to receive(:valid?).and_return(false)
    post :create, params: { smtp_setting_group_id: @smtp_setting_group, smtp_setting: params_for(:smtp_setting) }
    response.should render_template(:new)
  end

  it 'create action should redirect when model is valid' do
    expect_any_instance_of(SmtpSetting).to receive(:valid?).and_return(true)
    post :create, params: { smtp_setting_group_id: @smtp_setting_group, smtp_setting: params_for(:smtp_setting) }
    response.should redirect_to(admin_smtp_setting_group_smtp_settings_path(@smtp_setting_group))
  end

  it 'edit action should render edit template' do
    get :edit, params: { id: SmtpSetting.first, smtp_setting_group_id: @smtp_setting_group }
    response.should render_template(:edit)
  end

  it 'update action should render edit template when model is invalid' do
    expect_any_instance_of(SmtpSetting).to receive(:valid?).and_return(false)
    put :update,
        params: { id: SmtpSetting.first, smtp_setting_group_id: @smtp_setting_group,
                  smtp_setting: params_for(:smtp_setting) }
    response.should render_template(:edit)
  end

  it 'update action should redirect when model is valid' do
    expect_any_instance_of(SmtpSetting).to receive(:valid?).and_return(true)
    put :update,
        params: { id: SmtpSetting.first, smtp_setting_group_id: @smtp_setting_group,
                  smtp_setting: params_for(:smtp_setting) }
    response.should redirect_to(admin_smtp_setting_group_smtp_settings_path(@smtp_setting_group))
  end

  it 'destroy action should destroy model and redirect to index action' do
    custom_var = SmtpSetting.first
    delete :destroy, params: { id: custom_var, smtp_setting_group_id: @smtp_setting_group }
    response.should redirect_to(admin_smtp_setting_group_smtp_settings_path(@smtp_setting_group))
    SmtpSetting.exists?(custom_var.id).should be_falsey
  end
end
