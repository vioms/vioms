# frozen_string_literal: true

require 'rails_helper'

describe Admin::SmsMessagesController do
  render_views
  before(:all) do
    create_list(:sms_message, 2)
  end

  context 'as admin user' do
    login_admin_user

    it 'index action should render index template' do
      get :index
      expect(response).to have_rendered(:index)
    end

    it 'edit action should render edit template' do
      get :edit, params: { id: SmsMessage.first }
      expect(response).to have_rendered(:edit)
    end

    it 'update action should render edit template when model is invalid' do
      expect_any_instance_of(SmsMessage).to receive(:valid?).and_return(false)
      put :update, params: { id: SmsMessage.first, sms_message: params_for(:sms_message) }
      expect(response).to have_rendered(:edit)
    end

    it 'update action should redirect when model is valid' do
      expect_any_instance_of(SmsMessage).to receive(:valid?).and_return(true)
      put :update, params: { id: SmsMessage.first, sms_message: params_for(:sms_message) }
      expect(response).to redirect_to(admin_sms_messages_url)
    end
  end
end
