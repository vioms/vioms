require 'rails_helper'

describe Admin::EmailListCustomVarsController do
  render_views
  before(:all) do
    @email_list = create(:email_list)
    create_list(:email_list_custom_var, 2, email_list: @email_list)
  end

  login_admin_user

  it 'index action should render index template' do
    get :index, params: { email_list_id: @email_list }
    response.should render_template(:index)
  end

  it 'new action should render new template' do
    get :new, params: { email_list_id: @email_list }
    response.should render_template(:new)
  end

  it 'create action should render new template when model is invalid' do
    expect_any_instance_of(EmailListCustomVar).to receive(:valid?).and_return(false)
    post :create, params: { email_list_id: @email_list, email_list_custom_var: params_for(:email_list_custom_var) }
    response.should render_template(:new)
  end

  it 'create action should redirect when model is valid' do
    expect_any_instance_of(EmailListCustomVar).to receive(:valid?).and_return(true)
    post :create, params: { email_list_id: @email_list, email_list_custom_var: params_for(:email_list_custom_var) }
    response.should redirect_to(admin_email_list_email_list_custom_vars_url(@email_list))
  end

  it 'edit action should render edit template' do
    get :edit, params: { id: EmailListCustomVar.first, email_list_id: @email_list }
    response.should render_template(:edit)
  end

  it 'update action should render edit template when model is invalid' do
    expect_any_instance_of(EmailListCustomVar).to receive(:valid?).and_return(false)
    put :update,
        params: { id: EmailListCustomVar.first, email_list_id: @email_list,
                  email_list_custom_var: params_for(:email_list_custom_var) }
    response.should render_template(:edit)
  end

  it 'update action should redirect when model is valid' do
    expect_any_instance_of(EmailListCustomVar).to receive(:valid?).and_return(true)
    put :update,
        params: { id: EmailListCustomVar.first, email_list_id: @email_list,
                  email_list_custom_var: params_for(:email_list_custom_var) }
    response.should redirect_to(admin_email_list_email_list_custom_vars_url(@email_list))
  end

  it 'destroy action should destroy model and redirect to index action' do
    custom_var = EmailListCustomVar.first
    delete :destroy, params: { id: custom_var, email_list_id: @email_list }
    response.should redirect_to(admin_email_list_email_list_custom_vars_url(@email_list))
    EmailListCustomVar.exists?(custom_var.id).should be_falsey
  end
end
