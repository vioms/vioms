require 'rails_helper'

describe Admin::NewsItemsController do
  render_views
  before(:all) do
    create_list(:news_item, 2)
  end

  context 'as admin user' do
    login_admin_user

    it 'index action should render index template' do
      get :index
      response.should render_template(:index)
    end

    it 'new action should render new template' do
      get :new
      response.should render_template(:new)
    end

    it 'create action should render new template when model is invalid' do
      expect_any_instance_of(NewsItem).to receive(:valid?).and_return(false)
      post :create, params: { news_item: params_for(:news_item) }
      response.should render_template(:new)
    end

    it 'create action should redirect when model is valid' do
      expect_any_instance_of(NewsItem).to receive(:valid?).and_return(true)
      post :create, params: { news_item: params_for(:news_item) }
      response.should redirect_to(admin_news_items_path)
    end

    it 'edit action should render edit template' do
      get :edit, params: { id: NewsItem.first }
      response.should render_template(:edit)
    end

    it 'update action should render edit template when model is invalid' do
      expect_any_instance_of(NewsItem).to receive(:valid?).and_return(false)
      put :update, params: { id: NewsItem.first, news_item: params_for(:news_item) }
      response.should render_template(:edit)
    end

    it 'update action should redirect when model is valid' do
      expect_any_instance_of(NewsItem).to receive(:valid?).and_return(true)
      params_for = params_for(:news_item)
      put :update, params: { id: NewsItem.first, news_item: params_for }
      response.should redirect_to(admin_news_items_path)
    end

    it 'destroy action should destroy model and redirect to index action' do
      news_item = NewsItem.first
      delete :destroy, params: { id: news_item }
      response.should redirect_to(admin_news_items_path)
      NewsItem.exists?(news_item.id).should be_falsey
    end
  end
end
