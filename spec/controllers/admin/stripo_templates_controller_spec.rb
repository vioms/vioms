# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Admin::StripoTemplatesController do
  let(:valid_attributes) { build(:stripo_template, template_group: stripo_template_group).attributes }
  let(:invalid_attributes) { attributes_for(:stripo_template, :invalid) }
  let(:stripo_template_group) { create(:stripo_template_group) }
  let!(:stripo_template) { create(:stripo_template) }

  login_admin_user
  render_views

  describe 'GET #index' do
    it 'returns a success response' do
      get :index, params: { stripo_template_group_id: stripo_template_group }
      expect(response).to be_successful
    end
  end

  describe 'GET #new' do
    it 'returns a success response' do
      get :new, params: { stripo_template_group_id: stripo_template_group }
      expect(response).to be_successful
    end
  end

  describe 'GET #edit' do
    it 'returns a success response' do
      get :edit, params: { id: stripo_template.to_param, stripo_template_group_id: stripo_template_group }
      expect(response).to be_successful
    end
  end

  describe 'GET #edit_stripo' do
    it 'returns a success response' do
      get :edit_stripo, params: { id: stripo_template.to_param, stripo_template_group_id: stripo_template_group }
      expect(response).to be_successful
    end
  end

  describe 'POST #create' do
    before do
      allow(Setting).to receive('get_setting').with('stripo_app_id').and_return('app_id')
    end

    context 'with valid params' do
      it 'creates a new Stripo::Template' do
        expect do
          post :create, params: { stripo_template: valid_attributes, stripo_template_group_id: stripo_template_group }
        end.to change(Stripo::Template, :count).by(1)
      end

      it 'redirects to the created stripo_template' do
        post :create, params: { stripo_template: valid_attributes, stripo_template_group_id: stripo_template_group }
        expect(response).to redirect_to(admin_stripo_template_group_stripo_templates_url(stripo_template_group))
      end
    end

    context 'with invalid params' do
      it "returns a success response (i.e. to display the 'new' template)" do
        post :create, params: { stripo_template: invalid_attributes, stripo_template_group_id: stripo_template_group }
        expect(response).to be_successful
      end
    end
  end

  describe 'POST #create_from_mailing' do
    before do
      allow(Setting).to receive('get_setting').with('stripo_app_id').and_return('app_id')
    end

    let(:mailing) { create(:mailing, :for_stripo) }

    context 'with valid params' do
      let(:params) { valid_attributes.merge(mailing_id: mailing.id) }

      it 'creates a new Stripo::Template' do
        expect do
          post :create_from_mailing, params: { stripo_template: params,
                                               stripo_template_group_id: stripo_template_group }
        end.to change(Stripo::Template, :count).by(1)
      end

      it 'redirects to the created stripo_template' do
        post :create_from_mailing, params: { stripo_template: params, stripo_template_group_id: stripo_template_group }
        expect(response).to redirect_to(admin_stripo_template_group_stripo_templates_url(stripo_template_group))
      end
    end

    context 'with invalid params' do
      let(:params) { invalid_attributes.merge(mailing_id: mailing.id) }

      it "returns a success response (i.e. to display the 'new' template)" do
        post :create_from_mailing, params: { stripo_template: params, stripo_template_group_id: stripo_template_group }
        expect(response).to be_successful
      end
    end
  end

  describe 'PUT #update' do
    context 'with valid params' do
      it 'redirects to the stripo_template' do
        put :update, params: { id: stripo_template.to_param, stripo_template: valid_attributes,
                               stripo_template_group_id: stripo_template_group }
        expect(response).to redirect_to(admin_stripo_template_group_stripo_templates_url(stripo_template_group))
      end
    end

    context 'with invalid params' do
      it "returns a success response (i.e. to display the 'edit' template)" do
        put :update, params: { id: stripo_template.to_param, stripo_template: invalid_attributes,
                               stripo_template_group_id: stripo_template_group }
        expect(response).to be_successful
      end
    end
  end

  describe 'DELETE #destroy' do
    it 'destroys the requested stripo_template' do
      expect do
        delete :destroy, params: { id: stripo_template.to_param, stripo_template_group_id: stripo_template_group }
      end.to change(Stripo::Template, :count).by(-1)
    end

    it 'redirects to the stripo_templates list' do
      delete :destroy, params: { id: stripo_template.to_param, stripo_template_group_id: stripo_template_group }
      expect(response).to redirect_to(admin_stripo_template_group_stripo_templates_url(stripo_template_group))
    end
  end
end
