# frozen_string_literal: true

require 'rails_helper'

describe Admin::SubscriberImportPeopleController do
  before(:all) do
    @subscriber_import = create(:subscriber_import, :valid)
    create_list(:subscriber_import_person, 2, subscriber_import: @subscriber_import, error_id: 1)
  end

  render_views

  context 'as admin user' do
    login_admin_user

    it 'index action should render index template' do
      get :index, params: { subscriber_import_id: @subscriber_import }
      expect(response).to have_rendered(:index)
    end

    it 'edit action should render edit template' do
      get :edit, params: { id: SubscriberImportPerson.first, subscriber_import_id: @subscriber_import }
      expect(response).to have_rendered(:edit)
    end

    it 'update action should redirect' do
      put :update, params: { id: SubscriberImportPerson.first, subscriber_import_id: @subscriber_import,
                             subscriber_import_person: params_for(:subscriber_import_person) }
      response.should redirect_to(admin_subscriber_import_subscriber_import_people_url(@subscriber_import))
    end
  end
end
