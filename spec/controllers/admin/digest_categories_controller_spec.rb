require 'rails_helper'

describe Admin::DigestCategoriesController do
  render_views
  before(:all) do
    create_list(:digest_category, 2)
  end

  context 'as admin user' do
    login_admin_user

    it 'index action should render index template' do
      get :index
      response.should render_template(:index)
    end

    it 'new action should render new template' do
      get :new
      response.should render_template(:new)
    end

    it 'create action should render new template when model is invalid' do
      expect_any_instance_of(DigestCategory).to receive(:valid?).and_return(false)
      post :create, params: { digest_category: params_for(:digest_category) }
      response.should render_template(:new)
    end

    it 'create action should redirect when model is valid' do
      expect_any_instance_of(DigestCategory).to receive(:valid?).and_return(true)
      post :create, params: { digest_category: params_for(:digest_category) }
      response.should redirect_to(admin_digest_categories_path(email_list: DigestCategory.reorder(:id).reload.last.email_list_id))
    end

    it 'edit action should render edit template' do
      get :edit, params: { id: DigestCategory.first }
      response.should render_template(:edit)
    end

    it 'update action should render edit template when model is invalid' do
      expect_any_instance_of(DigestCategory).to receive(:valid?).and_return(false)
      put :update, params: { id: DigestCategory.first, digest_category: params_for(:digest_category) }
      response.should render_template(:edit)
    end

    it 'update action should redirect when model is valid' do
      expect_any_instance_of(DigestCategory).to receive(:valid?).and_return(true)
      params_for = params_for(:digest_category)
      put :update, params: { id: DigestCategory.first, digest_category: params_for }
      response.should redirect_to(admin_digest_categories_path(email_list: params_for[:email_list_id]))
    end

    it 'destroy action should destroy model and redirect to index action' do
      digest_category = DigestCategory.first
      email_list_id = digest_category.email_list_id
      delete :destroy, params: { id: digest_category }
      response.should redirect_to(admin_digest_categories_path(email_list: email_list_id))
      DigestCategory.exists?(digest_category.id).should be_falsey
    end
  end
end
