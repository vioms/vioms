# frozen_string_literal: true

require 'rails_helper'

describe Admin::BannersController do
  before(:all) do
    @banner_order = create(:banner_order)
    create_list(:banner, 2, banner_order: @banner_order)
  end

  render_views

  context 'as admin user' do
    login_admin_user

    it 'index action should render index template' do
      get :index, params: { banner_order_id: @banner_order }
      expect(response).to have_rendered(:index)
    end

    it 'show action should render show template' do
      get :show, params: { id: Banner.first, banner_order_id: @banner_order }
      expect(response).to have_rendered(:show)
    end

    it 'new action should render new template' do
      get :new, params: { banner_order_id: @banner_order }
      expect(response).to have_rendered(:new)
    end

    it 'create action should render new template when model is invalid' do
      expect_any_instance_of(Banner).to receive(:valid?).and_return(false)
      post :create, params: { banner_order_id: @banner_order, banner: params_for(:banner) }
      expect(response).to have_rendered(:new)
    end

    it 'create action should redirect when model is valid' do
      expect_any_instance_of(Banner).to receive(:valid?).and_return(true)
      post :create, params: { banner_order_id: @banner_order, banner: params_for(:banner) }
      response.should redirect_to(admin_banner_order_banners_url(@banner_order))
    end

    it 'edit action should render edit template' do
      get :edit, params: { id: Banner.first, banner_order_id: @banner_order }
      expect(response).to have_rendered(:edit)
    end

    it 'update action should render edit template when model is invalid' do
      expect_any_instance_of(Banner).to receive(:valid?).and_return(false)
      put :update, params: { id: Banner.first, banner_order_id: @banner_order, banner: params_for(:banner) }
      expect(response).to have_rendered(:edit)
    end

    it 'update action should redirect when model is valid' do
      expect_any_instance_of(Banner).to receive(:valid?).and_return(true)
      put :update, params: { id: Banner.first, banner_order_id: @banner_order, banner: params_for(:banner) }
      expect(response).to redirect_to(admin_banner_order_banners_url(@banner_order))
    end

    it 'destroy action should destroy model and redirect to index action' do
      banner = Banner.first
      banner_order = banner.banner_order
      delete :destroy, params: { id: banner, banner_order_id: banner_order }
      expect(response).to redirect_to(admin_banner_order_banners_url(banner_order))
      expect(Banner.exists?(banner.id)).to be(false)
    end
  end
end
