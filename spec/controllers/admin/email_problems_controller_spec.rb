require 'rails_helper'

describe Admin::EmailProblemsController do
  render_views
  before(:all) do
    @subscriber = create(:subscriber_with_email)
    create_list(:email_problem, 2, subscriber: @subscriber)
  end

  login_admin_user

  it 'index action should render index template' do
    get :index, params: { subscriber_id: @subscriber }
    response.should render_template(:index)
  end

  it 'new action should render new template' do
    get :new, params: { subscriber_id: @subscriber }
    response.should render_template(:new)
  end

  it 'create action should render new template when model is invalid' do
    expect_any_instance_of(EmailProblem).to receive(:valid?).and_return(false)
    post :create, params: { subscriber_id: @subscriber, email_problem: params_for(:email_problem) }
    response.should render_template(:new)
  end

  it 'create action should redirect when model is valid' do
    expect_any_instance_of(EmailProblem).to receive(:valid?).and_return(true)
    post :create, params: { subscriber_id: @subscriber, email_problem: params_for(:email_problem) }
    response.should redirect_to(admin_subscriber_email_problems_path(@subscriber))
  end

  it 'edit action should render edit template' do
    get :edit, params: { id: EmailProblem.first, subscriber_id: @subscriber }
    response.should render_template(:edit)
  end

  it 'update action should render edit template when model is invalid' do
    expect_any_instance_of(EmailProblem).to receive(:valid?).and_return(false)
    put :update,
        params: { id: EmailProblem.first, subscriber_id: @subscriber, email_problem: params_for(:email_problem) }
    response.should render_template(:edit)
  end

  it 'update action should redirect when model is valid' do
    expect_any_instance_of(EmailProblem).to receive(:valid?).and_return(true)
    put :update,
        params: { id: EmailProblem.first, subscriber_id: @subscriber, email_problem: params_for(:email_problem) }
    response.should redirect_to(admin_subscriber_email_problems_path(@subscriber))
  end
end
