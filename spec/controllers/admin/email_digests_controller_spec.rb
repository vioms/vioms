# frozen_string_literal: true

require 'rails_helper'

describe Admin::EmailDigestsController do
  render_views
  before_all do
    create_list(:email_digest, 2)
    create(:email_digest, :published)
  end

  context 'as admin user' do
    login_admin_user

    it 'index action should render index template' do
      get :index
      expect(response).to render_template(:index)
    end

    it 'show action should render show template' do
      get :show, params: { id: EmailDigest.first }
      expect(response.body).to be_present
    end

    it 'new action should render new template' do
      get :new
      expect(response).to render_template(:new)
    end

    it 'create action should render new template when model is invalid' do
      expect_any_instance_of(EmailDigest).to receive(:valid?).and_return(false)
      post :create, params: { email_digest: params_for(:email_digest) }
      expect(response).to render_template(:new)
    end

    it 'create action should redirect when model is valid' do
      # Don't stub here because year is filled in before_validation
      # expect_any_instance_of(EmailDigest).to receive(:valid?).and_return(true)
      post :create, params: { email_digest: params_for(:email_digest) }
      expect(response).to redirect_to(admin_email_digests_url)
    end

    it 'edit action should render edit template' do
      get :edit, params: { id: EmailDigest.first }
      expect(response).to render_template(:edit)
    end

    it 'update action should render edit template when model is invalid' do
      expect_any_instance_of(EmailDigest).to receive(:valid?).and_return(false)
      put :update, params: { id: EmailDigest.first, email_digest: params_for(:email_digest) }
      expect(response).to render_template(:edit)
    end

    it 'update action should redirect when model is valid' do
      expect_any_instance_of(EmailDigest).to receive(:valid?).and_return(true)
      put :update, params: { id: EmailDigest.first, email_digest: params_for(:email_digest) }
      expect(response).to redirect_to(admin_email_digests_url)
    end

    it 'send_digests' do
      put :send_digest, params: { id: EmailDigest.first }
      expect(response).to redirect_to(admin_email_digests_url)
    end

    it 'cancel_send_digests' do
      email_digest = EmailDigest.first
      email_digest.create_email_digest_bg_job!
      put :cancel_sending, params: { id: email_digest }
      email_digest.reload.email_digest_bg_job.should be_nil
      expect(response).to redirect_to(admin_email_digests_url)
    end

    it 'destroy action should destroy model and redirect to index action' do
      email_digest = EmailDigest.first
      delete :destroy, params: { id: email_digest }
      expect(response).to redirect_to(admin_email_digests_url)
      expect(EmailDigest.exists?(email_digest.id)).to be(false)
    end
  end
end
