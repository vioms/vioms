# frozen_string_literal: true

require 'rails_helper'

describe Admin::UsersController do
  render_views
  before(:all) do
    create_list(:user, 2)
  end

  describe 'as admin user' do
    login_admin_user

    it 'index action should render index template' do
      get :index
      expect(response).to render_template(:index)
    end

    it 'show action should render show template' do
      get :show, params: { id: User.first }
      expect(response).to render_template(:show)
    end

    it 'new action should render new template' do
      get :new
      expect(response).to render_template(:new)
    end

    it 'create action should render new template when model is invalid' do
      expect_any_instance_of(User).to receive(:valid?).and_return(false)
      post :create, params: { user: params_for(:user) }
      expect(response).to render_template(:new)
    end

    it 'create action should redirect when model is valid' do
      expect_any_instance_of(User).to receive(:valid?).and_return(true)
      post :create, params: { user: params_for(:user) }
      expect(response).to redirect_to(admin_users_url)
    end

    it 'edit action should render edit template' do
      get :edit, params: { id: User.first }
      expect(response).to render_template(:edit)
    end

    it 'update action should render edit template when model is invalid' do
      expect_any_instance_of(User).to receive(:valid?).and_return(false)
      put :update, params: { id: User.first, user: params_for(:user) }
      expect(response).to render_template(:edit)
    end

    it 'update action should redirect when model is valid' do
      expect_any_instance_of(User).to receive(:valid?).and_return(true)
      put :update, params: { id: User.first, user: params_for(:user) }
      expect(response).to redirect_to(admin_users_url)
    end

    it 'destroy action should destroy model and redirect to index action' do
      user = User.first
      delete :destroy, params: { id: user }
      expect(response).to redirect_to(admin_users_url)
      expect(User.exists?(user.id)).to be(false)
    end
  end
end
