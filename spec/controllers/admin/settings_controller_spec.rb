# frozen_string_literal: true

require 'rails_helper'

describe Admin::SettingsController do
  before(:all) { create_list(:setting, 2) }

  render_views

  context 'as admin user' do
    login_admin_user

    it 'index action should render index template' do
      get :index
      expect(response).to have_rendered(:index)
    end

    it 'show action should render show template' do
      get :show, params: { id: Setting.first.name }, xhr: true
      expect(response).to have_rendered(:show)
    end

    it 'edit action should render edit template' do
      get :edit, params: { id: Setting.first, setting: params_for(:setting) }
      expect(response).to have_rendered(:edit)
    end

    it 'update action should render edit template when model is invalid' do
      expect_any_instance_of(Setting).to receive(:valid?).and_return(false)
      put :update, params: { id: Setting.first, setting: params_for(:setting) }
      expect(response).to have_rendered(:edit)
    end

    it 'update action should redirect when model is valid' do
      expect_any_instance_of(Setting).to receive(:valid?).and_return(true)
      put :update, params: { id: Setting.first, setting: params_for(:setting) }
      expect(response).to redirect_to(admin_settings_url)
    end
  end
end
