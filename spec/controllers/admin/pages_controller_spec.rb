# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Admin::PagesController do
  let(:valid_attributes) do
    build(:page).attributes
  end

  let(:invalid_attributes) do
    attributes_for(:page, :invalid)
  end

  let(:page) { create(:page) }

  login_admin_user
  render_views

  describe 'GET #index' do
    it 'returns a success response' do
      Page.create! valid_attributes
      get :index, params: {}
      expect(response).to be_successful
    end
  end

  describe 'GET #new' do
    it 'returns a success response' do
      get :new, params: {}
      expect(response).to be_successful
    end
  end

  describe 'GET #edit' do
    it 'returns a success response' do
      get :edit, params: { id: page.to_param }
      expect(response).to be_successful
    end
  end

  describe 'POST #create' do
    context 'with valid params' do
      it 'creates a new Page' do
        expect do
          post :create, params: { page: valid_attributes }
        end.to change(Page, :count).by(1)
      end

      it 'redirects to the created page' do
        post :create, params: { page: valid_attributes }
        expect(response).to redirect_to(admin_pages_url)
      end
    end

    context 'with invalid params' do
      it "returns a success response (i.e. to display the 'new' template)" do
        post :create, params: { page: invalid_attributes }
        expect(response).to be_successful
      end
    end
  end

  describe 'PUT #update' do
    context 'with valid params' do
      it 'redirects to the page' do
        put :update, params: { id: page.to_param, page: valid_attributes }
        expect(response).to redirect_to(admin_pages_url)
      end
    end

    context 'with invalid params' do
      it "returns a success response (i.e. to display the 'edit' template)" do
        put :update, params: { id: page.to_param, page: invalid_attributes }
        expect(response).to be_successful
      end
    end
  end

  describe 'DELETE #destroy' do
    it 'destroys the requested page' do
      page = Page.create! valid_attributes
      expect do
        delete :destroy, params: { id: page.to_param }
      end.to change(Page, :count).by(-1)
    end

    it 'redirects to the pages list' do
      delete :destroy, params: { id: page.to_param }
      expect(response).to redirect_to(admin_pages_url)
    end
  end
end
