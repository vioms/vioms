require 'rails_helper'

describe Admin::EmailProblemNamesController do
  render_views
  before(:all) { create_list(:email_problem_name, 2) }

  context 'as admin user' do
    login_admin_user

    it 'index action should render index template' do
      get :index
      response.should render_template(:index)
    end

    it 'new action should render new template' do
      get :new
      response.should render_template(:new)
    end

    it 'create action should render new template when model is invalid' do
      expect_any_instance_of(EmailProblemName).to receive(:valid?).and_return(false)
      post :create, params: { email_problem_name: params_for(:email_problem_name) }
      response.should render_template(:new)
    end

    it 'create action should redirect when model is valid' do
      expect_any_instance_of(EmailProblemName).to receive(:valid?).and_return(true)
      post :create, params: { email_problem_name: params_for(:email_problem_name) }
      response.should redirect_to(admin_email_problem_names_path)
    end

    it 'edit action should render edit template' do
      get :edit, params: { id: EmailProblemName.first }
      response.should render_template(:edit)
    end

    it 'update action should render edit template when model is invalid' do
      expect_any_instance_of(EmailProblemName).to receive(:valid?).and_return(false)
      put :update, params: { id: EmailProblemName.first, email_problem_name: params_for(:email_problem_name) }
      response.should render_template(:edit)
    end

    it 'update action should redirect when model is valid' do
      expect_any_instance_of(EmailProblemName).to receive(:valid?).and_return(true)
      put :update, params: { id: EmailProblemName.first, email_problem_name: params_for(:email_problem_name) }
      response.should redirect_to(admin_email_problem_names_path)
    end

    it 'destroy action should destroy model and redirect to index action' do
      email_problem_name = EmailProblemName.first
      delete :destroy, params: { id: email_problem_name }
      response.should redirect_to(admin_email_problem_names_path)
      EmailProblemName.exists?(email_problem_name.id).should be_falsey
    end
  end
end
