require 'rails_helper'

describe Admin::BannerOwnersController do
  render_views
  before(:all) do
    create_list(:banner_owner, 2)
  end

  context 'as admin user' do
    login_admin_user

    it 'index action should render index template' do
      get :index
      response.should render_template(:index)
    end

    it 'new action should render new template' do
      get :new
      response.should render_template(:new)
    end

    it 'create action should render new template when model is invalid' do
      expect_any_instance_of(BannerOwner).to receive(:valid?).and_return(false)
      post :create, params: { banner_owner: params_for(:banner_owner) }
      response.should render_template(:new)
    end

    it 'create action should redirect when model is valid' do
      expect_any_instance_of(BannerOwner).to receive(:valid?).and_return(true)
      post :create, params: { banner_owner: params_for(:banner_owner) }
      response.should redirect_to(admin_banner_owners_path)
    end

    it 'edit action should render edit template' do
      get :edit, params: { id: BannerOwner.first }
      response.should render_template(:edit)
    end

    it 'update action should render edit template when model is invalid' do
      expect_any_instance_of(BannerOwner).to receive(:valid?).and_return(false)
      put :update, params: { id: BannerOwner.first, banner_owner: params_for(:banner_owner) }
      response.should render_template(:edit)
    end

    it 'update action should redirect when model is valid' do
      expect_any_instance_of(BannerOwner).to receive(:valid?).and_return(true)
      put :update, params: { id: BannerOwner.first, banner_owner: params_for(:banner_owner) }
      response.should redirect_to(admin_banner_owners_path)
    end

    it 'destroy action should destroy model and redirect to index action' do
      banner_owner = BannerOwner.first
      delete :destroy, params: { id: banner_owner }
      response.should redirect_to(admin_banner_owners_path)
      BannerOwner.exists?(banner_owner.id).should be_falsey
    end
  end
end
