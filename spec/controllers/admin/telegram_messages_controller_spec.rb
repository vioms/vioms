# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Admin::TelegramMessagesController do
  let(:valid_attributes) do
    build(:telegram_message).attributes
  end

  let(:invalid_attributes) do
    attributes_for(:telegram_message, :invalid)
  end

  let(:telegram_message) { create(:telegram_message) }

  login_admin_user
  render_views

  describe 'GET #index' do
    it 'returns a success response' do
      create(:telegram_message)
      create(:telegram_message, :published)
      get :index, params: {}
      expect(response).to be_successful
    end
  end

  describe 'GET #show' do
    it 'returns a success response' do
      TelegramMessage.create! valid_attributes
      get :show, params: { id: telegram_message.to_param }
      expect(response).to be_successful
    end
  end

  describe 'GET #new' do
    it 'returns a success response' do
      get :new, params: {}
      expect(response).to be_successful
    end
  end

  describe 'GET #edit' do
    it 'returns a success response' do
      get :edit, params: { id: telegram_message.to_param }
      expect(response).to be_successful
    end
  end

  describe 'POST #create' do
    context 'with valid params' do
      it 'creates a new TelegramMessage' do
        expect do
          post :create, params: { telegram_message: valid_attributes }
        end.to change(TelegramMessage, :count).by(1)
      end

      it 'redirects to the created telegram_message' do
        post :create, params: { telegram_message: valid_attributes }
        expect(response).to redirect_to(admin_telegram_messages_url)
      end
    end

    context 'with invalid params' do
      it "returns a success response (i.e. to display the 'new' template)" do
        post :create, params: { telegram_message: invalid_attributes }
        expect(response).to be_successful
      end
    end
  end

  describe 'PUT #update' do
    context 'with valid params' do
      it 'redirects to the telegram_message' do
        put :update, params: { id: telegram_message.to_param, telegram_message: valid_attributes }
        expect(response).to redirect_to(admin_telegram_messages_url)
      end
    end

    context 'with invalid params' do
      it "returns a success response (i.e. to display the 'edit' template)" do
        put :update, params: { id: telegram_message.to_param, telegram_message: invalid_attributes }
        expect(response).to be_successful
      end
    end
  end

  describe 'DELETE #destroy' do
    it 'destroys the requested telegram_message' do
      telegram_message = TelegramMessage.create! valid_attributes
      expect do
        delete :destroy, params: { id: telegram_message.to_param }
      end.to change(TelegramMessage, :count).by(-1)
    end

    it 'redirects to the telegram_messages list' do
      delete :destroy, params: { id: telegram_message.to_param }
      expect(response).to redirect_to(admin_telegram_messages_url)
    end
  end

  describe 'PUT #copy' do
    it 'returns a success response' do
      put :copy, params: { id: telegram_message.to_param }
      expect(response).to be_successful
    end
  end

  describe 'PUT #send_message' do
    let(:context) { double('Interactor::Context', success?: success, message: 'message') }

    before do
      expect(Telegram::PublishMessage).to receive(:call).once.and_return(context)
    end

    context 'when successful' do
      let(:success) { true }

      it 'redirects to the telegram_messages list' do
        put :send_message, params: { id: telegram_message.to_param }
        expect(response).to redirect_to(admin_telegram_messages_url)
      end
    end

    context 'when unsuccessful' do
      let(:success) { false }

      it 'redirects to the telegram_messages list' do
        put :send_message, params: { id: telegram_message.to_param }
        expect(response).to redirect_to(admin_telegram_messages_url)
      end
    end
  end

  describe 'PUT #cancel_sending' do
    let(:context) { double('Interactor::Context', success?: success, message: 'message') }

    before do
      expect(Telegram::CancelMessage).to receive(:call).once.and_return(context)
    end

    context 'when succeeded' do
      let(:success) { true }

      it 'redirects to the telegram_messages list' do
        put :cancel_sending, params: { id: telegram_message.to_param }
        expect(response).to redirect_to(admin_telegram_messages_url)
      end
    end

    context 'when failed' do
      let(:success) { false }

      it 'redirects to the telegram_messages list' do
        put :cancel_sending, params: { id: telegram_message.to_param }
        expect(response).to redirect_to(admin_telegram_messages_url)
      end
    end
  end

  describe 'GET #recent_mailings' do
    before { create_list(:mailing, 2) }

    context 'without email list' do
      it 'returns a success response' do
        get :recent_mailings, params: { email_list_id: nil }
        expect(response).to be_successful
      end
    end

    context 'with email list' do
      let(:email_list) { create(:email_list) }

      it 'returns a success response' do
        get :recent_mailings, params: { email_list_id: email_list.id }
        expect(response).to be_successful
      end
    end
  end
end
