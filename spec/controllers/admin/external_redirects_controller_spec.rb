# frozen_string_literal: true

require 'rails_helper'

describe Admin::ExternalRedirectsController do
  render_views
  before(:all) do
    create_list(:external_redirect, 2)
  end

  context 'as admin user' do
    login_admin_user

    it 'index action should render index template' do
      get :index
      response.should render_template(:index)
    end

    it 'new action should render new template' do
      get :new
      response.should render_template(:new)
    end

    it 'create action should render new template when model is invalid' do
      expect_any_instance_of(ExternalRedirect).to receive(:valid?).and_return(false)
      post :create, params: { external_redirect: params_for(:external_redirect) }
      response.should render_template(:new)
    end

    it 'create action should redirect when model is valid' do
      expect_any_instance_of(ExternalRedirect).to receive(:valid?).and_return(true)
      post :create, params: { external_redirect: params_for(:external_redirect) }
      response.should redirect_to(admin_external_redirects_url)
    end

    it 'edit action should render edit template' do
      get :edit, params: { id: ExternalRedirect.first }
      response.should render_template(:edit)
    end

    it 'update action should render edit template when model is invalid' do
      expect_any_instance_of(ExternalRedirect).to receive(:valid?).and_return(false)
      put :update, params: { id: ExternalRedirect.first, external_redirect: params_for(:external_redirect) }
      response.should render_template(:edit)
    end

    it 'update action should redirect when model is valid' do
      expect_any_instance_of(ExternalRedirect).to receive(:valid?).and_return(true)
      params_for = params_for(:external_redirect)
      put :update, params: { id: ExternalRedirect.first, external_redirect: params_for }
      response.should redirect_to(admin_external_redirects_url)
    end

    it 'destroy action should destroy model and redirect to index action' do
      external_redirect = ExternalRedirect.first
      delete :destroy, params: { id: external_redirect }
      response.should redirect_to(admin_external_redirects_url)
      ExternalRedirect.exists?(external_redirect.id).should be_falsey
    end
  end
end
