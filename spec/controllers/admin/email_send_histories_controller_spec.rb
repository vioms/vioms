# frozen_string_literal: true

require 'rails_helper'

describe Admin::EmailSendHistoriesController do
  render_views
  before(:all) { create_list(:email_send_history, 2) }

  context 'as admin user' do
    login_admin_user

    it 'index action should render index template' do
      get :index
      expect(response).to render_template(:index)
    end

    it 'destroy action should destroy model and redirect to index action' do
      manage_email_send_history = EmailSendHistory.first
      delete :destroy, params: { id: manage_email_send_history }
      expect(response).to redirect_to(admin_email_send_histories_url)
      expect(EmailSendHistory.exists?(manage_email_send_history.id)).to be(false)
    end
  end
end
