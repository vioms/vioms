# frozen_string_literal: true

require 'rails_helper'

describe Admin::PollsController do
  render_views
  before(:all) do
    create_list(:poll, 2)
  end

  context 'as admin user' do
    login_admin_user

    it 'index action should render index template' do
      get :index
      expect(response).to have_rendered(:index)
    end

    it 'new action should render new template' do
      get :new
      expect(response).to have_rendered(:new)
    end

    it 'create action should render new template when model is invalid' do
      expect_any_instance_of(Poll).to receive(:valid?).and_return(false)
      post :create, params: { poll: params_for(:poll) }
      expect(response).to have_rendered(:new)
    end

    it 'create action should redirect when model is valid' do
      expect_any_instance_of(Poll).to receive(:valid?).and_return(true)
      post :create, params: { poll: params_for(:poll) }
      expect(response).to redirect_to(admin_polls_url)
    end

    it 'edit action should render edit template' do
      get :edit, params: { id: Poll.first }
      expect(response).to have_rendered(:edit)
    end

    it 'update action should render edit template when model is invalid' do
      expect_any_instance_of(Poll).to receive(:valid?).and_return(false)
      put :update, params: { id: Poll.first, poll: params_for(:poll) }
      expect(response).to have_rendered(:edit)
    end

    it 'update action should redirect when model is valid' do
      expect_any_instance_of(Poll).to receive(:valid?).and_return(true)
      put :update, params: { id: Poll.first, poll: params_for(:poll) }
      expect(response).to redirect_to(admin_polls_url)
    end

    it 'destroy action should destroy model and redirect to index action' do
      poll = Poll.first
      delete :destroy, params: { id: poll }
      expect(response).to redirect_to(admin_polls_url)
      expect(Poll.exists?(poll.id)).to be(false)
    end
  end
end
