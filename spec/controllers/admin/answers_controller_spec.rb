# frozen_string_literal: true

require 'rails_helper'

describe Admin::AnswersController do
  before(:all) do
    @poll = create(:poll)
    create_list(:answer, 2, poll: @poll)
  end

  render_views

  context 'as admin user' do
    login_admin_user

    it 'index action should render index template' do
      get :index, params: { poll_id: @poll }
      expect(response).to have_rendered(:index)
    end

    it 'new action should render new template' do
      get :new, params: { poll_id: @poll }
      expect(response).to have_rendered(:new)
    end

    it 'create action should render new template when model is invalid' do
      expect_any_instance_of(Answer).to receive(:valid?).and_return(false)
      post :create, params: { poll_id: @poll, answer: params_for(:answer) }
      expect(response).to have_rendered(:new)
    end

    it 'create action should redirect when model is valid' do
      expect_any_instance_of(Answer).to receive(:valid?).and_return(true)
      post :create, params: { poll_id: @poll, answer: params_for(:answer) }
      response.should redirect_to(admin_poll_answers_url(@poll))
    end

    it 'edit action should render edit template' do
      get :edit, params: { id: Answer.first, poll_id: @poll }
      expect(response).to have_rendered(:edit)
    end

    it 'update action should render edit template when model is invalid' do
      expect_any_instance_of(Answer).to receive(:valid?).and_return(false)
      put :update, params: { id: Answer.first, poll_id: @poll, answer: params_for(:answer) }
      expect(response).to have_rendered(:edit)
    end

    it 'update action should redirect when model is valid' do
      expect_any_instance_of(Answer).to receive(:valid?).and_return(true)
      put :update, params: { id: Answer.first, poll_id: @poll, answer: params_for(:answer) }
      expect(response).to redirect_to(admin_poll_answers_url(@poll))
    end

    it 'destroy action should destroy model and redirect to index action' do
      answer = Answer.first
      poll = answer.poll
      delete :destroy, params: { id: answer, poll_id: poll }
      expect(response).to redirect_to(admin_poll_answers_url(poll))
      expect(Answer.exists?(answer.id)).to be(false)
    end
  end
end
