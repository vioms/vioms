# frozen_string_literal: true

require 'rails_helper'

describe Admin::SmtpSettingGroupsController do
  render_views
  before(:all) do
    create_list(:smtp_setting_group, 2)
  end

  context 'as admin user' do
    login_admin_user

    it 'index action should render index template' do
      get :index
      expect(response).to have_rendered(:index)
    end

    it 'new action should render new template' do
      get :new
      expect(response).to have_rendered(:new)
    end

    it 'create action should render new template when model is invalid' do
      expect_any_instance_of(SmtpSettingGroup).to receive(:valid?).and_return(false)
      post :create, params: { smtp_setting_group: params_for(:smtp_setting_group) }
      expect(response).to have_rendered(:new)
    end

    it 'create action should redirect when model is valid' do
      expect_any_instance_of(SmtpSettingGroup).to receive(:valid?).and_return(true)
      post :create, params: { smtp_setting_group: params_for(:smtp_setting_group) }
      expect(response).to redirect_to(admin_smtp_setting_groups_url)
    end

    it 'edit action should render edit template' do
      get :edit, params: { id: SmtpSettingGroup.first }
      expect(response).to have_rendered(:edit)
    end

    it 'update action should render edit template when model is invalid' do
      expect_any_instance_of(SmtpSettingGroup).to receive(:valid?).and_return(false)
      put :update, params: { id: SmtpSettingGroup.first, smtp_setting_group: params_for(:smtp_setting_group) }
      expect(response).to have_rendered(:edit)
    end

    it 'update action should redirect when model is valid' do
      expect_any_instance_of(SmtpSettingGroup).to receive(:valid?).and_return(true)
      put :update, params: { id: SmtpSettingGroup.first, smtp_setting_group: params_for(:smtp_setting_group) }
      expect(response).to redirect_to(admin_smtp_setting_groups_url)
    end

    it 'destroy action should destroy model and redirect to index action' do
      smtp_setting_group = SmtpSettingGroup.first
      delete :destroy, params: { id: smtp_setting_group }
      expect(response).to redirect_to(admin_smtp_setting_groups_url)
      expect(SmtpSettingGroup.exists?(smtp_setting_group.id)).to be(false)
    end
  end
end
