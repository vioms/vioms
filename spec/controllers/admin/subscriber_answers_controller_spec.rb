# frozen_string_literal: true

require 'rails_helper'

describe Admin::SubscriberAnswersController do
  before(:all) do
    @poll = create(:poll)
    create_list(:answer, 2, poll: @poll)
  end

  render_views

  context 'as admin user' do
    login_admin_user

    it 'index action should render index template' do
      get :index, params: { poll_id: @poll }
      expect(response).to have_rendered(:index)
    end
  end
end
