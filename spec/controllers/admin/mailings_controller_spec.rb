# frozen_string_literal: true

require 'rails_helper'

describe Admin::MailingsController do
  render_views
  before_all do
    create_list(:mailing, 2)
    create(:mailing, :published_not_sent)
    create(:mailing, :for_digest)
    create(:mailing, :for_digest).email_digest.destroy
  end

  context 'as admin user' do
    login_admin_user

    it 'index action should render index template' do
      get :index
      expect(response).to have_rendered(:index)
    end

    it 'show action should render show template' do
      get :show, params: { id: Mailing.first }
      expect(response).to have_rendered(:show)
    end

    it 'full action should render full template' do
      get :full, params: { id: Mailing.first }
      expect(response).to be_successful
    end

    it 'new action should render new template when I have authorized email lists' do
      subject.current_user.email_lists = [build(:email_list)]
      get :new
      expect(response).to have_rendered(:new)
    end

    it 'create action should render new template when model is invalid' do
      expect_any_instance_of(Mailing).to receive(:valid?).and_return(false)
      post :create, params: { mailing: params_for(:mailing) }
      expect(response).to have_rendered(:new)
    end

    it 'create action should redirect when model is valid' do
      expect_any_instance_of(Mailing).to receive(:valid?).twice.and_return(true)
      post :create, params: { mailing: params_for(:mailing) }
      expect(response).to redirect_to(edit_admin_mailing_url(Mailing.last))
    end

    it 'edit action should render edit template' do
      get :edit, params: { id: Mailing.first }
      expect(response).to have_rendered(:edit)
    end

    it 'update action should render edit template when model is invalid' do
      expect_any_instance_of(Mailing).to receive(:valid?).and_return(false)
      put :update, params: { id: Mailing.first, mailing: params_for(:mailing) }
      expect(response).to have_rendered(:edit)
    end

    it 'update action should redirect when model is valid' do
      expect_any_instance_of(Mailing).to receive(:valid?).and_return(true)
      put :update, params: { id: Mailing.first, mailing: params_for(:mailing) }
      expect(response).to redirect_to(admin_mailings_url)
    end

    specify 'copy action' do
      put :copy, params: { id: Mailing.first }
      expect(response).to redirect_to(edit_admin_mailing_url(Mailing.last))
    end

    it 'send_emails non digest' do
      put :send_mailing, params: { id: Mailing.where(digest: false).first }
      expect(response).to redirect_to(admin_mailings_url)
    end

    it 'send_emails digest' do
      put :send_mailing, params: { id: Mailing.where(digest: true).first }
      expect(response).to redirect_to(admin_mailings_url)
    end

    it 'cancel_send_emails' do
      mailing = Mailing.first
      mailing.create_mailing_bg_job!
      put :cancel_sending, params: { id: mailing }
      expect(mailing.reload.mailing_bg_job).to be_nil
      expect(response).to redirect_to(admin_mailings_url)
    end

    it 'destroy action should destroy model and redirect to index action' do
      mailing = Mailing.first
      delete :destroy, params: { id: mailing }
      expect(response).to redirect_to(admin_mailings_url)
      expect(Mailing.exists?(mailing.id)).to be(false)
    end
  end
end
