# frozen_string_literal: true

require 'rails_helper'

describe Admin::SubscribersController do
  render_views

  before(:all) do
    create_sms_send_history_types
    create(:subscriber_with_email)
    create(:subscriber_with_phone)
  end

  context 'as admin user' do
    login_admin_user

    it 'index action should render index template' do
      get :index
      expect(response).to have_rendered(:index)
    end

    it 'new action should render new template' do
      get :new
      expect(response).to have_rendered(:new)
    end

    it 'create action should render new template when model is invalid' do
      expect_any_instance_of(Subscriber).to receive(:valid?).and_return(false)
      post :create, params: { subscriber: params_for(:subscriber) }
      expect(response).to have_rendered(:new)
    end

    it 'create action should redirect when model is valid' do
      expect_any_instance_of(Subscriber).to receive(:valid?).and_return(true)
      post :create, params: { subscriber: params_for(:subscriber) }
      expect(response).to redirect_to(admin_subscribers_url)
    end

    it 'edit action should render edit template' do
      get :edit, params: { id: Subscriber.first }
      expect(response).to have_rendered(:edit)
    end

    it 'update action should render edit template when model is invalid' do
      expect_any_instance_of(Subscriber).to receive(:valid?).and_return(false)
      put :update, params: { id: Subscriber.first, subscriber: params_for(:subscriber) }
      expect(response).to have_rendered(:edit)
    end

    it 'update action should redirect when model is valid' do
      expect_any_instance_of(Subscriber).to receive(:valid?).and_return(true)
      put :update, params: { id: Subscriber.first, subscriber: params_for(:subscriber) }
      expect(response).to redirect_to(admin_subscribers_url)
    end

    it 'destroy action should destroy model and redirect to index action' do
      subscriber = Subscriber.first
      delete :destroy, params: { id: subscriber }
      expect(response).to redirect_to(admin_subscribers_url)
      expect(Subscriber.exists?(subscriber.id)).to be(false)
    end
  end

  describe 'subscriptions and unsubscriptions' do
    before(:all) do
      create(:email_list, visible: true)
      create(:email_list, visible: false)
      create(:email_list, never_visible: true)
      create(:sms_list, visible: true, public: true)
      create(:sms_list, visible: false, public: true)
      create(:sms_list, never_visible: true)
    end
  end
end
