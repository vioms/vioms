require 'rails_helper'

describe Admin::CitiesController do
  render_views
  before(:all) do
    create_list(:city, 2)
  end

  context 'as user with cities role' do
    login_user_with_cities_role

    it 'index action should render index template' do
      get :index
      response.should render_template(:index)
    end

    it 'new action should render new template' do
      get :new
      response.should render_template(:new)
    end

    it 'create action should render new template when model is invalid' do
      expect_any_instance_of(City).to receive(:valid?).and_return(false)
      post :create, params: { city: params_for(:city) }
      response.should render_template(:new)
    end

    it 'create action should redirect when model is valid' do
      expect_any_instance_of(City).to receive(:valid?).and_return(true)
      post :create, params: { city: params_for(:city) }
      response.should redirect_to(admin_cities_url)
    end

    it 'edit action should render edit template' do
      get :edit, params: { id: City.first }
      response.should render_template(:edit)
    end

    it 'update action should render edit template when model is invalid' do
      expect_any_instance_of(City).to receive(:valid?).and_return(false)
      put :update, params: { id: City.first, city: params_for(:city) }
      response.should render_template(:edit)
    end

    it 'update action should redirect when model is valid' do
      expect_any_instance_of(City).to receive(:valid?).and_return(true)
      put :update, params: { id: City.first, city: params_for(:city) }
      response.should redirect_to(admin_cities_url)
    end

    it 'destroy action should destroy model and redirect to index action' do
      city = City.first
      delete :destroy, params: { id: city }
      response.should redirect_to(admin_cities_url)
      City.exists?(city.id).should be_falsey
    end
  end

  context 'as guest' do
    it_should_require_admin_user_for_actions Admin::CitiesController, City,
                                             { index: :get, new: :get, create: :post, edit: :get, update: :put, destroy: :delete }
  end
end
