# frozen_string_literal: true

require 'rails_helper'

describe EmailListsController do
  render_views

  before { DatabaseCleaner.clean }

  let(:email_list1) { create(:email_list, visible: true) }
  let(:email_list_invisible) { create(:email_list, visible: false) }
  let(:subscriber) { create(:subscriber_user_with_email, email_lists: [email_list_invisible]) }
  let(:email_list2) { create(:email_list, visible: true) }
  let(:email_list3) { create(:email_list_with_template, visible: true) }

  before do
    EmailListsMailingsInclusion.create!(email_list: email_list2,
                                        mailing: create(:mailing, email_list: email_list2))
    create(:mailing, html_content: '', editor_type: 'template', email_list: email_list3, sent: true, published: true,
                     deliver_at: 1.day.ago)
  end

  it 'index action should render index template' do
    get :index
    expect(response).to have_rendered(:index)
  end

  it 'index action should render index rss template' do
    get :index, format: 'rss'
    expect(response).to have_rendered(:index)
  end

  it 'index action should render index atom template' do
    get :index, format: 'atom'
    expect(response).to have_rendered(:index)
  end

  context 'without included email lists' do
    it 'show action should render show template' do
      get :show, params: { id: email_list1 }
      expect(response).to redirect_to(email_list_mailings_path(email_list1))
    end

    it 'show action should render show rss template' do
      get :show, params: { id: email_list1 }, format: 'rss'
      expect(response).to have_rendered(:show)
    end

    it 'show action should render show atom template' do
      get :show, params: { id: email_list1 }, format: 'atom'
      expect(response).to have_rendered(:show)
    end
  end

  context 'with included email lists' do
    it 'show action should render show template' do
      get :show, params: { id: email_list2 }
      expect(response).to redirect_to(email_list_mailings_path(email_list2))
    end

    it 'show action should render show rss template' do
      get :show, params: { id: email_list2 }, format: 'rss'
      expect(response).to have_rendered(:show)
    end

    it 'show action should render show atom template' do
      get :show, params: { id: email_list2 }, format: 'atom'
      expect(response).to have_rendered(:show)
    end
  end

  context 'with email list template' do
    it 'show action should render show template' do
      get :show, params: { id: email_list3 }
      expect(response).to redirect_to(email_list_mailings_path(email_list3))
    end

    it 'show action should render show rss template' do
      get :show, params: { id: email_list3 }, format: 'rss'
      expect(response).to have_rendered(:show)
    end

    it 'show action should render show atom template' do
      get :show, params: { id: email_list3 }, format: 'atom'
      expect(response).to have_rendered(:show)
    end
  end

  describe 'invisible lists' do
    context 'as admin user' do
      login_admin_user

      it 'show action should render show template' do
        get :show, params: { id: email_list_invisible }
        expect(response).to redirect_to(root_url)
      end

      it 'show action should render show rss template' do
        get :show, params: { id: email_list_invisible }, format: 'rss'
        expect(response).to redirect_to(root_url)
      end

      it 'show action should render show atom template' do
        get :show, params: { id: email_list_invisible }, format: 'atom'
        expect(response).to redirect_to(root_url)
      end
    end

    context 'as user' do
      before { sign_in_subscriber_user(subscriber, nil) }

      it 'show action should render show template' do
        get :show, params: { id: email_list_invisible }
        expect(response).to redirect_to(email_list_mailings_path(email_list_invisible))
      end

      it 'show action should render show rss template' do
        get :show, params: { id: email_list_invisible }, format: 'rss'
        expect(response).to have_rendered(:show)
      end

      it 'show action should render show atom template' do
        get :show, params: { id: email_list_invisible }, format: 'atom'
        expect(response).to have_rendered(:show)
      end
    end
  end
end
