require 'rails_helper'

describe NewsItemsController do
  render_views
  before(:all) do
    create_list(:news_item, 2)
  end

  it 'show action should render show template' do
    get :show, params: { id: NewsItem.first }
    expect(response).to have_rendered(:show)
  end
end
