# frozen_string_literal: true

require 'rails_helper'

describe ContactController do
  render_views

  it 'new action should render new template' do
    get :new
    expect(response).to have_rendered(:new)
  end

  it 'create action should render new template when model is invalid' do
    expect_any_instance_of(Contact).to receive(:valid?).and_return(false)
    post :create, params: { contact: attributes_for(:contact) }
    expect(response).to have_rendered(:new)
  end

  it 'create action should redirect when model is valid' do
    ActionMailer::Base.deliveries = []
    expect_any_instance_of(Contact).to receive(:valid?).and_return(true)
    post :create, params: { contact: attributes_for(:contact) }
    expect(response).to redirect_to(root_url)
    expect(ActionMailer::Base.deliveries.size).to eq(1)
  end
end
