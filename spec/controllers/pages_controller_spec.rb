# frozen_string_literal: true

require 'rails_helper'

describe PagesController do
  render_views
  before(:all) do
    create_list(:page, 2)
  end

  context 'as admin user' do
    login_user_with_pages_role

    it 'new action should render new template' do
      get :new
      expect(response).to have_rendered(:new)
    end

    it 'create action should render new template when model is invalid' do
      expect_any_instance_of(Page).to receive(:valid?).and_return(false)
      post :create, params: { page: params_for(:page) }
      expect(response).to have_rendered(:new)
    end

    it 'create action should redirect when model is valid' do
      expect_any_instance_of(Page).to receive(:valid?).and_return(true)
      post :create, params: { page: params_for(:page) }
      expect(response).to redirect_to(page_url(assigns[:page]))
    end

    it 'edit action should render edit template' do
      get :edit, params: { id: Page.first.id }
      expect(response).to have_rendered(:edit)
    end

    it 'update action should render edit template when model is invalid' do
      expect_any_instance_of(Page).to receive(:valid?).and_return(false)
      put :update, params: { id: Page.first.id, page: params_for(:page) }
      expect(response).to have_rendered(:edit)
    end

    it 'update action should redirect when model is valid' do
      expect_any_instance_of(Page).to receive(:valid?).and_return(true)
      put :update, params: { id: Page.first.id, page: params_for(:page) }
      expect(response).to redirect_to(page_url(assigns[:page]))
    end

    it 'destroy action should destroy model and redirect to index action' do
      page = Page.first
      delete :destroy, params: { id: page.id }
      expect(response).to redirect_to(pages_url)
      expect(Page.exists?(page.id)).to be(false)
    end
  end

  context 'as guest' do
    it 'index action should render show template' do
      get :index
      expect(response).to have_rendered(:show)
    end

    it 'show action should render show template' do
      get :show, params: { id: Page.first.id }
      expect(response).to have_rendered(:show)
    end
  end
end
