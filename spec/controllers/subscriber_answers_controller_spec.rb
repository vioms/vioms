# frozen_string_literal: true

require 'rails_helper'

describe SubscriberAnswersController do
  let(:subscriber) { create(:subscriber_with_email) }
  let(:answer) { create(:answer) }

  it 'redirects to home page' do
    allow(VoteService).to receive(:vote).and_return(true)
    get :new, params: { subscriber_id: subscriber.id, answer_id: answer.id }
    expect(response).to redirect_to(root_url)
  end
end
