# frozen_string_literal: true

require 'rails_helper'

describe BannerClicksController do
  it 'new action should render new template' do
    banner = create(:banner, link: 'http://example.com')
    expect(banner.clicks.count).to eq(0)
    get :new, params: { id: banner.id }
    expect(banner.reload.clicks.reload.count).to eq(1)
    expect(response).to redirect_to('http://example.com')
  end
end
