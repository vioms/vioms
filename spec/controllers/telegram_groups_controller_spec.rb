# frozen_string_literal: true

require 'rails_helper'

describe TelegramGroupsController do
  render_views

  describe 'GET #index' do
    it 'returns a success response' do
      create(:telegram_group)
      create(:telegram_group, :without_description)
      create(:telegram_group, :invisible)
      get :index, params: {}
      expect(response).to be_successful
    end
  end
end
