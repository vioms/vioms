# frozen_string_literal: true

require 'rails_helper'

describe API::Stripo::FilesController do
  before do
    allow(Setting).to receive('get_setting').with('stripo_username').and_return('stripo')
    allow(Setting).to receive('get_setting').with('stripo_password').and_return('password')
    request.env['HTTP_AUTHORIZATION'] =
      ActionController::HttpAuthentication::Basic.encode_credentials('stripo', 'password')
  end

  specify 'list' do
    allow(Stripo::FileOperations).to receive(:list).and_return([])
    get :list
    expect(response).to be_successful
    expect(json_body).to eq([])
  end

  specify 'info' do
    allow(Stripo::FileOperations).to receive(:info).and_return([])
    get :info, params: { src: 'src' }
    expect(response).to be_successful
    expect(json_body).to eq([])
  end

  specify 'upload' do
    allow(Stripo::FileOperations).to receive(:upload).and_return([])
    post :upload, params: { key: 'key', file: 'file' }
    expect(response).to be_successful
    expect(json_body).to eq([])
  end

  specify 'delete' do
    allow(Stripo::FileOperations).to receive(:delete).and_return([])
    post :delete, params: { key: 'key', url: 'url' }
    expect(response).to be_successful
    expect(json_body).to eq([])
  end
end
