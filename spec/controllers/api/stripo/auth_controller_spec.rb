# frozen_string_literal: true

require 'rails_helper'

describe API::Stripo::AuthController do
  before do
    expect(Setting).to receive('get_setting').with('stripo_app_id').and_return('app_id')
    expect(Setting).to receive('get_setting').with('stripo_secret_key').and_return('secret_key')
    stub_request(:post, Stripo::RequestToken::AUTH_URL)
      .with(body: { pluginId: 'app_id', secretKey: 'secret_key' }.to_json)
      .to_return(status: 200, body: { token: 'token' }.to_json)
  end

  login_admin_user

  specify 'token' do
    get :token
    expect(response).to be_successful
    expect(json_body).to eq(token: 'token')
  end
end
