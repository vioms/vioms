require 'rails_helper'

describe UnsubscriptionsController do
  render_views

  context 'as subscriber user with email' do
    before do
      @email_list_subscribed1 = create(:email_list)
      @email_list_subscribed2 = create(:email_list)
      @email_list_not_subscribed = create(:email_list)
      @subscriber = create(:subscriber_user_with_email, email_lists: [@email_list_subscribed1, @email_list_subscribed2])
      sign_in_subscriber_user(@subscriber, nil)
    end

    it 'new action should render new template' do
      get :new, params: { email_list_id: @email_list_subscribed1 }
      response.should render_template(:new)
    end

    it 'new action should redirect if subscriber is not subscribed' do
      get :new, params: { email_list_id: @email_list_not_subscribed }
      response.should redirect_to(root_path)
    end

    describe 'quick mode' do
      it 'unsubscribes' do
        get :new, params: { email_list_id: @email_list_subscribed1, quick: 1 }
        response.body.should
        @subscriber.reload.email_lists.should == [@email_list_subscribed2]
      end

      it 'says ok on not subscribed list' do
        @subscriber.email_lists = [@email_list_subscribed2]
        get :new, params: { email_list_id: @email_list_subscribed1, quick: 1 }
        response.body.should
        @subscriber.reload.email_lists.should == [@email_list_subscribed2]
      end
    end

    it 'create action should redirect' do
      post :create, params: { unsubscription: params_for(:unsubscription, email_list_id: @email_list_subscribed1.id) }
      response.should redirect_to(root_path)
      flash[:notice].should
      I18n.t('flash.unsubscription.create')
      @subscriber.reload.email_lists.should == [@email_list_subscribed2]
    end

    it 'create action should redirect and report error when not subscribed' do
      post :create,
           params: { unsubscription: params_for(:unsubscription, email_list_id: @email_list_not_subscribed.id) }
      response.should redirect_to(root_path)
      flash[:alert].should == I18n.t('flash.unsubscription.not_subscribed')
    end
  end
end
