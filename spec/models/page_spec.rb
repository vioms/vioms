require 'rails_helper'

describe Page do
  it { expect(build(:page)).to be_valid }
  it { expect(build(:page, :invalid)).not_to be_valid }
end
