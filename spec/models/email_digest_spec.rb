require 'rails_helper'

RSpec.describe EmailDigest do
  it 'is valid' do
    expect(build(:email_digest)).to be_valid
  end

  it 'is valid with mailings' do
    expect(build(:email_digest_with_mailings)).to be_valid
  end

  describe 'nums and years' do
    before(:all) { Timecop.travel(Time.gm(2008, 9, 1, 6, 5, 0)) }

    after(:all) { Timecop.return }

    before { ActiveRecord::Base.connection.execute('TRUNCATE email_digests') }

    let(:email_list) { create(:email_list) }
    let(:email_list2) { create(:email_list) }

    it 'verifies nums' do
      expect(create(:email_digest, email_list:, num: 1, deliver_at: '2009-01-01 14:00:00')).to be_valid
      expect(build(:email_digest, email_list:, num: 1, deliver_at: '2009-01-01 15:00:00')).not_to be_valid
      expect(build(:email_digest, email_list:, num: 2, deliver_at: '2009-01-01 15:00:00')).to be_valid
      expect(build(:email_digest, email_list: email_list2, num: 1, deliver_at: '2009-01-01 15:00:00')).to be_valid
      expect(build(:email_digest, email_list:, num: 1, deliver_at: '2010-01-01 15:00:00')).to be_valid
      expect(build(:email_digest, email_list: email_list2, num: 1, deliver_at: '2010-01-01 15:00:00')).to be_valid
    end

    it 'sets year' do
      email_digest = build(:email_digest, email_list:, deliver_at: '2009-01-01')
      email_digest.valid?
      expect(email_digest.year).to eq(2009)
    end
  end

  describe 'delivery time' do
    before(:all) do
      EmailDigest.delete_all
      Timecop.freeze
    end

    after(:all) { Timecop.travel }

    it 'is not valid in the past' do
      expect(build(:email_digest, deliver_at: 1.second.ago)).not_to be_valid
    end

    it 'is valid in the past and sent' do
      expect(build(:email_digest, deliver_at: 1.second.ago, sent: true)).to be_valid
    end

    it 'is valid in the future' do
      expect(build(:email_digest, deliver_at: 1.second.from_now)).to be_valid
    end

    it 'is valid after save (do not conflict with itself)' do
      expect(create(:email_digest)).to be_valid
    end

    describe 'overlapping time' do
      before(:all) do
        @delivery_time = 1.day.from_now
        create(:email_digest, deliver_at: @delivery_time)
      end

      describe 'before' do
        it 'is valid before' do
          expect(
            build(:email_digest, deliver_at: @delivery_time - EmailDigest::MINIMUM_INTERVAL - 1.second)
          ).to be_valid
        end

        it 'is not valid at the beginning' do
          expect(
            build(:email_digest, deliver_at: @delivery_time - EmailDigest::MINIMUM_INTERVAL + 1.second)
          ).not_to be_valid
        end

        it 'is not at the end' do
          expect(build(:email_digest, deliver_at: @delivery_time - 1.second)).not_to be_valid
        end
      end

      describe 'after' do
        it 'is not valid at the beginning' do
          expect(build(:email_digest, deliver_at: @delivery_time + 1.second)).not_to be_valid
        end

        it 'is not valid at the end' do
          expect(
            build(:email_digest, deliver_at: @delivery_time + EmailDigest::MINIMUM_INTERVAL - 1.second)
          ).not_to be_valid
        end

        it 'is valid after' do
          expect(
            build(:email_digest, deliver_at: @delivery_time + EmailDigest::MINIMUM_INTERVAL + 1.second)
          ).to be_valid
        end
      end
    end
  end
end
