# frozen_string_literal: true

require 'rails_helper'

describe SmsMessage do
  it 'is valid' do
    expect(build(:sms_message)).to be_valid
  end
end
