# frozen_string_literal: true

require 'rails_helper'

describe 'SmsSendHistory' do
  before { SmsSendHistory.delete_all }

  before(:all) { create_sms_send_history_types }

  it 'checks' do
    expect(SmsSendHistory.check('1111111111', 1, 'password_recovery')).to be(true)
  end

  it 'checks 3 times' do
    expect(SmsSendHistory.check('1111111111', 1, '3_times')).to be(true)
    expect(SmsSendHistory.check('1111111111', 1, '3_times')).to be(true)
    expect(SmsSendHistory.check('1111111111', 1, '3_times')).to be(true)
    expect(SmsSendHistory.check('1111111111', 1, '3_times')).to be(false)
  end

  it 'is true after delete' do
    SmsSendHistory.check('1111111111', 1, 'password_recovery')
    SmsSendHistory.forget('1111111111', 'password_recovery')
    expect(SmsSendHistory.check('1111111111', 1, 'password_recovery')).to be(true)
  end

  it 'is true after delete all' do
    SmsSendHistory.check('1111111111', 1, 'password_recovery')
    SmsSendHistory.forget('1111111111')
    expect(SmsSendHistory.check('1111111111', 1, 'password_recovery')).to be(true)
  end

  it 'sets subscriber_id' do
    SmsSendHistory.check('1111111111', 123, 'password_recovery')
    expect(SmsSendHistory.last.subscriber_id).to eq(123)
  end

  describe 'expire_at' do
    it 'sets expire_at' do
      time = Time.zone.now
      SmsSendHistory.check('1111111111', 1, 'password_recovery', expire_at: time)
      expect(SmsSendHistory.last.expire_at.to_i).to eq(time.to_i)
    end

    it 'expires' do
      Timecop.freeze do
        expect(SmsSendHistory.check('1111111111', 1, 'password_recovery', expire_at: 1.minute.from_now)).to be(true)
        Timecop.freeze(2.minutes.from_now) do
          expect(SmsSendHistory.check('1111111111', 1, 'password_recovery', expire_at: 1.minute.from_now)).to be(true)
          Timecop.freeze(2.minutes.from_now) do
            expect(SmsSendHistory.check('1111111111', 1, 'password_recovery',
                                        expire_at: 1.minute.from_now)).to be(true)
            expect(SmsSendHistory.check('1111111111', 1, 'password_recovery',
                                        expire_at: 1.minute.from_now)).to be(false)
            Timecop.freeze(2.minutes.from_now) do
              expect(SmsSendHistory.check('1111111111', 1, 'password_recovery',
                                          expire_at: 1.minute.from_now)).to be(true)
              expect(SmsSendHistory.check('1111111111', 1, 'password_recovery',
                                          expire_at: 1.minute.from_now)).to be(false)
              expect(SmsSendHistory.check('1111111111', 1, 'password_recovery',
                                          expire_at: 1.minute.from_now)).to be(false)
              Timecop.freeze(2.minutes.from_now) do
                expect(SmsSendHistory.check('1111111111', 1, 'password_recovery',
                                            expire_at: 1.minute.from_now)).to be(true)
                expect(SmsSendHistory.check('1111111111', 1, 'password_recovery',
                                            expire_at: 1.minute.from_now)).to be(false)
                expect(SmsSendHistory.check('1111111111', 1, 'password_recovery',
                                            expire_at: 1.minute.from_now)).to be(false)
              end
            end
          end
        end
      end
    end

    it 'expires after 3 attempts' do
      Timecop.freeze do
        expect(SmsSendHistory.check('1111111111', 1, '3_times', expire_at: 1.minute.from_now)).to be(true)
        expect(SmsSendHistory.check('1111111111', 1, '3_times', expire_at: 1.minute.from_now)).to be(true)
        expect(SmsSendHistory.check('1111111111', 1, '3_times', expire_at: 1.minute.from_now)).to be(true)
        expect(SmsSendHistory.check('1111111111', 1, '3_times', expire_at: 1.minute.from_now)).to be(false)
        expect(SmsSendHistory.check('1111111111', 1, '3_times', expire_at: 1.minute.from_now)).to be(false)
        Timecop.freeze(5.minutes.from_now) do
          expect(SmsSendHistory.check('1111111111', 1, '3_times', expire_at: 1.minute.from_now)).to be(true)
          expect(SmsSendHistory.check('1111111111', 1, '3_times', expire_at: 1.minute.from_now)).to be(true)
          Timecop.freeze(5.minutes.from_now) do
            expect(SmsSendHistory.check('1111111111', 1, '3_times', expire_at: 1.minute.from_now)).to be(true)
            expect(SmsSendHistory.check('1111111111', 1, '3_times', expire_at: 1.minute.from_now)).to be(true)
            expect(SmsSendHistory.check('1111111111', 1, '3_times', expire_at: 1.minute.from_now)).to be(true)
            expect(SmsSendHistory.check('1111111111', 1, '3_times', expire_at: 1.minute.from_now)).to be(false)
            expect(SmsSendHistory.check('1111111111', 1, '3_times', expire_at: 1.minute.from_now)).to be(false)
          end
        end
      end
    end

    it 'does not expire' do
      expect(SmsSendHistory.check('1111111111', 1, 'password_recovery', expire_at: Date.tomorrow)).to be(true)
      expect(SmsSendHistory.check('1111111111', 1, 'password_recovery')).to be(false)
    end

    it 'does not expire' do
      expect(SmsSendHistory.check('1111111111', 1, 'password_recovery', expire_at: Date.tomorrow)).to be(true)
      expect(SmsSendHistory.check('1111111111', 1, 'password_recovery', expire_at: Date.tomorrow)).to be(false)
    end
  end

  describe 'query' do
    it 'queries' do
      expect(SmsSendHistory.query('1111111111', 'password_recovery')[0]).to be(true)
      expect(SmsSendHistory.check('1111111111', 1, 'password_recovery')).to be(true)
      expect(SmsSendHistory.query('1111111111', 'password_recovery')[0]).to be(false)
    end

    it 'returns last value' do
      expect(SmsSendHistory.check('1111111111', 1, 'password_recovery', value: 'val')).to be(true)
      expect(SmsSendHistory.query('1111111111', 'password_recovery')[1]).to eq('val')
    end

    it 'does not return last value when expired' do
      expect(SmsSendHistory.check('1111111111', 1, 'password_recovery', value: 'val',
                                                                        expire_at: 1.minute.from_now)).to be(true)
      expect(SmsSendHistory.query('1111111111', 'password_recovery')[0]).to be(false)
      Timecop.freeze(2.minutes.from_now) do
        expect(SmsSendHistory.query('1111111111', 'password_recovery')[0]).to be(true)
      end
    end
  end
end
