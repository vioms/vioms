require 'rails_helper'

describe TextBlock do
  before { DatabaseCleaner.clean }

  it { expect(build(:text_block)).to be_valid }
  it { expect(build(:text_block, :invalid)).not_to be_valid }

  describe '.get' do
    let!(:text_block) { create(:text_block, name: 'name', content: 'some content') }

    it { expect(TextBlock.get(:name)).to eq('some content') }

    context 'when text block does not exist' do
      it { expect(TextBlock.get(:nonexistent)).to be_nil }
    end
  end
end
