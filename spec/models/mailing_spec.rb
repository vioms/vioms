# frozen_string_literal: true

require 'rails_helper'

describe Mailing do
  it 'is valid' do
    expect(build(:mailing)).to be_valid
  end

  it 'is valid' do
    expect(build(:mailing, :for_creating)).to be_valid
  end

  context 'with stripo plugin' do
    it 'is valid' do
      expect(build(:mailing, :for_stripo)).to be_valid
    end

    it 'is not valid' do
      expect(build(:mailing, :for_stripo_invalid)).not_to be_valid
    end
  end

  describe 'external redirects' do
    before_all do
      create(:external_redirect, url: 'mailist.example.org')
      create(:external_redirect, url: 'subscribe.example.org')
    end

    it 'does not fill backup field if content was not altered' do
      html_content = '<html></html>'
      mailing = create(:mailing, html_content:)
      mailing.resolve_external_redirects
      expect(mailing.html_content).to eq(html_content)
      expect(mailing.html_content_backup).to be_blank
    end

    it 'replaces urls and fills backup' do
      html_content = '<html>text<a href="http://mailist.example.org?follow=link&id=108">Click here</a>text' \
                     '<A HREF=\'http://subscribe.example.org?page=test&uid=123\'>And here</A>text' \
                     '<A HREF=\'http://subscribe.example.org?page=test&uid=124\'>And here</A>text</html>'
      mailing = create(:mailing, html_content:)
      expect(ExternalRedirect).to receive(:internet_resolve).with('http://mailist.example.org?follow=link&id=108')
                                                            .and_return('http://one.example.com')
      expect(ExternalRedirect).to receive(:internet_resolve).with('http://subscribe.example.org?page=test&uid=123')
                                                            .and_return('http://two.example.com')
      expect(ExternalRedirect).to receive(:internet_resolve).with('http://subscribe.example.org?page=test&uid=124')
                                                            .and_return('http://three.example.com')
      mailing.resolve_external_redirects
      expect(mailing.html_content).to eq('<html>text<a href="http://one.example.com">Click here</a>text' \
                                         '<A HREF=\'http://two.example.com\'>And here</A>text' \
                                         '<A HREF=\'http://three.example.com\'>And here</A>text</html>')
      expect(mailing.html_content_backup).not_to eq(mailing.html_content)
      expect(mailing.html_content_backup).to eq(html_content)
    end
  end

  describe 'email attachments size' do
    before { allow_any_instance_of(EmailAttachmentUploader).to receive(:size).and_return(131_072) }

    it 'is valid' do
      expect(build(:mailing, max_attachments_size: 256,
                             email_attachments: create_list(:email_attachment, 2))).to be_valid
    end

    it 'is not valid' do
      expect(build(:mailing, max_attachments_size: 256,
                             email_attachments: create_list(:email_attachment, 3))).not_to be_valid
    end
  end

  describe 'delivery time' do
    before_all do
      DatabaseCleaner.clean_with(:truncation)
      reset_next_delivery_time
      Timecop.freeze
    end

    after(:all) { Timecop.travel }

    it 'is not valid in the past' do
      expect(build(:mailing, deliver_at: 1.second.ago)).not_to be_valid
    end

    it 'is valid in the past and sent' do
      expect(build(:mailing, deliver_at: 1.second.ago, sent: true)).to be_valid
    end

    it 'is valid in the future' do
      expect(build(:mailing, deliver_at: 1.second.from_now)).to be_valid
    end

    it 'is valid after save (do not conflict with itself)' do
      expect(create(:mailing)).to be_valid
    end

    describe 'overlapping time' do
      before_all do
        @delivery_time = 1.day.from_now
        create(:mailing, deliver_at: @delivery_time)
      end

      describe 'before' do
        it 'is valid before' do
          expect(build(:mailing, deliver_at: @delivery_time - Mailing::MINIMUM_INTERVAL - 1.second)).to be_valid
        end

        it 'is not valid at the beginning' do
          expect(build(:mailing, deliver_at: @delivery_time - Mailing::MINIMUM_INTERVAL + 1.second)).not_to be_valid
        end

        it 'is not at the end' do
          expect(build(:mailing, deliver_at: @delivery_time - 1.second)).not_to be_valid
        end
      end

      describe 'after' do
        it 'is not valid at the beginning' do
          expect(build(:mailing, deliver_at: @delivery_time + 1.second)).not_to be_valid
        end

        it 'is not valid at the end' do
          expect(build(:mailing, deliver_at: @delivery_time + Mailing::MINIMUM_INTERVAL - 1.second)).not_to be_valid
        end

        it 'is valid after' do
          expect(build(:mailing, deliver_at: @delivery_time + Mailing::MINIMUM_INTERVAL + 1.second)).to be_valid
        end
      end

      describe 'mailing and email digest' do
        before_all do
          create(:mailing, deliver_at: 1.hour.from_now)
          create(:email_digest, deliver_at: 2.hours.from_now)
        end

        context 'without mailing' do
          it 'is be valid' do
            Mailing.delete_all
            EmailDigest.delete_all
            expect(build(:email_digest, deliver_at: 1.minute.from_now)).to be_valid
            expect(Mailing.count).to eq(0)
            expect(EmailDigest.count).to eq(0)
          end
        end

        context 'without email digest' do
          it 'is valid' do
            Mailing.delete_all
            EmailDigest.delete_all
            expect(build(:mailing, deliver_at: 1.minute.from_now)).to be_valid
            expect(Mailing.count).to eq(0)
            expect(EmailDigest.count).to eq(0)
          end
        end

        context 'mailings for digest' do
          it 'skips deliver_at validation' do
            mailing = create(:mailing)
            expect(build(:mailing, :for_digest, deliver_at: mailing.deliver_at)).to be_valid
          end

          it 'email digest skips deliver_at validation' do
            email_digest = create(:email_digest)
            expect(build(:mailing, :for_digest, deliver_at: email_digest.deliver_at)).to be_valid
          end
        end

        it 'is valid' do
          expect(build(:mailing, deliver_at: 3.hours.from_now)).to be_valid
        end

        specify 'email digest is valid' do
          expect(build(:email_digest, deliver_at: 3.hours.from_now)).to be_valid
        end

        it 'is not valid' do
          expect(build(:mailing, deliver_at: 2.hours.from_now)).not_to be_valid
        end

        specify 'email digest is not valid' do
          expect(build(:email_digest, deliver_at: 1.hour.from_now)).not_to be_valid
        end
      end
    end
  end
end
