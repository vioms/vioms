# frozen_string_literal: true

require 'rails_helper'

RSpec.describe MobileToken do
  it 'is valid' do
    expect(create(:mobile_token)).to be_valid
  end
end
