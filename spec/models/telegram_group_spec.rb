# frozen_string_literal: true

require 'rails_helper'

RSpec.describe TelegramGroup do
  it 'is valid' do
    expect(build(:telegram_group)).to be_valid
  end
end
