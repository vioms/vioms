require 'rails_helper'

RSpec.describe ExternalRedirect do
  it 'is valid' do
    expect(build(:external_redirect)).to be_valid
  end

  context 'simple redirect' do
    it 'is valid' do
      expect(build(:external_redirect, is_simple: true, param_name: 'To')).to be_valid
    end

    it 'is not valid' do
      expect(build(:external_redirect, is_simple: true)).not_to be_valid
    end
  end

  describe 'replacements' do
    it 'replaces simple url' do
      er = create(:external_redirect, url: 'e.mail.ru/cgi-bin/link?', is_simple: true, param_name: 'url')
      url = er.replace_url('http://e.mail.ru/cgi-bin/link?check=1&amp;cnf=3db518&amp;url=http%3A%2F%2Fwww.festofindia.ru&amp;msgid=13365465280000000550')
      expect(url).to eq('http://www.festofindia.ru')
      url = er.replace_url('https://e.mail.ru/cgi-bin/link?check=1&amp;cnf=3db518&amp;url=http%3A%2F%2Fwww.festofindia.ru&amp;msgid=13365465280000000550')
      expect(url).to eq('http://www.festofindia.ru')
    end

    it 'does not enter endless loop and should behave as unresolved' do
      er = create(:external_redirect, url: 'mailist.example.org')
      expect(ExternalRedirect).to receive(:internet_resolve).at_most(10).times.with('http://mailist.example.org?follow=link&id=108')
                                                            .and_return('http://mailist.example.org?follow=link&id=108')
      ExternalRedirect.cache_records do
        expect(ExternalRedirect.replace_url('http://mailist.example.org?follow=link&id=108')).to be_nil
      end
      er.reload
      expect(er.resolved_count).to eq(0)
      expect(er.unresolved_count).to eq(1)
    end
  end
end
