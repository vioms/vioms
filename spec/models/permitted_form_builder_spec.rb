require 'rails_helper'

describe PermittedFormBuilder do
  it 'permitted_params should add arrays and convert id to params' do
    parameters = ActionController::Parameters.new({ user: { email_list_ids: [1, 2, 3], time_zone_id: 1 } })
    expect(PermittedParams.new(parameters, Admin::UsersController.new, :user).permitted_params.to_h)
      .to eq({ 'email_list_ids' => [1, 2, 3] })
  end
end
