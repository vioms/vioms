# frozen_string_literal: true

require 'rails_helper'

RSpec.describe SurveyQuestion do
  it 'is valid' do
    expect(build(:survey_question)).to be_valid
  end

  context 'options type' do
    it 'is valid' do
      expect(build(:survey_question, :options)).to be_valid
    end
  end
end
