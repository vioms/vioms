# frozen_string_literal: true

require 'rails_helper'

RSpec.describe SurveySubscriberAnswer do
  it 'is valid' do
    expect(build(:survey_subscriber_answer)).to be_valid
  end
end
