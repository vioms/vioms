# frozen_string_literal: true

require 'rails_helper'

RSpec.describe SubscriberImportPerson do
  it 'is valid' do
    expect(create(:subscriber_import_person)).to be_valid
  end
end
