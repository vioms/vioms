# frozen_string_literal: true

require 'rails_helper'

RSpec.describe MailingView do
  it 'is valid' do
    expect(create(:mailing_view)).to be_valid
  end
end
