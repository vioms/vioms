# frozen_string_literal: true

require 'rails_helper'

RSpec.describe EmailDigestBgJob do
  it 'is valid' do
    expect(create(:email_digest_bg_job)).to be_valid
  end
end
