# frozen_string_literal: true

require 'rails_helper'

RSpec.describe MailingRequestMessage do
  it 'is valid' do
    expect(create(:mailing_request_message)).to be_valid
  end
end
