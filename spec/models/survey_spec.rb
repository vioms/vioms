# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Survey do
  it 'is valid' do
    expect(build(:survey)).to be_valid
  end
end
