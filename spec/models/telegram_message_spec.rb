# frozen_string_literal: true

require 'rails_helper'

RSpec.describe TelegramMessage do
  it { expect(build(:telegram_message)).to be_valid }

  context 'when it is invalid' do
    it { expect(build(:telegram_message, :invalid)).not_to be_valid }
  end

  context 'when it is published' do
    it { expect(build(:telegram_message, :published)).to be_valid }
  end

  context 'when it has a schedule' do
    it { expect(build(:telegram_message, :published, :with_schedule)).to be_valid }
  end
end
