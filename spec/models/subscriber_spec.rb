# frozen_string_literal: true

require 'rails_helper'

describe Subscriber do
  before(:all) do
    create_email_problem_names
  end

  it 'is not invalid' do
    expect(build(:subscriber)).not_to be_valid
  end

  it 'is be valid' do
    expect(build(:subscriber_with_email)).to be_valid
    expect(build(:subscriber_with_phone)).to be_valid
    expect(build(:subscriber_with_email_and_phone)).to be_valid
  end

  describe 'authentication_token' do
    it 'is not blank for subscriber' do
      expect(create(:subscriber_with_email).authentication_token).not_to be_blank
    end

    it 'is not blank for subscriber user' do
      expect(create(:subscriber_user_with_email).authentication_token).not_to be_blank
    end
  end

  it 'extracts phones' do
    expect(Subscriber.extract_phone(nil)).to be_nil
    expect(Subscriber.extract_phone('')).to be_nil
    expect(Subscriber.extract_phone(' ')).to be_nil
    expect(Subscriber.extract_phone('900123456')).to be(false)
    expect(Subscriber.extract_phone('900123456789')).to be(false)
    expect(Subscriber.extract_phone('9001234567')).to eq('9001234567')
    expect(Subscriber.extract_phone('89001234567')).to eq('9001234567')
    expect(Subscriber.extract_phone('79001234567')).to eq('9001234567')
    expect(Subscriber.extract_phone('389001234567')).to eq('9001234567')
    expect(Subscriber.extract_phone('69001234567')).to be(false)
    expect(Subscriber.extract_phone('8900111223')).to be(false)
    expect(Subscriber.extract_phone('+8(900)123-45-67')).to eq('9001234567')
    expect(Subscriber.extract_phone('+7(900)123-45-67')).to eq('9001234567')
    expect(Subscriber.extract_phone('+6(900)123-45-67')).to be(false)
    expect(Subscriber.extract_phone('9091234567@example.com')).to be(false)
  end

  it 'detects email' do
    expect(Subscriber.is_login_email?('mail@example.com')).to be(true)
    expect(Subscriber.is_login_email?('89091112233')).to be(false)
  end

  it 'fixes phone number' do
    subscriber = build(:subscriber_with_phone, phone: '+7-900-123-45-67')
    subscriber.valid? # trigger before_validation callback
    expect(subscriber.phone).to eq('9001234567')
  end

  it 'unsubscribes from never visible lists' do
    subscriber = create(:subscriber_with_email, email_lists:
        [build(:email_list), build(:email_list, never_visible: true)])
    expect(subscriber.email_lists.reload.size).to eq(2)
    subscriber.update(email_list_ids: [subscriber.email_list_ids.first])
    expect(subscriber.email_lists.reload.size).to eq(1)
  end

  it 'sends unsubscription notifications on subscriber destroy' do
    subscriber = create(:subscriber_with_email, email_list_ids: [
                          create(:email_list, notify_subscribed: true, notify_email: generate(:email)).id,
                          create(:email_list, notify_subscribed: true).id,
                          create(:email_list, notify_subscribed: false, notify_email: generate(:email)).id,
                          create(:email_list, notify_unsubscribed: true, notify_email: generate(:email)).id,
                          create(:email_list, notify_unsubscribed: true).id,
                          create(:email_list, notify_unsubscribed: false, notify_email: generate(:email)).id
                        ])
    expect { subscriber.destroy }.to change { ActionMailer::Base.deliveries.size }.by(1)
  end

  describe 'confirmations' do
    before do
      EmailSendHistory.destroy_all
      Subscriber.destroy_all
    end

    describe 'for subscribers with email' do
      it 'sends confirmation email' do
        [build(:subscriber_with_email), create(:subscriber_with_email)].each do |subscriber|
          ActionMailer::Base.deliveries = []
          subscriber.email_confirmation_task = '1'
          subscriber.save
          expect(subscriber.reload.confirmed_at).to be_nil
          expect(ActionMailer::Base.deliveries.size).to eq(1)
          EmailSendHistory.forget subscriber.email
        end
      end

      it 'confirms email' do
        [build(:subscriber_with_email), create(:subscriber_with_email)].each do |subscriber|
          ActionMailer::Base.deliveries = []
          expect(subscriber.confirmed_at).to be_nil
          subscriber.email_confirmation_task = '2'
          subscriber.save
          expect(subscriber.reload.confirmed_at).not_to be_nil
          expect(ActionMailer::Base.deliveries.size).to eq(0)
        end
      end

      it 'does not unconfirm email when adding info problem' do
        subscriber = create(:subscriber_with_confirmed_email)
        subscriber.email_problems = [build(:email_problem, email_problem_name_id: 1001, subscriber:)]
        expect(subscriber.reload.confirmed_at).not_to be_nil
      end

      it 'unconfirms email when adding temporary problem' do
        subscriber = create(:subscriber_with_confirmed_email)
        subscriber.email_problems = [build(:email_problem, email_problem_name_id: 1002, subscriber:)]
        expect(subscriber.reload.confirmed_at).to be_nil
      end

      it 'unconfirms email when adding permanent problem' do
        subscriber = create(:subscriber_with_confirmed_email)
        subscriber.email_problems = [build(:email_problem, email_problem_name_id: 1003, subscriber:)]
        expect(subscriber.reload.confirmed_at).to be_nil
      end
    end
  end

  describe 'confirmation tokens' do
    it 'get email confirmation token' do
      subscriber = create(:subscriber_with_email)
      create(:email_send_history, email: subscriber.email, subscriber_id: subscriber.id, last_value: '123456')
      expect(subscriber.email_confirmation_token).to eq('123456')

      # Suppose subscriber has changed his email address
      subscriber.update_attribute(:email, generate(:email))
      create(:email_send_history, email: subscriber.email, subscriber_id: subscriber.id, last_value: '123456')
      expect(subscriber.email_confirmation_token).to eq('123456')
    end
  end

  describe 'problems' do
    context 'email' do
      it 'has problems' do
        subscriber = create(:subscriber_with_email)
        create_list(:email_problem, 2, subscriber:)
        expect(subscriber.reload.email_problems_count).to eq(2)
      end

      it 'does not have problems' do
        subscriber = create(:subscriber_with_email)
        create_list(:email_problem_solved, 2, subscriber:)
        expect(subscriber.reload.email_problems_count).to eq(0)
      end
    end
  end
end
