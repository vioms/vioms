# frozen_string_literal: true

require 'rails_helper'

RSpec.describe SubscriberImport do
  before { FactoryBot.rewind_sequences }

  it 'is not valid without columns' do
    expect(build(:subscriber_import)).not_to be_valid
  end

  describe 'column validations' do
    context 'when valid' do
      it { expect(build(:subscriber_import, email_column: 1)).to be_valid }

      it {
        expect(build(:subscriber_import, name_column: 1, email_column: 2, phone_column: 3)).to be_valid
      }

      it { expect(build(:subscriber_import, phone_column: 1)).to be_valid }

      it {
        expect(build(:subscriber_import, email_column: 1, phone_column: 2)).to be_valid
      }

      it {
        expect(build(:subscriber_import, name_column: 1, phone_column: 2)).to be_valid
      }

      it { expect(build(:subscriber_import, email_column: 1, name_column: 2)).to be_valid }

      it { expect(build(:subscriber_import, email_column: 1, first_name_column: 2, last_name_column: 3)).to be_valid }

      it {
        expect(build(:subscriber_import, phone_column: 1, email_column: 2, first_name_column: 3,
                                         last_name_column: 4)).to be_valid
      }
    end

    context 'when not valid' do
      it { expect(build(:subscriber_import, name_column: 1)).not_to be_valid }

      it {
        expect(build(:subscriber_import, email_column: 1, name_column: 2, first_name_column: 3,
                                         last_name_column: 4)).not_to be_valid
      }

      it { expect(build(:subscriber_import, email_column: 1, first_name_column: 2)).not_to be_valid }

      it { expect(build(:subscriber_import, email_column: 1, last_name_column: 2)).not_to be_valid }

      it { expect(build(:subscriber_import, email_column: 1, name_column: 1)).not_to be_valid }

      it {
        expect(build(:subscriber_import, phone_column: 1, email_column: 2, first_name_column: 2,
                                         last_name_column: 3)).not_to be_valid
      }
    end
  end
end
