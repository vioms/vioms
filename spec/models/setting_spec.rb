# frozen_string_literal: true

require 'rails_helper'

describe Setting do
  before(:all) do
    create(:setting, name: 'boolean_value', value: '1', value_type: 'boolean')
    create(:setting, name: 'integer_value', value: '1', value_type: 'integer')
    create(:setting, name: 'string_value', value: '1', value_type: 'string')
  end

  before do
    allow(Setting).to receive(:get_setting).and_call_original
  end

  it 'is valid' do
    expect(build(:setting)).to be_valid
  end

  it 'gets boolean values' do
    Setting.put_setting('boolean_value', true)
    expect(Setting.get_setting('boolean_value')).to be(true)

    Setting.put_setting('boolean_value', false)
    expect(Setting.get_setting('boolean_value')).to be(false)
  end

  it 'gets integer value' do
    Setting.put_setting('integer_value', 100)
    expect(Setting.get_setting('integer_value')).to eq(100)
  end

  it 'shoulds put value' do
    Setting.put_setting('string_value', 'def')
    expect(Setting.get_setting('string_value')).to eq('def')
  end
end
