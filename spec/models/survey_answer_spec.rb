# frozen_string_literal: true

require 'rails_helper'

RSpec.describe SurveyAnswer do
  it 'is valid' do
    expect(build(:survey_answer)).to be_valid
  end
end
