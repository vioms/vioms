# frozen_string_literal: true

require 'rails_helper'

describe 'EmailSendHistory' do
  before { EmailSendHistory.delete_all }

  before(:all) do
    create(:subscriber_with_email, id: 1)
    create(:subscriber_with_email, id: 2)
    create(:subscriber_with_email, id: 123)
  end

  it 'checks' do
    expect(EmailSendHistory.check('mail1@example.com', 1)).to be(true)
    expect(EmailSendHistory.check('mail2@example.com', 1)).to be(true)
    expect(EmailSendHistory.check('mail1@example.com', 1)).to be(false)
    expect(EmailSendHistory.check('mail1@example.com', 2)).to be(false)
  end

  it 'is true after delete' do
    EmailSendHistory.check('mail1@example.com', 1)
    EmailSendHistory.check('mail2@example.com', 1)
    EmailSendHistory.forget('mail1@example.com')
    expect(EmailSendHistory.check('mail1@example.com', 1)).to be(true)
    expect(EmailSendHistory.check('mail2@example.com', 1)).to be(false)
  end

  it 'sets subscriber_id' do
    EmailSendHistory.check('mail1@example.com', 123)
    expect(EmailSendHistory.last.subscriber_id).to eq(123)
  end

  describe 'query' do
    it 'queries' do
      expect(EmailSendHistory.query('mail1@example.com')[0]).to be(true)
      expect(EmailSendHistory.check('mail1@example.com', 1)).to be(true)
      expect(EmailSendHistory.query('mail1@example.com')[0]).to be(false)
    end

    it 'returns last value' do
      expect(EmailSendHistory.check('mail1@example.com', 1, value: 'val')).to be(true)
      expect(EmailSendHistory.query('mail1@example.com')[1]).to eq('val')
    end
  end
end
