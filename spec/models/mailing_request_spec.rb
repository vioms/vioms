# frozen_string_literal: true

require 'rails_helper'

RSpec.describe MailingRequest do
  it 'is valid' do
    expect(create(:mailing_request)).to be_valid
  end
end
