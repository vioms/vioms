require 'rails_helper'

describe EmailList do
  it 'is valid' do
    expect(build(:email_list)).to be_valid
  end

  it 'adds full url' do
    # This test is to ensure that Ckeditor::AddFullUrl is included in ActiveRecord::Base
    Rails.application.config.action_mailer.default_url_options = { host: 'example.com' }
    email_list = build(:email_list)
    email_list.description = "<p><img alt='' src='/ckeditor_assets/pictures/image001.jpg' style='width: 250px;height: 250px;' /><br /></p>"
    email_list.valid?
    expect(email_list.description).to eq("<p><img alt='' src='http://example.com/ckeditor_assets/pictures/image001.jpg' style='width: 250px;height: 250px;' /><br /></p>")
  end

  it 'migrates users' do
    subscriber1 = create(:subscriber_with_email)
    subscriber2 = create(:subscriber_with_email)
    subscriber3 = create(:subscriber_with_email)
    email_list1 = create(:email_list, subscribers: [subscriber1, subscriber2])
    email_list2 = create(:email_list, subscribers: [subscriber2, subscriber3])
    email_list2.migrate_subscribers(email_list1.id)
    subscriber_ids = email_list2.reload.subscriber_ids
    expect(subscriber_ids).to include(subscriber1.id)
    expect(subscriber_ids).to include(subscriber2.id)
    expect(subscriber_ids).to include(subscriber3.id)
  end

  describe 'subscribers count' do
    it 'calculates and update cache' do
      email_list = create(:email_list)
      expect(email_list.reload.subscribers_count_cache).to be_nil
      create(:subscriber_with_confirmed_email, email_lists:
        [email_list])
      expect(email_list.subscribers_count).to eq(1)
      expect(email_list.subscribers_count_cache).to eq(1)
    end

    it 'resets cache after subscriber updates' do
      email_list = create(:email_list)
      email_list.update_column(:subscribers_count_cache, 1)
      create(:subscriber_with_confirmed_email, email_lists: [email_list])
      expect(email_list.reload.subscribers_count_cache).to be_nil
    end
  end
end
