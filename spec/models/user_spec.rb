# frozen_string_literal: true

require 'rails_helper'

describe User do
  it 'admin user should be valid' do
    expect(build(:user, :admin)).to be_valid
  end

  it 'user should be valid' do
    expect(build(:user)).to be_valid
  end

  it 'user with email lists should be valid' do
    expect(build(:user, :with_email_lists)).to be_valid
  end

  it 'user with sms lists should be valid' do
    expect(build(:user, :with_sms_lists)).to be_valid
  end

  it 'user with email list and sms list should be valid' do
    expect(build(:user, :with_email_and_sms_lists)).to be_valid
  end

  it 'sets and get roles' do
    user = User.new
    User::ROLES.each_key do |role|
      expect(user.respond_to?("is_#{role}?")).to be(true)
      expect(user.respond_to?("is_#{role}=")).to be(true)
    end
  end

  context 'with attachment permission' do
    it 'is invalid with zero size' do
      expect(build(:user, is_email_attachment: '1', max_attachments_size: 0)).not_to be_valid
    end

    it 'is valid with positive size' do
      expect(build(:user, is_email_attachment: '0', max_attachments_size: 512)).to be_valid
    end
  end
end
