# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Stripo::TemplateGroup do
  it { expect(build(:stripo_template_group)).to be_valid }
  it { expect(build(:stripo_template_group, :with_owner)).to be_valid }
end
