# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Stripo::File do
  it 'is valid' do
    expect(create(:stripo_file)).to be_valid
  end
end
