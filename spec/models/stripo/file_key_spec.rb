# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Stripo::FileKey do
  before { DatabaseCleaner.clean }

  it 'is valid' do
    expect(create(:stripo_file_key)).to be_valid
  end

  describe 'queries' do
    let(:stripo_file_key) { create(:stripo_file_key, plugin_id: 'ThfscSyzSdqux25NLNghafYkZO8Pqb6D') }

    specify '#stripo_key' do
      expect(stripo_file_key.stripo_key)
        .to eq('pluginId_ThfscSyzSdqux25NLNghafYkZO8Pqb6D_email_' + stripo_file_key.email_id.to_s)
    end
  end
end
