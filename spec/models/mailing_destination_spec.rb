# frozen_string_literal: true

require 'rails_helper'

RSpec.describe MailingDestination do
  it 'is valid' do
    expect(create(:mailing_destination)).to be_valid
  end
end
