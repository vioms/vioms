# frozen_string_literal: true

require 'rails_helper'

RSpec.describe MailingRequestImage do
  it 'is valid' do
    expect(create(:mailing_request_image)).to be_valid
  end
end
