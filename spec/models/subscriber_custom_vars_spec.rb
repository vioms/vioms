# frozen_string_literal: true

require 'rails_helper'

describe SubscriberCustomVar do
  it 'is valid' do
    expect(build(:subscriber_custom_var)).to be_valid
  end
end
