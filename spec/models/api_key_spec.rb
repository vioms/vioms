# frozen_string_literal: true

require 'rails_helper'

RSpec.describe APIKey do
  it 'is valid' do
    expect(build(:api_key)).to be_valid
  end
end
