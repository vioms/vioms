# frozen_string_literal: true

require 'rails_helper'

describe SubscriberUser do
  describe 'Reset password instructions' do
    before(:all) do
      @with_email = create(:subscriber_with_email)
    end

    it 'send_reset_password_instructions' do
      [
        [@with_email.email, /Вы сделали запрос на изменение пароля/, 1],
        ['something', nil, 0]
      ].each do |login, expected_text, deliveries|
        ActionMailer::Base.deliveries = []
        SubscriberUser.send_reset_password_instructions(login:)
        expect(ActionMailer::Base.deliveries.size).to eq(deliveries)
        expect(ActionMailer::Base.deliveries.first.body).to match(expected_text) if expected_text.present?
      end
    end
  end

  describe '.authenticate' do
    subject { subscriber.authenticate(password) }

    let(:subscriber) do
      create(:subscriber_user_with_email, password: 'password123', password_confirmation: 'password123')
    end

    context 'with right password' do
      let(:password) { 'password123' }

      it { is_expected.to eq(subscriber) }
    end

    context 'with wrong password' do
      let(:password) { 'wrong_password' }

      it { is_expected.to be(false) }
    end
  end
end
