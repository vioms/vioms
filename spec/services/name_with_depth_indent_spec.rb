# frozen_string_literal: true

require 'rails_helper'

describe NameWithDepthIndent do
  subject { described_class.call(record) }

  let(:record) do
    record_struct = Struct.new(:name, :depth)
    record_struct.new('The Record', depth)
  end

  context 'when depth is 0' do
    let(:depth) { 0 }

    it { is_expected.to eq('The Record') }
  end

  context 'when depth is 1' do
    let(:depth) { 1 }

    it { is_expected.to eq('|–– The Record') }
  end

  context 'when depth is 2' do
    let(:depth) { 2 }

    it { is_expected.to eq('|–––– The Record') }
  end
end
