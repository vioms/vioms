# frozen_string_literal: true

require 'rails_helper'

describe Subscriber::ConfirmEmail do
  subject(:call) { described_class.call(subscriber) }

  before { DatabaseCleaner.clean }

  let(:subscriber) { create(:subscriber_with_email) }
  let!(:esh) { create(:email_send_history, subscriber:, email: subscriber.email, last_value: '123456') }

  it 'confirms email' do
    expect { call }.to change { subscriber.reload.confirmed_at }.from(nil)
    expect(EmailSendHistory.exists?(esh.id)).to be(false)
  end

  context 'when email is already confirmed' do
    let(:subscriber) { create(:subscriber_with_confirmed_email) }

    it 'confirms email again' do
      expect { call }.to(change { subscriber.reload.confirmed_at })
      expect(EmailSendHistory.exists?(esh.id)).to be(false)
    end
  end

  describe 'problems' do
    before { create_email_problem_names }

    it 'solves temporary problems on confirmation' do
      subscriber.email_problems.create(email_problem_name_id: 1002)
      expect { call }.to change { subscriber.reload.email_problems_count }.from(1).to(0)
    end

    it 'does not solve permanent problems on confirmation' do
      subscriber.email_problems.create(email_problem_name_id: 1003)
      expect { call }.not_to change { subscriber.reload.email_problems_count }.from(1)
    end
  end
end
