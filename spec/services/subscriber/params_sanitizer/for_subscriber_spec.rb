# frozen_string_literal: true

require 'rails_helper'

describe Subscriber::ParamsSanitizer::ForSubscriber do
  describe 'email' do
    it 'subscribes to visible lists' do
      subscriber = create(:subscriber_with_email)
      new_email_list = create(:email_list, visible: true)
      expect(described_class.call({ email_list_ids: [new_email_list.id] }, subscriber:)[:email_list_ids])
        .to contain_exactly(new_email_list.id)
    end

    it 'does not subscribe to invisible lists' do
      subscriber = create(:subscriber_with_email)
      new_email_list = create(:email_list, visible: false)
      expect(described_class.call({ email_list_ids: [new_email_list.id] }, subscriber:)[:email_list_ids])
        .to be_empty
    end

    it 'does not subscribe to never visible lists' do
      subscriber = create(:subscriber_with_email)
      new_email_list = create(:email_list, visible: false, never_visible: true)
      expect(described_class.call({ email_list_ids: [new_email_list.id] }, subscriber:)[:email_list_ids])
        .to be_empty
    end

    it 'unsubscribes from visible lists' do
      subscribed_email_list1 = create(:email_list, visible: true)
      subscribed_email_list2 = create(:email_list, visible: true)
      subscriber = create(:subscriber_with_email, email_lists: [subscribed_email_list1, subscribed_email_list2])
      expect(described_class.call({ email_list_ids: [subscribed_email_list1.id] }, subscriber:)[:email_list_ids])
        .to contain_exactly(subscribed_email_list1.id)
    end

    it 'unsubscribes from invisible lists' do
      subscribed_email_list1 = create(:email_list, visible: false)
      subscribed_email_list2 = create(:email_list, visible: false)
      subscriber = create(:subscriber_with_email, email_lists: [subscribed_email_list1, subscribed_email_list2])
      expect(described_class.call({ email_list_ids: [subscribed_email_list1.id] }, subscriber:)[:email_list_ids])
        .to contain_exactly(subscribed_email_list1.id)
    end

    it 'does not unsubscribe from never visible lists' do
      subscribed_email_list1 = create(:email_list, visible: true)
      subscribed_email_list2 = create(:email_list, never_visible: true)
      subscriber = create(:subscriber_with_email, email_lists: [subscribed_email_list1, subscribed_email_list2])
      expect(described_class.call({ email_list_ids: [subscribed_email_list1.id] }, subscriber:)[:email_list_ids])
        .to contain_exactly(subscribed_email_list1.id, subscribed_email_list2.id)
    end
  end
end
