# frozen_string_literal: true

require 'rails_helper'

describe Subscriber::ParamsSanitizer::ForAdmin do
  describe 'email' do
    let(:permitted_email_list) { create(:email_list, :invisible) }
    let(:visible_email_list) { create(:email_list, :visible) }
    let(:invisible_email_list) { create(:email_list, :invisible) }
    let(:subscriber) { create(:subscriber_with_email, email_list_ids: subscription_ids) }
    let(:subscription_ids) { [] }

    context 'when admin user' do
      let(:user) { create(:user, :admin, email_lists: [permitted_email_list]) }

      describe 'subscriptions' do
        it 'subscribes to permitted list' do
          expect(described_class.call({ email_list_ids: [permitted_email_list.id] },
                                      subscriber:, user:)[:email_list_ids])
            .to contain_exactly(permitted_email_list.id)
        end

        it 'subscribes to visible list' do
          expect(described_class.call({ email_list_ids: [visible_email_list.id] },
                                      subscriber:, user:)[:email_list_ids])
            .to contain_exactly(visible_email_list.id)
        end

        it 'subscribes to invisible list' do
          expect(described_class.call({ email_list_ids: [invisible_email_list.id] },
                                      subscriber:, user:)[:email_list_ids])
            .to contain_exactly(invisible_email_list.id)
        end
      end

      describe 'unsubscriptions' do
        let(:subscription_ids) { [permitted_email_list.id, visible_email_list.id, invisible_email_list.id] }

        it 'unsubscribes from permitted list' do
          expect(described_class.call({ email_list_ids: [visible_email_list.id, invisible_email_list.id] },
                                      subscriber:, user:)[:email_list_ids])
            .to contain_exactly(visible_email_list.id, invisible_email_list.id)
        end

        it 'unsubscribes from visible list' do
          expect(described_class.call({ email_list_ids: [permitted_email_list.id, invisible_email_list.id] },
                                      subscriber:, user:)[:email_list_ids])
            .to contain_exactly(permitted_email_list.id, invisible_email_list.id)
        end

        it 'unsubscribes from invisible list' do
          expect(described_class.call({ email_list_ids: [permitted_email_list.id, visible_email_list.id] },
                                      subscriber:, user:)[:email_list_ids])
            .to contain_exactly(permitted_email_list.id, visible_email_list.id)
        end
      end
    end

    context 'when user' do
      let(:user) { create(:user, email_lists: [permitted_email_list]) }

      describe 'subscriptions' do
        it 'subscribes to permitted list' do
          expect(described_class.call({ email_list_ids: [permitted_email_list.id] },
                                      subscriber:, user:)[:email_list_ids])
            .to contain_exactly(permitted_email_list.id)
        end

        it 'subscribes to visible list' do
          expect(described_class.call({ email_list_ids: [visible_email_list.id] },
                                      subscriber:, user:)[:email_list_ids])
            .to contain_exactly(visible_email_list.id)
        end

        it 'does not subscribe to invisible list' do
          expect(described_class.call({ email_list_ids: [invisible_email_list.id] },
                                      subscriber:, user:)[:email_list_ids])
            .to be_empty
        end
      end

      describe 'unsubscriptions' do
        let(:subscription_ids) { [permitted_email_list.id, visible_email_list.id, invisible_email_list.id] }

        it 'unsubscribes from permitted list' do
          expect(described_class.call({ email_list_ids: [visible_email_list.id, invisible_email_list.id] },
                                      subscriber:, user:)[:email_list_ids])
            .to contain_exactly(visible_email_list.id, invisible_email_list.id)
        end

        it 'unsubscribes from visible list' do
          expect(described_class.call({ email_list_ids: [permitted_email_list.id, invisible_email_list.id] },
                                      subscriber:, user:)[:email_list_ids])
            .to contain_exactly(permitted_email_list.id, invisible_email_list.id)
        end

        it 'does not unsubscribe from invisible list' do
          expect(described_class.call({ email_list_ids: [permitted_email_list.id, visible_email_list.id] },
                                      subscriber:, user:)[:email_list_ids])
            .to contain_exactly(permitted_email_list.id, visible_email_list.id, invisible_email_list.id)
        end
      end
    end
  end
end
