# frozen_string_literal: true

require 'rails_helper'

describe Subscriber::Import do
  before do
    DatabaseCleaner.clean_with(:truncation)
    FactoryBot.rewind_sequences
  end

  let!(:email_list1) { create(:email_list) }
  let!(:email_list2) { create(:email_list) }
  let!(:email_list3) { create(:email_list) }
  let!(:subscriber) do
    create(:subscriber_with_confirmed_email, email: 'ivan@example.com', email_lists: [email_list1])
  end
  let!(:subscriber_unsubscribe) do
    create(:subscriber_with_phone, email: 'ivan_unsub@example.com', confirmed_at: Time.zone.now, phone: '9001002035',
                                   email_lists: [email_list1, email_list2])
  end
  let(:import) do
    create(:subscriber_import, :valid, confirm_email: true, delete_old: true, email_lists: [email_list2, email_list3])
  end

  before do
    allow(Subscriber::Import::ParseSubscribersData).to receive(:new).and_return(
      instance_double(Subscriber::Import::ParseSubscribersData, call:
        [
          %w[Ivan ivan@example.com 9001002030],
          %w[Ivan ivan@example.comzz 9001002031],
          %w[Peter peter@example.com 9001002032],
          %w[Ivan ivan2@example.comzz 9001002033]
        ])
    )

    Subscriber::Import.new(import).call
  end

  after do
    DatabaseCleaner.clean_with(:truncation)
  end

  it 'subscribes to email list' do
    expect(subscriber.reload.email_list_ids).to contain_exactly(email_list1.id, email_list2.id, email_list3.id)
  end

  it 'unsubscribes from email list' do
    expect(subscriber_unsubscribe.reload.email_list_ids).to contain_exactly(email_list1.id)
  end

  it 'counts errors' do
    expect(import.errors_count).to eq(2)
  end
end
