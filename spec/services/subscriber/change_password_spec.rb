# frozen_string_literal: true

require 'rails_helper'

describe Subscriber::ChangePassword do
  subject(:call) { described_class.call(subscriber_user, params) }

  context 'when subscriber does not have a password' do
    let(:subscriber_user) { create(:subscriber_user_with_email, password: nil) }

    context 'without new password' do
      let(:params) { { password: '', password_confirmation: '' } }

      it { expect(call).to be(false) }
    end

    context 'with new password' do
      let(:params) { { password: 'new_password', password_confirmation: 'new_password' } }

      it { expect(call).to be(true) }
    end

    context 'with new password and wrong confirmation' do
      let(:params) { { password: 'new_password', password_confirmation: 'wrong_password' } }

      it { expect(call).to be(false) }
    end
  end

  context 'when subscriber has a password' do
    let(:subscriber_user) { create(:subscriber_user_with_email, password: 'old_password') }

    context 'with correct old password' do
      context 'without new password' do
        let(:params) { { current_password: 'old_password', password: '', password_confirmation: '' } }

        it { expect(call).to be(false) }
      end

      context 'with new password' do
        let(:params) do
          { current_password: 'old_password', password: 'new_password', password_confirmation: 'new_password' }
        end

        it { expect(call).to be(true) }
      end

      context 'with new password and wrong confirmation' do
        let(:params) do
          { current_password: 'old_password', password: 'new_password', password_confirmation: 'wrong_password' }
        end

        it { expect(call).to be(false) }
      end
    end

    context 'with wrong old password' do
      context 'without new password' do
        let(:params) { { current_password: 'wrong_password', password: '', password_confirmation: '' } }

        it { expect(call).to be(false) }
      end

      context 'with new password' do
        let(:params) do
          { current_password: 'wrong_password', password: 'new_password', password_confirmation: 'new_password' }
        end

        it { expect(call).to be(false) }
      end

      context 'with new password and wrong confirmation' do
        let(:params) do
          { current_password: 'wrong_password', password: 'new_password', password_confirmation: 'wrong_password' }
        end

        it { expect(call).to be(false) }
      end
    end
  end
end
