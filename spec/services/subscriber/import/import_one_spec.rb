# frozen_string_literal: true

require 'rails_helper'

describe Subscriber::Import::ImportOne do
  before do
    DatabaseCleaner.clean_with(:truncation)
    FactoryBot.rewind_sequences
  end

  let(:email_list1) { create(:email_list) }
  let(:email_list2) { create(:email_list) }
  let(:email_list3) { create(:email_list) }
  let!(:subscriber_with_email) do
    create(:subscriber_with_confirmed_email, email: 'ivan@example.com', email_lists: [email_list1])
  end
  let!(:subscriber_with_phone) { create(:subscriber_with_phone, phone: '9001002034') }
  let(:import) do
    import = build(:subscriber_import, email_lists: [email_list2, email_list3], confirm_email: true)
    import.save!(validate: false)
    import
  end

  before do
    create(:email_list)
    create(:subscriber_with_phone, email: 'ivan2@example.com', confirmed_at: Time.zone.now, email_lists: [email_list1])
  end

  describe 'imports' do
    describe 'import of existing person' do
      let(:import_person) do
        import.subscriber_import_people.build(name: 'Ivanov Ivan', email: 'ivan@example.com', phone: '9001002030')
      end
      let(:import_one) { described_class.new(import_person) }

      it { expect { import_one.call }.not_to change(Subscriber, :count) }

      describe 'attributes' do
        let(:subscribers) { import_one.call }

        before { subscribers.each(&:reload) }

        it 'has correct attributes' do
          expect(subscribers.size).to eq(1)
          expect(subscribers.first.id).to eq(subscriber_with_email.id)
          expect(subscribers.first.email_list_ids).to contain_exactly(email_list1.id, email_list2.id, email_list3.id)
          expect(subscribers.first.confirmed_at).to be_present
          expect(subscribers.first.phone).to eq('9001002030')
          expect(import_person.error_id).to be_nil
        end
      end
    end

    describe 'import of new person with email' do
      let(:import_person) { import.subscriber_import_people.build(name: 'Ivanov Peter', email: 'peter@example.com') }
      let(:import_one) { described_class.new(import_person) }

      it { expect { import_one.call }.to change(Subscriber, :count).by(1) }

      describe 'attributes' do
        let(:subscribers) { import_one.call }

        before { subscribers.each(&:reload) }

        it 'has correct attributes' do
          expect(subscribers.size).to eq(1)
          expect(subscribers.first.email_list_ids).to contain_exactly(email_list2.id, email_list3.id)
          expect(subscribers.first.email).to eq('peter@example.com')
          expect(subscribers.first.confirmed_at).to be_present
          expect(import_person.error_id).to be_nil
        end
      end
    end

    describe 'import of new person with phone' do
      let(:import_person) { import.subscriber_import_people.build(name: 'Ivanov Peter', phone: '9001002031') }
      let(:import_one) { described_class.new(import_person) }

      it { expect { import_one.call }.to change(Subscriber, :count).by(1) }

      describe 'attributes' do
        let(:subscribers) { import_one.call }

        before { subscribers.each(&:reload) }

        it 'has correct attributes' do
          expect(subscribers.size).to eq(1)
          expect(subscribers.first.email_list_ids).to contain_exactly(email_list2.id, email_list3.id)
          expect(subscribers.first.phone).to eq('9001002031')
          expect(import_person.error_id).to be_nil
        end
      end
    end

    describe 'import of new person with email and phone' do
      let(:import_person) do
        import.subscriber_import_people.build(name: 'Ivanov Peter', email: 'peter2@example.com', phone: '9001002032')
      end
      let(:import_one) { described_class.new(import_person) }

      it { expect { import_one.call }.to change(Subscriber, :count).by(1) }

      describe 'attributes' do
        let(:subscribers) { import_one.call }

        before { subscribers.each(&:reload) }

        it 'has correct attributes' do
          expect(subscribers.size).to eq(1)
          expect(subscribers.first.email_list_ids).to contain_exactly(email_list2.id, email_list3.id)
          expect(subscribers.first.email).to eq('peter2@example.com')
          expect(subscribers.first.confirmed_at).to be_present
          expect(subscribers.first.phone).to eq('9001002032')
          expect(import_person.error_id).to be_nil
        end
      end
    end

    describe 'adding email to subscriber with phone' do
      let(:import_person) do
        import.subscriber_import_people.build(name: 'Ivanov Ivan', email: 'ivan3@example.com', phone: '9001002034')
      end
      let(:import_one) { described_class.new(import_person) }

      it { expect { import_one.call }.not_to change(Subscriber, :count) }

      describe 'attributes' do
        let(:subscribers) { import_one.call }

        before { subscribers.each(&:reload) }

        it 'has correct attributes' do
          expect(subscribers.size).to eq(1)
          expect(subscribers.first.id).to eq(subscriber_with_phone.id)
          expect(subscribers.first.email_list_ids).to contain_exactly(email_list2.id, email_list3.id)
          expect(subscribers.first.email).to eq('ivan3@example.com')
          expect(subscribers.first.confirmed_at).to be_present
          expect(subscribers.first.phone).to eq('9001002034')
          expect(import_person.error_id).to be_nil
        end
      end
    end

    describe 'merging of subscribers and email subscriptions' do
      let(:import_person) do
        import.subscriber_import_people.build(name: 'Ivanov Ivan', email: 'ivan@example.com', phone: '9001002034')
      end
      let(:import_one) { described_class.new(import_person) }

      it { expect { import_one.call }.not_to change(Subscriber, :count) }

      describe 'attributes' do
        let(:subscribers) { import_one.call }

        before { subscribers.each(&:reload) }

        it 'has correct attributes' do
          expect(subscribers.size).to eq(2)
          expect(subscribers.first.email_list_ids).to contain_exactly(email_list1.id, email_list2.id, email_list3.id)
          expect(subscribers.first.email).to eq('ivan@example.com')
          expect(subscribers.first.confirmed_at).to be_present
          expect(subscribers.first.phone).to be_nil
          expect(subscribers.last.email_list_ids).to contain_exactly(email_list2.id, email_list3.id)
          expect(subscribers.last.email).to be_nil
          expect(subscribers.last.phone).to eq('9001002034')
          expect(import_person.error_id).to be_nil
        end
      end
    end

    describe 'when phone is different' do
      let(:import_person) do
        import.subscriber_import_people.build(name: '', email: 'ivan2@example.com', phone: '9001002036')
      end
      let(:import_one) { described_class.new(import_person) }

      it { expect { import_one.call }.to change(Subscriber, :count).by(1) }

      describe 'attributes' do
        let(:subscribers) { import_one.call }

        before { subscribers.each(&:reload) }

        it 'has correct attributes' do
          expect(subscribers.size).to eq(2)
          expect(subscribers.first.email_list_ids).to contain_exactly(email_list1.id, email_list2.id, email_list3.id)
          expect(subscribers.first.email).to eq('ivan2@example.com')
          expect(subscribers.first.confirmed_at).to be_present
          expect(subscribers.first.phone).to eq('9091110001')
          expect(subscribers.last.email_list_ids).to contain_exactly(email_list2.id, email_list3.id)
          expect(subscribers.last.email).to be_nil
          expect(subscribers.last.phone).to eq('9001002036')
          expect(import_person.error_id).to be_nil
          expect(import_person.error_id).to be_nil
        end
      end
    end
  end

  describe 'errors' do
    describe 'does not import bad phone' do
      let(:import_person) do
        import.subscriber_import_people.build(name: '', email: '', phone: '900100203')
      end
      let(:import_one) { described_class.new(import_person) }

      it { expect { import_one.call }.not_to change(Subscriber, :count) }

      describe 'attributes' do
        let(:subscribers) { import_one.call }

        it 'sets error' do
          expect(subscribers).to be_empty
          expect(import_person.error_id).to eq(SubscriberImport::ERROR_IDS[:bad_phone])
        end
      end
    end

    describe 'does not import if no email and no phone' do
      let(:import_person) do
        import.subscriber_import_people.build(name: 'Name', email: '', phone: '')
      end
      let(:import_one) { described_class.new(import_person) }

      it { expect { import_one.call }.not_to change(Subscriber, :count) }

      describe 'attributes' do
        let(:subscribers) { import_one.call }

        it 'sets error' do
          expect(subscribers).to be_empty
          expect(import_person.error_id).to eq(SubscriberImport::ERROR_IDS[:no_data])
        end
      end
    end

    describe 'does not import if not valid' do
      let(:import_person) do
        import.subscriber_import_people.build(name: 'Name', email: 'ivan@example.comzz', phone: '')
      end
      let(:import_one) { described_class.new(import_person) }

      it { expect { import_one.call }.not_to change(Subscriber, :count) }

      describe 'attributes' do
        let(:subscribers) { import_one.call }

        it 'sets error' do
          expect(subscribers).to be_empty
          expect(import_person.error_id).to eq(SubscriberImport::ERROR_IDS[:not_valid])
        end
      end
    end
  end
end
