# frozen_string_literal: true

require 'rails_helper'

describe Subscriber::Import::ParseSubscribersData do
  subject { described_class.new(import).call }

  let(:result_hash) do
    [['Ivan Ivanov', 'ivan@example.com', '9001002030'],
     ['Peter Ivanov', 'peter@example.com', '9001002031']]
  end

  context 'when subscribers are with full name' do
    let(:import) do
      build(:subscriber_import, name_column: 1, email_column: 2, phone_column: 3,
                                subscribers_data: "Ivan Ivanov, ivan@example.com, 9001002030
Peter Ivanov, peter@example.com, 9001002031")
    end

    it { is_expected.to eq(result_hash) }
  end

  context 'when subscribers are with splitted name' do
    let(:import) do
      build(:subscriber_import, first_name_column: 1, last_name_column: 2, email_column: 3, phone_column: 4,
                                subscribers_data: 'Ivanov, Ivan, ivan@example.com, 9001002030
Ivanov, Peter, peter@example.com, 9001002031')
    end

    it { is_expected.to eq(result_hash) }
  end
end
