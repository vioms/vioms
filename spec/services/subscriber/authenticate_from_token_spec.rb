# frozen_string_literal: true

require 'rails_helper'

describe Subscriber::AuthenticateFromToken do
  subject(:authenticator) { described_class.new(email, auth_token) }

  let(:email) { 'subscriber@example.com' }
  let!(:subscriber) do
    create(:subscriber, email: 'subscriber@example.com', authentication_token: 'mkCtrhNxPAqU8GH4Jpqz')
  end

  after { DatabaseCleaner.clean }

  context 'with correct auth_token' do
    let(:auth_token) { 'mkCtrhNxPAqU8GH4Jpqz' }

    specify do
      expect(authenticator.call).to be_truthy
      expect(authenticator.subscriber).to eq(subscriber)
    end
  end

  context 'with incorrect auth_token' do
    let(:auth_token) { 'bydzrvY1JWY3xztyuQUF' }

    specify do
      expect(authenticator.call).to be_falsey
      expect(authenticator.subscriber).to eq(subscriber)
    end
  end

  context 'with incorrect email' do
    let(:email) { 'subscriber2@example.com' }
    let(:auth_token) { 'mkCtrhNxPAqU8GH4Jpqz' }

    specify do
      expect(authenticator.call).to be_falsey
      expect(authenticator.subscriber).to be_nil
    end
  end
end
