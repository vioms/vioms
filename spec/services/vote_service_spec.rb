require 'rails_helper'

describe VoteService do
  let(:subscriber)    { create(:subscriber_with_email) }
  let(:answer)        { create(:answer) }

  it 'votes' do
    expect(
      described_class.vote(subscriber:, answer:)
    ).to be(true)
  end

  it 'does not vote if subscriber is nil' do
    expect(
      described_class.vote(subscriber: nil, answer:)
    ).to be(false)
  end

  it 'does not vote if answer is nil' do
    expect(
      described_class.vote(subscriber:, answer: nil)
    ).to be(false)
  end

  it 'creates subscriber_answer' do
    expect do
      described_class.vote(answer:, subscriber:)
    end.to(change(SubscriberAnswer, :count).by(1))
  end

  it 'does not create subscriber_answer twice' do
    create(:subscriber_answer, subscriber:, answer:,
                               poll: answer.poll)
    expect do
      described_class.vote(answer:, subscriber:)
    end.not_to(change(SubscriberAnswer, :count))
  end

  it 'updates subscriber_answer' do
    poll = create(:poll)
    answer = create(:answer, poll:)
    new_answer = create(:answer, poll:)
    create(:subscriber_answer, subscriber:, answer:,
                               poll: answer.poll)
    described_class.vote(answer: new_answer, subscriber:)
    expect(SubscriberAnswer.last.answer).to eq(new_answer)
  end

  it 'creates subscriber_answer for a different poll' do
    new_answer = create(:answer)
    create(:subscriber_answer, subscriber:, answer:,
                               poll: answer.poll)
    expect do
      described_class.vote(answer: new_answer, subscriber:)
    end.to(change(SubscriberAnswer, :count).by(1))
  end
end
