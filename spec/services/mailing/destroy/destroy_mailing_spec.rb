# frozen_string_literal: true

require 'rails_helper'

describe Mailing::Destroy::DestroyMailing do
  subject { described_class.call(mailing:) }

  before { DatabaseCleaner.clean }

  let!(:mailing) { create(:mailing, stripo_file_key:) }

  context 'with stripo file key' do
    let(:stripo_file_key) { create(:stripo_file_key) }

    it { expect { subject }.to change { stripo_file_key.reload.mailing_id }.to(nil) }
  end

  context 'without stripo file key' do
    let(:stripo_file_key) { nil }

    it { is_expected.to be_a_success }
    it { expect { subject }.to change(Mailing, :count).by(-1) }
  end
end
