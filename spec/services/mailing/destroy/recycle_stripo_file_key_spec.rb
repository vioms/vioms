# frozen_string_literal: true

require 'rails_helper'

describe Mailing::Destroy::RecycleStripoFileKey do
  subject { described_class.call(mailing:) }

  before { DatabaseCleaner.clean }

  let(:stripo_file_key) { create(:stripo_file_key, :with_files) }
  let!(:mailing) { create(:mailing, :for_stripo, stripo_file_key:, sent:) }

  context 'when mailing was sent' do
    let(:sent) { true }

    it { is_expected.to be_a_success }

    it 'does not destroy all files' do
      expect { subject }.not_to(change { stripo_file_key.files.count })
    end

    it 'does not make file_key reusable' do
      expect { subject }.not_to change(stripo_file_key, :reusable)
    end

    it 'does not nullify mailing' do
      expect { subject }.not_to change(stripo_file_key, :mailing)
    end
  end

  context 'when mailing was not sent' do
    let(:sent) { false }

    it { is_expected.to be_a_success }

    it 'destroys all files' do
      expect { subject }.to change { stripo_file_key.files.count }.from(2).to(0)
    end

    it 'makes file_key reusable' do
      expect { subject }.to change(stripo_file_key, :reusable).from(false).to(true)
    end

    it 'nullifies mailing' do
      expect { subject }.to change(stripo_file_key, :mailing).to(nil)
    end
  end
end
