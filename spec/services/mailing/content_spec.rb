# frozen_string_literal: true

require 'rails_helper'

describe Mailing::Content do
  subject { described_class.call(mailing) }

  let(:email_list) { create(:email_list) }
  let(:stripo_template_id) { nil }
  let(:mailing) do
    create(:mailing, subject: 'The subject', html_content:, email_list:,
                     editor_type:, stripo_template_id:)
  end

  describe 'title tag replacement' do
    let(:editor_type) { 'none' }

    context 'when downcased tags' do
      let(:html_content) { '<html><head><title>No title</title></head></html>' }

      it { is_expected.to eq("<html><head><title>The subject</title></head></html>\n[FOOTER]") }
    end

    context 'when downcased tags' do
      let(:html_content) { '<HTML><HEAD><TITLE>No title</TITLE></HEAD></HTML>' }

      it { is_expected.to eq("<HTML><HEAD><TITLE>The subject</TITLE></HEAD></HTML>\n[FOOTER]") }
    end
  end

  describe '[FOOTER]' do
    context 'without [FOOTER]' do
      let(:html_content) { 'the content' }

      context 'when none' do
        let(:editor_type) { 'none' }

        it { is_expected.to include('[FOOTER]') }
      end

      context 'when ckeditor' do
        let(:editor_type) { 'ckeditor' }

        it { is_expected.to include('[FOOTER]') }
      end

      context 'when template' do
        let(:editor_type) { 'template' }
        let(:email_list) { create(:email_list, template: 'the template') }
        let(:html_template) { nil }

        it { is_expected.to include('[FOOTER]') }
      end
    end

    context 'with [FOOTER]' do
      let(:html_content) { 'the content [FOOTER]' }

      context 'when none' do
        let(:editor_type) { 'none' }

        it { is_expected.to include('[FOOTER]') }
      end

      context 'when ckeditor' do
        let(:editor_type) { 'ckeditor' }

        it { is_expected.to include('[FOOTER]') }
      end

      context 'when template' do
        let(:editor_type) { 'template' }
        let(:email_list) { create(:email_list, template: 'the template') }
        let(:html_template) { nil }

        it { is_expected.to include('[FOOTER]') }
      end

      context 'when stripo' do
        let(:editor_type) { 'stripo' }
        let(:stripo_template_id) { 1 }

        before { expect(Stripo::Compress).to receive(:call).and_return('the content [FOOTER]') }

        it { is_expected.to include('[FOOTER]') }
      end
    end
  end
end
