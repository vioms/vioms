# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Mailing::Deliver::SendPushNotifications do
  before_all { DatabaseCleaner.clean }

  let_it_be(:all_subscribers) do
    [
      create(:subscriber_with_confirmed_email),
      create(:subscriber_with_confirmed_email, :with_legacy_mobile_token),
      create(:subscriber_with_confirmed_email, :with_android_mobile_token),
      create(:subscriber_with_confirmed_email, :with_ios_mobile_token)
    ]
  end
  let_it_be(:email_list) do
    create(:email_list, subscribers: all_subscribers)
  end
  let_it_be(:mailing) { create(:mailing, email_list:) }

  describe '.message' do
    subject { JSON.parse(described_class.new(all_subscribers:, mailing:, email_list:).message, symbolize_names: true) }

    let(:expected_message) do
      {
        operation: 'push_notifications',
        params: {
          title: 'Email list name',
          body: 'Subject',
          email_list_id: 1,
          mailing_id: 1,
          tokens: %w[
            dS1-za1vFts:APA91bHsJzjTtZ5uOwT52VLe1F3QfXPbK2a29VXvOj1PyZfiJz6V4zW5AwOp_qbUrmhR1GHfYKjDR5h3qxs_pOzN3g7h-TShCdUogmhZzUzwFS1gJjRl5Kx5Q3bq-32yH4dEdDh7-yZ
            eX7y_aGh123:APA91bHbQPiNpZg8XKjQ8vQW8iSMLGp4nRNd4qZJH8s2YJ7m3UkJqH6Bh3_fHp2G3gMvC0s7Q3L9Fs4_RkQq3LXq0dV5UzAb7KhUnlF8Pc2Zxr3p9tYsZy_jD7l8pEtCyDQf9OPfRuTX
          ]
        }
      }
    end

    it { is_expected.to eq(expected_message) }
  end
end
