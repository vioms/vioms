# frozen_string_literal: true

require 'rails_helper'

describe Mailing::Create::SetContentHeaders do
  subject(:call) { described_class.call(mailing:) }

  let(:mailing) { create(:mailing, :for_stripo, editor_type:) }

  context 'when editor_type is none' do
    let(:editor_type) { EmailList::EDITOR_TYPES[:none] }

    it { expect { call }.not_to change(mailing, :enable_content_headers).from(false) }
  end

  context 'when editor_type is ckeditor' do
    let(:editor_type) { EmailList::EDITOR_TYPES[:ckeditor] }

    it { expect { call }.to change(mailing, :enable_content_headers).to(true) }
  end

  context 'when editor_type is template' do
    let(:editor_type) { EmailList::EDITOR_TYPES[:template] }

    it { expect { call }.to change(mailing, :enable_content_headers).to(true) }
  end

  context 'when editor_type is stripo' do
    let(:editor_type) { EmailList::EDITOR_TYPES[:stripo] }

    it { expect { call }.not_to change(mailing, :enable_content_headers).from(false) }
  end
end
