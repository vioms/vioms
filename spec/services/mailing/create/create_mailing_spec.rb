# frozen_string_literal: true

require 'rails_helper'

describe Mailing::Create::CreateMailing do
  subject { described_class.call(params:) }

  context 'with valid params' do
    let(:params) { build(:mailing).attributes }

    it { is_expected.to be_a_success }
  end

  context 'with not valid params' do
    let(:params) { build(:mailing, :for_creating, email_list_id: nil).attributes }

    it { is_expected.to be_a_failure }
  end
end
