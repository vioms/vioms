# frozen_string_literal: true

require 'rails_helper'

describe Mailing::Create::CreateStripoFileKey do
  subject { described_class.call(mailing:) }

  context 'with ckeditor editor type' do
    let(:mailing) { create(:mailing) }

    it { is_expected.to be_a_success }
  end

  context 'with stripo editor type' do
    let(:mailing) { create(:mailing, :for_stripo) }

    before do
      expect(Setting).to receive('get_setting').with('stripo_app_id').and_return('app_id')
    end

    after { DatabaseCleaner.clean }

    context 'without a reusable file key' do
      it { is_expected.to be_a_success }

      it 'creates a stripo file key' do
        expect { subject }.to change(Stripo::FileKey, :count).by(1)
      end
    end

    context 'with a reusable file key' do
      let!(:stripo_file_key) { create(:stripo_file_key, plugin_id: 'app_id', reusable: true) }

      it { is_expected.to be_a_success }

      it 'reuses a stripo file key' do
        expect { subject }.not_to change(Stripo::FileKey, :count)
      end

      it 'relates to stripo file key' do
        expect { subject }.to change(mailing, :stripo_file_key).from(nil).to(stripo_file_key)
      end
    end
  end
end
