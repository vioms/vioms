# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Mailing::Render do
  subject { described_class.call(mailing) }

  let(:mailing) { create(:mailing, rendered_html:) }

  context 'without rendered_html' do
    let(:rendered_html) { nil }

    before do
      expect(Mailing::Content).to receive(:call).and_return('rendered html')
    end

    it { is_expected.to eq('rendered html') }
  end

  context 'with rendered_html' do
    let(:rendered_html) { 'rendered html' }

    it { is_expected.to eq('rendered html') }
  end
end
