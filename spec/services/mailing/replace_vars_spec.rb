# frozen_string_literal: true

require 'rails_helper'

describe Mailing::ReplaceVars do
  let(:editor_type) { 'ckeditor' }
  let(:email_list) { create(:email_list) }
  let(:html_content) { 'this is the content' }
  let(:mailing) do
    create(:mailing, html_content:, email_list:, editor_type:)
  end
  let(:subscriber) do
    instance_double(Subscriber, id: 8, name: 'Joe Doe', email: 'mail@example.org', authentication_token: 'token')
  end

  describe 'standard vars' do
    it 'replaces vars' do
      content = described_class.call(content: 'a [SUBSCRIBER_ID] b [NAME] c', mailing:, subscriber:)
      expect(content).to eq('a 8 b Joe Doe c')
    end
  end

  describe 'custom vars' do
    let(:email_list) { create(:email_list, use_custom_vars: true) }
    let(:wrong_email_list) { create(:email_list, use_custom_vars: true) }
    let(:mailing) { create(:mailing, email_list:) }

    it 'replaces custom vars' do
      custom_vars = []
      wrong_custom_vars = []
      5.times { |i| custom_vars[i] = create(:email_list_custom_var, name: "var#{i}") }
      5.times { |i| wrong_custom_vars[i] = create(:email_list_custom_var, name: "wrong_var#{i}") }
      subscribers = []
      Subscriber.transaction do
        5.times do |i|
          subscribers[i] = create(:subscriber_with_email)
          5.times do |j|
            create(:subscriber_custom_var, subscriber: subscribers[i], email_list_custom_var: custom_vars[j],
                                           value: "value#{i}#{j}")
            create(:subscriber_custom_var, subscriber: subscribers[i], email_list_custom_var: wrong_custom_vars[j],
                                           value: "wrong_value#{i}#{j}")
          end
        end
      end
      email_list.subscribers = subscribers
      email_list.email_list_custom_vars = custom_vars
      wrong_email_list.email_list_custom_vars = wrong_custom_vars
      i = 0
      email_list.subscribers.include_custom_vars.each do |subscriber|
        content = described_class.call(content: 'a [NAME] b [var0][var1] c [var2] d [var3] e [var4] f [wrong_var0]',
                                       mailing:, subscriber:)
        expect(content).to eq("a Joe Doe b value#{i}0value#{i}1 c value#{i}2 d value#{i}3 e value#{i}4 f [wrong_var0]")
        i += 1
      end
    end
  end
end
