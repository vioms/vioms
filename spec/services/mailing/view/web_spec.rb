# frozen_string_literal: true

require 'rails_helper'

describe Mailing::View::Web do
  subject { described_class.call(rendered_html) }

  let(:rendered_html) { 'the content [FOOTER]' }

  before { DatabaseCleaner.clean }

  it { is_expected.not_to include('[FOOTER]') }
  it { is_expected.not_to include('/unsubscriptions/new') }
  it { is_expected.not_to include('/my_settings/edit') }
  it { is_expected.not_to include('%5BAUTH_TOKEN%5D') }
  it { is_expected.not_to include('%5BSUBSCRIBER_EMAIL%5D') }
end
