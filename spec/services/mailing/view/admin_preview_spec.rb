# frozen_string_literal: true

require 'rails_helper'

describe Mailing::View::AdminPreview do
  subject { described_class.call(content: rendered_html, mailing:, email_list:) }

  let(:rendered_html) { 'the content [FOOTER]' }
  let(:mailing) { create(:mailing, rendered_html:) }
  let(:email_list) { create(:email_list) }

  before { DatabaseCleaner.clean }

  it { is_expected.not_to include('[FOOTER]') }
  it { is_expected.to include('%5BAUTH_TOKEN%5D') }
  it { is_expected.to include('%5BSUBSCRIBER_EMAIL%5D') }
end
