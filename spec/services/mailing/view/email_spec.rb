# frozen_string_literal: true

require 'rails_helper'

describe Mailing::View::Email do
  subject do
    described_class.call(content: rendered_html, mailing:, email_list:, subscriber:)
  end

  let(:rendered_html) { 'the content [FOOTER]' }
  let(:mailing) { create(:mailing, rendered_html:) }
  let(:email_list) { create(:email_list, never_visible:) }
  let(:subscriber) do
    create(:subscriber_with_email, email: 'mail@example.org', authentication_token: 'token', email_lists: [email_list])
  end

  before { DatabaseCleaner.clean }

  context 'when not never_visible' do
    let(:never_visible) { false }

    it { is_expected.not_to include('[FOOTER]') }
    it { is_expected.to include('/unsubscriptions/new?auth_token=token&amp;email=mail%40example.org') }
    it { is_expected.to include('/my_settings/edit?auth_token=token&amp;email=mail%40example.org') }
  end

  context 'when never_visible' do
    let(:never_visible) { true }

    it { is_expected.not_to include('[FOOTER]') }
    it { is_expected.not_to include('/unsubscriptions/new') }
    it { is_expected.not_to include('/my_settings/edit') }
  end

  describe '.tracking_pixel' do
    subject { described_class.tracking_pixel(mailing:, subscriber:) }

    let(:email) { 'subscriber1@example.com' }
    let(:auth_token) { 'mkCtrhNxPAqU8GH4Jpqz' }
    let(:mailing) { create(:mailing) }
    let(:subscriber) { create(:subscriber_with_email, email:, authentication_token: auth_token) }
    let(:result) do
      "<img src=\"http://vioms.example.com/mailings/#{mailing.id}/track_email.gif?email=subscriber1%40example.com&" \
        'auth_token=mkCtrhNxPAqU8GH4Jpqz">'
    end

    it { is_expected.to eq(result) }
  end
end
