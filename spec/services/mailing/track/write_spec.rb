# frozen_string_literal: true

require 'rails_helper'

describe Mailing::Track::Write do
  let(:call) { described_class.call(mailing:, subscriber:) }

  let!(:mailing) { create(:mailing, deliver_at: 1.minute.from_now) }
  let!(:subscriber) { create(:subscriber_with_email) }

  after do
    DatabaseCleaner.clean
  end

  context 'without view' do
    it { expect { call }.to change(MailingView, :count).from(0).to(1) }
  end

  context 'with existing view' do
    let!(:mailing_view) { create(:mailing_view, mailing:, subscriber:, count: 2) }

    it { expect { call }.not_to change(MailingView, :count) }
    it { expect { call }.to change { mailing_view.reload.count }.from(2).to(3) }
  end
end
