# frozen_string_literal: true

require 'rails_helper'

describe Mailing::Track::FromEmailAndToken do
  let(:call) do
    described_class.call(mailing:, email: 'subscriber@example.com', auth_token: 'mkCtrhNxPAqU8GH4Jpqz')
  end

  let!(:mailing) { create(:mailing, deliver_at: 1.minute.from_now) }
  let(:auth_token) { 'mkCtrhNxPAqU8GH4Jpqz' }

  before do
    allow(Mailing::Track::Write).to receive(:call)
    Timecop.freeze
  end

  after do
    Timecop.return
    DatabaseCleaner.clean
  end

  context 'without subscriber' do
    context 'with valid token' do
      it 'does not write' do
        call
        expect(Mailing::Track::Write).not_to have_received(:call)
      end
    end

    context 'with invalid token' do
      let(:auth_token) { 'bydzrvY1JWY3xztyuQUF' }

      it 'does not write' do
        call
        expect(Mailing::Track::Write).not_to have_received(:call)
      end
    end
  end

  context 'with subscriber' do
    before { create(:subscriber, email: 'subscriber@example.com', authentication_token: auth_token) }

    context 'with valid token' do
      it 'writes' do
        call
        expect(Mailing::Track::Write).to have_received(:call).once
      end
    end

    context 'with invalid token' do
      let(:auth_token) { 'bydzrvY1JWY3xztyuQUF' }

      it 'does not write' do
        call
        expect(Mailing::Track::Write).not_to have_received(:call)
      end
    end
  end
end
