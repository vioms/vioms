# frozen_string_literal: true

require 'rails_helper'

describe Mailing::Content::None do
  subject { described_class.call(mailing) }

  let(:email_list) do
    create(:email_list, default_editor_type: 'none', content_header: 'the header | ',
                        content_footer: ' | the footer')
  end
  let(:mailing) do
    Mailing::Create.call(params: { email_list:, html_content: 'the content', editor_type: 'none' }).mailing
  end

  it { is_expected.not_to include('<!DOCTYPE html>') }
  it { is_expected.not_to include('font-family:sans-serif, Arial, Verdana, "Trebuchet MS"') }
  it { is_expected.to include('the content') }
  it { is_expected.not_to include('the header') }
  it { is_expected.not_to include('the footer') }

  context 'with content headers' do
    before { mailing.update_column(:enable_content_headers, true) }

    it { is_expected.to include('the header | the content | the footer') }
  end
end
