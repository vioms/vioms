# frozen_string_literal: true

require 'rails_helper'

describe Mailing::Content::Stripo do
  subject { described_class.call(mailing) }

  before { allow(Setting).to receive('get_setting').with('stripo_app_id').and_return('app_id') }

  let(:email_list) do
    create(:email_list, default_editor_type: 'stripo', content_header: 'the header | ', content_footer: ' | the footer')
  end
  let(:stripo_template) { create(:stripo_template, html: 'the content', css: 'css') }
  let(:mailing) do
    Mailing::Create.call(params: { email_list:, editor_type: 'stripo', rendered_html: nil,
                                   stripo_template_id: stripo_template.id })
                   .mailing
  end

  context 'without content headers' do
    before do
      expect(Stripo::Compress).to receive(:call).with(html: 'the content', css: 'css')
                                                .and_return('<!DOCTYPE html><html>the content</html>')
    end

    it { is_expected.to eq('<!DOCTYPE html><html>the content</html>') }
    # Make sure Ckeditor wrapper is not added
    it { is_expected.not_to include('font-family:sans-serif, Arial, Verdana, "Trebuchet MS"') }
  end

  context 'with content headers' do
    before { mailing.update_column(:enable_content_headers, true) }

    before do
      expect(Stripo::Compress).to receive(:call)
        .with(html: 'the header | the content | the footer', css: 'css')
        .and_return('<!DOCTYPE html><html>the header | the content | the footer</html>')
    end

    it { is_expected.to eq('<!DOCTYPE html><html>the header | the content | the footer</html>') }
  end
end
