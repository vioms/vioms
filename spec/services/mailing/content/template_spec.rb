# frozen_string_literal: true

require 'rails_helper'

describe Mailing::Content::Template do
  subject { described_class.call(mailing) }

  context 'without vars' do
    let(:email_list) do
      create(:email_list, template: 'the template', default_editor_type: 'template', content_header: 'the header | ',
                          content_footer: ' | the footer')
    end
    let(:mailing) do
      Mailing::Create.call(params: { email_list:, html_content: '', editor_type: 'template' }).mailing
    end

    it { is_expected.to include('<!DOCTYPE html>') }
    it { is_expected.to include('font-family:sans-serif, Arial, Verdana, "Trebuchet MS"') }
    it { is_expected.to include('the header | the template | the footer') }

    context 'without content headers' do
      before { mailing.update_column(:enable_content_headers, false) }

      it { is_expected.to include('<!DOCTYPE html>') }
      it { is_expected.to include('font-family:sans-serif, Arial, Verdana, "Trebuchet MS"') }
      it { is_expected.to include('the template') }
      it { is_expected.not_to include('the header') }
      it { is_expected.not_to include('the footer') }
    end
  end

  context 'with vars' do
    let(:var_name) { create(:email_list_var_name, name: 'NAME', description: 'Name') }
    let(:var_text) { create(:email_list_var_name, name: 'TEXT', description: 'Text') }
    let(:var_author) { create(:email_list_var_name, name: 'AUTHOR', description: 'Author') }
    let(:email_list) do
      create(:email_list, template: 'Quote [NAME], [TEXT]. From [AUTHOR]. Bye', default_editor_type: 'template',
                          email_list_var_names: [var_name, var_text, var_author], content_header: 'the header | ',
                          content_footer: ' | the footer')
    end
    let(:mailing) do
      Mailing::Create.call(params:
        {
          email_list:, html_content: '', editor_type: 'template', email_list_vars:
          [
            create(:email_list_var, email_list_var_name: var_name, value: 'Maharaj'),
            create(:email_list_var, email_list_var_name: var_text, value: 'this is the text'),
            create(:email_list_var, email_list_var_name: var_author, value: 'Joe Doe')
          ]
        }).mailing
    end

    it { is_expected.to include('<!DOCTYPE html>') }
    it { is_expected.to include('font-family:sans-serif, Arial, Verdana, "Trebuchet MS"') }
    it { is_expected.to include('the header | Quote Maharaj, this is the text. From Joe Doe. Bye | the footer') }

    context 'without content headers' do
      before { mailing.update_column(:enable_content_headers, false) }

      it { is_expected.to include('<!DOCTYPE html>') }
      it { is_expected.to include('font-family:sans-serif, Arial, Verdana, "Trebuchet MS"') }
      it { is_expected.to include('Quote Maharaj, this is the text. From Joe Doe. Bye') }
      it { is_expected.not_to include('the header') }
      it { is_expected.not_to include('the footer') }
    end
  end
end
