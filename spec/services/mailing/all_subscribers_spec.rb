# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Mailing::AllSubscribers do
  before_all { DatabaseCleaner.clean }

  subject { described_class.new(mailing:, email_list:, exclude_email_list_ids:).call.map(&:id) }

  let_it_be(:subscribers) { create_list(:subscriber_with_confirmed_email, 7) }
  let_it_be(:email_lists) do
    [
      create(:email_list, subscribers: subscribers[0..2]),
      create(:email_list, subscribers: subscribers[1..4]),
      create(:email_list, subscribers: subscribers[1..5]),
      create(:email_list, subscribers: subscribers[0..7]),
      create(:email_list, subscribers: [subscribers[2]]),
      create(:email_list, subscribers: [subscribers[3]])
    ]
  end
  let_it_be(:mailing) do
    create(:mailing, email_list: email_lists[0],
                     included_email_lists: email_lists[1..2],
                     excluded_email_lists: email_lists[4..5])
  end

  describe 'email_lists[0]' do
    let(:exclude_email_list_ids) { [] }
    let(:email_list) { email_lists[0] }

    it { is_expected.to contain_exactly(subscribers[0].id, subscribers[1].id) }
  end

  describe 'email_lists[1]' do
    let(:exclude_email_list_ids) { [email_lists[0].id] }
    let(:email_list) { email_lists[1] }

    it { is_expected.to contain_exactly(subscribers[4].id) }
  end

  describe 'email_lists[2]' do
    let(:exclude_email_list_ids) { email_lists[0..1].map(&:id) }
    let(:email_list) { email_lists[2] }

    it { is_expected.to contain_exactly(subscribers[5].id) }
  end
end
