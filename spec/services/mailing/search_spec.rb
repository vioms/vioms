# frozen_string_literal: true

require 'rails_helper'

describe Mailing::Search do
  subject(:call) { described_class.call(mailings, email_list_id, search) }

  before { DatabaseCleaner.clean }

  let(:email_list1) { create(:email_list, :visible) }
  let(:email_list2) { create(:email_list, :visible) }
  let(:email_list_invisible) { create(:email_list, :invisible) }
  let!(:mailing1) do
    create(:mailing, email_list: email_list1, plain_content: 'Продам квартиру', published: true, sent: true,
                     deliver_at: 1.hour.ago)
  end
  let!(:mailing2) do
    create(:mailing, email_list: email_list2, plain_content: 'Продам дом', published: true, sent: true,
                     deliver_at: 2.hours.ago)
  end
  let!(:mailing_invisible) do
    create(:mailing, email_list: email_list_invisible, subject: 'Продам квартиру', published: true, sent: true,
                     deliver_at: 3.hours.ago)
  end
  let(:mailings) { MailingPolicy::Scope.new({ user: nil, subscriber: }, Mailing).resolve }
  let(:search) { 'квартира' }

  context 'as guest' do
    let(:subscriber) { nil }

    context 'All email lists' do
      let(:email_list_id) { nil }

      context 'First keyword' do
        let(:search) { 'квартира' }

        specify 'searching' do
          expect(call).to include(mailing1)
          expect(call).not_to include(mailing2)
          expect(call).not_to include(mailing_invisible)
        end
      end

      context 'Second keyword' do
        let(:search) { 'дом' }

        specify 'searching' do
          expect(call).not_to include(mailing1)
          expect(call).to include(mailing2)
          expect(call).not_to include(mailing_invisible)
        end
      end
    end

    context 'First email lists' do
      let(:email_list_id) { email_list1.id }

      context 'First keyword' do
        let(:search) { 'квартира' }

        specify 'searching' do
          expect(call).to include(mailing1)
          expect(call).not_to include(mailing2)
          expect(call).not_to include(mailing_invisible)
        end
      end

      context 'Second keyword' do
        let(:search) { 'дом' }

        specify 'searching' do
          expect(call).not_to include(mailing1)
          expect(call).not_to include(mailing2)
          expect(call).not_to include(mailing_invisible)
        end
      end
    end
  end

  context 'as subscriber' do
    let(:subscriber) { create(:subscriber_with_email, email_lists: [email_list_invisible]) }

    context 'All email lists' do
      let(:email_list_id) { nil }

      context 'First keyword' do
        let(:search) { 'квартира' }

        specify 'searching' do
          expect(call).to include(mailing1)
          expect(call).not_to include(mailing2)
          expect(call).to include(mailing_invisible)
        end
      end

      context 'Second keyword' do
        let(:search) { 'дом' }

        specify 'searching' do
          expect(call).not_to include(mailing1)
          expect(call).to include(mailing2)
          expect(call).not_to include(mailing_invisible)
        end
      end
    end

    context 'First email lists' do
      let(:email_list_id) { email_list1.id }

      context 'First keyword' do
        let(:search) { 'квартира' }

        specify 'searching' do
          expect(call).to include(mailing1)
          expect(call).not_to include(mailing2)
          expect(call).not_to include(mailing_invisible)
        end
      end

      context 'Second keyword' do
        let(:search) { 'дом' }

        specify 'searching' do
          expect(call).not_to include(mailing1)
          expect(call).not_to include(mailing2)
          expect(call).not_to include(mailing_invisible)
        end
      end
    end
  end
end
