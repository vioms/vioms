# frozen_string_literal: true

require 'rails_helper'

describe Stripo::TemplateGroup::Usable do
  subject { described_class.new(email_list:, user:).call.pluck(:name) }

  let(:email_list) { create(:email_list) }

  before do
    create(:stripo_template_group, :with_owner, name: 'The Group', email_lists: [email_list], users: [user])
    create(:stripo_template_group, name: 'A public Group')
  end

  after { DatabaseCleaner.clean } # TODO: should be done globally

  context 'when admin user' do
    let(:user) { create(:user, :admin) }

    it { is_expected.to contain_exactly('A public Group', 'The Group') }
  end

  context 'when user' do
    context 'without email list' do
      let(:user) { create(:user) }

      it { is_expected.to contain_exactly('A public Group') }
    end

    context 'with email list' do
      let(:user) { create(:user, email_lists: [email_list]) }

      it { is_expected.to contain_exactly('A public Group', 'The Group') }
    end
  end
end
