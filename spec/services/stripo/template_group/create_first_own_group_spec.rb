# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Stripo::TemplateGroup::CreateFirstOwnGroup do
  subject(:call) { described_class.call(user) }

  let(:user) { create(:user) }

  after { DatabaseCleaner.clean }

  context 'without any group' do
    it { expect { call }.to change(Stripo::TemplateGroup, :count).by(1) }
  end

  context 'with a group' do
    before { create(:stripo_template_group, user:) }

    it { expect { call }.not_to change(Stripo::TemplateGroup, :count) }
  end
end
