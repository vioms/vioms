# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Stripo::TemplateGroup::ParamsSanitizer do
  subject { described_class.call(params, user:, template_group: stripo_template_group).permit!.to_h }

  let(:params) { ActionController::Parameters.new(params_hash) }
  let(:permitted_email_list) { create(:email_list) }
  let(:not_permitted_email_list) { create(:email_list) }
  let(:user) { create(:user, email_lists: [permitted_email_list]) }
  let(:not_permitted_user) { create(:user) }
  let(:stripo_template_group) { create(:stripo_template_group) }

  context 'without email lists and users' do
    let(:params_hash) { { 'name' => 'Auto' } }

    it { is_expected.to eq({ 'name' => 'Auto', 'email_list_ids' => [], 'user_ids' => [] }) }
  end

  context 'with email lists and users' do
    let(:params_hash) do
      {
        email_list_ids: [permitted_email_list.id, not_permitted_email_list.id],
        user_ids: [user.id, not_permitted_user.id]
      }
    end

    it { is_expected.to eq({ 'email_list_ids' => [permitted_email_list.id], 'user_ids' => [user.id] }) }
  end
end
