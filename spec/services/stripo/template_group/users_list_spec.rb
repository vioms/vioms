# frozen_string_literal: true

require 'rails_helper'

describe Stripo::TemplateGroup::UsersList do
  subject { described_class.call(user) }

  before { create_list(:user, 2, :with_email_lists) }

  context 'when admin user' do
    let(:user) { create(:user, :admin) }

    it { is_expected.to match_array(User.all) }
  end

  context 'when user' do
    context 'without common email lists' do
      let(:user) { create(:user, :with_email_lists) }

      it { is_expected.to contain_exactly(user) }
    end

    context 'with common email lists' do
      let(:email_lists) { create_list(:email_list, 2) }
      let(:user) { create(:user, email_lists:) }
      let(:visible_users) { create_list(:user, 2, email_lists:) }

      it { is_expected.to match_array([user] + visible_users) }
    end
  end
end
