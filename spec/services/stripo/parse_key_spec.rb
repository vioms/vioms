# frozen_string_literal: true

require 'rails_helper'

describe Stripo::ParseKey do
  subject { described_class.call(key) }

  context 'when key contains pluginId and emailId' do
    let(:key) { 'pluginId_ppplugggin_email_123' }

    it { is_expected.to eq(plugin_id: 'ppplugggin', email_id: 123) }
  end

  context 'without emailId' do
    let(:key) { 'pluginId_ppplugggin_banner' }

    it { is_expected.to eq(plugin_id: 'ppplugggin', email_id: nil) }
  end
end
