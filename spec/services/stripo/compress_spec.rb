# frozen_string_literal: true

require 'rails_helper'

describe Stripo::Compress do
  subject(:call) { described_class.call(html: 'html', css: 'css') }

  before do
    allow(Stripo::RequestToken).to receive(:call).and_return('the_token')
    stub_request(:post, 'https://plugins.stripo.email/api/v1/cleaner/v1/compress')
      .with(
        body: '{"html":"html","css":"css","minimize":true}',
        headers: {
          'Accept' => 'application/json',
          'Content-Type' => 'application/json',
          'Es-Plugin-Auth' => 'Bearer the_token',
          'Host' => 'plugins.stripo.email'
        }
      )
      .to_return(status: 200, body: '{"html":"compressed html"}')
  end

  it do
    expect(call).to eq('compressed html')
    expect(Stripo::RequestToken).to have_received(:call)
  end
end
