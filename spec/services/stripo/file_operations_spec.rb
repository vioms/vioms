# frozen_string_literal: true

require 'rails_helper'

describe Stripo::FileOperations do
  def file
    Rails.root.join('spec', 'support', 'images', 'spacer.gif').open('rb')
  end

  def uploaded_file_json(url, thumb_url)
    {
      url:,
      originalName: 'spacer.gif',
      uploadTime: Time.zone.now.to_i * 1000,
      size: 45,
      height: 5,
      width: 5,
      thumbnailUrl: thumb_url
    }
  end

  let(:plugin_id) { 'ThfscSyzSdqux25NLNghafYkZO8Pqb6D' }

  before do
    DatabaseCleaner.clean # TODO: should be done globally
    Timecop.freeze
    allow_any_instance_of(Shrine).to receive(:generate_uid).and_return('12345678')
    create(:stripo_file_key, plugin_id:, email_id: 1)
  end

  after { Timecop.return }

  describe '.list' do
    subject { described_class.list }

    before do
      create(:stripo_file_key, plugin_id:, email_id: 2)
      create(:stripo_file_key, plugin_id:, email_id: 3)
      described_class.upload(key: 'pluginId_ThfscSyzSdqux25NLNghafYkZO8Pqb6D_email_1', file:)
      described_class.upload(key: 'pluginId_ThfscSyzSdqux25NLNghafYkZO8Pqb6D_email_2', file:)
    end

    let(:result) do
      [
        {
          key: "pluginId_#{plugin_id}_email_1",
          documents: [uploaded_file_json("memory://stripo/#{plugin_id}/1/12345678.gif",
                                         "memory://stripo/#{plugin_id}/1/12345678.gif")]
        },
        {
          key: 'pluginId_ThfscSyzSdqux25NLNghafYkZO8Pqb6D_email_2',
          documents: [uploaded_file_json("memory://stripo/#{plugin_id}/2/12345678.gif",
                                         "memory://stripo/#{plugin_id}/2/12345678.gif")]
        },
        { key: "pluginId_#{plugin_id}_email_3", documents: [] }
      ]
    end

    it { is_expected.to eq(result) }
  end

  describe '.info' do
    subject { described_class.info(src: "memory://stripo/#{plugin_id}/1/12345678.gif") }

    before { described_class.upload(key: "pluginId_#{plugin_id}_email_1", file:) }

    it { is_expected.to eq(originalName: 'spacer.gif', size: 45) }
  end

  describe '.upload' do
    context 'with emailId' do
      subject { described_class.upload(key: "pluginId_#{plugin_id}_email_1", file:) }

      it {
        expect(subject).to eq(uploaded_file_json("memory://stripo/#{plugin_id}/1/12345678.gif",
                                                 "memory://stripo/#{plugin_id}/1/12345678.gif"))
      }
    end

    context 'without emailId' do
      subject { described_class.upload(key: "pluginId_#{plugin_id}_banner", file:) }

      it {
        expect(subject).to eq(uploaded_file_json("memory://stripo/#{plugin_id}/12345678.gif",
                                                 "memory://stripo/#{plugin_id}/12345678.gif"))
      }
    end
  end

  describe '.delete' do
    subject do
      described_class.delete(key: "pluginId_#{plugin_id}_email_1",
                             url: "memory://stripo/#{plugin_id}/1/12345678.gif")
    end

    before { described_class.upload(key: "pluginId_#{plugin_id}_email_1", file:) }

    it {
      expect(subject).to eq(key: "pluginId_#{plugin_id}_email_1",
                            url: "memory://stripo/#{plugin_id}/1/12345678.gif")
    }
  end
end
