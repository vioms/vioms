# frozen_string_literal: true

require 'rails_helper'

describe Stripo::Version do
  subject { described_class.call }

  before do
    allow(Setting).to receive('get_setting').with('stripo_version').and_return(version)
  end

  context 'when version is latest' do
    let(:version) { 'latest' }

    it { is_expected.to eq('latest') }
  end

  context 'when version is 1.0' do
    let(:version) { '1.0' }

    it { is_expected.to eq('rev/1.0') }
  end
end
