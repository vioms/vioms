# frozen_string_literal: true

require 'rails_helper'

describe BaseUrl do
  subject { described_class.call }

  before { allow(Rails).to receive(:env).and_return(env) }

  context 'when env is production' do
    let(:env) { 'production' }

    it do
      expect(subject).to be_present
      expect(Rails).to have_received(:env)
    end
  end

  context 'when env is development' do
    let(:env) { 'development' }

    it do
      expect(subject).to be_present
      expect(Rails).to have_received(:env)
    end
  end

  context 'when env is test' do
    let(:env) { 'test' }

    it do
      expect(subject).to be_present
      expect(Rails).to have_received(:env)
    end
  end

  context 'when env is unknown' do
    let(:env) { 'unknown' }

    it do
      expect { subject }.to raise_exception(BaseUrl::UnknownEnvironment)
      expect(Rails).to have_received(:env)
    end
  end
end
