# frozen_string_literal: true

require 'rails_helper'

describe ImportProblemsService do
  let(:email_list) { create(:email_list) }
  let(:user)       { create(:user, email_lists: [email_list]) }

  context 'email' do
    let!(:subscriber)        { create(:subscriber_with_email, email: 'mail@example.com', email_lists: [email_list]) }
    let(:email_problem_name) { create(:email_problem_name) }
    let(:importer)           { described_class.new(contact_type: :email, user:) }

    it 'creates problems' do
      importer.import!(input: 'mail@example.com,mail2@example.com,mail3@example.com',
                       problem_name_id: email_problem_name.id)
      expect(subscriber.reload.email_problems_count).to eq(1)
      expect(importer.not_imported_contacts).to match_array(%w[mail2@example.com mail3@example.com])
    end
  end
end
