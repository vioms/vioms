# frozen_string_literal: true

require 'rails_helper'

describe RolesService do
  let(:service) { described_class.new({ admin: 0, import: 1, export: 2 }) }

  describe 'is_role?' do
    specify 'no roles' do
      expect(service.is_role?(:admin, 0)).to be(false)
      expect(service.is_role?(:import, 0)).to be(false)
      expect(service.is_role?(:export, 0)).to be(false)
    end

    specify 'admin role' do
      expect(service.is_role?(:admin, 1)).to be(true)
      expect(service.is_role?(:import, 1)).to be(false)
      expect(service.is_role?(:export, 1)).to be(false)
    end

    specify 'import role' do
      expect(service.is_role?(:admin, 2)).to be(false)
      expect(service.is_role?(:import, 2)).to be(true)
      expect(service.is_role?(:export, 2)).to be(false)
    end

    specify 'import and export roles' do
      expect(service.is_role?(:admin, 6)).to be(false)
      expect(service.is_role?(:import, 6)).to be(true)
      expect(service.is_role?(:export, 6)).to be(true)
    end

    specify 'all roles' do
      expect(service.is_role?(:admin, 7)).to be(true)
      expect(service.is_role?(:import, 7)).to be(true)
      expect(service.is_role?(:export, 7)).to be(true)
    end
  end

  describe 'set_role' do
    it 'does not add roles' do
      expect(service.set_role(:admin, 0, false)).to eq(0)
      expect(service.set_role(:import, 0, false)).to eq(0)
      expect(service.set_role(:export, 0, false)).to eq(0)
    end

    it 'adds import role' do
      expect(service.set_role(:import, 0, true)).to eq(2)
      expect(service.set_role(:import, 1, true)).to eq(3)
      expect(service.set_role(:import, 2, true)).to eq(2)
      expect(service.set_role(:import, 4, true)).to eq(6)
    end

    it 'removes import role' do
      expect(service.set_role(:export, 6, false)).to eq(2)
      expect(service.set_role(:export, 4, false)).to eq(0)
    end
  end
end
