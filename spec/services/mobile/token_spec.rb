# frozen_string_literal: true

require 'rails_helper'

describe Mobile::Token do
  before { DatabaseCleaner.clean }

  let!(:subscriber) { create(:subscriber_with_confirmed_email) }

  describe '.create!' do
    subject { Mobile::Encoder.decode(described_class.create!(subscriber:, device_token: nil, device_type: nil)) }

    it { is_expected.to eq({ 'id' => 1, 'serial' => 0 }) }
    it { expect { subject }.to change(MobileToken, :count).by(1) }
  end

  describe '.next!' do
    subject { Mobile::Encoder.decode(described_class.next!(mobile_token, serial)) }

    let!(:mobile_token) { create(:mobile_token, subscriber:, serial: 2) }

    context 'when serial is current' do
      let(:serial) { 2 }

      it { is_expected.to eq({ 'id' => 1, 'serial' => 3 }) }
      it { expect { subject }.not_to change(MobileToken, :count) }
    end

    context 'when serial is previous' do
      let(:serial) { 1 }

      it { is_expected.to eq({ 'id' => 1, 'serial' => 2 }) }
      it { expect { subject }.not_to change(MobileToken, :count) }
    end
  end
end
