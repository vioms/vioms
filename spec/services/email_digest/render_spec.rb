# frozen_string_literal: true

require 'rails_helper'

RSpec.describe EmailDigest::Render do
  subject { described_class.call(email_digest) }

  let(:email_digest) { create(:email_digest, rendered_html:) }

  context 'without rendered_html' do
    let(:rendered_html) { nil }

    before do
      expect(EmailDigest::Content).to receive(:call).and_return('rendered html')
    end

    it { is_expected.to eq('rendered html') }
  end

  context 'with rendered_html' do
    let(:rendered_html) { 'rendered html' }

    it { is_expected.to eq('rendered html') }
  end
end
