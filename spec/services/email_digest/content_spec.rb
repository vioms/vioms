# frozen_string_literal: true

require 'rails_helper'

describe EmailDigest::Content do
  subject { described_class.call(email_digest) }

  before(:all) { Timecop.travel(Time.gm(2008, 9, 1, 6, 5, 0)) }

  after(:all) { Timecop.return }

  let(:email_list) do
    create(:email_list_with_digest, digest_subject_template: 'Newsletter [NUM] ([YEAR])',
                                    digest_template: 'Body [NUM] ([YEAR]) [MAILINGS]')
  end
  let(:email_digest) { create(:email_digest, email_list:, num: 2) }

  before do
    create(:mailing, :for_digest, email_list:, digest_category: email_list.digest_categories.first,
                                  published: true, email_digest:)
  end

  it { is_expected.to eq("Body 2 (2008) Big template Template entry Subject Content\n[FOOTER]") }
end
