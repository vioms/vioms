# frozen_string_literal: true

require 'rails_helper'

describe EmailDigest::Subject do
  subject { described_class.call(email_digest) }

  before(:all) { Timecop.travel(Time.gm(2008, 9, 1, 6, 5, 0)) }

  after(:all) { Timecop.return }

  let(:email_list) do
    create(:email_list_with_digest, digest_subject_template: 'Newsletter [NUM] ([YEAR])',
                                    digest_template: 'Body [NUM] ([YEAR])')
  end
  let(:email_digest) { create(:email_digest, email_list:, num: 2) }

  it { is_expected.to eq('Newsletter 2 (2008)') }
end
