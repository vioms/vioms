# frozen_string_literal: true

require 'rails_helper'

describe Telegram::UrlBuilder do
  subject { described_class.new(telegram_message, telegram_group) }

  let(:telegram_group) { create(:telegram_group) }

  context 'without image' do
    let(:telegram_message) { create(:telegram_message, telegram_group:) }

    it { is_expected.to be_a(Telegram::UrlBuilders::Message) }
  end

  context 'with image' do
    let(:telegram_message) { create(:telegram_message, :with_image, telegram_group:) }

    it { is_expected.to be_a(Telegram::UrlBuilders::Photo) }
  end
end
