# frozen_string_literal: true

require 'rails_helper'

describe Telegram::UrlBuilders::Message do
  let(:telegram_group) { create(:telegram_group, bot_token: 'thetoken', chat_id: '-16108') }
  let(:telegram_message) { create(:telegram_message, telegram_group:, message: 'test message') }
  let(:url_builder) { described_class.new(telegram_message, telegram_group) }
  let(:params) do
    {
      chat_id: '-16108',
      disable_web_page_preview: true,
      parse_mode: 'HTML',
      text: 'test message'
    }
  end

  specify { expect(url_builder.url).to eq('https://api.telegram.org/botthetoken/sendMessage') }
  specify { expect(url_builder.params).to match(params) }
  specify { expect(url_builder.format).to eq(:json) }
end
