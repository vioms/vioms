# frozen_string_literal: true

require 'rails_helper'

describe Telegram::UrlBuilders::Photo do
  let(:telegram_group) { create(:telegram_group, bot_token: 'thetoken', chat_id: '-16108') }
  let(:telegram_message) do
    create(:telegram_message, :with_image, telegram_group:, message: 'test message')
  end
  let(:url_builder) { described_class.new(telegram_message, telegram_group) }
  let(:params) do
    {
      chat_id: '-16108',
      caption: 'test message',
      photo: telegram_message.image.to_io,
      parse_mode: 'HTML'
    }
  end

  specify { expect(url_builder.url).to eq('https://api.telegram.org/botthetoken/sendPhoto') }
  specify { expect(url_builder.params).to match(params) }
  specify { expect(url_builder.format).to eq(:json) }
end
