# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Telegram::Deliver do
  let(:chat_id) { '-16108' }
  let(:bot_token) { 'thetoken' }
  let(:telegram_group) { create(:telegram_group, chat_id:, bot_token:) }
  let(:groups_count) { 1 }

  let(:http_response) { instance_double(RestClient::Response, code: 200) }
  let(:http_response_error) { instance_double(RestClient::Response, code: 500) }

  RSpec.shared_examples 'a telegram message' do
    context 'when valid message' do
      before do
        allow(RestClient).to receive(:post).and_return(http_response)
        described_class.call(telegram_message)
        telegram_message.reload
      end

      specify { expect(RestClient).to have_received(:post).exactly(groups_count) }
      specify { expect(telegram_message.sent).to be(true) }
      specify { expect(telegram_message.send_failed).to be(false) }
      specify { expect(telegram_message.raised_exception_at).to be_nil }
    end

    context 'when Telegram API error' do
      before do
        allow(RestClient).to receive(:post).and_return(http_response_error)
        described_class.call(telegram_message)
        telegram_message.reload
      end

      specify { expect(RestClient).to have_received(:post).once }
      specify { expect(telegram_message.sent).to be(false) }
      specify { expect(telegram_message.send_failed).to be(true) }
      specify { expect(telegram_message.raised_exception_at).to be_nil }
    end

    context 'when an exception occurred' do
      before do
        allow(RestClient).to receive(:post).and_raise(RestClient::ExceptionWithResponse)
        described_class.call(telegram_message)
        telegram_message.reload
      end

      specify { expect(RestClient).to have_received(:post).once }
      specify { expect(telegram_message.sent).to be(false) }
      specify { expect(telegram_message.send_failed).to be(true) }
      specify { expect(telegram_message.raised_exception_at).not_to be_nil }
    end
  end

  describe 'telegram message without image' do
    context 'with one group' do
      it_behaves_like 'a telegram message' do
        let(:telegram_message) do
          create(:telegram_message, :published, telegram_group:, message: 'test message')
        end
      end
    end

    context 'with included group' do
      it_behaves_like 'a telegram message' do
        let(:groups_count) { 2 }
        let(:telegram_message) do
          create(:telegram_message, :published, telegram_group:, message: 'test message',
                                                included_telegram_groups: [create(:telegram_group)])
        end
      end
    end
  end

  describe 'telegram message with image' do
    context 'with one group' do
      it_behaves_like 'a telegram message' do
        let(:telegram_message) do
          create(:telegram_message, :with_image, :published, telegram_group:, message: 'test message')
        end
      end
    end

    context 'with included group' do
      it_behaves_like 'a telegram message' do
        let(:groups_count) { 2 }
        let(:telegram_message) do
          create(:telegram_message, :with_image, :published, telegram_group:, message: 'test message',
                                                             included_telegram_groups: [create(:telegram_group)])
        end
      end
    end
  end
end
