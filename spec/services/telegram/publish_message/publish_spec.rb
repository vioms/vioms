# frozen_string_literal: true

require 'rails_helper'

describe Telegram::PublishMessage::Publish do
  subject(:result) { described_class.call(telegram_message:, user:) }

  let(:user) { create(:user) }

  describe '#call' do
    context 'when given valid telegram message' do
      let(:telegram_message) { build(:telegram_message) }

      it 'succeeds' do
        expect(result).to be_a_success
      end

      it 'sets the user' do
        result
        expect(telegram_message.user).to eq(user)
      end

      it 'publishes telegram message' do
        result
        expect(telegram_message.published).to be true
      end
    end

    context 'when given not valid telegram message' do
      let(:telegram_message) { build(:telegram_message, :invalid) }

      it 'fails' do
        expect(result).to be_a_failure
      end

      it 'provides a failure message' do
        expect(result.message).to be_present
      end
    end
  end
end
