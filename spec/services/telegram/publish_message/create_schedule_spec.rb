# frozen_string_literal: true

require 'rails_helper'

describe Telegram::PublishMessage::CreateSchedule do
  subject(:result) { described_class.call(telegram_message:) }

  describe '#call' do
    let(:telegram_message) { create(:telegram_message) }

    it 'succeeds' do
      expect(result).to be_a_success
    end

    it 'creates a schedule' do
      result
      expect(telegram_message.schedule).to be_present
    end
  end
end
