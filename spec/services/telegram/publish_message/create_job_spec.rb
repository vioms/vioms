# frozen_string_literal: true

require 'rails_helper'

describe Telegram::PublishMessage::CreateJob do
  subject(:result) { described_class.call(telegram_message:) }

  before { ActiveJob::Base.queue_adapter = :test }

  describe '#call' do
    context 'when a job is created' do
      let(:telegram_message) { build(:telegram_message, :published, :with_schedule) }

      it 'succeeds' do
        expect(result).to be_a_success
      end

      it 'enqueues a job' do
        expect { result }.to have_enqueued_job(TelegramMessageJob)
          .with(telegram_message.schedule.id)
      end

      it 'provides a success message' do
        expect(result.message).to be_present
      end
    end

    context 'when a job is not created' do
      let(:telegram_message) { build(:telegram_message, :invalid) }

      it 'fails' do
        expect(result).to be_a_failure
      end

      it 'provides a failure message' do
        expect(result.message).to be_present
      end
    end
  end
end
