# frozen_string_literal: true

require 'rails_helper'

describe Telegram::CancelMessage::Unpublish do
  subject(:result) { described_class.call(telegram_message:) }

  describe '#call' do
    context 'when given valid telegram message' do
      let(:telegram_message) { build(:telegram_message, :published) }

      it 'succeeds' do
        expect(result).to be_a_success
      end

      it 'unsets the user' do
        result
        expect(telegram_message.user).to be_nil
      end

      it 'publishes telegram message' do
        result
        expect(telegram_message.published).to be false
      end
    end
  end
end
