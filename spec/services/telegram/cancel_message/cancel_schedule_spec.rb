# frozen_string_literal: true

require 'rails_helper'

describe Telegram::CancelMessage::CancelSchedule do
  subject(:result) { described_class.call(telegram_message:) }

  describe '#call' do
    context 'when it is possible to cancel' do
      let(:telegram_message) { create(:telegram_message, :published, :with_schedule) }

      it 'succeeds' do
        expect(result).to be_a_success
      end

      it 'destroys the schedule' do
        result
        expect(telegram_message.reload.schedule).to be_nil
      end
    end
  end
end
