require 'rails_helper'

describe ActionView::Helpers::JavaScriptHelper do
  before { extend ActionView::Helpers::JavaScriptHelper }

  it 'replaces custom symbols' do
    escape_javascript("\342\200\251".force_encoding('UTF-8')).should eq('&#x2029;')
    j("\342\200\251".force_encoding('UTF-8')).should eq('&#x2029;')
  end

  it 'does not break default behaviour' do
    escape_javascript("\r\n").should eq('\n')
    escape_javascript("\342\200\250".force_encoding('UTF-8')).should eq('&#x2028;')
    j("\342\200\250".force_encoding('UTF-8')).should eq('&#x2028;')
    j("\r\n").should eq('\n')
  end
end
