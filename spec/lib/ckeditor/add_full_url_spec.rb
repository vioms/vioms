require 'rails_helper'

describe Ckeditor::AddFullUrl do
  let(:email_list) { build(:email_list) }

  context 'http protocol' do
    before { Rails.application.config.action_mailer.default_url_options = { host: 'example.com' } }

    it 'handles nil string' do
      email_list.valid?
      email_list.description.should be_nil
    end

    it 'adds full url with single quotes' do
      email_list.description = "<p><img alt='' src='/ckeditor_assets/pictures/image001.jpg' style='width: 250px;height: 250px;' /><br /></p>"
      email_list.valid?
      email_list.description.should == "<p><img alt='' src='http://example.com/ckeditor_assets/pictures/image001.jpg' style='width: 250px;height: 250px;' /><br /></p>"
    end

    it 'adds full url with double quotes' do
      email_list.description = '<p><img alt="" src="/ckeditor_assets/pictures/image001.jpg" style="width: 250px;height: 250px;" /><br /></p>'
      email_list.valid?
      email_list.description.should == '<p><img alt="" src="http://example.com/ckeditor_assets/pictures/image001.jpg" style="width: 250px;height: 250px;" /><br /></p>'
    end

    it 'does not add full url twice' do
      email_list.description = '<p><img alt="" src="/ckeditor_assets/pictures/image001.jpg" style="width: 250px;height: 250px;" /><br /></p>'
      email_list.valid?
      email_list.valid?
      email_list.description.should == '<p><img alt="" src="http://example.com/ckeditor_assets/pictures/image001.jpg" style="width: 250px;height: 250px;" /><br /></p>'
    end
  end

  context 'https protocol' do
    it 'adds full url with https' do
      Rails.application.config.action_mailer.default_url_options = { host: 'example.com', protocol: 'https://' }
      email_list.description = '<p><img alt="" src="/ckeditor_assets/pictures/image001.jpg" style="width: 250px;height: 250px;" /><br /></p>'
      email_list.valid?
      email_list.description.should == '<p><img alt="" src="https://example.com/ckeditor_assets/pictures/image001.jpg" style="width: 250px;height: 250px;" /><br /></p>'
    end
  end
end
