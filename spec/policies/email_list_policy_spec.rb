# frozen_string_literal: true

require 'rails_helper'

describe EmailListPolicy do
  subject { described_class }

  let!(:email_list_visible)   { create(:email_list, :visible) }
  let!(:email_list_invisible) { create(:email_list, :invisible) }

  context 'when user is a guest' do
    let(:user) { { user: nil, subscriber: nil } }

    permissions :show? do
      it { is_expected.to permit(user, email_list_visible) }
      it { is_expected.not_to permit(user, email_list_invisible) }
    end
  end

  context 'when user is a subscriber' do
    let(:user) { { user: nil, subscriber: create(:user) } }

    permissions :show? do
      it { is_expected.to permit(user, email_list_visible) }
      it { is_expected.not_to permit(user, email_list_invisible) }
    end
  end
end
