# frozen_string_literal: true

require 'rails_helper'

describe MailingRequestPolicy do
  subject { described_class }

  let(:subscriber) { create(:subscriber_with_email) }
  let(:mailing_request) { create(:mailing_request, subscriber:) }
  let(:mailing_request_processed) { create(:mailing_request, subscriber:, status: :processed) }

  context 'as guest' do
    let(:users_guest) { { user: nil, subscriber: nil } }

    permissions :show?, :manage? do
      it { is_expected.not_to permit(users_guest, mailing_request) }
      it { is_expected.not_to permit(users_guest, mailing_request_processed) }
    end
  end

  context 'as subscriber' do
    let(:users) { { user: nil, subscriber: } }

    permissions :show?, :manage? do
      it { is_expected.to permit(users, mailing_request) }
    end

    permissions :show? do
      it { is_expected.to permit(users, mailing_request_processed) }
    end

    permissions :manage? do
      it { is_expected.not_to permit(users, mailing_request_processed) }
    end
  end

  context 'as subscriber who is not owner' do
    let(:users) { { user: nil, subscriber: create(:subscriber_with_email) } }

    permissions :show?, :manage? do
      it { is_expected.not_to permit(users, mailing_request) }
      it { is_expected.not_to permit(users, mailing_request_processed) }
    end
  end
end
