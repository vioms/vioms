# frozen_string_literal: true

require 'rails_helper'

describe Admin::SubscriberImportPolicy do
  subject { described_class }

  let!(:email_list1)       { create(:email_list) }
  let!(:email_list2)       { create(:email_list) }
  let!(:email_list3)       { create(:email_list) }
  let!(:subscriber_import) { create(:subscriber_import, :valid) }

  context 'as user' do
    let(:user) { create(:user) }

    permissions :index?, :show?, :new?, :create?, :edit?, :update?, :destroy? do
      it { is_expected.not_to permit(user, subscriber_import) }
    end
  end

  context 'as user with import role' do
    let(:user) { create(:user, is_import: true) }

    permissions :index?, :new? do
      it { is_expected.to permit(user, SubscriberImport) }
    end

    permissions :show?, :create?, :edit?, :update?, :destroy? do
      it { is_expected.not_to permit(user, subscriber_import) }
    end
  end

  context 'as user with email lists and import role' do
    let(:user) { create(:user, email_lists: [email_list1, email_list2], is_import: true) }

    permissions :show?, :create?, :edit?, :update? do
      it { is_expected.to permit(user, SubscriberImport.new(email_lists: [email_list1])) }
      it { is_expected.to permit(user, SubscriberImport.new(email_lists: [email_list1, email_list2])) }
      it { is_expected.not_to permit(user, SubscriberImport.new(email_lists: [email_list1, email_list3])) }
      it { is_expected.not_to permit(user, SubscriberImport.new(email_lists: [email_list3])) }
    end

    permissions :destroy? do
      it { is_expected.not_to permit(user, SubscriberImport.new(email_lists: [email_list1])) }
      it { is_expected.not_to permit(user, SubscriberImport.new(email_lists: [email_list1, email_list2])) }
      it { is_expected.not_to permit(user, SubscriberImport.new(email_lists: [email_list1, email_list3])) }
      it { is_expected.not_to permit(user, SubscriberImport.new(email_lists: [email_list3])) }
    end
  end

  context 'as admin user' do
    let(:user) { create(:user, :admin) }

    permissions :index?, :show?, :new?, :create?, :edit?, :update?, :destroy? do
      it { is_expected.to permit(user, SubscriberImport.new) }
    end
  end
end
