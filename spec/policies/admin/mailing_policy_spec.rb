# frozen_string_literal: true

require 'rails_helper'

describe Admin::MailingPolicy do
  subject { described_class }

  context 'as user' do
    let(:user)    { create(:user) }
    let(:mailing) { create(:mailing) }

    permissions :new? do
      it { is_expected.to permit(user, Mailing) }
    end

    permissions :show?, :create?, :edit?, :update?, :copy?, :destroy? do
      it { is_expected.not_to permit(user, mailing) }
    end
  end

  context 'as user with email list' do
    let(:email_list) { create(:email_list) }
    let(:user)       { create(:user, email_lists: [email_list]) }
    let(:mailing)    { create(:mailing, email_list:) }

    permissions :show?, :new?, :create?, :edit?, :update?, :copy?, :destroy? do
      it { is_expected.to permit(user, mailing) }
    end
  end

  context 'as admin' do
    let(:user)       { create(:user, :admin) }
    let(:mailing)    { create(:mailing) }

    permissions :show?, :new?, :create?, :edit?, :update?, :copy?, :destroy? do
      it { is_expected.to permit(user, mailing) }
    end
  end

  context 'as admin with email list' do
    let(:email_list) { create(:email_list) }
    let(:user)       { create(:user, :admin, email_lists: [email_list]) }
    let(:mailing)    { create(:mailing, email_list:) }

    permissions :show?, :new?, :create?, :edit?, :update?, :copy?, :destroy? do
      it { is_expected.to permit(user, mailing) }
    end
  end
end
