# frozen_string_literal: true

require 'rails_helper'

describe Admin::Stripo::TemplateGroupPolicy do
  subject { described_class }

  context 'when admin' do
    let(:user)           { create(:user, :admin) }
    let(:template_group) { create(:stripo_template_group) }

    permissions :index? do
      it { is_expected.to permit(user, Stripo::TemplateGroup) }
    end

    permissions :show?, :new?, :create?, :edit?, :update?, :destroy? do
      it { is_expected.to permit(user, template_group) }
    end
  end

  context 'when user' do
    let(:email_list) { create(:email_list) }
    let(:user)       { create(:user, email_lists: [email_list]) }

    permissions :index? do
      it { is_expected.to permit(user, Stripo::TemplateGroup) }
    end

    context 'when group is root' do
      permissions :new?, :create? do
        let(:template_group) { build(:stripo_template_group) }

        it { is_expected.to permit(user, template_group) }
      end

      context 'when group is not public' do
        let(:template_group) { create(:stripo_template_group, :with_owner) }

        permissions :show?, :edit?, :update?, :destroy? do
          it { is_expected.not_to permit(user, template_group) }
        end
      end

      context 'when group is public' do
        let(:template_group) { create(:stripo_template_group) }

        permissions :show? do
          it { is_expected.to permit(user, template_group) }
        end

        permissions :edit?, :update?, :destroy? do
          it { is_expected.not_to permit(user, template_group) }
        end
      end

      context 'when group has allowed user' do
        let(:template_group) { create(:stripo_template_group, :with_owner, users: [user]) }

        permissions :show? do
          it { is_expected.to permit(user, template_group) }
        end

        permissions :edit?, :update?, :destroy? do
          it { is_expected.not_to permit(user, template_group) }
        end
      end

      context 'when group has allowed email list' do
        let(:template_group) { create(:stripo_template_group, :with_owner, email_lists: [email_list]) }

        permissions :show? do
          it { is_expected.to permit(user, template_group) }
        end

        permissions :edit?, :update?, :destroy? do
          it { is_expected.not_to permit(user, template_group) }
        end
      end
    end

    context 'when group has a parent' do
      let(:parent_group) { create(:stripo_template_group, :with_owner) }
      let(:template_group) { create(:stripo_template_group, :with_parent, parent: parent_group) }

      permissions :new?, :create? do
        let(:template_group) { Stripo::TemplateGroup.new(parent: parent_group) }

        it { is_expected.not_to permit(user, template_group) }
      end

      context 'when root group is not public' do
        permissions :show?, :edit?, :update?, :destroy? do
          it { is_expected.not_to permit(user, template_group) }
        end
      end

      context 'when root group is public' do
        let(:parent_group) { create(:stripo_template_group) }

        permissions :show? do
          it { is_expected.to permit(user, template_group) }
        end

        permissions :edit?, :update?, :destroy? do
          it { is_expected.not_to permit(user, template_group) }
        end
      end

      context 'when group has allowed user' do
        let(:parent_group) { create(:stripo_template_group, :with_owner, users: [user]) }

        permissions :show? do
          it { is_expected.to permit(user, template_group) }
        end

        permissions :edit?, :update?, :destroy? do
          it { is_expected.not_to permit(user, template_group) }
        end
      end

      context 'when group has allowed email list' do
        let(:parent_group) { create(:stripo_template_group, :with_owner, email_lists: [email_list]) }

        permissions :show? do
          it { is_expected.to permit(user, template_group) }
        end

        permissions :edit?, :update?, :destroy? do
          it { is_expected.not_to permit(user, template_group) }
        end
      end
    end
  end

  context 'when owner' do
    let(:user) { create(:user) }

    context 'when group is root' do
      permissions :show?, :edit?, :update?, :destroy? do
        let(:template_group) { create(:stripo_template_group, user:) }

        it { is_expected.to permit(user, template_group) }
      end
    end

    context 'when group has a parent' do
      let(:parent_group) { create(:stripo_template_group, :with_owner, user:) }
      let(:template_group) { create(:stripo_template_group, :with_parent, parent: parent_group) }

      permissions :new?, :create? do
        let(:template_group) { Stripo::TemplateGroup.new(parent: parent_group) }

        it { is_expected.to permit(user, template_group) }
      end

      context 'when root group is not public' do
        permissions :show?, :edit?, :update?, :destroy? do
          it { is_expected.to permit(user, template_group) }
        end
      end
    end
  end
end
