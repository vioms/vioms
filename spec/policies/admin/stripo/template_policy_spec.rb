# frozen_string_literal: true

require 'rails_helper'

describe Admin::Stripo::TemplatePolicy do
  subject { described_class }

  context 'when admin' do
    let(:user)     { create(:user, :admin) }
    let(:template) { create(:stripo_template) }

    permissions :index? do
      it { is_expected.to permit(user, Stripo::Template) }
    end

    permissions :show?, :new?, :create?, :edit?, :update?, :destroy? do
      it { is_expected.to permit(user, template) }
    end
  end

  permissions :show? do
    let(:email_list) { create(:email_list) }
    let(:user)       { create(:user, email_lists: [email_list]) }
    let(:group)      { create(:stripo_template_group, :with_parent, parent: parent_group) }
    let(:template)   { create(:stripo_template, template_group: group) }

    context 'when parent group is public' do
      let(:parent_group) { create(:stripo_template_group) }

      it { is_expected.to permit(user, template) }
    end

    context 'when parent group does not belong to user' do
      let(:parent_group) { create(:stripo_template_group, :with_owner) }

      it { is_expected.not_to permit(user, template) }
    end

    context 'when parent group belongs to user' do
      let(:parent_group) { create(:stripo_template_group, :with_owner, user:) }

      it { is_expected.not_to permit(user, template) }
    end

    context 'when parent group has an allowed user' do
      let(:parent_group) { create(:stripo_template_group, :with_owner, users: [user]) }

      it { is_expected.not_to permit(user, template) }
    end

    context 'when parent group has an allowed email list' do
      let(:parent_group) { create(:stripo_template_group, :with_owner, email_lists: [email_list]) }

      it { is_expected.to permit(user, template) }
    end
  end

  permissions :new?, :create?, :edit?, :update?, :destroy? do
    let(:email_list) { create(:email_list) }
    let(:user)       { create(:user, email_lists: [email_list]) }
    let(:group)      { create(:stripo_template_group, :with_parent, parent: parent_group) }
    let(:template)   { create(:stripo_template, template_group: group) }

    context 'when parent group is public' do
      let(:parent_group) { create(:stripo_template_group) }

      it { is_expected.not_to permit(user, template) }
    end

    context 'when parent group does not belong to user' do
      let(:parent_group) { create(:stripo_template_group, :with_owner) }

      it { is_expected.not_to permit(user, template) }
    end

    context 'when parent group belongs to user' do
      let(:parent_group) { create(:stripo_template_group, :with_owner, user:) }

      it { is_expected.to permit(user, template) }
    end

    context 'when parent group has an allowed user' do
      let(:parent_group) { create(:stripo_template_group, :with_owner, users: [user]) }

      it { is_expected.not_to permit(user, template) }
    end

    context 'when parent group has an allowed email list' do
      let(:parent_group) { create(:stripo_template_group, :with_owner, email_lists: [email_list]) }

      it { is_expected.not_to permit(user, template) }
    end

    context 'when parent group has an allowed user and email list' do
      let(:parent_group) { create(:stripo_template_group, :with_owner, users: [user], email_lists: [email_list]) }

      it { is_expected.to permit(user, template) }
    end
  end
end
