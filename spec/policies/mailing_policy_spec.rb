# frozen_string_literal: true

require 'rails_helper'

describe MailingPolicy do
  subject { described_class }

  let!(:email_list_visible) { create(:email_list, :visible, expiration_days:) }
  let!(:email_list_invisible1) { create(:email_list, :invisible, expiration_days:) }
  let!(:email_list_invisible2) { create(:email_list, :invisible, expiration_days:) }
  let!(:mailing1) { create(:mailing, email_list: email_list_visible) }
  let!(:mailing2) { create(:mailing, email_list: email_list_visible, visible: false) }
  let!(:mailing3) { create(:mailing, email_list: email_list_invisible1) }
  let!(:mailing4) do
    create(:mailing, email_list: email_list_invisible2, included_email_lists: [email_list_visible])
  end
  let!(:mailing5) do
    create(:mailing, email_list: email_list_invisible2, included_email_lists: [email_list_invisible1])
  end
  let(:users) { { user: nil, subscriber: } }

  before { Timecop.travel(2.months.from_now) }

  after { Timecop.return }

  context 'without mailing expiration' do
    let(:expiration_days) { nil }

    context 'when guest' do
      let(:subscriber) { nil }

      permissions :show? do
        it { is_expected.to permit(users, mailing1) }
        it { is_expected.not_to permit(users, mailing2) }
        it { is_expected.not_to permit(users, mailing3) }
        it { is_expected.to permit(users, mailing4) }
        it { is_expected.not_to permit(users, mailing5) }
      end
    end

    context 'when subscriber' do
      let(:subscriber) { create(:subscriber_user_with_email, email_lists: [email_list_invisible1]) }

      permissions :show? do
        it { is_expected.to permit(users, mailing1) }
        it { is_expected.not_to permit(users, mailing2) }
        it { is_expected.to permit(users, mailing3) }
        it { is_expected.to permit(users, mailing4) }
        it { is_expected.to permit(users, mailing5) }
      end
    end
  end

  context 'with mailing expiration' do
    context 'when before expiration' do
      let(:expiration_days) { 90 }

      context 'when guest' do
        let(:subscriber) { create(:user) }

        permissions :show? do
          it { is_expected.to permit(users, mailing1) }
          it { is_expected.not_to permit(users, mailing2) }
          it { is_expected.not_to permit(users, mailing3) }
          it { is_expected.to permit(users, mailing4) }
          it { is_expected.not_to permit(users, mailing5) }
        end
      end

      context 'when subscriber' do
        let(:subscriber) { create(:subscriber_user_with_email, email_lists: [email_list_invisible1]) }

        permissions :show? do
          it { is_expected.to permit(users, mailing1) }
          it { is_expected.not_to permit(users, mailing2) }
          it { is_expected.to permit(users, mailing3) }
          it { is_expected.to permit(users, mailing4) }
          it { is_expected.to permit(users, mailing5) }
        end
      end
    end

    context 'when after expiration' do
      let(:expiration_days) { 30 }

      context 'when guest' do
        let(:subscriber) { nil }

        permissions :show? do
          it { is_expected.not_to permit(users, mailing1) }
          it { is_expected.not_to permit(users, mailing2) }
          it { is_expected.not_to permit(users, mailing3) }
          it { is_expected.not_to permit(users, mailing4) }
          it { is_expected.not_to permit(users, mailing5) }
        end
      end

      context 'when subscriber' do
        let(:subscriber) { create(:subscriber_user_with_email, email_lists: [email_list_invisible1]) }

        permissions :show? do
          it { is_expected.not_to permit(users, mailing1) }
          it { is_expected.not_to permit(users, mailing2) }
          it { is_expected.not_to permit(users, mailing3) }
          it { is_expected.not_to permit(users, mailing4) }
          it { is_expected.not_to permit(users, mailing5) }
        end
      end
    end
  end
end
