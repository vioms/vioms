# frozen_string_literal: true

require 'rails_helper'

RSpec.describe '/admin/subscribers/:subscriber_id/subscriber_statistics' do
  let(:subscriber) { create(:subscriber_with_email) }

  before do
    DatabaseCleaner.clean
    create_list(:mailing_view, 2, subscriber:)
  end

  login_admin_user

  describe 'GET /' do
    it 'renders index template' do
      get admin_subscriber_subscriber_statistics_url(subscriber)
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:index)
    end
  end
end
