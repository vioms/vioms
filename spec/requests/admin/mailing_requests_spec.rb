# frozen_string_literal: true

require 'rails_helper'

RSpec.describe '/admin/mailing_requests' do
  before { DatabaseCleaner.clean }

  login_admin_user

  let(:valid_attributes) { attributes_for(:mailing_request) }
  let(:invalid_attributes) { { subject: '' } }
  let!(:mailing_request) { create(:mailing_request) }

  describe 'GET /index' do
    it 'renders a successful response' do
      get admin_mailing_requests_url
      expect(response).to be_successful
      expect(response).to have_rendered(:index)
    end
  end

  describe 'GET /edit' do
    it 'render a successful response' do
      get edit_admin_mailing_request_url(mailing_request)
      expect(response).to be_successful
      expect(response).to have_rendered(:edit)
    end
  end

  describe 'PATCH /update' do
    context 'with valid parameters' do
      it 'updates the requested mailing_request' do
        patch admin_mailing_request_url(mailing_request), params: { mailing_request: valid_attributes }
        expect(response).to redirect_to(admin_mailing_requests_url)
      end
    end

    context 'with invalid parameters' do
      it 'renders edit template' do
        patch admin_mailing_request_url(mailing_request), params: { mailing_request: invalid_attributes }
        expect(response).to be_successful
        expect(response).to have_rendered(:edit)
      end
    end
  end

  describe 'DELETE /destroy' do
    it 'destroys the requested mailing_request' do
      expect do
        delete admin_mailing_request_url(mailing_request)
      end.to change(MailingRequest, :count).by(-1)
    end

    it 'redirects to the mailing_requests list' do
      delete admin_mailing_request_url(mailing_request)
      expect(response).to redirect_to(admin_mailing_requests_url)
    end
  end
end
