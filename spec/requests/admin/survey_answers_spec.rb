# frozen_string_literal: true

require 'rails_helper'

RSpec.describe '/admin/surveys/:survey_id/survey_questions/:survey_question_id/survey_answers' do
  before { DatabaseCleaner.clean }

  login_admin_user

  let(:valid_attributes) { attributes_for(:survey_answer) }
  let!(:survey) { create(:survey) }
  let!(:survey_question) { create(:survey_question, survey:) }
  let!(:survey_answer) { create(:survey_answer, survey_question:) }

  describe 'GET /index' do
    it 'renders a successful response' do
      get admin_survey_survey_question_survey_answers_url(survey, survey_question)
      expect(response).to be_successful
    end
  end

  describe 'GET /new' do
    it 'renders a successful response' do
      get new_admin_survey_survey_question_survey_answer_url(survey, survey_question)
      expect(response).to be_successful
    end
  end

  describe 'GET /edit' do
    it 'render a successful response' do
      get edit_admin_survey_survey_question_survey_answer_url(survey, survey_question, survey_answer)
      expect(response).to be_successful
    end
  end

  describe 'POST /create' do
    context 'with valid parameters' do
      it 'creates a new SurveyAnswer' do
        expect do
          post admin_survey_survey_question_survey_answers_url(survey, survey_question),
               params: { survey_answer: valid_attributes }
        end.to change(SurveyAnswer, :count).by(1)
      end

      it 'render a successful response' do
        post admin_survey_survey_question_survey_answers_url(survey, survey_question),
             params: { survey_answer: valid_attributes }
        expect(response).to redirect_to(admin_survey_survey_question_survey_answers_url(survey, survey_question))
      end
    end
  end

  describe 'PATCH /update' do
    context 'with valid parameters' do
      it 'updates the requested survey' do
        patch admin_survey_survey_question_survey_answer_url(survey, survey_question, survey_answer),
              params: { survey_answer: valid_attributes }
        expect(response).to redirect_to(admin_survey_survey_question_survey_answers_url(survey, survey_question))
      end
    end
  end

  describe 'DELETE /destroy' do
    it 'destroys the requested survey' do
      expect do
        delete admin_survey_survey_question_survey_answer_url(survey, survey_question, survey_answer)
      end.to change(SurveyAnswer, :count).by(-1)
    end

    it 'redirects to the surveys list' do
      delete admin_survey_survey_question_survey_answer_url(survey, survey_question, survey_answer)
      expect(response).to redirect_to(admin_survey_survey_question_survey_answers_url(survey, survey_question))
    end
  end
end
