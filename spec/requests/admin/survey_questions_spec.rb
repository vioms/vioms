# frozen_string_literal: true

require 'rails_helper'

RSpec.describe '/admin/surveys/:survey_id/survey_questions' do
  before { DatabaseCleaner.clean }

  login_admin_user

  let(:valid_attributes) { attributes_for(:survey_question) }
  let!(:survey) { create(:survey) }
  let!(:survey_question) { create(:survey_question, survey:) }

  describe 'GET /index' do
    it 'renders a successful response' do
      get admin_survey_survey_questions_url(survey)
      expect(response).to be_successful
    end
  end

  describe 'GET /new' do
    it 'renders a successful response' do
      get new_admin_survey_survey_question_url(survey)
      expect(response).to be_successful
    end
  end

  describe 'GET /edit' do
    it 'render a successful response' do
      get edit_admin_survey_survey_question_url(survey, survey_question)
      expect(response).to be_successful
    end
  end

  describe 'POST /create' do
    context 'with valid parameters' do
      it 'creates a new SurveyQuestion' do
        expect do
          post admin_survey_survey_questions_url(survey), params: { survey_question: valid_attributes }
        end.to change(SurveyQuestion, :count).by(1)
      end

      it 'render a successful response' do
        post admin_survey_survey_questions_url(survey), params: { survey_question: valid_attributes }
        expect(response).to redirect_to(admin_survey_survey_questions_url(survey))
      end
    end
  end

  describe 'PATCH /update' do
    context 'with valid parameters' do
      it 'updates the requested survey' do
        patch admin_survey_survey_question_url(survey, survey_question), params: { survey_question: valid_attributes }
        expect(response).to redirect_to(admin_survey_survey_questions_url(survey))
      end
    end
  end

  describe 'DELETE /destroy' do
    it 'destroys the requested survey' do
      expect do
        delete admin_survey_survey_question_url(survey, survey_question)
      end.to change(SurveyQuestion, :count).by(-1)
    end

    it 'redirects to the surveys list' do
      delete admin_survey_survey_question_url(survey, survey_question)
      expect(response).to redirect_to(admin_survey_survey_questions_url(survey))
    end
  end
end
