# frozen_string_literal: true

require 'rails_helper'

RSpec.describe '/admin/api_keys' do
  before do
    DatabaseCleaner.clean
  end

  login_admin_user

  let(:valid_attributes) { attributes_for(:api_key) }
  let!(:api_key) { create(:api_key) }

  describe 'GET /index' do
    it 'renders a successful response' do
      get admin_api_keys_url
      expect(response).to be_successful
    end
  end

  describe 'GET /new' do
    it 'renders a successful response' do
      get new_admin_api_key_url
      expect(response).to be_successful
    end
  end

  describe 'GET /edit' do
    it 'render a successful response' do
      get edit_admin_api_key_url(api_key)
      expect(response).to be_successful
    end
  end

  describe 'POST /create' do
    context 'with valid parameters' do
      it 'creates a new APIKey' do
        expect do
          post admin_api_keys_url, params: { api_key: valid_attributes }
        end.to change(APIKey, :count).by(1)
      end

      it 'render a successful response' do
        post admin_api_keys_url, params: { api_key: valid_attributes }
        expect(response).to be_successful
      end
    end
  end

  describe 'PATCH /update' do
    context 'with valid parameters' do
      it 'updates the requested api_key' do
        patch admin_api_key_url(api_key), params: { api_key: valid_attributes }
        expect(response).to redirect_to(admin_api_keys_url)
      end
    end
  end

  describe 'DELETE /destroy' do
    it 'destroys the requested api_key' do
      expect do
        delete admin_api_key_url(api_key)
      end.to change(APIKey, :count).by(-1)
    end

    it 'redirects to the api_keys list' do
      delete admin_api_key_url(api_key)
      expect(response).to redirect_to(admin_api_keys_url)
    end
  end
end
