# frozen_string_literal: true

require 'rails_helper'

RSpec.describe '/admin/surveys/:survey_id/survey_questions/:survey_question_id/survey_subscriber_answers' do
  before { DatabaseCleaner.clean }

  login_admin_user

  let(:valid_attributes) { attributes_for(:survey_subscriber_answer) }
  let(:survey) { create(:survey) }
  let(:survey_question) { create(:survey_question, survey:) }

  before { create(:survey_subscriber_answer, survey_question:) }

  describe 'GET /index' do
    it 'renders a successful response' do
      get admin_survey_survey_question_survey_subscriber_answers_url(survey, survey_question)
      expect(response).to be_successful
    end
  end
end
