# frozen_string_literal: true

require 'rails_helper'

RSpec.describe '/admin/mailings/:mailing_id/mailing_statistics' do
  let(:mailing) { create(:mailing, email_list: create(:email_list, :visible)) }

  before do
    DatabaseCleaner.clean
    create_list(:mailing_view, 2, mailing:)
  end

  login_admin_user

  describe 'GET /' do
    it 'renders index template' do
      get admin_mailing_mailing_statistics_url(mailing)
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:index)
    end
  end
end
