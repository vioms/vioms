# frozen_string_literal: true

require 'rails_helper'

RSpec.describe '/admin/surveys' do
  before { DatabaseCleaner.clean }

  login_admin_user

  let(:valid_attributes) { attributes_for(:survey) }
  let!(:survey) { create(:survey) }

  describe 'GET /index' do
    it 'renders a successful response' do
      get admin_surveys_url
      expect(response).to be_successful
    end
  end

  describe 'GET /new' do
    it 'renders a successful response' do
      get new_admin_survey_url
      expect(response).to be_successful
    end
  end

  describe 'GET /edit' do
    it 'render a successful response' do
      get edit_admin_survey_url(survey)
      expect(response).to be_successful
    end
  end

  describe 'POST /create' do
    context 'with valid parameters' do
      it 'creates a new Survey' do
        expect do
          post admin_surveys_url, params: { survey: valid_attributes }
        end.to change(Survey, :count).by(1)
      end

      it 'render a successful response' do
        post admin_surveys_url, params: { survey: valid_attributes }
        expect(response).to redirect_to(admin_surveys_url)
      end
    end
  end

  describe 'PATCH /update' do
    context 'with valid parameters' do
      it 'updates the requested survey' do
        patch admin_survey_url(survey), params: { survey: valid_attributes }
        expect(response).to redirect_to(admin_surveys_url)
      end
    end
  end

  describe 'DELETE /destroy' do
    it 'destroys the requested survey' do
      expect do
        delete admin_survey_url(survey)
      end.to change(Survey, :count).by(-1)
    end

    it 'redirects to the surveys list' do
      delete admin_survey_url(survey)
      expect(response).to redirect_to(admin_surveys_url)
    end
  end
end
