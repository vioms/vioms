# frozen_string_literal: true

require 'rails_helper'

RSpec.describe '/admin/mailing_requests' do
  before { DatabaseCleaner.clean }

  login_admin_user

  let(:valid_attributes) { attributes_for(:mailing_request_message) }
  let(:mailing_request) { create(:mailing_request) }

  before { create(:mailing_request_message, mailing_request:) }

  describe 'GET /index' do
    it 'renders a successful response' do
      get admin_mailing_request_mailing_request_messages_url(mailing_request)
      expect(response).to be_successful
      expect(response).to have_rendered(:index)
    end
  end

  describe 'POST /create' do
    context 'with valid parameters' do
      it 'creates a new MailingRequestMessage' do
        expect do
          post admin_mailing_request_mailing_request_messages_url(mailing_request),
               params: { mailing_request_message: valid_attributes }
        end.to change(MailingRequestMessage, :count).by(1)
      end

      it 'redirects to the created mailing_request' do
        post admin_mailing_request_mailing_request_messages_url(mailing_request),
             params: { mailing_request_message: valid_attributes }
        expect(response).to redirect_to(admin_mailing_requests_url)
      end
    end
  end
end
