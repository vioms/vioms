# frozen_string_literal: true

require 'rails_helper'

RSpec.describe '/admin/mailing_destinations' do
  before { DatabaseCleaner.clean }

  login_admin_user

  let(:valid_attributes) { attributes_for(:mailing_destination) }
  let!(:mailing_destination) { create(:mailing_destination) }

  describe 'GET /index' do
    it 'renders a successful response' do
      get admin_mailing_destinations_url
      expect(response).to be_successful
    end
  end

  describe 'GET /new' do
    it 'renders a successful response' do
      get new_admin_mailing_destination_url
      expect(response).to be_successful
    end
  end

  describe 'GET /edit' do
    it 'render a successful response' do
      get edit_admin_mailing_destination_url(mailing_destination)
      expect(response).to be_successful
    end
  end

  describe 'POST /create' do
    context 'with valid parameters' do
      it 'creates a new Survey' do
        expect do
          post admin_mailing_destinations_url, params: { mailing_destination: valid_attributes }
        end.to change(MailingDestination, :count).by(1)
      end

      it 'render a successful response' do
        post admin_mailing_destinations_url, params: { mailing_destination: valid_attributes }
        expect(response).to redirect_to(admin_mailing_destinations_url)
      end
    end
  end

  describe 'PATCH /update' do
    context 'with valid parameters' do
      it 'updates the requested mailing_destination' do
        patch admin_mailing_destination_url(mailing_destination), params: { mailing_destination: valid_attributes }
        expect(response).to redirect_to(admin_mailing_destinations_url)
      end
    end
  end

  describe 'DELETE /destroy' do
    it 'destroys the requested mailing_destination' do
      expect do
        delete admin_mailing_destination_url(mailing_destination)
      end.to change(MailingDestination, :count).by(-1)
    end

    it 'redirects to the mailing_destinations list' do
      delete admin_mailing_destination_url(mailing_destination)
      expect(response).to redirect_to(admin_mailing_destinations_url)
    end
  end
end
