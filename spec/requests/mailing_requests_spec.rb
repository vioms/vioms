require 'rails_helper'

RSpec.describe '/mailing_requests' do
  let(:valid_attributes) { attributes_for(:mailing_request) }
  let(:invalid_attributes) { { subject: '' } }
  let(:subscriber_user) { create(:subscriber_user_with_email) }
  let!(:mailing_request) { create(:mailing_request, subscriber_id: subscriber_user.id) }

  before { sign_in_subscriber_user(subscriber_user, nil) }

  describe 'GET /index' do
    it 'renders a successful response' do
      get mailing_requests_url
      expect(response).to be_successful
      expect(response).to have_rendered(:index)
    end
  end

  describe 'GET /new' do
    it 'renders a successful response' do
      get new_mailing_request_url
      expect(response).to be_successful
      expect(response).to have_rendered(:new)
    end
  end

  describe 'GET /edit' do
    it 'renders a successful response' do
      get edit_mailing_request_url(mailing_request)
      expect(response).to be_successful
      expect(response).to have_rendered(:edit)
    end
  end

  describe 'POST /create' do
    context 'with valid parameters' do
      it 'creates a new MailingRequest' do
        expect do
          post mailing_requests_url, params: { mailing_request: valid_attributes }
        end.to change(MailingRequest, :count).by(1)
      end

      it 'redirects to the created mailing_request' do
        post mailing_requests_url, params: { mailing_request: valid_attributes }
        expect(response).to redirect_to(mailing_requests_url)
      end
    end

    context 'with invalid parameters' do
      it 'does not create a new MailingRequest' do
        expect do
          post mailing_requests_url, params: { mailing_request: invalid_attributes }
        end.not_to change(MailingRequest, :count)
      end

      it "renders the 'new' template" do
        post mailing_requests_url, params: { mailing_request: invalid_attributes }
        expect(response).to be_successful
        expect(response).to have_rendered(:new)
      end
    end
  end

  describe 'PATCH /update' do
    context 'with valid parameters' do
      it 'updates the requested mailing_request' do
        patch mailing_request_url(mailing_request), params: { mailing_request: valid_attributes }
        mailing_request.reload
      end

      it 'redirects to the mailing_request' do
        patch mailing_request_url(mailing_request), params: { mailing_request: valid_attributes }
        mailing_request.reload
        expect(response).to redirect_to(mailing_requests_url)
      end
    end

    context 'with invalid parameters' do
      it "renders the 'edit'" do
        patch mailing_request_url(mailing_request), params: { mailing_request: invalid_attributes }
        expect(response).to be_successful
        expect(response).to have_rendered(:edit)
      end
    end
  end

  describe 'DELETE /destroy' do
    it 'destroys the requested mailing_request' do
      expect do
        delete mailing_request_url(mailing_request)
      end.to change(MailingRequest, :count).by(-1)
    end

    it 'redirects to the mailing_requests list' do
      delete mailing_request_url(mailing_request)
      expect(response).to redirect_to(mailing_requests_url)
    end
  end
end
