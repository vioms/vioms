# frozen_string_literal: true

require 'rails_helper'

RSpec.describe '/admin/mailing_requests' do
  before { DatabaseCleaner.clean }

  let(:subscriber_user) { create(:subscriber_user_with_email) }
  let!(:mailing_request) { create(:mailing_request, subscriber_id: subscriber_user.id) }
  let(:valid_attributes) { attributes_for(:mailing_request_message, mailing_request:) }
  let(:invalid_attributes) { { message: '', new_status: :awaiting_subscriber_response } }

  before { sign_in_subscriber_user(subscriber_user, nil) }

  before { create(:mailing_request_message, mailing_request:) }

  describe 'GET /index' do
    it 'renders a successful response' do
      get mailing_request_mailing_request_messages_url(mailing_request)
      expect(response).to be_successful
      expect(response).to have_rendered(:index)
    end
  end

  describe 'POST /create' do
    context 'with valid parameters' do
      it 'creates a new MailingRequestMessage' do
        expect do
          post mailing_request_mailing_request_messages_url(mailing_request),
               params: { mailing_request_message: valid_attributes }
        end.to change(MailingRequestMessage, :count).by(1)
      end

      it 'redirects to the created mailing_request' do
        post mailing_request_mailing_request_messages_url(mailing_request),
             params: { mailing_request_message: valid_attributes }
        expect(response).to redirect_to(mailing_request_mailing_request_messages_url(mailing_request))
      end
    end

    context 'with invalid parameters' do
      it 'does not create a new MailingRequestMessage' do
        expect do
          post mailing_request_mailing_request_messages_url(mailing_request),
               params: { mailing_request_message: invalid_attributes }
        end.not_to change(MailingRequestMessage, :count)
      end

      it "renders the 'new' template" do
        post mailing_request_mailing_request_messages_url(mailing_request),
             params: { mailing_request_message: invalid_attributes }
        expect(response).to be_successful
        expect(response).to have_rendered(:new)
      end
    end
  end
end
