# frozen_string_literal: true

require 'rails_helper'

RSpec.describe '/survey_subscriber_answers/:id/edit' do
  before { DatabaseCleaner.clean }

  login_subscriber_user(:subscriber_user_with_email)

  let(:survey) { create(:survey, visible: true) }
  let(:question1) { create(:survey_question, survey:) }
  let(:question2) { create(:survey_question, :text, survey:) }
  let(:question3) { create(:survey_question, :options, survey:) }
  let(:question4) { create(:survey_question, :vote, survey:) }
  let(:valid_attributes) do
    {
      question1.id => 'Answer 1',
      question2.id => 'Answer 2',
      question3.id => 'Answer 3',
      question4.id => 'Answer 4'
    }
  end

  before do
    create(:survey_answer, survey_question: question3)
    create(:survey_answer, survey_question: question3)
  end

  describe 'GET /edit' do
    it 'render a successful response' do
      get edit_survey_subscriber_answer_url(survey)
      expect(response).to be_successful
    end
  end

  describe 'PATCH /update' do
    it 'updates the requested survey' do
      expect do
        patch survey_subscriber_answer_url(survey), params: { form: valid_attributes }
      end.to change(SurveySubscriberAnswer, :count).by(4)
      expect(response).to redirect_to(root_url)
    end
  end
end
