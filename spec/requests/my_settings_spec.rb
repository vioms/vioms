# frozen_string_literal: true

require 'rails_helper'

describe '/my_settings' do
  let(:subscribed_email_lists) { [] }
  let(:subscriber_user) { create(:subscriber_user_with_email, email_lists: subscribed_email_lists) }

  before do
    sign_in_subscriber_user(subscriber_user, nil)
    ActionMailer::Base.deliveries = []
  end

  it 'edit action should render edit template' do
    get edit_my_settings_url
    expect(response).to have_rendered(:edit)
    expect(ActionMailer::Base.deliveries.size).to eq(0)
  end

  it 'update action should render edit template when model is invalid' do
    expect_any_instance_of(Subscriber).to receive(:valid?).and_return(false)
    put my_settings_url, params: { subscriber: params_for(:subscriber) }
    expect(response).to have_rendered(:edit)
    expect(ActionMailer::Base.deliveries.size).to eq(0)
  end

  it 'update action should redirect when model is valid' do
    expect_any_instance_of(Subscriber).to receive(:valid?).and_return(true)
    put my_settings_url, params: { subscriber: params_for(:subscriber) }
    expect(response).to redirect_to(edit_my_settings_url)
    expect(ActionMailer::Base.deliveries.size).to eq(0)
  end

  it 'sends notification on subscription' do
    expect_any_instance_of(Subscriber).to receive(:valid?).and_return(true)
    put my_settings_url, params: {
      subscriber: { email_list_ids: [
        create(:email_list, visible: true, notify_subscribed: true, notify_email: generate(:email)).id,
        create(:email_list, visible: true, notify_subscribed: true).id,
        create(:email_list, visible: true, notify_subscribed: false, notify_email: generate(:email)).id
      ] }
    }
    expect(ActionMailer::Base.deliveries.size).to eq(1)
  end

  describe 'notifications on unsubscriptions' do
    let(:subscribed_email_lists) do
      [
        create(:email_list, notify_unsubscribed: true, notify_email: generate(:email)),
        create(:email_list, notify_unsubscribed: true),
        create(:email_list, notify_unsubscribed: false, notify_email: generate(:email))
      ]
    end

    it 'sends notification' do
      expect_any_instance_of(Subscriber).to receive(:valid?).and_return(true)
      put my_settings_url, params: { subscriber: { email_list_ids: [''] } }
      expect(ActionMailer::Base.deliveries.size).to eq(1)
    end
  end
end
