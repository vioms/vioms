# frozen_string_literal: true

require 'rails_helper'

RSpec.describe '/api/mobile/token' do
  let(:subscriber) { create(:subscriber_user_with_confirmed_email, password: 'password') }

  describe 'POST /create' do
    before { post '/api/mobile/token', params: { email: subscriber.email, password: } }

    context 'when password is correct' do
      let(:password) { 'password' }

      it { expect(response).to have_http_status(:ok) }
    end

    context 'when password is incorrect' do
      let(:password) { 'bad_password' }

      it { expect(response).to have_http_status(:unauthorized) }
    end
  end

  describe 'GET /validate' do
    let(:subscriber) { create(:subscriber_user_with_confirmed_email) }

    before { get '/api/mobile/token/1/validate', headers: }

    context 'when token is correct' do
      let(:headers) { auth_header(subscriber) }

      it { expect(response).to have_http_status(:ok) }
    end

    context 'when token is incorrect' do
      let(:headers) { { Authorization: 'Bearer bad_token' } }

      it { expect(response).to have_http_status(:unauthorized) }
    end

    context 'without token' do
      let(:headers) { {} }

      it { expect(response).to have_http_status(:unauthorized) }
    end
  end

  describe 'POST /destroy' do
    let(:subscriber) { create(:subscriber_user_with_confirmed_email) }

    before { delete '/api/mobile/token/1', headers: auth_header(subscriber) }

    context 'when password is correct' do
      it { expect(response).to have_http_status(:ok) }
    end
  end
end
