# frozen_string_literal: true

require 'rails_helper'

RSpec.describe '/api/mobile/email_lists/email_list_id/mailings' do
  before { DatabaseCleaner.clean }

  describe 'GET /' do
    let!(:email_list) { create(:email_list, :visible) }
    let!(:mailing) { create(:mailing, :published, email_list:) }

    before { get "/api/mobile/email_lists/#{email_list.id}/mailings", headers: }

    context 'without token' do
      let(:headers) { {} }

      it { expect(response).to have_http_status(:ok) }
      it { expect(json_body).to eq([{ id: mailing.id, subject: mailing.subject }]) }
    end

    context 'with token' do
      let(:subscriber) { create(:subscriber_user_with_confirmed_email) }
      let(:headers) { auth_header(subscriber) }

      it { expect(response).to have_http_status(:ok) }
    end
  end

  describe 'GET /show' do
    let!(:email_list) { create(:email_list, :visible) }
    let!(:mailing) { create(:mailing, :published, email_list:) }

    before { get "/api/mobile/email_lists/#{email_list.id}/mailings/#{mailing.id}.json", headers: }

    context 'without token' do
      let(:headers) { {} }

      it { expect(response).to have_http_status(:ok) }

      it do
        expect(json_body).to eq({ id: mailing.id, subject: mailing.subject, rendered_html: mailing.rendered_html })
      end
    end

    context 'with token' do
      let(:subscriber) { create(:subscriber_user_with_confirmed_email) }
      let(:headers) { auth_header(subscriber) }

      it { expect(response).to have_http_status(:ok) }
    end
  end
end
