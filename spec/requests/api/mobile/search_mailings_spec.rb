# frozen_string_literal: true

require 'rails_helper'

RSpec.describe '/api/mobile/search_mailings' do
  it 'renders index action' do
    get api_mobile_search_mailings_path
    expect(json_body).to eq({ mailings: [], last_page: true })
  end
end
