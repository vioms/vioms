# frozen_string_literal: true

require 'rails_helper'

describe '/api/mobile/registrations' do
  before do
    DatabaseCleaner.clean
    FactoryBot.rewind_sequences
    create_sms_send_history_types
    create_sms_messages
  end

  describe 'create' do
    context 'without password' do
      specify 'unsuccessful create' do
        post api_mobile_registration_url, params: { subscriber_user: { email: '', phone: '' } }
        expect(response).to have_http_status(:unprocessable_entity)
      end

      specify 'successful create' do
        post api_mobile_registration_url,
             params: { subscriber_user: { email: generate(:email), phone: generate(:phone),
                                          time_zone_id: create(:time_zone).id } }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'with password' do
      specify 'unsuccessful create' do
        post api_mobile_registration_url, params: { subscriber_user:
                                           { email: generate(:email), phone: generate(:phone),
                                             set_password: '1', password: 'password', password_confirmation: '' } }
        expect(response).to have_http_status(:unprocessable_entity)
      end

      specify 'successful create' do
        post api_mobile_registration_url, params: { subscriber_user:
                                           { email: generate(:email), phone: generate(:phone),
                                             time_zone_id: create(:time_zone).id, set_password: '1',
                                             password: 'password', password_confirmation: 'password' } }
        expect(response).to have_http_status(:ok)
      end
    end
  end

  describe 'update' do
    context 'when not authenticated' do
      it 'returns unauthorized' do
        put api_mobile_registration_url,
            params: { subscriber_user: { email: '', phone: '' } }
        expect(response).to have_http_status(:unauthorized)
      end
    end

    context 'when authenticated' do
      let(:subscriber_user) { create(:subscriber_user_with_email, password: 'old_password') }

      specify 'unsuccessful update' do
        put api_mobile_registration_url,
            headers: auth_header(subscriber_user),
            params: { subscriber_user: { email: '', phone: '' } }
        expect(response).to have_http_status(:unprocessable_entity)
      end

      specify 'successful update' do
        put api_mobile_registration_url,
            headers: auth_header(subscriber_user),
            params: { subscriber_user: params_for(:subscriber_user_with_email) }
        expect(response).to have_http_status(:ok)
      end
    end
  end

  describe 'destroy' do
    context 'when not authenticated' do
      it 'returns unauthorized' do
        delete api_mobile_registration_url
        expect(response).to have_http_status(:unauthorized)
      end
    end

    context 'when authenticated' do
      let(:subscriber_user) { create(:subscriber_user_with_email) }

      specify 'successful destruction' do
        delete api_mobile_registration_url,
               headers: auth_header(subscriber_user)
        expect(response).to have_http_status(:ok)
        expect(SubscriberUser.exists?(subscriber_user.id)).to be false
      end
    end
  end
end
