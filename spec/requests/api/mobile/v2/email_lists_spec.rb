# frozen_string_literal: true

require 'rails_helper'

RSpec.describe '/api/mobile/v2/email_lists' do
  before { DatabaseCleaner.clean }

  describe 'GET /' do
    let!(:email_list_visible) { create(:email_list, :visible) }
    let!(:email_list_invisible) { create(:email_list, :invisible) }

    before { get '/api/mobile/v2/email_lists', headers: }

    context 'without token' do
      let(:headers) { {} }

      it { expect(response).to have_http_status(:ok) }

      it do
        res = {
          subscribed: [],
          unsubscribed: [
            { id: email_list_visible.id, name: email_list_visible.name }
          ]
        }
        expect(json_body).to eq(res)
      end
    end

    context 'with token' do
      let(:subscriber) { create(:subscriber_user_with_confirmed_email, email_lists: [email_list_invisible]) }
      let(:headers) { auth_header(subscriber) }

      it { expect(response).to have_http_status(:ok) }

      it do
        res = {
          subscribed: [
            { id: email_list_invisible.id, name: email_list_invisible.name }
          ],
          unsubscribed: [
            { id: email_list_visible.id, name: email_list_visible.name }
          ]
        }
        expect(json_body).to eq(res)
      end
    end
  end
end
