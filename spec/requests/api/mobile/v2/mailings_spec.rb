# frozen_string_literal: true

require 'rails_helper'

RSpec.describe '/api/mobile/v2/email_lists/email_list_id/mailings' do
  before { DatabaseCleaner.clean }

  describe 'GET /' do
    let!(:email_list) { create(:email_list, :visible) }
    let!(:mailing) { create(:mailing, :published, email_list:, deliver_at: '2024-07-08T18:19:37.664Z') }

    before { get "/api/mobile/v2/email_lists/#{email_list.id}/mailings", headers: }

    context 'without token' do
      let(:headers) { {} }
      let(:expected_response) do
        {
          mailings: [
            { id: mailing.id, subject: mailing.subject, published_at: '2024-07-08T18:19:37.664Z' }
          ],
          last_page: true
        }
      end

      it { expect(response).to have_http_status(:ok) }
      it { expect(json_body).to eq(expected_response) }
    end

    context 'with token' do
      let(:subscriber) { create(:subscriber_user_with_confirmed_email) }
      let(:headers) { auth_header(subscriber) }

      it { expect(response).to have_http_status(:ok) }
    end
  end
end
