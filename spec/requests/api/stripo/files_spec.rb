# frozen_string_literal: true

require 'rails_helper'

RSpec.describe '/api/stripo/files' do
  before { DatabaseCleaner.clean }

  let(:username) { 'test_stripo_username' }
  let(:password) { 'test_stripo_password' }
  let(:headers) do
    allow(Setting).to receive('get_setting').with('stripo_username').and_return(username)
    allow(Setting).to receive('get_setting').with('stripo_password').and_return(password)
    { HTTP_AUTHORIZATION: ActionController::HttpAuthentication::Basic.encode_credentials(username, password) }
  end

  describe 'GET /' do
    before { get '/api/stripo/files', headers: }

    it { expect(response).to have_http_status(:ok) }
    it { expect(json_body).to eq([]) }
  end

  describe 'GET /info' do
    before { get '/api/stripo/files/info', headers:, params: { src: stripo_file.cached_url } }

    let(:stripo_file) { create(:stripo_file) }

    it { expect(response).to have_http_status(:ok) }
    it { expect(json_body).to eq({ originalName: 'spacer.gif', size: 45 }) }
  end

  describe 'POST /' do
    let(:key) { 'pluginId_test1_emailId_test2' }
    let(:file) { fixture_file_upload('spacer.gif', 'image/gif') }

    before { post '/api/stripo/files', headers:, params: { key:, file: } }

    it { expect(response).to have_http_status(:ok) }

    it 'responds with JSON body' do
      body = json_body
      expect(body[:originalName]).to eq('spacer.gif')
      expect(body[:width]).to eq(5)
      expect(body[:height]).to eq(5)
      expect(body[:url]).to match('memory://stripo/test1_emailId')
      expect(body[:thumbnailUrl]).to match('memory://stripo/test1_emailId')
    end
  end
end
