# frozen_string_literal: true

require 'rails_helper'

describe '/search_mailings' do
  it 'renders index action' do
    get search_mailings_path
    expect(response).to have_rendered(:index)
  end
end
