# frozen_string_literal: true

require 'rails_helper'

RSpec.describe '/mailings' do
  before { DatabaseCleaner.clean }

  let!(:email_list1) { create(:email_list, visible: true) }
  let!(:email_list2) { create(:email_list, visible: true) }
  let!(:email_list3) { create(:email_list_with_template, visible: true) }
  let!(:email_list_invisible) { create(:email_list, visible: false) }
  let!(:mailing) do
    create(:mailing, html_content: '', editor_type: 'template', email_list: email_list3, sent: true, published: true,
                     deliver_at: 1.day.ago)
  end

  describe 'GET /index' do
    before do
      EmailListsMailingsInclusion.create!(email_list: email_list2,
                                          mailing: create(:mailing, email_list: email_list2))
    end

    context 'without included email lists' do
      it 'renders index template' do
        get email_list_mailings_url(email_list1)
        expect(response).to have_rendered(:index)
      end
    end

    context 'with included email lists' do
      it 'renders index template' do
        get email_list_mailings_url(email_list2)
        expect(response).to have_rendered(:index)
      end
    end

    context 'with email list template' do
      it 'renders index template' do
        get email_list_mailings_url(email_list3)
        expect(response).to have_rendered(:index)
      end
    end

    describe 'invisible lists' do
      let(:subscriber) { create(:subscriber_user_with_email, email_lists: [email_list_invisible]) }

      before { sign_in_subscriber_user(subscriber, nil) }

      it 'renders index template' do
        get email_list_mailings_url(email_list_invisible)
        expect(response).to have_rendered(:index)
      end
    end
  end

  describe 'GET /show' do
    it 'renders show template' do
      get mailing_url(mailing)
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:show)
    end

    context 'with attachment' do
      let(:mailing) do
        create(:mailing, email_list: create(:email_list, :visible), max_attachments_size: 1,
                         email_attachments: [create(:email_attachment)])
      end

      it 'renders show template' do
        get mailing_url(mailing)
        expect(response).to have_http_status(:ok)
        expect(response).to render_template(:show)
      end
    end
  end

  describe 'GET /full' do
    it 'renders a successful response' do
      get full_mailing_url(mailing)
      expect(response).to have_http_status(:ok)
    end
  end

  describe 'GET /track_email' do
    let(:auth_token) { 'mkCtrhNxPAqU8GH4Jpqz' }
    let(:subscriber) { create(:subscriber_with_email, authentication_token: auth_token) }

    it 'renders a successful response' do
      get track_email_mailing_url(mailing, email: subscriber.email, auth_token:, format: :gif)
      expect(response).to have_http_status(:ok)
    end
  end
end
