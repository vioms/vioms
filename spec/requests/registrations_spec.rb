# frozen_string_literal: true

require 'rails_helper'

describe '/registrations' do
  let(:subscriber_user) { create(:subscriber_user_with_email) }

  before do
    ActionMailer::Base.deliveries = []
    EmailSendHistory.delete_all
  end

  specify 'new' do
    get new_registration_url
    expect(response).to have_rendered(:new)
  end

  describe 'create' do
    context 'without password' do
      specify 'unsuccessful create' do
        post registration_url, params: { subscriber_user: { email: '' } }
        expect(response).to have_rendered(:new)
      end

      specify 'successful create' do
        post registration_url, params: { subscriber_user: { email: generate(:email) } }
        expect(response).to redirect_to(root_url)
        expect(ActionMailer::Base.deliveries.size).to eq(1)
        expect(EmailSendHistory.count).to eq(1)
      end
    end

    context 'with password' do
      specify 'unsuccessful create' do
        post registration_url, params: { subscriber_user:
                                           { email: generate(:email), set_password: '1',
                                             password: 'password', password_confirmation: '' } }
        expect(response).to have_rendered(:new)
      end

      specify 'successful create' do
        post registration_url, params: { subscriber_user:
                                           { email: generate(:email), set_password: '1',
                                             password: 'password', password_confirmation: 'password' } }
        expect(response).to redirect_to(root_url)
        expect(ActionMailer::Base.deliveries.size).to eq(1)
        expect(EmailSendHistory.count).to eq(1)
      end
    end
  end

  describe 'edit and update' do
    before { sign_in_subscriber_user(subscriber_user, nil) }

    specify 'edit' do
      get edit_registration_url
      expect(response).to have_rendered(:edit)
    end

    specify 'unsuccessful update' do
      allow(Subscriber::ChangePassword).to receive(:call).and_return(false)
      put registration_url, params: { subscriber_user: { password: '', password_confirmation: '' } }
      expect(response).to have_rendered(:edit)
      expect(Subscriber::ChangePassword).to have_received(:call)
    end

    specify 'successful update' do
      allow(Subscriber::ChangePassword).to receive(:call).and_return(true)
      put registration_url, params: { subscriber_user: { password: '', password_confirmation: '' } }
      expect(response).to redirect_to(root_url)
      expect(Subscriber::ChangePassword).to have_received(:call)
    end
  end

  describe 'destroy' do
    before { sign_in_subscriber_user(subscriber_user, nil) }

    it 'destroys the current subscriber user' do
      expect do
        delete registration_url
      end.to change(SubscriberUser, :count).by(-1)
    end

    it 'redirects to home page' do
      delete registration_url
      expect(response).to redirect_to(root_url)
    end
  end
end
