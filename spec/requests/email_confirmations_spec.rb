# frozen_string_literal: true

require 'rails_helper'

describe '/email_confirmations' do
  before { DatabaseCleaner.clean }

  let(:subscriber) { create(:subscriber_with_email) }

  before do
    create(:email_send_history, subscriber:, email: subscriber.email, last_value: '123456')
    get confirm_email_confirmations_url, params: {
      email: subscriber.email,
      auth_token: subscriber.authentication_token,
      confirmation_token:
    }
  end

  context 'with valid token' do
    let(:confirmation_token) { '123456' }

    it 'confirms email' do
      expect(response).to redirect_to(edit_my_settings_url)
    end
  end

  context 'with invalid token' do
    let(:confirmation_token) { '12' }

    it 'does not confirm email' do
      expect(response).to redirect_to(root_url)
    end
  end
end
