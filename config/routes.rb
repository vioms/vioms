# frozen_string_literal: true

Rails.application.routes.draw do
  mount Ckeditor::Engine => '/ckeditor'
  authenticate :user, ->(u) { u.effective_admin? } do
    mount Sidekiq::Web => '/admin/sidekiq'
  end

  scope path_names: { sign_in: 'login', sign_out: 'logout' } do
    devise_for :user, controllers: { sessions: 'devise/user/sessions' }
    devise_for :subscriber_user, controllers: { passwords: 'devise/subscriber/passwords' }
  end

  resource :admin, only: :show

  namespace :admin do
    get 'welcome', to: 'welcome#index'
    get 'toggle_become_admin', to: 'welcome#toggle_become_admin'
    scope except: :show do
      resources :sms_lists do
        member do
          get :export
        end
      end
      resources :subscribers do
        collection do
          get :sms_operator
        end
        resources :email_problems, except: %i[show destroy]
        resources :phone_problems, except: %i[show destroy]
        resources :subscriber_statistics, only: :index
      end
      resources :import_email_problems, only: %i[new create]
      resources :import_phone_problems, only: %i[new create]
      resources :time_zones
      resources :smtp_setting_groups do
        collection { post :sort }
        resources :smtp_settings
      end
      resources :unsubscription_reasons do
        collection { post :sort }
      end
      resources :email_problem_names do
        collection { post :sort }
      end
      resources :phone_problem_names do
        collection { post :sort }
      end
      resources :digest_categories
      resources :news_items
      resources :settings, only: %i[index show edit update]
      resources :sms_operators do
        collection do
          get :export
        end
      end
      resources :messenger_types
      resources :phone_prefixes
      resources :blacklist_emails
      resources :banner_owners
      resources :external_redirects
      resources :polls do
        resources :answers
        resources :subscriber_answers, only: :index
      end
      resources :pages
      resources :stripo_template_groups do
        resources :stripo_templates do
          member do
            get :edit_stripo
          end
        end
      end
      resources :stripo_templates, only: [] do
        collection do
          get :new_from_mailing, to: 'stripo_templates#new_from_mailing'
          post :new_from_mailing, to: 'stripo_templates#create_from_mailing'
        end
      end
      resources :api_keys
      resources :donation_methods
      resources :mailing_requests do
        resources :mailing_request_messages, only: %i[index create]
      end
      resources :mailing_destinations
    end
    resources :email_lists do
      resources :email_list_custom_vars
      resources :email_list_var_names do
        collection { post :sort }
      end
      member do
        get :export
      end
    end
    resources :email_digests do
      member do
        put :send_digest
        put :cancel_sending
      end
    end
    resources :unsubscriptions, only: %i[index show destroy]
    resources :users
    resources :mailings do
      member do
        get :full
        get :frame
        put :copy
        put :send_mailing
        put :cancel_sending
        get :crop
      end
      resources :mailing_statistics, only: :index
    end
    resources :sms_sendings do
      collection do
        put :multiple
      end
      member do
        put :copy
        put :send_sms
        put :cancel_sending
      end
    end
    resources :telegram_groups
    resources :telegram_messages do
      collection do
        get :recent_mailings
      end
      member do
        put :copy
        put :send_message
        put :cancel_sending
      end
    end
    resources :cities
    resources :sms_messages, only: %i[index edit update]
    resources :sms_send_history_types, only: %i[index edit update]
    resources :email_send_histories, only: %i[index destroy]
    resources :sms_send_histories, only: %i[index destroy]
    resources :sms_accounts do
      resources :sms_account_actions, except: :destroy
    end
    resources :banner_orders, except: :show do
      resources :banners
    end
    resources :subscriber_imports, except: :show do
      resources :subscriber_import_people, only: %i[index edit update]
    end
    resources :text_blocks, only: %i[index edit update]
    resources :surveys do
      resources :survey_questions do
        resources :survey_answers
        resources :survey_subscriber_answers, only: %i[index show]
      end
    end

    wiki_root '/wiki'
  end

  namespace :api do
    namespace :mobile do
      resources :token, only: %i[create destroy] do
        member do
          get :validate
        end
      end
      resources :email_lists, only: :index do
        resources :mailings, only: %i[index show]
      end
      resource :registration, only: %i[show create update destroy]
      namespace :v2 do
        resources :email_lists, only: :index do
          resources :mailings, only: :index
        end
      end
      resources :time_zones, only: :index
      resources :search_mailings, only: :index
    end
    namespace :stripo do
      scope :files do
        get '/', to: 'files#list'
        get 'info', to: 'files#info'
        post '/', to: 'files#upload'
        post 'delete', to: 'files#delete'
      end
      get 'token', to: 'auth#token'
    end
  end

  resource :registration, only: %i[new create edit update destroy]
  resource :email_confirmations, only: %i[show] do
    collection do
      get :confirm
    end
  end
  resources :email_lists, only: %i[index show] do
    resources :mailings, only: :index
  end
  resources :facebook_groups, only: :index
  resources :telegram_groups, only: :index
  resources :viber_groups, only: :index
  resources :vk_groups, only: :index
  resources :whatsapp_groups, only: :index
  resources :mailings, only: :show do
    member do
      get :full
      get :track_email
    end
  end
  resource :my_settings, only: %i[edit update] do
    collection do
      get :time_zone
    end
  end
  resources :pages
  resource :contact, only: %i[new create]
  resources :unsubscriptions, only: %i[new create]
  resources :news_items, only: %i[index show], path: 'news'
  resources :subscriber_answers, only: :new
  resources :search_mailings, only: %i[index]
  resources :survey_subscriber_answers, only: %i[edit update]
  resources :mailing_requests, except: :show do
    resources :mailing_request_messages, only: %i[index create]
  end

  get 'banner_click' => 'banner_clicks#new'
  post 'phones_prolong/create'

  root to: 'pages#index'
end
