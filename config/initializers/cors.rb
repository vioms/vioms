# frozen_string_literal: true

Rails.application.config.middleware.insert_before 0, Rack::Cors do
  allow do
    origins Rails.env.production? ? 'https://app.vioms.ru' : 'http://lvh.me:19006'
    resource '/api/mobile/*', headers: :any, methods: %i[get post patch put]
  end
end
