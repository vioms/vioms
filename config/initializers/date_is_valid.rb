module DateIsValidForNilClass
  def future?
    false
  end

  def past?
    false
  end
end

NilClass.include DateIsValidForNilClass
