# frozen_string_literal: true

# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path.
# Rails.application.config.assets.paths << Emoji.images_path
# Add Yarn node_modules folder to the asset load path.
Rails.application.config.assets.paths << Rails.root.join('node_modules')
Rails.application.config.assets.paths << Rails.root.join('vendor/assets/stylesheets/feedicons')
Rails.application.config.assets.paths << Rails.root.join('vendor/assets/stylesheets/custom')

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in the app/assets
# folder are already added.
Rails.application.config.assets.precompile +=
  %w[*.png *.gif admin.js jquery3.js jquery.ba-url.min.js admin/mailing.js pagination.js sms_sending.js subscriber.js
     time_zone.js admin.css ckeditor/config.js admin/popup.js smtp_setting_groups.js wiki.css sms_sendings.js
     unsubscription_reasons.js subscriber_admin.js email_problem_names.js phone_problem_names.js jquery.Jcrop.js
     jquery.Jcrop.css digest_image_crop.js email_digest.js email_list_var_names.js subscriber_import.js
     telegram_message.js admin/stripo_mailing.js admin/email_list.js admin/stripo_template.js
     admin/stripo_template_group.js admin/init_stripo.js subscriber_registration.js application.js]
