# frozen_string_literal: true

if Rails.env.production?
  $email_settings = YAML.load(File.open("#{Rails.root}/config/email.yml"))[:settings]

  ActionMailer::Base.smtp_settings = $email_settings
end
