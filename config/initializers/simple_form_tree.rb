Rails.application.config.to_prepare do
  ActionView::Helpers::FormBuilder.include SimpleFormTree::BuilderMethods if Object.const_defined?('SimpleForm')
end
