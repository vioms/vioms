module ActionView
  module Helpers
    module JavaScriptHelper
      alias parent_escape_javascript escape_javascript

      def escape_javascript(javascript)
        result = parent_escape_javascript(javascript).gsub(/\342\200\251/u, '&#x2029;')
        javascript.html_safe? ? result.html_safe : result
      end

      alias j escape_javascript
    end
  end
end
