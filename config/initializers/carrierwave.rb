CarrierWave.configure do |config|
  config.asset_host = if Rails.env.production?
                        'https://www.vioms.ru'
                      else
                        'http://lvh.me:3000'
                      end
end
