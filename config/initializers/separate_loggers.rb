# frozen_string_literal: true

module Rails
  def self.telegram_logger
    @problems_logger ||= ActiveSupport::TaggedLogging.new(
      Logger.new(Rails.root.join('log', "#{Rails.env}_telegram.log"), formatter: ::Logger::Formatter.new)
    )
  end
end

Rails.application.config.to_prepare do
  ViomsMailer.logger = ActiveSupport::TaggedLogging.new(
    Logger.new(
      Rails.root.join('log', "#{Rails.env}_vioms_mailer.log"),
      formatter: Logger::Formatter.new
    )
  )
end
